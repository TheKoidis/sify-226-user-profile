package org.simplifyworks.template.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * Created by Svanda on 12.8.2015.
 */
public class CoreTemplateDto extends AbstractDto {

	private String code;
	private String subject;
	private String description;
	private String body;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
