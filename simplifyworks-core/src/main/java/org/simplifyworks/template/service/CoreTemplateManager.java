package org.simplifyworks.template.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.template.domain.ProcessedTemplate;
import org.simplifyworks.template.domain.TemplateVariablesWrapper;
import org.simplifyworks.template.model.dto.CoreTemplateDto;
import org.simplifyworks.template.model.entity.CoreTemplate;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
public interface CoreTemplateManager extends ReadWriteManager<CoreTemplateDto, CoreTemplate> {

    TemplateVariablesWrapper createVariables();

	ProcessedTemplate getHtmlByTemplate(String templateCode, TemplateVariablesWrapper variables);
}