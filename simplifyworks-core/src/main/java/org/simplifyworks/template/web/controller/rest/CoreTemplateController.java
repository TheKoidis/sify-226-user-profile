package org.simplifyworks.template.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.template.model.dto.CoreTemplateDto;
import org.simplifyworks.template.model.entity.CoreTemplate;
import org.simplifyworks.template.service.CoreTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
@RestController
@RequestMapping(value = "/api/core/templates")
public class CoreTemplateController extends BasicTableController<CoreTemplateDto, CoreTemplate> {

	@Autowired
	private CoreTemplateManager coreTemplateManager;

	@Override
	protected ReadWriteManager<CoreTemplateDto, CoreTemplate> getManager() {
		return coreTemplateManager;
	}

}