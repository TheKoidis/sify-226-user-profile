/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.Collections;
import java.util.List;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreRoleManagerImpl extends DefaultReadWriteManager<CoreRoleDto, CoreRole> implements CoreRoleManager {

	@Override
	public CoreRoleDto searchUsingName(String roleName) {		
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("name", FilterOperator.EQUALS, Collections.singletonList(roleName)));
		
		return (CoreRoleDto) elasticsearchService.searchForSingleResult(dtoClass, parameters, null);
	}
	
	@Override
	public List<CoreRoleDto> getRoleByName(String role) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("name", FilterOperator.EQUALS, Collections.singletonList(role)));

		return search(parameters);
	}
}
