package org.simplifyworks.uam.service;

/**
 *
 * @author Svanda
 */
public interface CoreRoleHierarchy extends org.springframework.security.access.hierarchicalroles.RoleHierarchy {

	public void init();
}
