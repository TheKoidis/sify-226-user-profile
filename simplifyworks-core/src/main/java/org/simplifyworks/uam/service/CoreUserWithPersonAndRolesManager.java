/**
 * 
 */
package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.ReadManager;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.model.entity.CoreUser;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface CoreUserWithPersonAndRolesManager extends ReadManager<CoreUserWithPersonAndRolesDto, CoreUser> {

	public CoreUserWithPersonAndRolesDto findByUsername(String username);
}
