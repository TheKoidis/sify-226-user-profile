package org.simplifyworks.uam.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.simplifyworks.uam.model.entity.CoreRoleComposition;
import org.simplifyworks.uam.service.CoreRoleHierarchy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.stereotype.Service;

/**
 * Load and set composition coreRoles.
 *
 * @author Svanda
 */
@Service
public class CoreRoleHierarchyImpl extends RoleHierarchyImpl implements CoreRoleHierarchy {

	private static final Logger LOG = LoggerFactory.getLogger(CoreRoleHierarchyImpl.class);

	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;

	@Override
	public void init() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreRoleComposition> criteria = cb.createQuery(CoreRoleComposition.class);
		criteria.from(CoreRoleComposition.class);

		StringBuilder sb = new StringBuilder();
		for (CoreRoleComposition rc : entityManager.createQuery(criteria).getResultList()) {
			if (rc.getSubRole() != null && rc.getSuperiorRole() != null) {
				sb.append(rc.getSuperiorRole().getName().toUpperCase());
				sb.append(" > ");
				sb.append(rc.getSubRole().getName().toUpperCase());
				sb.append(" ");
			}
		}

		LOG.debug("Roles hierarchy from DB: " + sb.toString());
		setHierarchy(sb.toString());
	}

}
