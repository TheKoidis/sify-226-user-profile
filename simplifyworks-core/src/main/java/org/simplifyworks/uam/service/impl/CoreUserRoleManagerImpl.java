/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.Collections;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.model.entity.CoreUserRole;
import org.simplifyworks.uam.model.entity.CoreUserRole_;
import org.simplifyworks.uam.service.CoreUserRoleWithUserManager;
import org.simplifyworks.uam.service.OrganizationStructureService;
import org.simplifyworks.uam.service.RoleStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreUserRoleManagerImpl extends DefaultReadWriteManager<CoreUserRoleWithUserDto, CoreUserRole> implements CoreUserRoleWithUserManager {

	@Autowired
	private RoleStructureService roleStructureService;
	@Autowired
	private OrganizationStructureService organizationStructureService;

	@Override
	public List<CoreUserRoleWithUserDto> findByUsername(String username) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("user.username", FilterOperator.EQUALS, Collections.singletonList(username)));

		return elasticsearchService.search(dtoClass, parameters, null);
	}

	@Override
	public List<CoreUserRoleWithUserDto> findByOrganizationAndRole(String roleName, String organizationName) {
		List<String> ancestorRoles = roleStructureService.findAllAncestorRoles(roleName);
		List<String> ancestorOrganizations = organizationStructureService.findAllAncestorOrganizations(organizationName);

		SearchParameters searchParameters = new SearchParameters();
		FilterValue filterValue = new FilterValue(CoreUserRole_.role.getName() + "." + CoreRole_.name.getName(), FilterOperator.EQUALS, ancestorRoles);
		searchParameters.addFilter(filterValue);
		filterValue = new FilterValue(CoreUserRole_.organization.getName() + "." + CoreOrganization_.name.getName(), FilterOperator.EQUALS, ancestorOrganizations);
		searchParameters.addFilter(filterValue);

		List<CoreUserRoleWithUserDto> users = searchWithoutSecurity(searchParameters);
		return users;
	}

	@Override
	protected CriteriaQuery<CoreUserRole> allCriteriaQuery() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreUserRole> criteria = cb.createQuery(entityClass);
		Root<CoreUserRole> root = criteria.from(entityClass);
		root.fetch(CoreUserRole_.user);
		root.fetch(CoreUserRole_.role);
		root.fetch(CoreUserRole_.organization, JoinType.LEFT);
		return criteria;
	}
}
