
package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;

/**
 * Role manager
 *
 * @author VŠ
 */
public interface CoreRoleCompositionManager extends ReadWriteManager<CoreRoleCompositionDto, CoreRoleComposition> {

}
