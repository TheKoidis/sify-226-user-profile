/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.entity.AbstractPersonExtension;
import org.simplifyworks.uam.model.entity.CorePerson;
import org.simplifyworks.uam.service.CorePersonManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CorePersonManagerImpl extends DefaultReadWriteManager<CorePersonDto, CorePerson> implements CorePersonManager {

	@Override
	public CorePerson toEntity(CorePersonDto object) {
		CorePerson entity = super.toEntity(object);
		AbstractPersonExtension personExtension = entity.getPersonExtension();
		if (personExtension != null) {
			personExtension.setCorePerson(entity);
		}

		return entity;
	}
}
