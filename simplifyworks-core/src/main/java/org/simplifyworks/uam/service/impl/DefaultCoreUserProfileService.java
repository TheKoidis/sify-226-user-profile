package org.simplifyworks.uam.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.CoreUserProfileDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.service.CoreUserProfileService;
import org.simplifyworks.uam.service.CoreUserRoleWithUserManager;
import org.simplifyworks.uam.service.CoreUserWithPersonAndRolesManager;
import org.simplifyworks.uam.service.RoleStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultCoreUserProfileService implements CoreUserProfileService {

	@Autowired
	private SecurityService securityService;

	@Autowired
	private CoreUserWithPersonAndRolesManager userWithPersonAndRolesManager;

	@Autowired
	private CoreUserRoleWithUserManager userRoleWithUserManager;

	@Autowired
	private RoleStructureService roleStructureService;

	@Override
	public CoreUserProfileDto getCurrentUserProfile() {
		CoreUserProfileDto userProfile = new CoreUserProfileDto();

		String currentUsername = securityService.getUsername();
		String originalUsername = securityService.getOriginalUsername();

		userProfile.setCurrentUser(getUserWithPerson(currentUsername));
		userProfile.setOriginalUser(getUserWithPerson(originalUsername));
		userProfile.setAllRoles(getAllRoles(currentUsername));

		return userProfile;
	}

	private CoreUserWithPersonAndRolesDto getUserWithPerson(String username) {
		return userWithPersonAndRolesManager.findByUsername(username);
	}

	private Set<String> getAllRoles(String username) {
		List<String> mstrRoles = new ArrayList<>();

		for (CoreUserRoleWithUserDto userRoleWithUser : userRoleWithUserManager.findByUsername(username)) {
			mstrRoles.add(userRoleWithUser.getRole().getName());
		}
		return new HashSet<>(roleStructureService.findAllDescendantRoles(mstrRoles));
	}
}
