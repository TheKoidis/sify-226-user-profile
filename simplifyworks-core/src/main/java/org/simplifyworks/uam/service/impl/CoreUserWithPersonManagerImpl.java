/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.model.entity.CorePermission_;
import org.simplifyworks.uam.model.entity.CorePerson;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUserRole_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserWithPersonManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreUserWithPersonManagerImpl extends DefaultReadWriteManager<CoreUserWithPersonDto, CoreUser> implements CoreUserWithPersonManager {

	@Override
	public void createInitialPermissions(CoreUser entity) {
		CorePermission selfPermission = new CorePermission();
		selfPermission.setObjectIdentifier(entity.getId().toString());
		selfPermission.setObjectType(entityClass.getName());

		selfPermission.setUser(entity);
		selfPermission.setCanRead(true);
		selfPermission.setCanWrite(true);
		selfPermission.setCanDelete(false);

		entityManager.persist(selfPermission);

		super.createInitialPermissions(entity);
	}

	@Override
	@Transactional
	public void removePermissions(CoreUser entity) {
		removePermissions(entity.getId());
	}

	@Transactional
	private void removePermissions(long id) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaDelete<CorePermission> query = cb.createCriteriaDelete(CorePermission.class);
		Root<CorePermission> root = query.from(CorePermission.class);
		query.where(cb.equal(root.get(CorePermission_.user), entityManager.find(CoreUser.class, id)));
		entityManager.createQuery(query).executeUpdate();
	}

	@Override
	@Transactional
	public void remove(long id) {
		removePermissions(id);
		super.remove(id);
	}

	@Override
	@Transactional(readOnly = true)
	public CoreUserWithPersonDto findByUsername(String username) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("username", FilterOperator.EQUALS, Collections.singletonList(username)));

		return elasticsearchService.searchForSingleResult(dtoClass, parameters, null);
	}
        
        @Override
	public List<CoreUserWithPersonDto> findByPerson(CorePerson person) {
		SearchParameters parameters = new SearchParameters();                
		parameters.addFilter(new FilterValue(CoreUser_.person.getName() + "." + CorePerson_.id.getName(), FilterOperator.EQUALS, person.getId()));
		return elasticsearchService.search(dtoClass, parameters, null);
	}

	@Override
	protected CriteriaQuery<CoreUser> allCriteriaQuery() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreUser> criteria = cb.createQuery(entityClass);
		Root<CoreUser> root = criteria.from(entityClass);
		root.fetch(CoreUser_.person, JoinType.LEFT);
		return criteria;
	}
}
