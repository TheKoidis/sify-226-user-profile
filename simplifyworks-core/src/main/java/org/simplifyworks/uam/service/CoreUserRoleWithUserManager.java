package org.simplifyworks.uam.service;

import java.util.List;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreUserRole;

/**
 *
 * @author Svanda
 */
public interface CoreUserRoleWithUserManager extends ReadWriteManager<CoreUserRoleWithUserDto, CoreUserRole> {

	public List<CoreUserRoleWithUserDto> findByUsername(String username);

	/**
	 * Gets users that have role in organization (with role and organization
	 * composition)
	 *
	 * @param roleName
	 * @param organizationName
	 * @return users
	 */
	public List<CoreUserRoleWithUserDto> findByOrganizationAndRole(String roleName, String organizationName);

}
