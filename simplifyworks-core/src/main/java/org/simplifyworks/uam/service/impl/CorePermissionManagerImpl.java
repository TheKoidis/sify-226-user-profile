package org.simplifyworks.uam.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.service.AfterCommitTransactionSynchronization;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.EntityEvent.EntityEventOperation;
import org.simplifyworks.core.service.JpaService;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.model.entity.CorePermission_;
import org.simplifyworks.uam.service.CorePermissionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
@Service
public class CorePermissionManagerImpl implements CorePermissionManager {

	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;
	
	@Autowired
	private JpaService jpaService;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	private AfterCommitTransactionSynchronization afterCommitTransactionSynchronization;
	
	@Override
	public List<CorePermission> search(String objectType, String objectIdentifier) {
		return search(objectType, objectIdentifier, null, null);
	}

	@Override
	public List<CorePermission> search(String objectType, String objectIdentifier, String relType) {
		return search(objectType, objectIdentifier, relType, null);
	}

	@Override
	public List<CorePermission> search(String objectType, String objectIdentifier, String relType, Long relId) {
		Assert.notNull(objectType, "objectType has to be filled");
		Assert.notNull(objectIdentifier, "objectIdentifier has to be filled");
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CorePermission> criteria = cb.createQuery(CorePermission.class);
        Root<CorePermission> from = criteria.from(CorePermission.class);

        List<Predicate> restrictions = new ArrayList<>();
        
        restrictions.add(cb.equal(from.get(CorePermission_.objectType), objectType));
        restrictions.add(cb.equal(from.get(CorePermission_.objectIdentifier), objectIdentifier));
        
        if(relType != null) {
        	restrictions.add(cb.equal(from.get(CorePermission_.relType), relType));
        }
        
        if(relId != null) {
        	restrictions.add(cb.equal(from.get(CorePermission_.relId), relId));
        }
        
        criteria.where(restrictions.toArray(new Predicate[restrictions.size()]));

        return entityManager.createQuery(criteria).getResultList();
	}

	@Override
	public CorePermission create(CorePermission entity) {
		CorePermission result = jpaService.create(entity);
		
		afterCommitTransactionSynchronization.executeAfterCommit(new Runnable() {
			
			@Override
			public void run() {
				eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.CREATE, result));
			};
		});
		
		return result;
	}

	@Override
	public CorePermission update(CorePermission entity) {
		CorePermission result = jpaService.update(entity);
		
		afterCommitTransactionSynchronization.executeAfterCommit(new Runnable() {
			
			@Override
			public void run() {
				eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.UPDATE, result));
			};
		});
		
		return result;
	}
	
	@Override
	public void remove(long id) {
		CorePermission result = jpaService.remove(CorePermission.class, id);
		
		afterCommitTransactionSynchronization.executeAfterCommit(new Runnable() {
			
			@Override
			public void run() {
				eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.REMOVE, result));
			};
		});
	}
}
