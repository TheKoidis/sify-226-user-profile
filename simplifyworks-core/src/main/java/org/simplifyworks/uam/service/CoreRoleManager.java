package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CoreRole;

/**
 * Role manager
 *
 * @author VŠ
 */
public interface CoreRoleManager extends ReadWriteManager<CoreRoleDto, CoreRole> {
	
	/**
	 * Searches for role using its name
	 * 
	 * @param roleName name of role
	 * @return role or null if not found
	 */
	public CoreRoleDto searchUsingName(String roleName);
	
	/**
	 * Use {@link CoreRoleManager#searchUsingName(String)} instead
	 */
	@Deprecated
	public List<CoreRoleDto> getRoleByName(String role);
}
