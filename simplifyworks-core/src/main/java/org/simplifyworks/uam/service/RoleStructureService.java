package org.simplifyworks.uam.service;

import java.util.List;

/**
 * Service that provide access to role structure 
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface RoleStructureService {
	
	/**
	 * Finds all ancestor role names
	 * 
	 * @param roleName name of role where searching starts 
	 * @return names of all ancestors (including roleName)
	 */
	public List<String> findAllAncestorRoles(String roleName);

	/**
	 * Finds all ancestor role names
	 * 
	 * @param roleNames names of roles where searching starts 
	 * @return names of all ancestors (including roleNames)
	 */
	public List<String> findAllAncestorRoles(List<String> roleNames);
	
	/**
	 * Finds all descendant role names
	 * 
	 * @param roleName name of role where searching starts
	 * @return names of all descendants (including roleName);
	 */
	public List<String> findAllDescendantRoles(String roleName);
	
	/**
	 * Finds all descendant role names
	 * 
	 * @param roleNames names of roles where searching starts
	 * @return names of all descendants (including roleNames);
	 */
	public List<String> findAllDescendantRoles(List<String> roleNames);
	
	/**
	 * Rebuilds whole structure
	 */
	public void rebuild();
}
