/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.List;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreOrganizationWithParentDto;
import org.simplifyworks.uam.model.entity.AbstractOrganizationExtension;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.service.CoreOrganizationManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreOrganizationManagerImpl extends DefaultReadWriteManager<CoreOrganizationWithParentDto, CoreOrganization> implements CoreOrganizationManager {

	@Override
	public List<CoreOrganizationWithParentDto> getOrganizationByCode(String org) {
		SearchParameters searchParameters = new SearchParameters();
		FilterValue filterValue = new FilterValue();
		filterValue.setPropertyName(CoreOrganization_.code.getName());
		filterValue.setValue(org);
		filterValue.setOperator(FilterOperator.EQUALS);
		searchParameters.addFilter(filterValue);
		List<CoreOrganizationWithParentDto> resultOrg = search(searchParameters);
		return resultOrg;
	}

	@Override
	public CoreOrganization toEntity(CoreOrganizationWithParentDto object) {
		CoreOrganization entity = super.toEntity(object);
		AbstractOrganizationExtension organizationExtension = entity.getOrganizationExtension();
		if (organizationExtension != null) {
			organizationExtension.setCoreOrganization(entity);
		}

		return entity;
	}

}
