package org.simplifyworks.uam.service;

import org.simplifyworks.uam.model.domain.CoreUserProfileDto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface CoreUserProfileService {

	public CoreUserProfileDto getCurrentUserProfile();
}
