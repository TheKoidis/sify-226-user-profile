/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationDto;
import org.simplifyworks.uam.model.entity.AbstractPersonOrganizationExtension;
import org.simplifyworks.uam.model.entity.CorePersonOrganization;
import org.simplifyworks.uam.model.entity.CorePersonOrganization_;
import org.simplifyworks.uam.service.CorePersonOrganizationManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CorePersonOrganizationManagerImpl extends DefaultReadWriteManager<CorePersonOrganizationDto, CorePersonOrganization> implements CorePersonOrganizationManager {

	@Override
	protected CriteriaQuery<CorePersonOrganization> allCriteriaQuery() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CorePersonOrganization> criteria = cb.createQuery(entityClass);
		Root<CorePersonOrganization> root = criteria.from(entityClass);
		root.fetch(CorePersonOrganization_.person);
		root.fetch(CorePersonOrganization_.organization);
		return criteria;
	}

	@Override
	public CorePersonOrganization toEntity(CorePersonOrganizationDto object) {
		CorePersonOrganization entity = super.toEntity(object);
		AbstractPersonOrganizationExtension personOrganizationExtension = entity.getPersonOrganizationExtension();
		if (personOrganizationExtension != null) {
			personOrganizationExtension.setCorePersonOrganization(entity);
		}

		return entity;
	}
}
