/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;
import org.simplifyworks.uam.model.entity.CoreRoleComposition_;
import org.simplifyworks.uam.service.CoreRoleCompositionManager;
import org.simplifyworks.uam.service.RoleStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Svanda
 */
@Service
public class CoreRoleCompositionManagerImpl extends DefaultReadWriteManager<CoreRoleCompositionDto, CoreRoleComposition> implements CoreRoleCompositionManager {

	@Autowired
	private RoleStructureService roleStructureService;

	@Override
	@Transactional
	public CoreRoleCompositionDto create(CoreRoleCompositionDto dto) {
		if (roleStructureService.findAllAncestorRoles(dto.getSuperiorRole().getName()).contains(dto.getSubRole().getName())) {
			throw new CoreException("Role tree inconsistency");
		}

		return super.create(dto);
	}

	@Override
	@Transactional
	public CoreRoleCompositionDto update(CoreRoleCompositionDto dto) {
		if (roleStructureService.findAllAncestorRoles(dto.getSuperiorRole().getName()).contains(dto.getSubRole().getName())) {
			throw new CoreException("Role tree inconsistency");
		}

		return super.update(dto);
	}

	@Override
	protected CriteriaQuery<CoreRoleComposition> allCriteriaQuery() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreRoleComposition> criteria = cb.createQuery(entityClass);
		Root<CoreRoleComposition> root = criteria.from(entityClass);
		root.fetch(CoreRoleComposition_.subRole);
		root.fetch(CoreRoleComposition_.superiorRole);
		return criteria;
	}
}
