package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.AbstractOrganizationExtensionDto;
import org.simplifyworks.uam.model.entity.AbstractOrganizationExtension;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 * @param <T>
 * @param <U>
 */
public abstract class AbstractOrganizationExtensionManager<T extends AbstractOrganizationExtensionDto, U extends AbstractOrganizationExtension> extends DefaultReadWriteManager<T, U> {
	
}
