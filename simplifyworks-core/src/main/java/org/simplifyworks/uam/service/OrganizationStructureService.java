package org.simplifyworks.uam.service;

import java.util.List;

/**
 * Service that provide access to organization structure
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface OrganizationStructureService {	
	
	/**
	 * Finds all ancestor organization names
	 * 
	 * @param organizationName name of organization where searching starts 
	 * @return names of all ancestors (including organizationName)
	 */
	public List<String> findAllAncestorOrganizations(String organizationName);
	
	/**
	 * Finds all ancestor organization names
	 * 
	 * @param organizationNames names of organizations where searching starts 
	 * @return names of all ancestors (including organizationName)
	 */
	public List<String> findAllAncestorOrganizations(List<String> organizationNames);
	
	/**
	 * Finds all descendant organization names
	 * 
	 * @param organizationName name of organization where searching starts
	 * @return names of all ancestors (including organizationName)
	 */
	public List<String> findAllDescendantOrganizations(String organizationName);
	
	/**
	 * Finds all descendant organization names
	 * 
	 * @param organizationNames names of organizations where searching starts
	 * @return names of all ancestors (including organizationName)
	 */
	public List<String> findAllDescendantOrganizations(List<String> organizationNames);
	
	/**
	 * Rebuilds whole structure
	 */
	public void rebuild();	
}
