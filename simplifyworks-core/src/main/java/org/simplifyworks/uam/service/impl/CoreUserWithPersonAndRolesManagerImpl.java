/**
 *
 */
package org.simplifyworks.uam.service.impl;

import java.util.Collections;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.impl.DefaultReadManager;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUserRole_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserWithPersonAndRolesManager;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class CoreUserWithPersonAndRolesManagerImpl extends DefaultReadManager<CoreUserWithPersonAndRolesDto, CoreUser> implements CoreUserWithPersonAndRolesManager {

	@Override
	public CoreUserWithPersonAndRolesDto findByUsername(String username) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("username", FilterOperator.EQUALS, Collections.singletonList(username)));

		return elasticsearchService.searchForSingleResult(dtoClass, parameters, null);
	}

	@Override
	protected CriteriaQuery<CoreUser> allCriteriaQuery() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreUser> criteria = cb.createQuery(entityClass);
		Root<CoreUser> root = criteria.from(entityClass);
		root.fetch(CoreUser_.person, JoinType.LEFT);
		root.fetch(CoreUser_.roles, JoinType.LEFT).fetch(CoreUserRole_.role, JoinType.LEFT);
		return criteria;
	}
}
