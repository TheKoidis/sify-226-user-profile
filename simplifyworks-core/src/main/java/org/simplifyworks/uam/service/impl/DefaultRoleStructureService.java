package org.simplifyworks.uam.service.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;
import org.simplifyworks.uam.service.RoleStructureService;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link RoleStructureService}
 * 
 * Implementation uses {@link CoreRole} as entity and {@link CoreRoleComposition} as composition.
 *  
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultRoleStructureService extends AbstractStructureService<CoreRole, CoreRoleComposition>
	implements RoleStructureService, ApplicationListener<EntityEvent> {
	
	/**
	 * Document type in index
	 */
	public static final String INDEX_DOCUMENT_TYPE = "role_structure";
	
	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;
	
	@Override
	public List<String> findAllAncestorRoles(String roleName) {
		return findAllAncestors(INDEX_DOCUMENT_TYPE, Collections.singletonList(roleName));
	}
	
	@Override
	public List<String> findAllAncestorRoles(List<String> roleNames) {
		return findAllAncestors(INDEX_DOCUMENT_TYPE, roleNames);
	}
	
	@Override
	public List<String> findAllDescendantRoles(String roleName) {
		return findAllDescendants(INDEX_DOCUMENT_TYPE, Collections.singletonList(roleName));
	}
	
	@Override
	public List<String> findAllDescendantRoles(List<String> roleNames) {
		return findAllDescendants(INDEX_DOCUMENT_TYPE, roleNames);
	}
	
	@Override
	public void rebuild() {
		rebuild(INDEX_DOCUMENT_TYPE);
	}
	
	// TODO using @EventListener in Spring 4.2 - add annotation to rebuildFlatRoleCompositions method
	@Override
	public void onApplicationEvent(EntityEvent event) {
		if(event.getEntity() instanceof CoreRole || event.getEntity() instanceof CoreRoleComposition) {
			rebuild();
		}
	}	
	
	@Override
	protected List<CoreRole> findAllEntities() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreRole> criteria = cb.createQuery(CoreRole.class);
		criteria.from(CoreRole.class);
		
		return entityManager.createQuery(criteria).getResultList();
	}
	
	@Override
	protected List<CoreRoleComposition> findAllCompositions() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreRoleComposition> criteria = cb.createQuery(CoreRoleComposition.class);
		criteria.from(CoreRoleComposition.class);
		
		return entityManager.createQuery(criteria).getResultList();
	}
	
	@Override
	protected CoreRole getParent(CoreRoleComposition composition) {
		return composition.getSuperiorRole();
	}
	
	@Override
	protected CoreRole getChild(CoreRoleComposition composition) {
		return composition.getSubRole();
	}
	
	@Override
	protected String getId(CoreRole entity) {
		return entity.getName();
	}
}