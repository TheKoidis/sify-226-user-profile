package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.uam.model.entity.CorePermission;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
public interface CorePermissionManager {

	/**
	 * Searches for permissions using object type and identifier
	 * 
	 * @param objectType object type (object class name)
	 * @param objectIdentifier identifier of object
	 * @return related permissions
	 */
	List<CorePermission> search(String objectType, String objectIdentifier);
	
	/**
	 * Searches for permissions using object type and identifier
	 * 
	 * @param objectType object type (object class name)
	 * @param objectIdentifier identifier of object
	 * @param relType related object type
	 * @return related permissions
	 */
	List<CorePermission> search(String objectType, String objectIdentifier, String relType);
	
	/**
	 * Searches for permissions using object type and identifier
	 * 
	 * @param objectType object type (object class name)
	 * @param objectIdentifier identifier of object
	 * @param relType related object type
	 * @param relId identifier of related object
	 * @return related permissions
	 */
	List<CorePermission> search(String objectType, String objectIdentifier, String relType, Long relId);
	
	/**
	 * Creates and persists entity
	 *
	 * @param entity entity
	 * @return persisted entity
	 */
	CorePermission create(CorePermission entity);
	
	/**
	 * Updates entity
	 *
	 * @param entity entity
	 * @return persisted entity
	 */
	CorePermission update(CorePermission entity);
	
	/**
	 * Removes existing entity using id
	 *
	 * @param id id
	 */
	void remove(long id);
}