package org.simplifyworks.uam.service.impl;

import java.util.Objects;

import org.springframework.util.Assert;

/**
 * Common structure that represents element of structure (for example in role or organization hierarchy)
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
class StructureElement {
	
	private String parent;
	private String child;
	
	protected StructureElement() {
	}
	
	/**
	 * Creates a new instance
	 * 	
	 * @param parent parent (some unique identifier, required)
	 * @param child child (some unique identifier, required)
	 */
	public StructureElement(String parent, String child) {
		Assert.notNull(parent, "parent must be filled");
		Assert.notNull(child, "child must be filled");
		
		this.parent = parent;
		this.child = child;
	}

	public String getParent() {
		return parent;
	}

	protected void setParent(String parent) {
		this.parent = parent;
	}

	public String getChild() {
		return child;
	}

	protected void setChild(String child) {
		this.child = child;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof StructureElement) {
			StructureElement that = (StructureElement) obj;
			
			return Objects.equals(this.parent, that.parent) && Objects.equals(this.child, that.child);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(parent, child);
	}
}
