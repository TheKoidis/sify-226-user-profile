package org.simplifyworks.uam.model.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

/**
 * 	Default implementation of granted authority which uses names of role and organization
 *
 * 	@author Svanda
 * 	@author Štěpán Osmík (osmik@ders.cz)
 */
public class RoleOrganizationGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = -2393472063873247571L;
	
	/**
	 * Pattern for parsing 
	 */
	public static final Pattern PATTERN = Pattern.compile("(?<roleName>[^@]+)([@](?<organizationName>[^@]+))?");
	
	private final String roleName;
	private final String organizationName;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param roleName name of role (required)
	 * @param organizationName name of organization
	 */
	public RoleOrganizationGrantedAuthority(String roleName, String organizationName) {
		Assert.notNull(roleName, "roleName must be filled");
		
		this.roleName = roleName;
		this.organizationName = organizationName;
	}
	
	public String getRoleName() {
		return roleName;
	}
	
	public String getOrganizationName() {
		return organizationName;
	}

	@Override
	public String getAuthority() {
		return roleName + (organizationName == null ? "" : "@" + organizationName);
	}
	
	@Override
	public String toString() {
		return getAuthority();
	}
	
	/**
	 * Parses authority from string using pattern. 
	 * 
	 * @param string string to parse (required)
	 * @return parsed authority
	 * @throws IllegalArgumentException if string does not match pattern
	 */
	public static RoleOrganizationGrantedAuthority parse(String string) {
		Assert.notNull(string, "string must be filled");
		
		Matcher matcher = PATTERN.matcher(string);
		
		if(matcher.matches()) {
			return new RoleOrganizationGrantedAuthority(matcher.group("roleName"), matcher.group("organizationName"));
		} else {
			throw new IllegalArgumentException("String does not match pattern");
		}
	}
}
