package org.simplifyworks.uam.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum Sex {
	M, F;
}
