package org.simplifyworks.uam.model.domain;

import java.util.Set;

import org.simplifyworks.core.model.domain.Dependency;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.model.entity.CoreUserRole;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Dependency(entityClass = CoreUserRole.class, inversePath = "user.id")
public class CoreUserProfileDto {

	private CoreUserWithPersonAndRolesDto currentUser;
	private CoreUserWithPersonAndRolesDto originalUser;
	
	private Set<String> allRoles;
	
	public CoreUserProfileDto() {
	}
	
	public CoreUserWithPersonAndRolesDto getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(CoreUserWithPersonAndRolesDto currentUser) {
		this.currentUser = currentUser;
	}
	
	public CoreUserWithPersonAndRolesDto getOriginalUser() {
		return originalUser;
	}
	
	public void setOriginalUser(CoreUserWithPersonAndRolesDto originalUser) {
		this.originalUser = originalUser;
	}
	
	public Set<String> getAllRoles() {
		return allRoles;
	}
	
	public void setAllRoles(Set<String> allRoles) {
		this.allRoles = allRoles;
	}
}
