/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.domain.Sex;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Permission(createRoles = {"core_person_write"}, readRoles = {"core_user", "core_person_read"}, writeRoles = {"core_person_write"}, deleteRoles = {"core_person_delete"})
public class CorePerson extends AbstractEntity {

	@Size(max = 255)
	@Column(name = "PERSONAL_NUMBER", length = 255)
	private String personalNumber;
	@NotNull
	@Column(name = "SURNAME", nullable = false, length = 100)
	@Size(max = 100)
	private String surname;
	@Column(name = "FIRSTNAME", length = 100)
	@Size(max = 100)
	private String firstname;
	@Column(name = "TITLE_BEFORE", length = 100)
	@Size(max = 100)
	private String titleBefore;
	@Column(name = "TITLE_AFTER", length = 100)
	@Size(max = 100)
	private String titleAfter;
	@Email
	@Column(name = "EMAIL", length = 255)
	@Size(max = 255)
	private String email;
	@Column(name = "PHONE", length = 30)
	@Size(max = 30)
	private String phone;
	@Size(max = 200)
	@Column(name = "NOTE", length = 200)
	private String note;
	@Size(max = 100)
	@Column(name = "ADDRESSING_PHRASE", length = 100)
	private String addressingPhrase;
	@Max(999999999999999999L)
	@Column(name = "UNIT", precision = 18, scale = 0)
	private Long unit;
	@NotNull
	@Column(name = "ACTIVE", nullable = false)
	private boolean active = true;
	@Column(name = "VALID_FROM")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date validFrom;
	@Column(name = "VALID_TO")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date validTo;
	@Column(name = "DATE_OF_BIRTH")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dateOfBirth;
	@NotNull
	@Column(name = "SEX", nullable = false)
	@Enumerated(EnumType.STRING)
	private Sex sex = Sex.F;
	@OneToOne(mappedBy = "corePerson", cascade = {CascadeType.ALL})
	private AbstractPersonExtension personExtension;

	@OneToMany(mappedBy = "person")
	private List<CoreUser> users;
	@OneToMany(mappedBy = "person")
	private List<CorePersonOrganization> personOrganizations;

	public CorePerson() {
	}

	public CorePerson(Long id) {
		super(id);
	}

	public String getPersonalNumber() {
		return personalNumber;
	}

	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getTitleBefore() {
		return titleBefore;
	}

	public void setTitleBefore(String titleBefore) {
		this.titleBefore = titleBefore;
	}

	public String getTitleAfter() {
		return titleAfter;
	}

	public void setTitleAfter(String titleAfter) {
		this.titleAfter = titleAfter;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getAddressingPhrase() {
		return addressingPhrase;
	}

	public void setAddressingPhrase(String addressingPhrase) {
		this.addressingPhrase = addressingPhrase;
	}

	public Long getUnit() {
		return unit;
	}

	public void setUnit(Long unit) {
		this.unit = unit;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public AbstractPersonExtension getPersonExtension() {
		return personExtension;
	}

	public void setPersonExtension(AbstractPersonExtension personExtension) {
		this.personExtension = personExtension;
	}

}
