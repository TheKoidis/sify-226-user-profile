package org.simplifyworks.uam.model.dto;

import java.util.List;

import org.simplifyworks.core.model.domain.Dependency;
import org.simplifyworks.uam.model.entity.CoreUserRole;

@Dependency(entityClass = CoreUserRole.class, inversePath = "user.id")
public class CoreUserWithPersonAndRolesDto extends CoreUserWithPersonDto {

	private List<CoreUserRoleDto> roles;
	
	public CoreUserWithPersonAndRolesDto() {
	}

	public List<CoreUserRoleDto> getRoles() {
		return roles;
	}
	
	public void setRoles(List<CoreUserRoleDto> roles) {
		this.roles = roles;
	}
}
