/**
 * 
 */
package org.simplifyworks.uam.model.dto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class CoreUserRoleWithUserDto extends CoreUserRoleDto {

	private CoreUserWithPersonDto user;
	
	public CoreUserWithPersonDto getUser() {
		return user;
	}
	
	public void setUser(CoreUserWithPersonDto user) {
		this.user = user;
	}
}
