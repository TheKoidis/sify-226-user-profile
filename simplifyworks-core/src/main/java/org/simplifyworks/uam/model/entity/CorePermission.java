package org.simplifyworks.uam.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.domain.PermissionFor;

/**
 * Basic permission for entity
 *
 * @author Vít Švanda
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DISCRIMINATOR")
@DiscriminatorValue("ENTITY")
@Table(name = "CORE_PERMISSION")
//@Table(name = "CORE_PERMISSION", uniqueConstraints = @UniqueConstraint(columnNames = {"WF_DEFINITION_ID", "WF_USER_TASK", "WF_BUTTON_KEY"}))
public class CorePermission extends AbstractEntity {

	@NotNull
	@Column(name = "ACTIVE", nullable = false)
	private Boolean active = Boolean.TRUE;
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "PERMISSION_FOR", nullable = false)
	private PermissionFor permissionFor = PermissionFor.ENTITY;
	@Column(name = "OBJECT_IDENTIFIER", length = 100)
	@Size(max = 100)
	private String objectIdentifier;
	@NotNull
	@Size(max = 150)
	@Column(name = "OBJECT_TYPE", nullable = false, length = 150)
	private String objectType;
	@JoinColumn(name = "CORE_USER_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true)
	private CoreUser user;
	@JoinColumn(name = "CORE_ORGANIZATION_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true)
	private CoreOrganization organization;
	@JoinColumn(name = "CORE_ROLE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true)
	private CoreRole role;
	@Size(max = 150)
	@Column(name = "REL_TYPE", length = 150)
	private String relType;
	@Max(999999999999999999L)
	@Column(name = "REL_ID", precision = 18, scale = 0)
	private Long relId;
	@Column(name = "VALID_FROM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;
	@Column(name = "VALID_TILL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTill;
	//Action type
	@NotNull
	@Column(name = "CAN_READ", nullable = false)
	private Boolean canRead = Boolean.FALSE;
	@NotNull
	@Column(name = "CAN_WRITE", nullable = false)
	private Boolean canWrite = Boolean.FALSE;
	@NotNull
	@Column(name = "CAN_DELETE", nullable = false)
	private Boolean canDelete = Boolean.FALSE;

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public PermissionFor getPermissionFor() {
		return permissionFor;
	}

	public void setPermissionFor(PermissionFor permissionFor) {
		this.permissionFor = permissionFor;
	}

	public String getObjectIdentifier() {
		return objectIdentifier;
	}

	public void setObjectIdentifier(String objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public CoreUser getUser() {
		return user;
	}

	public void setUser(CoreUser user) {
		this.user = user;
	}

	public CoreOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganization organization) {
		this.organization = organization;
	}

	public Boolean getCanRead() {
		return canRead;
	}

	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}

	public Boolean getCanWrite() {
		return canWrite;
	}

	public void setCanWrite(Boolean canWrite) {
		this.canWrite = canWrite;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public CoreRole getRole() {
		return role;
	}

	public void setRole(CoreRole role) {
		this.role = role;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(String relType) {
		this.relType = relType;
	}

	public Long getRelId() {
		return relId;
	}

	public void setRelId(Long relId) {
		this.relId = relId;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

}
