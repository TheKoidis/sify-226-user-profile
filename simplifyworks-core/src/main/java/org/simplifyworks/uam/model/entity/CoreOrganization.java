/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Permission(createRoles = {"core_organization_write"}, readRoles = {"core_user", "core_organization_read"}, writeRoles = {"core_organization_write"}, deleteRoles = {"core_organization_delete"})
public class CoreOrganization extends AbstractEntity {

	@NotNull
	@Basic(optional = false)
	@Column(nullable = false, length = 100)
	@Size(max = 100)
	private String name;
	@ManyToOne
	@JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
	private CoreOrganization organization;
	@Size(max = 50)
	@Column(length = 50)
	private String code;
	@Size(max = 50)
	@Column(length = 50)
	private String abbreviation;
	@Max(999999999999999999L)
	@Column(name = "UNIT", precision = 18, scale = 0)
	private Long unit;
	@NotNull
	@Column(name = "ACTIVE",nullable = false)
	private boolean active = true;
	@Column(name = "VALID_FROM")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date validFrom;
	@Column(name = "VALID_TO")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date validTo;

	@OneToOne(mappedBy = "coreOrganization", cascade = {CascadeType.ALL})
	private AbstractOrganizationExtension organizationExtension;

	@OneToMany(mappedBy = "organization")
	private List<CoreUserRole> userRoles;
	@OrderBy("name")
	@OneToMany(mappedBy = "organization")
	private List<CoreOrganization> organizations;

	public CoreOrganization() {
	}

	public CoreOrganization(Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CoreOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganization organization) {
		this.organization = organization;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Long getUnit() {
		return unit;
	}

	public void setUnit(Long unit) {
		this.unit = unit;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public AbstractOrganizationExtension getOrganizationExtension() {
		return organizationExtension;
	}

	public void setOrganizationExtension(AbstractOrganizationExtension organizationExtension) {
		this.organizationExtension = organizationExtension;
	}

}
