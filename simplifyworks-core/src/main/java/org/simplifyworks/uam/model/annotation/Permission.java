package org.simplifyworks.uam.model.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Svanda on 21.7.2015.
 */
@Target(TYPE)
@Retention(RUNTIME)
public @interface Permission {

    String workplaceField() default "";

    String[] createRoles() default {};

    String[] readRoles() default {};

    String[] writeRoles() default {};

    String[] deleteRoles() default {};

}
