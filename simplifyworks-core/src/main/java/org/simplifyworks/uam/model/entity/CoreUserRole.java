/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Permission(createRoles = {"core_user_role_write"}, readRoles = {"core_user", "core_user_role_read"}, writeRoles = {"core_user_role_write"}, deleteRoles = {"core_user_role_delete"})
public class CoreUserRole extends AbstractEntity {

	@Basic(optional = false)
	@JoinColumn(name = "CORE_USER_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private CoreUser user;
	@Column(name = "VALID_FROM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;
	@Column(name = "VALID_TILL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTill;
	@JoinColumn(name = "CORE_ROLE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private CoreRole role;
	@JoinColumn(name = "CORE_ORGANIZATION_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private CoreOrganization organization;

	public CoreUserRole() {
	}

	public CoreUserRole(Long id) {
		super(id);
	}

	public CoreUser getUser() {
		return user;
	}

	public void setUser(CoreUser user) {
		this.user = user;
	}

	public CoreRole getRole() {
		return role;
	}

	public void setRole(CoreRole role) {
		this.role = role;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTo) {
		this.validTill = validTo;
	}

	public CoreOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganization organization) {
		this.organization = organization;
	}

}
