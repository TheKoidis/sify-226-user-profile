package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class AbstractOrganizationExtensionDto extends AbstractDto {
    
    private CoreOrganizationDto coreOrganization;

    public CoreOrganizationDto getCoreOrganization() {
        return coreOrganization;
    }

    public void setCoreOrganization(CoreOrganizationDto coreOrganization) {
        this.coreOrganization = coreOrganization;
    }  

}
