/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import java.util.Date;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.domain.Sex;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Permission(workplaceField = "organization", createRoles = {"admin"}, readRoles = {"user"}, writeRoles = {"admin"}, deleteRoles = {"admin"})
public class CorePersonDto extends AbstractDto {

	private String personalNumber;
	private String surname;
	private String firstname;
	private String titleBefore;
	private String titleAfter;
	private String email;
	private String phone;
	private String note;
	private String addressingPhrase;
	private Long unit;
	private boolean active = true;
	private Date validFrom;
	private Date validTo;
	private Date dateOfBirth;
	private Sex sex = Sex.F;

	public CorePersonDto() {
	}

	public CorePersonDto(Long id) {
		super(id);
	}

	public String getPersonalNumber() {
		return personalNumber;
	}

	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getTitleBefore() {
		return titleBefore;
	}

	public void setTitleBefore(String titleBefore) {
		this.titleBefore = titleBefore;
	}

	public String getTitleAfter() {
		return titleAfter;
	}

	public void setTitleAfter(String titleAfter) {
		this.titleAfter = titleAfter;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getAddressingPhrase() {
		return addressingPhrase;
	}

	public void setAddressingPhrase(String addressingPhrase) {
		this.addressingPhrase = addressingPhrase;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Long getUnit() {
		return unit;
	}

	public void setUnit(Long unit) {
		this.unit = unit;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
