package org.simplifyworks.uam.model.domain;

/**
 * Enum define for what is permission intend
 * Created by Svanda on 16.7.2015.
 */
public enum PermissionFor {
	ENTITY, SECTION, SCREEN, WORKFLOW;
}
