/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * TODO not used anywhere
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreContactDto extends AbstractDto {

	private String contact;

	public CoreContactDto() {
	}

	public CoreContactDto(Long id) {
		super(id);
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
}
