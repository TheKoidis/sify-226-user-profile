package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.uam.model.domain.PermissionFor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
public class CorePermissionDto extends AbstractDto {

    private Boolean active;
    private Boolean canDelete;
    private String objectIdentifier;
    private String objectType;
    private CoreUserWithPersonDto user;
    private Boolean canWrite;
    private PermissionFor permissionFor;
    private Boolean canRead;
    private CoreOrganizationDto organization;
    private CoreRoleDto role;
    private String relType;
    private Long relId;
    private Date validFrom;
    private Date validTill;

    public CorePermissionDto() {
    }

    public CorePermissionDto(Long id) {
        super(id);
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public CoreUserWithPersonDto getUser() {
        return user;
    }

    public void setUser(CoreUserWithPersonDto coreUser) {
        this.user = coreUser;
    }

    public Boolean getCanWrite() {
        return canWrite;
    }

    public void setCanWrite(Boolean canWrite) {
        this.canWrite = canWrite;
    }

    public PermissionFor getPermissionFor() {
        return permissionFor;
    }

    public void setPermissionFor(PermissionFor permissionFor) {
        this.permissionFor = permissionFor;
    }

    public Boolean getCanRead() {
        return canRead;
    }

    public void setCanRead(Boolean canRead) {
        this.canRead = canRead;
    }

    public CoreOrganizationDto getOrganization() {
        return organization;
    }

    public void setOrganization(CoreOrganizationDto organization) {
        this.organization = organization;
    }

    public CoreRoleDto getRole() {
        return role;
    }

    public void setRole(CoreRoleDto role) {
        this.role = role;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public Long getRelId() {
        return relId;
    }

    public void setRelId(Long relId) {
        this.relId = relId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTill() {
        return validTill;
    }

    public void setValidTill(Date validTill) {
        this.validTill = validTill;
    }
     
}
