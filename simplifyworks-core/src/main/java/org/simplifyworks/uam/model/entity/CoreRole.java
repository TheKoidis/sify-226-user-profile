package org.simplifyworks.uam.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 * Application Role
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Permission(createRoles = {"core_role_write"}, readRoles = {"core_user", "core_role_read"}, writeRoles = {"core_role_write"}, deleteRoles = {"core_role_delete"})
public class CoreRole extends AbstractEntity {

	@Size(min = 1, max = 64)
	@Column(name = "NAME", length = 64, nullable = false, unique = true, updatable = false)
	@NotNull
	private String name;
	@Size(max = 255)
	@Column(name = "DESCRIPTION", length = 255, nullable = true)
	private String description;

	@OneToMany(mappedBy = "role")
	private List<CoreUserRole> userRoles;

	public CoreRole() {
	}

	public CoreRole(Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
