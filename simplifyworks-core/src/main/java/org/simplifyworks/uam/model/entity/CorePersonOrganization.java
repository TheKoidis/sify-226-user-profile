package org.simplifyworks.uam.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Permission(createRoles = {"core_person_organization_write"}, readRoles = {"core_user", "core_person_organization_read"}, writeRoles = {"core_person_organization_write"}, deleteRoles = {"core_person_organization_delete"})
public class CorePersonOrganization extends AbstractEntity {

	@NotNull
	@JoinColumn(name = "CORE_PERSON_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(optional = false)
	private CorePerson person;
	@NotNull
	@JoinColumn(name = "CORE_ORGANIZATION_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(optional = false)
	private CoreOrganization organization;
	@Size(max = 50)
	@Column(name = "CATEGORY", length = 50)
	private String category;
	@Column(name = "VALID_FROM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;
	@Column(name = "VALID_TO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;
	@Min(0)
	@Max(1)
	@Column(name = "OBLIGATION", precision = 3, scale = 2)
	private BigDecimal obligation;
	@OneToOne(mappedBy = "corePersonOrganization", cascade = {CascadeType.ALL})
	private AbstractPersonOrganizationExtension personOrganizationExtension;

	public CorePersonOrganization() {
	}

	public CorePersonOrganization(Long id) {
		super(id);
	}

	public CorePerson getPerson() {
		return person;
	}

	public void setPerson(CorePerson person) {
		this.person = person;
	}

	public CoreOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganization organization) {
		this.organization = organization;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getObligation() {
		return obligation;
	}

	public void setObligation(BigDecimal obligation) {
		this.obligation = obligation;
	}

	public AbstractPersonOrganizationExtension getPersonOrganizationExtension() {
		return personOrganizationExtension;
	}

	public void setPersonOrganizationExtension(AbstractPersonOrganizationExtension personOrganizationExtension) {
		this.personOrganizationExtension = personOrganizationExtension;
	}

}
