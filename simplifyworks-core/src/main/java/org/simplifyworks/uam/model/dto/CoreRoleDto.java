package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreRoleDto extends AbstractDto {

    private String name;
    private String description;

    public CoreRoleDto() {
    }

    public CoreRoleDto(Long id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
