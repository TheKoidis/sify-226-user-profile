/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Date;
import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * Security user
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class)
public class CoreUserDto extends AbstractDto {

	private String username;
	private char[] password;
	private Date lastLogin;
	private char[] newPassword;
	private String wfProcessInstanceId;

	public CoreUserDto() {
	}

	public CoreUserDto(Long id) {
		super(id);
	}

	public CoreUserDto(String username, char[] password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public char[] getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(char[] newPassword) {
		this.newPassword = newPassword;
	}
}
