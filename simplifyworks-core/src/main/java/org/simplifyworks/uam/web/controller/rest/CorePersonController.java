package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.entity.CorePerson;
import org.simplifyworks.uam.service.CorePersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping(value = "/api/core/persons")
public class CorePersonController extends BasicTableController<CorePersonDto, CorePerson> {

	@Autowired
	private CorePersonManager corePersonManager;

	@Override
	protected ReadWriteManager<CorePersonDto, CorePerson> getManager() {
		return corePersonManager;
	}

}
