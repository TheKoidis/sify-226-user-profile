
package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;
import org.simplifyworks.uam.service.CoreRoleCompositionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping(value= "/api/core/role-compositions")
public class CoreRoleCompositionController extends BasicTableController<CoreRoleCompositionDto, CoreRoleComposition> {

	@Autowired
	private CoreRoleCompositionManager coreRoleCompositionManager;
	
	@Override
	protected ReadWriteManager<CoreRoleCompositionDto, CoreRoleComposition> getManager() {
		return coreRoleCompositionManager;
	}
}
