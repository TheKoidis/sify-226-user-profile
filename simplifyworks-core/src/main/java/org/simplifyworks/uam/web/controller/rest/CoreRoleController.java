
package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Svanda
 */
@RestController
@RequestMapping("/api/core/roles")
public class CoreRoleController extends BasicTableController<CoreRoleDto, CoreRole> {

	@Autowired
	private CoreRoleManager coreRoleManager;

	@Override
	protected ReadWriteManager<CoreRoleDto, CoreRole> getManager() {
		return coreRoleManager;
	}
	
}
