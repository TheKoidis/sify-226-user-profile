package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadManager;
import org.simplifyworks.core.web.controller.rest.DefaultReadController;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.service.CoreUserWithPersonAndRolesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping("/api/core/users-with-roles")
public class CoreUserWithPersonAndRolesController extends DefaultReadController<CoreUserWithPersonAndRolesDto, CoreUser> {

	@Autowired
	private CoreUserWithPersonAndRolesManager coreUserWithRolesManager;
	
	@Override
	protected ReadManager<CoreUserWithPersonAndRolesDto, CoreUser> getManager() {
		return coreUserWithRolesManager;
	}
}
