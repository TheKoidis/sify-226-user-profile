package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.exception.EntityNotFoundException;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.service.CoreUserWithPersonAndRolesManager;
import org.simplifyworks.uam.service.CoreUserWithPersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping("/api/core/users")
public class CoreUserController extends BasicTableController<CoreUserWithPersonDto, CoreUser> {

	@Autowired
	private SecurityService securityService;

	@Autowired
	private CoreUserWithPersonManager coreUserManager;

	@Autowired
	private CoreUserWithPersonAndRolesManager coreUserWithRolesManager;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	protected ReadWriteManager<CoreUserWithPersonDto, CoreUser> getManager() {
		return coreUserManager;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ResourceWrapper<CoreUserWithPersonAndRolesDto> profile() {
		if (!securityService.isAuthenticated()) {
			return null;
		}
		CoreUserWithPersonAndRolesDto user = coreUserWithRolesManager.findByUsername(securityService.getUsername());
		if (user==null) {
			throw new CoreException("Profile for user [" + securityService.getUsername() + "] not found");
		}
		return new ResourceWrapper<>(user);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResourceWrapper<CoreUserWithPersonDto> get(@PathVariable Long id) {
		ResourceWrapper<CoreUserWithPersonDto> resource = super.get(id);
		return resource;
	}

	@Override
	@RequestMapping(method = RequestMethod.POST)
	public ResourceWrapper<CoreUserWithPersonDto> create(@RequestBody CoreUserWithPersonDto dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
		char[] password = dto.getNewPassword();
		
		StringBuilder sb = new StringBuilder();
		sb.append(password);
		password = passwordEncoder.encode(sb).toCharArray();
		dto.setPassword(password);
		
		ResourceWrapper<CoreUserWithPersonDto> resource = super.create(dto, includeMetadata != null);
		return resource;
	}

	@Override
	@RequestMapping(method = RequestMethod.PUT)
	public ResourceWrapper<CoreUserWithPersonDto> update(@RequestBody CoreUserWithPersonDto dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
		CoreUserWithPersonDto refresh = getManager().search(dto.getId());
		char[] password = dto.getNewPassword();

		if (password == null || password.length == 0) {
			dto.setPassword(refresh.getPassword());
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(password);
			password = passwordEncoder.encode(sb).toCharArray();
			dto.setPassword(password);
		}

		CoreUserWithPersonDto result = getManager().update(dto);
		if (result == null) {
			throw new EntityNotFoundException(dto.getId());
		}

		return new ResourceWrapper<CoreUserWithPersonDto>(result);
	}

}
