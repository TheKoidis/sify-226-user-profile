package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.uam.model.domain.CoreUserProfileDto;
import org.simplifyworks.uam.service.CoreUserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping("/api/core/user-profile")
public class CoreUserProfileController {

	@Autowired
	private CoreUserProfileService userProfileService;
	
	@RequestMapping(value = "/current", method = RequestMethod.GET)
    public ResourceWrapper<CoreUserProfileDto> getCurrentUserProfile() {
		return new ResourceWrapper<CoreUserProfileDto>(userProfileService.getCurrentUserProfile());
	}
}
