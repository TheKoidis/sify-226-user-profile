package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationDto;
import org.simplifyworks.uam.model.entity.CorePersonOrganization;
import org.simplifyworks.uam.service.CorePersonOrganizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping(value = "/api/core/person-organizations")
public class CorePersonOrganizationController extends BasicTableController<CorePersonOrganizationDto, CorePersonOrganization> {

	@Autowired
	private CorePersonOrganizationManager corePersonOrganizationManager;

	@Override
	protected ReadWriteManager<CorePersonOrganizationDto, CorePersonOrganization> getManager() {
		return corePersonOrganizationManager;
	}

}
