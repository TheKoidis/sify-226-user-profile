/**
 * User access management package
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
package org.simplifyworks.uam;