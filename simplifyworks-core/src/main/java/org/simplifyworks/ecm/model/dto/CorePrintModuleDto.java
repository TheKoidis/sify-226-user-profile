package org.simplifyworks.ecm.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.ecm.domain.ReportType;
import org.simplifyworks.ecm.model.entity.CorePrintModule;

/**
 * Data object for {@link CorePrintModule}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class CorePrintModuleDto extends AbstractDto {

	private String name;
	private String template;
	private ReportType type;
	private String objectType;
	private String wfState;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public ReportType getType() {
		return type;
	}
	public void setType(ReportType type) {
		this.type = type;
	}
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getWfState() {
		return wfState;
	}
	public void setWfState(String wfState) {
		this.wfState = wfState;
	}
}