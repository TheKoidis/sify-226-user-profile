
package org.simplifyworks.ecm.model.dto;

import java.io.InputStream;
import java.util.List;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.ecm.model.entity.Attachable;

/**
 * Metadata for attachment DTO
 *
 * @author Svanda
 */

public class CoreAttachmentDto extends AbstractDto {

    private String objectType;
    private String objectIdentifier;
    private String contentGuid;
    private String name;
    private String description;
    private String mimetype;
    private String encoding;
    private Long filesize;
    private String contentPath;
    private String attachmentType;
    private String objectStatus;
    private Integer versionNumber;
    private String versionLabel;
    private CoreAttachmentDto parent;
    private CoreAttachmentDto nextVersion;
		
    private transient InputStream inputData;
   
    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public String getContentGuid() {
        return contentGuid;
    }

    public void setContentGuid(String contentGuid) {
        this.contentGuid = contentGuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public Long getFilesize() {
        return filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }


	public void setInputData(InputStream inputData) {
		this.inputData = inputData;
	}

	public InputStream getInputData() {
		return inputData;
	}

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public String getObjectStatus() {
        return objectStatus;
    }

    public void setObjectStatus(String objectStatus) {
        this.objectStatus = objectStatus;
    }

    public Integer getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Integer versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getVersionLabel() {
        return versionLabel;
    }

    public void setVersionLabel(String versionLabel) {
        this.versionLabel = versionLabel;
    }

    public CoreAttachmentDto getParent() {
        return parent;
    }

    public void setParent(CoreAttachmentDto parent) {
        this.parent = parent;
    }

    public CoreAttachmentDto getNextVersion() {
        return nextVersion;
    }

    public void setNextVersion(CoreAttachmentDto nextVersion) {
        this.nextVersion = nextVersion;
    }

}
