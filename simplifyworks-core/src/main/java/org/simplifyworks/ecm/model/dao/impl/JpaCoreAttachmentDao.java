package org.simplifyworks.ecm.model.dao.impl;

import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang.StringUtils;
import org.simplifyworks.core.model.dao.impl.JpaAnyTypeDao;
import org.simplifyworks.ecm.model.dao.CoreAttachmentDao;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.ecm.model.entity.CoreAttachment_;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

/**
 * Load metadata for attachment from DB and FS
 *
 * @author Svanda
 */
@Repository
public class JpaCoreAttachmentDao extends JpaAnyTypeDao<CoreAttachment> implements CoreAttachmentDao {

	public JpaCoreAttachmentDao() {
		super(CoreAttachment.class);
	}

	/**
	 * Return attachments for object in actual version
	 *
	 * @param objectType
	 * @param objectIdentifier
	 * @param objectStatus
	 * @param attachmentType
	 * @return
	 */
	@Override
	public List<CoreAttachment> getAttachments(String objectType, String objectIdentifier, String objectStatus, String attachmentType) {
		Assert.notNull(objectType, "Insert type of object");
		Assert.notNull(objectIdentifier, "Insert id of object");
//
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreAttachment> cqM = cb.createQuery(clazz);
		Root<CoreAttachment> root = cqM.from(clazz);
		Predicate restrictions = cb.and(
						cb.equal(root.get(CoreAttachment_.objectType), objectType),
						cb.equal(root.get(CoreAttachment_.objectIdentifier), objectIdentifier),
						cb.isNull(root.get(CoreAttachment_.nextVersion))
		);
		if (StringUtils.isNotEmpty(objectStatus)) {
			restrictions = cb.and(restrictions, cb.equal(root.get(CoreAttachment_.objectStatus), objectStatus));
		}
		if (StringUtils.isNotEmpty(attachmentType)) {
			restrictions = cb.and(restrictions, cb.equal(root.get(CoreAttachment_.attachmentType), attachmentType));
		}
		cqM.where(restrictions);
		cqM.orderBy(cb.asc(root.get(CoreAttachment_.name)));
		return entityManager.createQuery(cqM).getResultList();

	}

	/**
	 * Load attachment for guid
	 *
	 * @param contentGuid
	 * @return
	 */
	@Override
	public CoreAttachment getAttachment(String contentGuid) {
		Assert.notNull(contentGuid, "Insert guid of object");
		//
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<CoreAttachment> cqM = cb.createQuery(clazz);
			Root<CoreAttachment> root = cqM.from(clazz);
			cqM.where(cb.equal(root.get(CoreAttachment_.contentGuid), contentGuid));
			return entityManager.createQuery(cqM).getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * Load attachment in last version for name and object where is attache.
	 *
	 * @param objectType
	 * @param objectIdentifier
	 * @param name
	 * @return
	 */
	@Override
	public CoreAttachment getAttachmentByName(String objectType, String objectIdentifier, String name) {
		Assert.notNull(objectType, "Insert type of object");
		Assert.notNull(objectIdentifier, "Insert id of object");
		Assert.notNull(name, "Insert name of attachment");
		//
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<CoreAttachment> cqM = cb.createQuery(clazz);
			Root<CoreAttachment> root = cqM.from(clazz);
			cqM.where(cb.and(
							cb.equal(root.get(CoreAttachment_.objectType), objectType),
							cb.equal(root.get(CoreAttachment_.objectIdentifier), objectIdentifier),
							cb.equal(root.get(CoreAttachment_.name), name),
							cb.isNull(root.get(CoreAttachment_.nextVersion))
			));
			return entityManager.createQuery(cqM).getSingleResult();

		} catch (NoResultException ex) {
			return null;
		}
	}

	/**
	 * All versions attachment include it self.
	 *
	 * @param attachmentId
	 * @return
	 */
	@Override
	public List<CoreAttachment> getAttachmentVersions(Long attachmentId) {
		Assert.notNull(attachmentId, "Insert id of attachment");
		//
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreAttachment> cqM = cb.createQuery(clazz);
		Root<CoreAttachment> root = cqM.from(clazz);

		CriteriaQuery<CoreAttachment> subCriteriaQuery = cb.createQuery(clazz);
		Subquery<CoreAttachment> subquery = subCriteriaQuery.subquery(clazz);
		Root<CoreAttachment> subRoot = subquery.from(clazz);
		subquery.select(subRoot.get(CoreAttachment_.parent));

		subquery.where(cb.equal(subRoot.get(CoreAttachment_.id), attachmentId));

		Predicate restrictions = cb.or(
						cb.equal(root.get(CoreAttachment_.parent).get(CoreAttachment_.id), attachmentId),
						cb.equal(root.get(CoreAttachment_.id), attachmentId),
						cb.equal(root.get(CoreAttachment_.parent), subquery),
						cb.equal(root.get(CoreAttachment_.id), subquery)
		);
		cqM.orderBy(cb.desc(root.get(CoreAttachment_.versionNumber)));
		cqM.where(restrictions);
		return entityManager.createQuery(cqM).getResultList();
	}
}
