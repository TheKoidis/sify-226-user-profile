/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.ecm.model.entity;

/**
 * Object with attachment
 * 
 * @author Svanda
 */
public interface Attachable {
	
	public static final String DEFAULT_ENCODING = "UTF-8";
	
	/**
	 * Type of object
	 * @return 
	 */
	String getObjectType();
	
	/**
	 * Identifier of object
	 * @return 
	 */
	String getObjectIdentifier();
}
