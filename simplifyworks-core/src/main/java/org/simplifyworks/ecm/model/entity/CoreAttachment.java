
package org.simplifyworks.ecm.model.entity;

import org.simplifyworks.core.model.entity.AbstractEntity;

import java.io.InputStream;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 	Metadata for attachment
 *  @author Svanda
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class CoreAttachment extends AbstractEntity implements Attachable {

	@NotNull
	@Size(min = 1, max = 128)
	@Basic(optional = false)
	@Column(name = "OBJECT_TYPE", nullable = false, length = 128)
	private String objectType;
	@NotNull
	@Size(min = 1, max = 36)
	@Basic(optional = false)
	@Column(name = "OBJECT_IDENTIFIER", nullable = false, length = 36)
	private String objectIdentifier;
	@NotNull
	@Size(min = 1, max = 36)
	@Basic(optional = false)
	@Column(name = "CONTENT_GUID", nullable = false, length = 36)
	private String contentGuid;
	@NotNull
	@Size(min = 1, max = 255)
	@Basic(optional = false)
	@Column(name = "NAME", nullable = false, length = 255)
	private String name;
	@Size(max = 512)
	@Column(name = "DESCRIPTION", length = 512)
	private String description;
	@NotNull
	@Size(min = 1, max = 255)
	@Basic(optional = false)
	@Column(name = "MIMETYPE", nullable = false, length = 255)
	private String mimetype;
	@NotNull
	@Size(min = 1, max = 100)
	@Basic(optional = false)
	@Column(name = "ENCODING", nullable = false, length = 100)
	private String encoding;
	@NotNull
	@Basic(optional = false)
	@Column(name = "FILESIZE", nullable = false, precision = 18, scale = 0)
	@Max(999999999999999999l)
	private Long filesize;
	@Size(max = 512)
	@Column(name = "CONTENT_PATH", length = 512)
	private String contentPath;
	@Size(max = 50)
	@Column(name = "ATTACHMENT_TYPE", length = 50)
	private String attachmentType;
	@Size(max = 50)
	@Column(name = "OBJECT_STATUS", length = 50)
	private String objectStatus;
	@NotNull
	@Basic(optional = false)
	@Column(name = "VERSION_NUMBER", nullable = false, precision = 10, scale = 0)
	@Max(9999)
	@Min(1)
	private Integer versionNumber;
	@NotNull
	@Size(min = 1, max = 10)
	@Basic(optional = false)
	@Column(name = "VERSION_LABEL", nullable = false, length = 10)
	private String versionLabel;
	@ManyToOne
	@JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
	private CoreAttachment parent;
	@ManyToOne
	@JoinColumn(name = "NEXT_VERSION_ID", referencedColumnName = "ID")
	private CoreAttachment nextVersion;
	//
	private transient InputStream inputData; // for save binary date

	public CoreAttachment() {
	}

	public CoreAttachment(Long id){
		super(id);
	}

	public CoreAttachment(Attachable attachableObject) {
		objectType = attachableObject.getObjectType();
		objectIdentifier = attachableObject.getObjectIdentifier();
	}

	public void setEntity(Attachable attachableObject) {
		objectType = attachableObject.getObjectType();
		objectIdentifier = attachableObject.getObjectIdentifier();
	}

	@Override
	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	@Override
	public String getObjectIdentifier() {
		return objectIdentifier;
	}

	public void setObjectIdentifier(String objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	public String getContentGuid() {
		return contentGuid;
	}

	public void setContentGuid(String contentGuid) {
		this.contentGuid = contentGuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMimetype() {
		return mimetype;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public Long getFilesize() {
		return filesize;
	}

	public void setFilesize(Long filesize) {
		this.filesize = filesize;
	}

	public String getContentPath() {
		return contentPath;
	}

	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}

	/**
	 * Stringova reprezentace klice objektu
	 *
	 * @return
	 */
	public String getObjectKey() {
		return getObjectType() + ":" + getObjectIdentifier();
	}

	public void setInputData(InputStream inputData) {
		this.inputData = inputData;
	}

	public InputStream getInputData() {
		return inputData;
	}

	public String getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getObjectStatus() {
		return objectStatus;
	}

	public void setObjectStatus(String objectStatus) {
		this.objectStatus = objectStatus;
	}

	public Integer getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(Integer versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getVersionLabel() {
		return versionLabel;
	}

	public void setVersionLabel(String versionLabel) {
		this.versionLabel = versionLabel;
	}

	public CoreAttachment getParent() {
		return parent;
	}

	public void setParent(CoreAttachment parent) {
		this.parent = parent;
	}

	public CoreAttachment getNextVersion() {
		return nextVersion;
	}

	public void setNextVersion(CoreAttachment nextVersion) {
		this.nextVersion = nextVersion;
	}
}
