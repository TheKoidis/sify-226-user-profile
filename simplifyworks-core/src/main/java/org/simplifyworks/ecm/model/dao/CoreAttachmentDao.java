package org.simplifyworks.ecm.model.dao;

import org.simplifyworks.core.model.dao.AnyTypeDao;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.uam.model.entity.CoreOrganization;

import java.util.List;

public interface CoreAttachmentDao extends AnyTypeDao<CoreAttachment> {

    List<CoreAttachment> getAttachments(String objectType, String objectIdentifier, String objectStatus, String attachmentType);

    CoreAttachment getAttachment(String contentGuid);

    CoreAttachment getAttachmentByName(String objectType, String objectIdentifier, String name);

    List<CoreAttachment> getAttachmentVersions(Long attachmentId);
}
