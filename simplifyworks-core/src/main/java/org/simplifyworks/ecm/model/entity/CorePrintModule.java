/**
 * 
 */
package org.simplifyworks.ecm.model.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.ecm.domain.ReportType;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Entity
public class CorePrintModule extends AbstractEntity {

	@NotNull
	@Size(min = 1, max = 255)
	@Basic(optional = false)
	@Column(name = "NAME", nullable = false, length = 255)	
	private String name;
	
	@NotNull
	@Size(min = 1, max = 255)
	@Basic(optional = false)
	@Column(name = "TEMPLATE", nullable = false, length = 255)
	private String template;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Basic(optional = false)
	@Column(name = "TYPE", nullable = false, length = 255)
	private ReportType type;
	
	@NotNull
	@Size(min = 1, max = 128)
	@Basic(optional = false)
	@Column(name = "OBJECT_TYPE", nullable = false, length = 128)
	private String objectType;
	
	@Size(min = 1, max = 255)
	@Basic(optional = true)
	@Column(name = "WF_STATE", nullable = true, length = 255)
	private String wfState;
	
	public CorePrintModule() {
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public ReportType getType() {
		return type;
	}
	public void setType(ReportType type) {
		this.type = type;
	}
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getWfState() {
		return wfState;
	}
	public void setWfState(String wfState) {
		this.wfState = wfState;
	}
}
