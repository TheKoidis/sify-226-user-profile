/**
 * 
 */
package org.simplifyworks.ecm.service.impl;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.Base64;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.simplifyworks.ecm.service.ReportManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

/**
 * Standalone implementation of {@link ReportManager} which uses JasperServer.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class StandaloneJasperReportManager extends AbstractJasperReportManager implements ReportManager {
	
	protected static final Logger LOG = LoggerFactory.getLogger(StandaloneJasperReportManager.class);
	
	/**
	 * JasperServer reports path constant
	 */
	private final String REPORTS_PATH = "/rest_v2/reports/reports/";	
	
	private String url;
	private String username;
	private String password;
		
	private RestOperations rest = new RestTemplate();
	
	@Override
	public Report generateReport(String name, ReportType type, URI dataUri) {
		try {
			if(LOG.isDebugEnabled()) {
				LOG.debug("Generating report '{}.{}' from '{}'", name, type, dataUri);
			}
			
			File reportFile = createFile(reportBaseDir, REPORT_FILE_PREFIX, name, type.getSuffix());
			byte[] report = rest.exchange(createUrl(name, type, dataUri), HttpMethod.GET, createRequest(), byte[].class).getBody();
			
			Files.write(reportFile.toPath(), report);
			
			return new Report(name, type, dataUri, reportFile.toURI(), true);
		} catch (Exception e) {
			throw new CoreException(e);
		}
	}
	
	/**
	 * Creates URL for report REST requests.
	 * 
	 * @param name name of report
	 * @param type type of report
	 * @param dataUri URI with data for report in XML
	 * @return URL for JasperServer request
	 */
	private String createUrl(String name, ReportType type, URI dataUri) {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(url).append(REPORTS_PATH);
		buffer.append(name).append(".").append(type.getSuffix());
		buffer.append("?").append(XML_DATA_PARAM_NAME).append("=").append(dataUri.toString());
		
		return buffer.toString();
	}
	
	/**
	 * Creates HTTP request with authorization header.
	 * 
	 * @return HTTP request with authorization header
	 */
	private HttpEntity<String> createRequest() {
		String credentials = MessageFormat.format("{0}:{1}", username, password);

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.AUTHORIZATION, "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes()));
		
		return new HttpEntity<String>(headers);
	}

	// configuration properties setters
	
	public void setUrl(String url) {
		this.url = url;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}