package org.simplifyworks.ecm.service.impl;

import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.ecm.model.dto.CorePrintModuleDto;
import org.simplifyworks.ecm.model.entity.CorePrintModule;
import org.simplifyworks.ecm.service.PrintModuleManager;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link PrintModuleManager}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class PrintModuleManagerImpl extends DefaultReadWriteManager<CorePrintModuleDto, CorePrintModule> implements PrintModuleManager {
}
