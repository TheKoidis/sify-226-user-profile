package org.simplifyworks.ecm.service.impl;

import java.io.InputStream;
import java.util.List;

import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.ecm.service.CoreAttachmentManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Save attachment on FS
 *
 * @author Svanda
 */
@Service
@Transactional
public class CoreAttachmentManagerImpl extends DefaultAttachmentManager<CoreAttachmentDto, CoreAttachment> implements CoreAttachmentManager {

	@Override
	public void remove(long entityId) {
		removeAttachment(entityId);

	}

	@Override
	public CoreAttachmentDto search(long entityId) {
		CoreAttachmentDto coreAttachmentDto = super.search(entityId);
		//for dev and presetantion only
		List<CoreAttachmentDto> coreAttachmentDtos = getAttachments("Attachment", String.valueOf(coreAttachmentDto.getId()));
		return coreAttachmentDto;
	}

	@Override
	@Transactional
	public CoreAttachmentDto update(CoreAttachmentDto attachmentDto) {
		if(attachmentDto != null){
			if(attachmentDto.getId() == null){
				InputStream tempFileData = getTempFileData(attachmentDto);
				attachmentDto.setInputData(tempFileData);
				attachmentDto = saveAttachment(attachmentDto);
			}else{
				attachmentDto = super.update(attachmentDto);
			}
		}
		return attachmentDto;
	}

	@Override
	public boolean checkSuffix(String suffix, String[] suffixes) {
		boolean allowed = true;
		if (suffixes != null && suffixes.length != 0) {
			allowed = false;
			for (String s : suffixes) {
				if (s.equals(suffix)) {
					allowed = true;
					break;
				}
			}
		}
		return allowed;
	}

}
