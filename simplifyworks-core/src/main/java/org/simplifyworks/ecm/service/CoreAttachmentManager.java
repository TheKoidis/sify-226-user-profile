package org.simplifyworks.ecm.service;


import java.io.InputStream;
import java.util.List;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.entity.Attachable;
import org.simplifyworks.ecm.model.entity.CoreAttachment;

/**
 * @author Svanda
 *         Interface for attachment
 */
public interface CoreAttachmentManager extends AttachmentManager<CoreAttachmentDto, CoreAttachment> {
	public boolean checkSuffix(String suffix, String[] suffixes);
}
