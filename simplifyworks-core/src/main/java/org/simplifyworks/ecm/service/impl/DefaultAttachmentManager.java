package org.simplifyworks.ecm.service.impl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.common.collect.Lists;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.entity.Attachable;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.ecm.model.entity.CoreAttachment_;
import org.simplifyworks.ecm.service.AttachmentManager;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.io.*;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;

/**
 * Created by Sucharda on 27.8.2015.
 *
 * @param <T>
 * @param <E>
 */
@Transactional
public abstract class DefaultAttachmentManager<T extends CoreAttachmentDto, E extends CoreAttachment> extends DefaultReadWriteManager<T, E> implements AttachmentManager<T, E> {

	private static final transient Logger logger = LoggerFactory.getLogger(CoreAttachmentManagerImpl.class);
	private String storagePath = System.getProperty("java.io.tmpdir");
	private boolean showWarning = true;

	@Override
	@Transactional
	public T saveAttachment(T attachment) {
		Assert.notNull(attachment, "Insert attachment");
		Assert.notNull(attachment.getInputData(), "Insert binary data");
		if (attachment.getId() != null) {
			return updateAttachment(attachment);
		}
		if (attachment.getEncoding() == null) {
			attachment.setEncoding(Attachable.DEFAULT_ENCODING);
		}
		if (attachment.getVersionNumber() == null) {
			attachment.setVersionNumber(1);
		}
		if (StringUtils.isEmpty(attachment.getVersionLabel())) {
			attachment.setVersionLabel(attachment.getVersionNumber() + ".0");
		}
		File targetFile = null;
		try {
            // save on FS
			//
			// generate guid
			attachment.setContentGuid(UUID.randomUUID().toString());
			// save file
			targetFile = saveFile(attachment, attachment.getInputData());

			return create(attachment);
		} catch (Exception ex) {
			FileUtils.deleteQuietly(targetFile);
			throw new CoreException("Failed attaching file [" + attachment.getName() + "] to object [" + attachment.getObjectType() + "]", ex);
		} finally {
			IOUtils.closeQuietly(attachment.getInputData());
		}
	}

	@Override
	public void removeAttachment(Long attachementId) {
		List<T> versions = getAttachmentVersions(attachementId);
		for (T attachment : versions) {
			super.remove(attachment.getId());
		}
		// remove data from FS in second cycle after end of delete from DB.
		for (T attachment : versions) {
			deleteFile(attachment.getContentPath());
		}
	}

	@Override
	@Transactional
	public void removeAttachments(List<T> attachments) {
		if (attachments == null) {
			return;
		}
		for (T attachment : attachments) {
			removeAttachment(attachment.getId());
		}
	}

	@Override
	public T updateAttachment(T attachment) {
		Assert.notNull(attachment, "Insert attachment");
		Assert.notNull(attachment.getId(), "Insert saved attachment");
		if (attachment.getEncoding() == null) {
			attachment.setEncoding(Attachable.DEFAULT_ENCODING);
		}
		File targetFile = null;
		try {

			String previousPath = null;
			if (attachment.getInputData() != null) {
				previousPath = attachment.getContentPath();
				targetFile = saveFile(attachment, attachment.getInputData());
			}
			attachment = update(attachment);
			if (previousPath != null) {
				deleteFile(previousPath);
			}
		} catch (Exception ex) {
			FileUtils.deleteQuietly(targetFile);
			throw new CoreException("Failed modification of file [" + attachment.getName() + "] to object [" + attachment.getObjectType() + "]", ex);
		} finally {
			IOUtils.closeQuietly(attachment.getInputData());
		}
		return attachment;
	}

	/**
	 * Save new version of attachment
	 *
	 * @param attachment
	 * @param previousVersion if not use, then is searching attachment for same
	 * object by name
	 * @return
	 */
	@Override
	public T saveAttachment(T attachment, T previousVersion) {
		if (previousVersion == null) {
			previousVersion = getAttachmentByName(attachment.getObjectType(), attachment.getObjectIdentifier(), attachment.getName());
		} else {
			// TODO: Check version - return last from previousVersion in attribute
		}
		if (previousVersion == null) {
			return saveAttachment(attachment);
		}
		attachment.setVersionNumber(previousVersion.getVersionNumber() + 1);
		attachment.setVersionLabel(attachment.getVersionNumber() + ".0"); // TODO: work with minor versions
		if (previousVersion.getParent() != null) {
			attachment.setParent(previousVersion.getParent());
		} else {
			attachment.setParent(previousVersion);
		}
		attachment = saveAttachment(attachment);
		previousVersion.setNextVersion(attachment);
		update(previousVersion);
		return attachment;
	}

	@Override
	public T saveTempFile(T attachment, InputStream in) throws Exception {
		if (in == null) {
			return null;
		}

		FileOutputStream fileOutputStream = null;
		File attachmentTempFile = null;
		try {
			attachmentTempFile = File.createTempFile("ATTACHMENT", null);
			fileOutputStream = new FileOutputStream(attachmentTempFile);
			IOUtils.copy(in, fileOutputStream);

			attachment.setContentPath(attachmentTempFile.getPath());
			attachment.setFilesize(attachmentTempFile.length());
			return attachment;
		} catch (Exception ex) {
			FileUtils.deleteQuietly(attachmentTempFile);
			throw ex;
		} finally {
			attachment.setInputData(null);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(fileOutputStream);
		}
	}

	@Override
	public void remove(long id) {
		removeAttachment(id);
	}

	@Override
	@Transactional(readOnly = true)
	public T getAttachment(Long attachmentId) {
		return search(attachmentId);
	}

	private File saveFile(T attachment, InputStream in) throws Exception {
		if (showWarning && storagePath.equals(System.getProperty("java.io.tmpdir"))) {
			showWarning = false;
			logger.warn("Attachments are saved under java.io.tmpdir [" + storagePath + "]. ");

		}
		File targetFile = null;
		FileOutputStream os = null;
		try {
			// create path
			Calendar calendar = Calendar.getInstance();
			String path = "/" + calendar.get(Calendar.YEAR)
							+ "/" + (calendar.get(Calendar.MONTH) + 1)
							+ "/" + calendar.get(Calendar.DATE);
			File directory = new File(storagePath + path);
			if (!directory.exists()) {
				directory.mkdirs();
			}
			// file not has same guid as on FS - guid attachment is not change, will be create new version
			attachment.setContentPath(path + "/" + UUID.randomUUID().toString() + ".bin");
			// save binary data
			targetFile = new File(storagePath + attachment.getContentPath());
			os = new FileOutputStream(targetFile);
			IOUtils.copy(in, os);
			attachment.setFilesize(targetFile.length());
			return targetFile;
		} catch (Exception ex) {
			FileUtils.deleteQuietly(targetFile);
			throw ex;
		} finally {
			attachment.setInputData(null);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(os);
		}
	}

	@Override
	public InputStream getTempFileData(T attachment) {
		Assert.notNull(attachment, "Insert DTO temp attachment");
		try {
			return new FileInputStream(attachment.getContentPath());
		} catch (FileNotFoundException ex) {
			throw new CoreException("Binary data for attachment [" + attachment.getName() + "] - [" + attachment.getContentPath() + "] not found");
		}
	}

	@Override
	@Transactional
	public T update(T attachmentDto) {
		if (attachmentDto != null) {
			if (attachmentDto.getId() == null) {
				InputStream tempFileData = getTempFileData(attachmentDto);
				attachmentDto.setInputData(tempFileData);
				attachmentDto = saveAttachment(attachmentDto);
			} else {
				attachmentDto = super.update(attachmentDto);
			}
		}
		return attachmentDto;
	}

	@Override
	public InputStream writeStreamToTemp(InputStream inputStream) {
		if (inputStream == null) {
			return null;
		}

		FileOutputStream fileOutputStream = null;
		File attachmentTempFile = null;
		try {
			attachmentTempFile = File.createTempFile("ATTACHMENT", null);
			fileOutputStream = new FileOutputStream(attachmentTempFile);
			IOUtils.copy(inputStream, fileOutputStream);
			return new FileInputStream(attachmentTempFile);
		} catch (IOException ex) {
			logger.error("Cannot store attachment data in temp directory " + storagePath, ex);
		} finally {
			IOUtils.closeQuietly(inputStream);
			IOUtils.closeQuietly(fileOutputStream);
		}

		return null;
	}

	@Override
	public T getAttachment(String contentGuid) {
		SearchParameters sp = new SearchParameters();
		FilterValue fvContentGuid = new FilterValue();
		fvContentGuid.setPropertyName(CoreAttachment_.contentGuid.getName());
		fvContentGuid.setValue(contentGuid);
		fvContentGuid.setOperator(FilterOperator.EQUALS);
		sp.addFilter(fvContentGuid);

		List<T> result = search(sp);
		if (result == null || result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}

	@Transactional(readOnly = true)
	public List<T> getAttachments(String objectType, String objectIdentifier, String objectStatus) {
		return getAttachments(objectType, objectIdentifier, objectStatus, null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> getAttachmentsByType(Attachable entity, String attchmentType) {
		return getAttachments(entity.getObjectType(), entity.getObjectIdentifier(), null, attchmentType);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> getAttachments(String objectType, String objectIdentifier) {
		return getAttachments(objectType, objectIdentifier, null, null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> getAttachments(Attachable entity) {
		return getAttachments(entity.getObjectType(), entity.getObjectIdentifier());
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> getAttachments(Attachable entity, String objectStatus) {
		return getAttachments(entity.getObjectType(), entity.getObjectIdentifier(), objectStatus, null);
	}

	/**
	 * Return attachments in current version
	 *
	 * @param objectType
	 * @param objectIdentifier
	 * @param objectStatus
	 * @param attachmentType
	 * @return
	 */
	@Override
	@Transactional
	public List<T> getAttachments(String objectType, String objectIdentifier, String objectStatus, String attachmentType) {
		Assert.notNull(objectType, "Insert type of object");
		Assert.notNull(objectIdentifier, "Insert id of object");

		SearchParameters sp = new SearchParameters();
		FilterValue fvObjectType = new FilterValue();
		fvObjectType.setPropertyName(CoreAttachment_.objectType.getName());
		fvObjectType.setValue(objectType);
		fvObjectType.setOperator(FilterOperator.EQUALS);
		sp.addFilter(fvObjectType);

		FilterValue fvObjId = new FilterValue();
		fvObjId.setPropertyName(CoreAttachment_.objectIdentifier.getName());
		fvObjId.setValue(objectIdentifier);
		fvObjId.setOperator(FilterOperator.EQUALS);
		sp.addFilter(fvObjId);

		FilterValue fvVersion = new FilterValue();
		fvVersion.setPropertyName(CoreAttachment_.nextVersion.getName());
		fvVersion.setOperator(FilterOperator.IS_NULL);
		sp.addFilter(fvVersion);

		if (StringUtils.isNotEmpty(objectStatus)) {
			FilterValue fvStatus = new FilterValue();
			fvStatus.setPropertyName(CoreAttachment_.objectStatus.getName());
			fvStatus.setValue(objectStatus);
			fvStatus.setOperator(FilterOperator.EQUALS);
			sp.addFilter(fvStatus);
		}
		if (StringUtils.isNotEmpty(attachmentType)) {
			FilterValue fvAttachType = new FilterValue();
			fvAttachType.setPropertyName(CoreAttachment_.attachmentType.getName());
			fvAttachType.setValue(attachmentType);
			fvAttachType.setOperator(FilterOperator.EQUALS);
			sp.addFilter(fvAttachType);
		}
		return search(sp);
	}

	private List<T> getAttachmentVersions(Long attachmentId) {
		List<T> attachments = Lists.newArrayList();
		T search = search(attachmentId);
		attachments.add(search);
		return attachments;
	}

	private void deleteFile(String path) {
		if (StringUtils.isBlank(path)) {
			return; // nothing to remove
		}
		// remove data from FS
		File targetFile = new File(storagePath + path);
		FileUtils.deleteQuietly(targetFile);
	}

	private T getAttachmentByName(String objectType, String objectIdentifier, String name) {
		Assert.notNull(objectType, "Insert type of object");
		Assert.notNull(objectIdentifier, "Insert ID of object");
		Assert.notNull(name, "Insert name of attachment");

		SearchParameters sp = new SearchParameters();
		FilterValue fvObjectType = new FilterValue();
		fvObjectType.setPropertyName(CoreAttachment_.objectType.getName());
		fvObjectType.setValue(objectType);
		fvObjectType.setOperator(FilterOperator.EQUALS);
		sp.addFilter(fvObjectType);
		FilterValue fvObjectId = new FilterValue();
		fvObjectId.setPropertyName(CoreAttachment_.objectIdentifier.getName());
		fvObjectId.setValue(objectIdentifier);
		fvObjectId.setOperator(FilterOperator.EQUALS);
		sp.addFilter(fvObjectId);
		FilterValue fvName = new FilterValue();
		fvName.setPropertyName(CoreAttachment_.name.getName());
		fvName.setValue(name);
		fvName.setOperator(FilterOperator.EQUALS);
		sp.addFilter(fvName);
		FilterValue fvVersion = new FilterValue();
		fvVersion.setPropertyName(CoreAttachment_.nextVersion.getName());
		fvVersion.setOperator(FilterOperator.IS_NULL);
		sp.addFilter(fvVersion);

		List<T> result = search(sp);
		if (result == null || result.isEmpty()) {
			return null;
		}
		return result.get(0);

	}

	@Override
	@Transactional(readOnly = true)
	public InputStream getAttachmentData(Long attachementId) {
		return getAttachmentData(getAttachment(attachementId));
	}

	@Override
	@Transactional(readOnly = true)
	public InputStream getAttachmentData(String contentGuid) {
		return getAttachmentData(getAttachment(contentGuid));
	}

	private InputStream getAttachmentData(T attachment) {
		Assert.notNull(attachment, "Insert attachment");
		try {
			return new FileInputStream(storagePath + attachment.getContentPath());
		} catch (FileNotFoundException ex) {
			throw new CoreException("Binary data for attachment [" + attachment.getId() + ":" + attachment.getName() + "] - [" + attachment.getContentPath() + "] not found.");
		}
	}

	/**
	 * Path to binary data to FS
	 *
	 * @param storagePath
	 */
	public void setStoragePath(String storagePath) {
		this.storagePath = storagePath;
	}
}
