package org.simplifyworks.ecm.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.ecm.model.dto.CorePrintModuleDto;
import org.simplifyworks.ecm.model.entity.CorePrintModule;

/**
 * Interface for print module manager implementations
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface PrintModuleManager extends ReadWriteManager<CorePrintModuleDto, CorePrintModule>{

}
