/**
 * 
 */
package org.simplifyworks.ecm.service;

import java.net.URI;

import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;

/**
 * Interface for report manager implementations.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface ReportManager {
	
	/**
	 * Generates report using data object.
	 * 
	 * @param name name of report
	 * @param type type of report (pdf, html etc.)
	 * @param data object containing data for report
	 * @return container with report information (contains name, type, data URI, report URI)
	 */
	public Report generateReport(String name, ReportType type, Object data);
	
	/**
	 * Generates report using data file.
	 * 
	 * @param name name of report
	 * @param type type of report (pdf, html etc.)
	 * @param dataUri URI with data for report in XML
	 * @return container with report information (contains name, type, data URI, report URI)
	 */
	public Report generateReport(String name, ReportType type, URI dataUri);
}
