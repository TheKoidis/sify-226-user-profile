package org.simplifyworks.ecm.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.entity.Attachable;
import org.simplifyworks.ecm.model.entity.CoreAttachment;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Sucharda on 27.8.2015.
 */
public interface AttachmentManager <T extends CoreAttachmentDto, E extends CoreAttachment>  extends ReadWriteManager<T,E> {
    /**
     * Remove attachment
     *
     * @param attachementId
     */
    void removeAttachment(Long attachementId);

    /**
     * Save attachment
     *
     * @param attachment
     */
    T saveAttachment(T attachment);

    /**
     * Update attachment
     *
     * @param attachment
     */
    T updateAttachment (T attachment);

    /**
     * Persist new version of attachment
     *
     * @param attachment
     * @param previousVersion - if not use, then is searching attachment for same object by name
     * @return
     */
    T saveAttachment(T attachment, T previousVersion);

    T saveTempFile(T attachment, InputStream in) throws Exception;

    InputStream getTempFileData(T attachment);

    /**
     * Persist input stream to tmp file and return reference on him.
     *
     * @param inputStream
     * @return
     */
    public InputStream writeStreamToTemp(InputStream inputStream);


    /**
     * Load attachment
     *
     * @param contentGuid
     * @return
     */
    T getAttachment(String contentGuid);

    /**
     * Load attachment
     *
     * @param attachmentId
     * @return
     */
    T getAttachment(Long attachmentId);

     /**
     * Attachments to last version
     *
     * @param entity
     * @return
     */
    List<T> getAttachments(Attachable entity);

    /**
     * Attachments to last version
     *
     * @param objectType
     * @param objectIdentifier
     * @return
     */
    List<T> getAttachments(String objectType, String objectIdentifier);

    /**
     * Attachments to last version
     *
     * @param entity
     * @param objectStatus
     * @return
     */
    List<T> getAttachments(Attachable entity, String objectStatus);

    /**
     * Attachments to last version
     *
     * @param objectType
     * @param objectIdentifier
     * @param objectStatus
     * @param attachmentType
     * @return
     */
    List<T> getAttachments(String objectType, String objectIdentifier, String objectStatus, String attachmentType);


    /**
     * Binary data of attachment
     *
     * @param attachementId
     * @return
     */
    InputStream getAttachmentData(Long attachementId);

    /**
     * Load attachments by type
     *
     * @param entity
     * @param attchmentType
     * @return
     */
    List<T> getAttachmentsByType(Attachable entity, String attchmentType);

    /**
     * Binary data of attachment
     *
     * @param contentGuid GUID attachment
     * @return
     */
    InputStream getAttachmentData(String contentGuid);


    /**
     * Remove attachments
     *
     * @param list
     */
    void removeAttachments(List<T> list);
}
