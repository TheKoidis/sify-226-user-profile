/**
 * 
 */
package org.simplifyworks.ecm.service.config;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.service.ReportManager;
import org.simplifyworks.ecm.service.impl.EmbeddedJasperReportManager;
import org.simplifyworks.ecm.service.impl.StandaloneJasperReportManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JasperReport configuration which choose implementation (standalone or embedded) using application properties.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Configuration
public class JasperReportConfig {
	
	protected static final Logger LOG = LoggerFactory.getLogger(JasperReportConfig.class);

	/**
	 * Standalone manager type
	 */
	public static final String STANDALONE = "standalone";
	
	/**
	 * Embedded manager type
	 */
	public static final String EMBEDDED = "embedded";
	
	@Value("${jasper.type:embedded}")
	private String type;
	
	@Value("${jasper.url:http://localhost:8081/jasperserver}")
	private String url;
	
	@Value("${jasper.username:jasperadmin}")
	private String username;
	
	@Value("${jasper.password:jasperadmin}")
	private String password;
	
	@Value("${jasper.dataBaseDir:jasper}")
	private String dataBaseDir;
	
	@Value("${jasper.reportBaseDir:jasper}")
	private String reportBaseDir;
	
	@Value("${jasper.templateBaseDir:jasper}")
	private String templateBaseDir;
	
	@Bean
	public ReportManager prepareManager() {
		if(LOG.isInfoEnabled()) {
			LOG.info("Preparing manager of type '{}'", type);
		}
		
		switch(type) {
			case STANDALONE:
				return createStandalone();
			case EMBEDDED:
				return createEmbedded();
			default:
				throw new CoreException("Cannot create JasperReport manager of type " + type);
		}
	}
	
	private ReportManager createStandalone() {
		StandaloneJasperReportManager manager = new StandaloneJasperReportManager();
		
		manager.setUrl(url);
		manager.setUsername(username);
		manager.setPassword(password);
		
		manager.setDataBaseDir(dataBaseDir);
		manager.setReportBaseDir(reportBaseDir);
		
		if(LOG.isInfoEnabled()) {
			LOG.info("StandaloneJasperReportManager with parameters [url: {}, username: {}, dataBaseDir: {}, reportBaseDir: {}] created",
					url, username, dataBaseDir, reportBaseDir);
		}
		
		return manager;
	}
	
	private ReportManager createEmbedded() {
		EmbeddedJasperReportManager manager = new EmbeddedJasperReportManager();
		
		manager.setTemplateBaseDir(templateBaseDir);
		
		manager.setDataBaseDir(dataBaseDir);
		manager.setReportBaseDir(reportBaseDir);
		
		if(LOG.isInfoEnabled()) {
			LOG.info("EmbeddedJasperReportManager with parameters [templateBaseDir: {}, dataBaseDir: {}, reportBaseDir: {}] created",
					templateBaseDir, dataBaseDir, reportBaseDir);
		}
		
		return manager;
	}
}
