package org.simplifyworks.ecm.web.controller.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Base controller for report generating and downloading.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class AbstractReportController {

	/**
	 * Name parameter name
	 */
	public static final String NAME_PARAMETER = "name";
	
	/**
	 * Template parameter name
	 */
	public static final String TEMPLATE_PARAMETER = "template";
	
	/**
	 * Type parameter name
	 */
	public static final String TYPE_PARAMETER = "type";
	
	/**
	 * IDs parameter name
	 */
	public static final String IDS_PARAMETER = "ids";
	
	/**
	 * IDs parameter separator
	 */
	public static final String IDS_SEPARATOR = ",|;";
	
	/**
	 * Generates report using request parameters.
	 * 
	 * @param request request with parameters (must contains report name, type and may contains ids)
	 * @return generated report
	 * @throws IOException if something with IO is broken
	 */
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> generate(HttpServletRequest request) throws IOException {
		String name = request.getParameter(NAME_PARAMETER);
		String template = request.getParameter(TEMPLATE_PARAMETER);
		ReportType type = ReportType.valueOf(request.getParameter(TYPE_PARAMETER).toUpperCase());
		List<Long> ids = new ArrayList<>();
		
		if(request.getParameter(IDS_PARAMETER) != null) {
			for(String id : request.getParameter(IDS_PARAMETER).split(IDS_SEPARATOR)) {
				ids.add(Long.parseLong(id));
			}
		}

		Report report = generateReport(template, type, ids);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType(type.getMimeType()));
		headers.setContentLength(new File(report.getReportUri()).length());
		headers.setContentDispositionFormData(name, name + "." + type.getSuffix());

		return new ResponseEntity<InputStreamResource>(
				new InputStreamResource(new FileInputStream(new File(report.getReportUri()))), headers, HttpStatus.OK);
	}
	
	/**
	 * Generates report using template name, type and ids
	 * 
	 * @param template name of template
	 * @param type type of template
	 * @param ids ids of records
	 * @return generated report
	 */
	protected abstract Report generateReport(String template, ReportType type, List<Long> ids);
}