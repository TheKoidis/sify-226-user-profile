package org.simplifyworks.ecm.web.controller.rest;

import java.net.URI;
import java.util.List;

import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.simplifyworks.ecm.service.ReportManager;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base report controller for custom data (your own XML resource)
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class CustomDataReportController extends AbstractReportController {

	@Autowired
	protected ReportManager reportManager;
	
	@Override
	protected Report generateReport(String template, ReportType type, List<Long> ids) {
		return reportManager.generateReport(template, type, prepareData(template, type, ids));
	}

	/**
	 * Prepares XML resource using ids.
	 * 
	 * @param template name of template
	 * @param type type of report output
	 * @param ids list of selected ids (may be empty, then ALL records must be used)
	 * @return URI of generated resource
	 */
	protected abstract URI prepareData(String template, ReportType type, List<Long> ids);
}
