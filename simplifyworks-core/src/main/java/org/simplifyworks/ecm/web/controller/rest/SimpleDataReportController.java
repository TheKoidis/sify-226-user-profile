package org.simplifyworks.ecm.web.controller.rest;

import java.util.List;

import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.simplifyworks.ecm.service.ReportManager;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base report controller for simple data (just create DTO using template name, type and ids)
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class SimpleDataReportController extends AbstractReportController {

	@Autowired
	protected ReportManager reportManager;
	
	@Override
	protected Report generateReport(String template, ReportType type, List<Long> ids) {
		return reportManager.generateReport(template, type, prepareData(template, type, ids));
	}
	
	/**
	 * Prepares data resource using ids.
	 * 
	 * @param template name of template
	 * @param type type of report output
	 * @param ids list of selected ids (may be empty, then ALL records must be used)
	 * @return data resource (i.e. DTO)
	 */
	protected abstract Object prepareData(String template, ReportType type, List<Long> ids);
}
