/**
 * 
 */
package org.simplifyworks.ecm.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.ecm.model.dto.CorePrintModuleDto;
import org.simplifyworks.ecm.model.entity.CorePrintModule;
import org.simplifyworks.ecm.service.PrintModuleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for print modules
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping(value = "/api/core/print-modules")
public class PrintModuleController extends BasicTableController<CorePrintModuleDto, CorePrintModule> {

	@Autowired
	private PrintModuleManager manager;
	
	@Override
	protected ReadWriteManager<CorePrintModuleDto, CorePrintModule> getManager() {
		return manager;
	}
}
