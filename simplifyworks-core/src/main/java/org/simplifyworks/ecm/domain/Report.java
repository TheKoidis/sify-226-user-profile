/**
 * 
 */
package org.simplifyworks.ecm.domain;

import java.io.File;
import java.net.URI;

/**
 * Report container
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class Report {

	private String name;
	private ReportType type;
	
	private URI dataUri;
	private URI reportUri;
	
	private boolean deleteOnFinalize;

	public Report(String name, ReportType type, URI dataUri, URI reportUri, boolean deleteOnFinalize) {
		this.name = name;
		this.type = type;
		this.dataUri = dataUri;
		this.reportUri = reportUri;
		this.deleteOnFinalize = deleteOnFinalize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ReportType getType() {
		return type;
	}

	public void setType(ReportType type) {
		this.type = type;
	}

	public URI getDataUri() {
		return dataUri;
	}
	
	public void setDataUri(URI dataUri) {
		this.dataUri = dataUri;
	}
	
	public URI getReportUri() {
		return reportUri;
	}
	
	public void setReportUri(URI reportUri) {
		this.reportUri = reportUri;
	}

	public boolean isDeleteOnFinalize() {
		return deleteOnFinalize;
	}

	public void setDeleteOnFinalize(boolean deleteOnFinalize) {
		this.deleteOnFinalize = deleteOnFinalize;
	}
	
	@Override
	protected void finalize() throws Throwable {
		if(deleteOnFinalize) {
			new File(dataUri).delete();
			new File(reportUri).delete();
		}
		
		super.finalize();
	}
}
