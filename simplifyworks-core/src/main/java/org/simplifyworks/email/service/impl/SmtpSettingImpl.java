/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.service.impl;


import org.simplifyworks.email.service.SmtpSetting;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author svanda
 */
@Service
public class SmtpSettingImpl implements SmtpSetting {

    private String mailhostAppParamKey;
    private String mailportAppParamKey;
    private String usernameAppParamKey;
    private String passwordAppParamKey;
    private String replytoAppParamKey;
    private boolean useSSLParamKey;
    private String fromParamKey;

    public SmtpSettingImpl() {
    }


    @Override
    public String getMailhost() {
        return mailhostAppParamKey;
    }

    @Override
    public String getMailport() {
        return mailportAppParamKey;
    }

    @Override
    public String getUsername() {
        return usernameAppParamKey;
    }

    @Override
    public String getPassword() {
        return passwordAppParamKey;
    }

    @Override
    public String getReplyto() {
        return replytoAppParamKey;
    }

    @Override
    public boolean getUseSSL() {
        return useSSLParamKey;
    }

    @Override
    public String getFrom() {
        return fromParamKey;
    }


    @Override
    public void setMailhost(String mailhostAppParamKey) {
        this.mailhostAppParamKey = mailhostAppParamKey;
    }

    @Override
    public void setMailport(String mailportAppParamKey) {
        this.mailportAppParamKey = mailportAppParamKey;
    }

    @Override
    public void setUsername(String usernameAppParamKey) {
        this.usernameAppParamKey = usernameAppParamKey;
    }

    @Override
    public void setPassword(String passwordAppParamKey) {
        this.passwordAppParamKey = passwordAppParamKey;
    }

    @Override
    public void setReplyto(String replytoAppParamKey) {
        this.replytoAppParamKey = replytoAppParamKey;
    }

    @Override
    public void setUseSSL(boolean useSSLParamKey) {
        this.useSSLParamKey = useSSLParamKey;
    }

    @Override
    public void setFrom(String fromParamKey) {
        this.fromParamKey = fromParamKey;
    }
}
