/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.service.impl;


import org.simplifyworks.email.service.SmtpSetting;

/**
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class DefaultSmtpSetting implements SmtpSetting {



    private String mailhost;
    private String mailport;
    private String username;
    private String password;
    private String replyto;
    private boolean useSSL;
    private String from;

    @Override
    public String getMailhost() {
        return mailhost;
    }

    public void setMailhost(String mailhost) {
        this.mailhost = mailhost;
    }

    @Override
    public String getMailport() {
        return mailport;
    }

    public void setMailport(String mailport) {
        this.mailport = mailport;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getReplyto() {
        return replyto;
    }

    public void setReplyto(String replyto) {
        this.replyto = replyto;
    }

    public void setUseSSL(boolean useSSL) {
        this.useSSL = useSSL;
    }

    @Override
    public boolean getUseSSL() {
        return this.useSSL;
    }

    @Override
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
