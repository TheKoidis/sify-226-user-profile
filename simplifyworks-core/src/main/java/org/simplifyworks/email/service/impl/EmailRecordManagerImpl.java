/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.service.impl;

import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.ecm.service.CoreAttachmentManager;
import org.simplifyworks.email.model.dto.CoreEmailRecordDto;
import org.simplifyworks.email.model.entity.CoreEmailRecord;
import org.simplifyworks.email.service.EmailRecordManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Save email
 * <p>
 * Attention: Attachments are save only on create email
 *
 * @author snitila@ders.cz
 */
@Service("emailRecordManager")
@Transactional
public class EmailRecordManagerImpl extends DefaultReadWriteManager<CoreEmailRecordDto, CoreEmailRecord> implements EmailRecordManager {

	private static final transient Logger logger = LoggerFactory.getLogger(EmailRecordManagerImpl.class);
	//	@Autowired(required = true)
//	private EmailRecordDao emailRecordDao;
	@Autowired(required = false)
	private CoreAttachmentManager attachmentManager;
//
//	@Override
//	public EmailRecordDao getDao() {
//		return emailRecordDao;
//	}
//

	@Override
	public CoreEmailRecordDto update(CoreEmailRecordDto emailRecord) {
		boolean isNew = emailRecord.getId() == null ? true : false;
		// prilozeni priloh
//        if (isNew && !emailRecord.getAttachments().isEmpty()) {
//            if (attachmentManager == null) {
//                logger.warn("Nebylo nakonfigurovano ECM pro ukladani priloh odesilanych emailu. Doplnte prosim konfiguraci ECM.");
//            } else {
//                List<CoreAttachment> persistedAttachments = Lists.newArrayList(); // musime vratit citelne prilohy pro dalsi zpracovani
//                for (CoreAttachment attachment : emailRecord.getAttachments()) {
//                    attachment.setEntity(emailRecord);
//                    CoreAttachment persistedAttachment = attachmentManager.saveAttachment(attachment);
//                    persistedAttachment.setInputData(attachmentManager.getAttachmentData(persistedAttachment.getId()));
//                    persistedAttachments.add(persistedAttachment);
//                }
//                emailRecord.setAttachments(persistedAttachments);
//            }
//        }
		return super.update(emailRecord);
	}

}
