package org.simplifyworks.scheduler.model.domain;

import java.util.Date;

import org.quartz.SimpleTrigger;

/**
 * One time task trigger
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class OneTimeTaskTrigger extends AbstractTaskTrigger {

	private Date fireTime;
	
	public OneTimeTaskTrigger() {
	}
	
	/**
	 * Creates a new instance using trigger and state
	 * 
	 * @param trigger trigger
	 * @param state state
	 */
	public OneTimeTaskTrigger(SimpleTrigger trigger, TaskTriggerState state) {
		super(trigger, state);
		
		this.fireTime = trigger.getStartTime();
	}
	
	public Date getFireTime() {
		return fireTime;
	}
	
	public void setFireTime(Date fireTime) {
		this.fireTime = fireTime;
	}
}
