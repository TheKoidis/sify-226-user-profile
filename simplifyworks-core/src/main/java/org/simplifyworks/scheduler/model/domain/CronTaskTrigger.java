package org.simplifyworks.scheduler.model.domain;

import org.quartz.CronTrigger;

/**
 * Cron task trigger
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class CronTaskTrigger extends AbstractTaskTrigger {

	private String cron;
	
	public CronTaskTrigger() {
	}
	
	/**
	 * Creates a new instance using trigger and state
	 * 
	 * @param trigger trigger
	 * @param state state
	 */
	public CronTaskTrigger(CronTrigger trigger, TaskTriggerState state) {
		super(trigger, state);
		
		cron = trigger.getCronExpression();
	}
	
	public String getCron() {
		return cron;
	}
	
	public void setCron(String cron) {
		this.cron = cron;
	}
}
