package org.simplifyworks.scheduler.model.domain;

import org.quartz.Trigger.TriggerState;

/**
 * State of task trigger
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public enum TaskTriggerState {

	ACTIVE,
	PAUSED;
	
	/**
	 * Converts state of trigger to state of task trigger
	 * 
	 * @param state state of trigger
	 * @return task trigger state
	 */
	public static TaskTriggerState convert(TriggerState state) {
		return TriggerState.PAUSED.equals(state) ? PAUSED : ACTIVE; 
	}
}
