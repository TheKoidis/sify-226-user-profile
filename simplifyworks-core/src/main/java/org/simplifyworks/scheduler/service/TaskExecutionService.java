package org.simplifyworks.scheduler.service;

import org.quartz.Job;

/**
 * Interface for task services (this services will be automatically registered)
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface TaskExecutionService extends Job {
}
