package org.simplifyworks.scheduler.service;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.simplifyworks.scheduler.service.impl.AbstractTaskExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Test implementation
 * 
 * TODO remove
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class ByeService extends AbstractTaskExecutionService {

	@Autowired
	ApplicationContext ct;
	
	@Override
	public void executeWithSecurity(JobExecutionContext context) throws JobExecutionException {
		System.out.println("Bye! " + (ct != null ? "(Application context autowired)" : "(No application context)"));
	}
}
