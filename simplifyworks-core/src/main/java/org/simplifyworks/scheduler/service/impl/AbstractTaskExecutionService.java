package org.simplifyworks.scheduler.service.impl;

import java.util.Collection;
import java.util.Collections;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.simplifyworks.scheduler.service.TaskExecutionService;
import org.simplifyworks.security.domain.UserAuthentication;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Template for task execution services. This template establishes security context before own task invocation.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class AbstractTaskExecutionService implements TaskExecutionService {
	
	public static final String SCHEDULER_USERNAME = "scheduler";
	public static final Collection<GrantedAuthority> SCHEDULER_AUTHORITIES = Collections.singletonList(new RoleOrganizationGrantedAuthority("admin", null));
		
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(SCHEDULER_USERNAME, SCHEDULER_USERNAME, SCHEDULER_AUTHORITIES, null));
		
		executeWithSecurity(context);
	}
	
	public abstract void executeWithSecurity(JobExecutionContext context) throws JobExecutionException;
}
