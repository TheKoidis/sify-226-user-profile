package org.simplifyworks.scheduler.web.controller.rest;

import javax.servlet.http.HttpServletResponse;

import org.simplifyworks.scheduler.model.domain.AbstractTaskTrigger;
import org.simplifyworks.scheduler.model.domain.CronTaskTrigger;
import org.simplifyworks.scheduler.model.domain.OneTimeTaskTrigger;
import org.simplifyworks.scheduler.model.domain.Task;
import org.simplifyworks.scheduler.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller for scheduler
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping("/api/scheduler")
public class SchedulerController {

	@Autowired
	private SchedulerService schedulerService;
	
	/**
	 * Returns all tasks
	 * 
	 * @return all tasks
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/tasks")
	public Resources<Task> getAll() {
		return new Resources<>(schedulerService.getAllTasks());
	}
	
	/**
	 * Creates one time trigger for task
	 * 
	 * @param taskName name of task
	 * @param trigger trigger data
	 * @return trigger data containing name
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/tasks/{taskName}/triggers/one-time")
	public AbstractTaskTrigger addOneTimeTrigger(@PathVariable String taskName, @RequestBody OneTimeTaskTrigger trigger) {
		return schedulerService.addTrigger(taskName, trigger);
	}
	
	/**
	 * Creates cron trigger for task
	 * 
	 * @param taskName name of task
	 * @param trigger trigger data
	 * @return trigger data containing name
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/tasks/{taskName}/triggers/cron")
	public AbstractTaskTrigger addCronTrigger(@PathVariable String taskName, @RequestBody CronTaskTrigger trigger) {
		return schedulerService.addTrigger(taskName, trigger);
	}
		
	/**
	 * Removes trigger
	 * 
	 * @param taskName name of task
	 * @param triggerName name of trigger
	 * @param response HTTP response (for status sending)
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/tasks/{taskName}/triggers/{triggerName}")
	public void removeTrigger(@PathVariable String taskName, @PathVariable String triggerName, HttpServletResponse response) {
		schedulerService.removeTrigger(taskName, triggerName);
		
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}
	
	/**
	 * Pauses trigger
	 * 
	 * @param taskName name of task
	 * @param triggerName name of trigger
	 * @param response HTTP response (for status sending)
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/tasks/{taskName}/triggers/{triggerName}/pause")
	public void pauseTrigger(@PathVariable String taskName, @PathVariable String triggerName, HttpServletResponse response) {
		schedulerService.pauseTrigger(taskName, triggerName);
		
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}
	
	/**
	 * Resumes trigger
	 * 
	 * @param taskName name of task
	 * @param triggerName name of trigger
	 * @param response HTTP response (for status sending)
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/tasks/{taskName}/triggers/{triggerName}/resume")
	public void resumeTrigger(@PathVariable String taskName, @PathVariable String triggerName, HttpServletResponse response) {
		schedulerService.resumeTrigger(taskName, triggerName);
		
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}
}
