package org.simplifyworks.workflow.web.controller.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.workflow.model.domain.WorkflowDeploymentDto;
import org.simplifyworks.workflow.service.WorkflowDeploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api/workflow/deployments")
public class WorkflowDeploymentController {

	@Autowired
	private WorkflowDeploymentService deploymentService;
	
	@RequestMapping(method = RequestMethod.POST)
	public ResourceWrapper<WorkflowDeploymentDto> create(String deploymentName, String fileName, MultipartFile data) throws IOException {
		return new ResourceWrapper<>(deploymentService.create(deploymentName, fileName, data.getInputStream()));
	}
	

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable String id, HttpServletResponse response) {
		Assert.notNull(id, "deployment id is missing");

		deploymentService.delete(id);
		
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}
}
