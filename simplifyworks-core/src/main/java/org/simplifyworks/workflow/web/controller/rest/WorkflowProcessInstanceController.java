package org.simplifyworks.workflow.web.controller.rest;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.workflow.model.domain.WorkflowProcessInstanceDto;
import org.simplifyworks.workflow.service.WorkflowProcessInstanceService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/workflow/process-instances")
public class WorkflowProcessInstanceController {

	@Autowired
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResourcesWrapper<WorkflowProcessInstanceDto> getAll(String processDefinitionId) {
		return new ResourcesWrapper<>(workflowProcessInstanceService.getAll(processDefinitionId));
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResourceWrapper<WorkflowProcessInstanceDto> create(@RequestBody Map<String, Object> data) {
		String entityType = (String) data.get(WorkflowService.PARAMETER_ENTITY_TYPE);
		Long entityId = (long) (Integer) data.get(WorkflowService.PARAMETER_ENTITY_ID);
		String processDefinitionId = (String) data.get(WorkflowService.PARAMETER_PROCESS_DEFINITION_ID);
		
		return new ResourceWrapper<WorkflowProcessInstanceDto>(workflowProcessInstanceService.create(entityType, entityId, processDefinitionId));
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/{processInstanceId}/{actionId}")	
	public void execute(@PathVariable String processInstanceId, @PathVariable String actionId, HttpServletResponse response) {
		workflowProcessInstanceService.executeAction(processInstanceId, actionId);
		
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{processInstanceId}")
	public void deleteProcessInstance(@PathVariable String processInstanceId) {
		workflowProcessInstanceService.deleteProcessInstance(processInstanceId);
	}
}
