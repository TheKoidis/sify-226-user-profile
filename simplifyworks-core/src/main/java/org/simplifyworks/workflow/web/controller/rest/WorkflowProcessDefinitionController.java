package org.simplifyworks.workflow.web.controller.rest;

import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping(value = "/api/workflow/process-definitions")
public class WorkflowProcessDefinitionController {

	@Autowired
	private WorkflowProcessDefinitionService processDefinitionService;

	@RequestMapping(method = RequestMethod.GET)
	public ResourcesWrapper<WorkflowProcessDefinitionDto> getAll() {
		return new ResourcesWrapper<>(processDefinitionService.getAll());
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResourceWrapper<WorkflowProcessDefinitionDto> processDefinitions(@PathVariable String id) {
		Assert.notNull(id, "process definition id is missing");

		return new ResourceWrapper<>(processDefinitionService.get(id));
	}

	@RequestMapping(value = "/{id}/image", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> image(@PathVariable String id) {
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentType(MediaType.IMAGE_PNG);

		InputStreamResource isr = new InputStreamResource(processDefinitionService.getImage(id));
		return new ResponseEntity<>(isr, respHeaders, HttpStatus.OK);
	}
}
