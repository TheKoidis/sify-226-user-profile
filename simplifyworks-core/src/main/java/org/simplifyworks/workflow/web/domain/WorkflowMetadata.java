package org.simplifyworks.workflow.web.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class WorkflowMetadata {

	private String entityType;
	private Long entityId;
	
	private List<WorkflowProcessMetadata> processes = new ArrayList<>();

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public List<WorkflowProcessMetadata> getProcesses() {
		return processes;
	}

	public void setProcesses(List<WorkflowProcessMetadata> processes) {
		this.processes = processes;
	}
}