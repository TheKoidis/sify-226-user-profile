package org.simplifyworks.workflow.web.domain;

import java.util.List;
import org.simplifyworks.workflow.model.domain.WorkflowActionDto;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricProcessInstanceDto;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.model.domain.WorkflowProcessInstanceDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class WorkflowProcessMetadata {

	private WorkflowProcessDefinitionDto processDefinition;
	private WorkflowProcessInstanceDto processInstance;
	private WorkflowHistoricProcessInstanceDto historicProcessInstance;
	private List<WorkflowActionDto> actions;

	public WorkflowProcessMetadata() {
	}

	public WorkflowProcessDefinitionDto getProcessDefinition() {
		return processDefinition;
	}

	public void setProcessDefinition(WorkflowProcessDefinitionDto processDefinition) {
		this.processDefinition = processDefinition;
	}

	public WorkflowProcessInstanceDto getProcessInstance() {
		return processInstance;
	}

	public void setProcessInstance(WorkflowProcessInstanceDto processInstance) {
		this.processInstance = processInstance;
	}

	public List<WorkflowActionDto> getActions() {
		return actions;
	}

	public void setActions(List<WorkflowActionDto> actions) {
		this.actions = actions;
	}

	public WorkflowHistoricProcessInstanceDto getHistoricProcessInstance() {
		return historicProcessInstance;
	}

	public void setHistoricProcessInstance(WorkflowHistoricProcessInstanceDto historicProcessInstance) {
		this.historicProcessInstance = historicProcessInstance;
	}

}
