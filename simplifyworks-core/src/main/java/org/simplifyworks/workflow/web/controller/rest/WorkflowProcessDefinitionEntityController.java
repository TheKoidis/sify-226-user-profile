package org.simplifyworks.workflow.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionEntityDto;
import org.simplifyworks.workflow.model.entity.WorkflowProcessDefinitionEntity;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@RestController
@RequestMapping(value = "/api/wf/definition-entities")
public class WorkflowProcessDefinitionEntityController extends BasicTableController<WorkflowProcessDefinitionEntityDto, WorkflowProcessDefinitionEntity> {

	@Autowired
	private WorkflowProcessDefinitionEntityManager workflowProcessDefinitionEntityManager;
	
	@Override
	protected ReadWriteManager<WorkflowProcessDefinitionEntityDto, WorkflowProcessDefinitionEntity> getManager() {
		return workflowProcessDefinitionEntityManager;
	}
}