package org.simplifyworks.workflow.web.controller.rest;

import javax.servlet.http.HttpServletRequest;
import org.simplifyworks.core.model.domain.SortOrder;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricActivityInstanceDto;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.service.WorkflowHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@RestController
@RequestMapping(value = "/api/workflow/history")
public class WorkflowHistoryController {

	protected static final String ORDER = "order";

	@Autowired
	private WorkflowHistoryService workflowHistoryService;

	@RequestMapping(method = RequestMethod.GET, value = "/user-tasks/{processInstanceId}")
	public ResourcesWrapper<WorkflowHistoricTaskInstanceDto> historicUserTasks(@PathVariable String processInstanceId, HttpServletRequest request) {
		String order = request.getParameter(ORDER);
		return new ResourcesWrapper<>(workflowHistoryService.historicUserTasks(processInstanceId, order != null ? SortOrder.valueOf(order.toUpperCase()) : SortOrder.ASC));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/activities/{processInstanceId}")
	public ResourcesWrapper<WorkflowHistoricActivityInstanceDto> historicActivities(@PathVariable String processInstanceId, HttpServletRequest request) {
		String order = request.getParameter(ORDER);
		return new ResourcesWrapper<>(workflowHistoryService.historicActivities(processInstanceId, order != null ? SortOrder.valueOf(order.toUpperCase()) : SortOrder.ASC));
	}

	@RequestMapping(value = "/{id}/image", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> image(@PathVariable String id) {
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentType(MediaType.IMAGE_PNG);

		InputStreamResource isr = new InputStreamResource(workflowHistoryService.getImage(id));
		return new ResponseEntity<>(isr, respHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}/image-current-position", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> imageCurrentPosition(@PathVariable String id) {
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentType(MediaType.IMAGE_PNG);

		InputStreamResource isr = new InputStreamResource(workflowHistoryService.getImageCurrentPosition(id));
		return new ResponseEntity<>(isr, respHeaders, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{processInstanceId}")
	public void deleteProcessInstance(@PathVariable String processInstanceId) {
		workflowHistoryService.deleteProcessInstance(processInstanceId);
	}
}
