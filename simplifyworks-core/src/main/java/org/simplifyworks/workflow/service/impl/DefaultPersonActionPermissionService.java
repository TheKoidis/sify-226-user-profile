package org.simplifyworks.workflow.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.PermissionFor;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserWithPersonManager;
import org.simplifyworks.workflow.model.domain.WorkflowPermissionDto;
import org.simplifyworks.workflow.service.WorkflowActionPermissionService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Parameters are paths to person id
 *
 * Example: person(path.to.person.id,anotherPath.to.person.id) to allow acces to
 * all users related to the person, which has the same id as filled in current
 * entity in one of the properties: path.to.person.id or
 * anotherPath.to.person.id
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultPersonActionPermissionService implements WorkflowActionPermissionService {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultPersonActionPermissionService.class);

	public static final String PERMISSION_TYPE = "person";

	@Autowired
	private SecurityService securityService;

	@Autowired
	private CoreUserWithPersonManager coreUserWithPersonManager;

	@Override
	public List<CorePermission> getCorePermissions(WorkflowPermissionDto actionPermission, AbstractEntity entity, Long relId, String relType) {
		List<CorePermission> permissions = new ArrayList<>();
		if (!PERMISSION_TYPE.equals(actionPermission.getType())) {
			return permissions;
		}

		for (String personIdPath : actionPermission.getParameters()) {
			try {
				Object personId = PropertyUtils.getProperty(entity, personIdPath);
				if (personId == null) {
					continue;
				}

				SearchParameters searchParameters = new SearchParameters();
				searchParameters.addFilter(FilterValue.single(CoreUser_.person.getName() + "." + CorePerson_.id.getName(), personId));
				List<CoreUserWithPersonDto> users = coreUserWithPersonManager.searchWithoutSecurity(searchParameters);

				for (CoreUserWithPersonDto user : users) {
					CorePermission corePermission = new CorePermission();
					corePermission.setActive(Boolean.TRUE);
					corePermission.setCanRead(Boolean.TRUE);
					corePermission.setCanWrite(Boolean.TRUE);
					corePermission.setCanDelete(Boolean.FALSE);
					corePermission.setObjectIdentifier(String.valueOf(entity.getId()));
					corePermission.setObjectType(entity.getClass().getName());
					corePermission.setPermissionFor(PermissionFor.WORKFLOW);
					corePermission.setUser(coreUserWithPersonManager.toEntity(user));
					corePermission.setRelId(relId);
					corePermission.setRelType(relType);

					permissions.add(corePermission);
				}
			} catch (NestedNullException e) {
				//null value of some field on path
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
				throw new CoreException("Cannot get permissions for entity (personIdPath=" + personIdPath + ", entity=" + entity + ")", ex);
			}
		}

		return permissions;
	}

	@Override
	public boolean isActionAllowed(WorkflowPermissionDto actionPermission, AbstractEntity entity) {
		if (!PERMISSION_TYPE.equals(actionPermission.getType())) {
			return false;
		}

		CoreUserWithPersonDto user = coreUserWithPersonManager.findByUsername(securityService.getUsername());

		for (String personIdPath : actionPermission.getParameters()) {
			try {
				Object personId = PropertyUtils.getProperty(entity, personIdPath);

				if (user != null && user.getPerson() != null && user.getPerson().getId().equals(personId)) {
					return true;
				}
			} catch (NestedNullException e) {
				//null value of some field on path
				return false;
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
				LOG.error("Cannot check right for action (personIdPath=" + personIdPath + ", entity=" + entity + ")", ex);
			}
		}

		return false;
	}
}
