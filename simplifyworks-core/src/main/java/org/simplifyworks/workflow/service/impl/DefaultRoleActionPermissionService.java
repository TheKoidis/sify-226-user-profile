package org.simplifyworks.workflow.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.PermissionFor;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.simplifyworks.workflow.model.domain.WorkflowPermissionDto;
import org.simplifyworks.workflow.service.WorkflowActionPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Parameters are role names
 *
 * Example: role(admin,anotherRole1,anotherRole2) to allow acces to all users
 * with any of these roles
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultRoleActionPermissionService implements WorkflowActionPermissionService {

	public static final String PERMISSION_TYPE = "role";

	@Autowired
	private SecurityService securityService;
	@Autowired
	private CoreRoleManager coreRoleManager;

	@Override
	public List<CorePermission> getCorePermissions(WorkflowPermissionDto actionPermission, AbstractEntity entity, Long relId, String relType) {
		List<CorePermission> permissions = new ArrayList<>();
		if (!PERMISSION_TYPE.equals(actionPermission.getType())) {
			return permissions;
		}

		for (String roleName : actionPermission.getParameters()) {
			CoreRoleDto role = coreRoleManager.searchUsingName(roleName);
			if (role == null) {
				continue;
			}

			CorePermission corePermission = new CorePermission();
			corePermission.setActive(Boolean.TRUE);
			corePermission.setCanRead(Boolean.TRUE);
			corePermission.setCanWrite(Boolean.TRUE);
			corePermission.setCanDelete(Boolean.FALSE);
			corePermission.setObjectIdentifier(String.valueOf(entity.getId()));
			corePermission.setObjectType(entity.getClass().getName());
			corePermission.setPermissionFor(PermissionFor.WORKFLOW);
			corePermission.setRole(coreRoleManager.toEntity(role));
			corePermission.setRelId(relId);
			corePermission.setRelType(relType);

			permissions.add(corePermission);
		}

		return permissions;
	}

	@Override
	public boolean isActionAllowed(WorkflowPermissionDto actionPermission, AbstractEntity entity) {
		return PERMISSION_TYPE.equals(actionPermission.getType()) && securityService.isAuthenticated() && securityService.hasAnyRole(actionPermission.getParameters());
	}
}
