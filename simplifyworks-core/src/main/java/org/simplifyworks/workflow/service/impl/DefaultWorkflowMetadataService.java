package org.simplifyworks.workflow.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.ReadManager;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricProcessInstanceDto;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.model.domain.WorkflowProcessInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionEntityDto;
import org.simplifyworks.workflow.model.entity.WorkflowProcessDefinitionEntity_;
import org.simplifyworks.workflow.service.WorkflowActionService;
import org.simplifyworks.workflow.service.WorkflowHistoryService;
import org.simplifyworks.workflow.service.WorkflowMetadataService;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionEntityManager;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionService;
import org.simplifyworks.workflow.service.WorkflowProcessInstanceService;
import org.simplifyworks.workflow.web.domain.WorkflowMetadata;
import org.simplifyworks.workflow.web.domain.WorkflowProcessMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultWorkflowMetadataService implements WorkflowMetadataService {

	@Autowired
	private WorkflowProcessDefinitionEntityManager workflowProcessDefinitionEntityManager;

	@Autowired
	private WorkflowProcessDefinitionService workflowProcessDefinitionService;

	@Autowired
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Autowired
	private WorkflowActionService workflowActionService;

	@Autowired
	private WorkflowHistoryService workflowHistoryService;
	
	@Async
	@Override
	public <T extends AbstractDto, E extends AbstractEntity> Future<WorkflowMetadata> getWorkflowMetadata(ReadManager<T, E> manager, T dto) {
		Map<String, WorkflowProcessDefinitionDto> definitions = Maps.newHashMap();
		Map<String, WorkflowProcessInstanceDto> instances = Maps.newHashMap();
		Map<String, WorkflowHistoricProcessInstanceDto> historicInstances = Maps.newHashMap();

		WorkflowMetadata workflowMetadata = new WorkflowMetadata();
		workflowMetadata.setEntityId(dto.getId());
		workflowMetadata.setEntityType(manager.getEntityClass().getName());

		getDefinitionsByEntity(manager, definitions, dto);
		getDefinitionsByRunningInstances(manager, dto, instances, definitions);
		getDefinitionsByHistoricInstances(manager, dto, historicInstances, definitions);

		for (WorkflowProcessDefinitionDto processDefinition : definitions.values()) {
			WorkflowProcessInstanceDto processInstance
					= (instances.containsKey(processDefinition.getKey())
							? instances.get(processDefinition.getKey())
							: workflowProcessInstanceService.get(manager.getEntityClass().getName(), dto.getId(), processDefinition.getId()));

			WorkflowHistoricProcessInstanceDto historicProcessInstance = (historicInstances.containsKey(processDefinition.getKey())
					? historicInstances.get(processDefinition.getKey())
					: workflowHistoryService.get(manager.getEntityClass().getName(), dto.getId(), processDefinition.getId()));

			WorkflowProcessMetadata workflowProcessMetadata = new WorkflowProcessMetadata();
			workflowProcessMetadata.setProcessDefinition(processDefinition);
			workflowProcessMetadata.setProcessInstance(processInstance);
			workflowProcessMetadata.setHistoricProcessInstance(historicProcessInstance);
			workflowProcessMetadata.setActions(processInstance == null ? null : workflowActionService.getAll(processInstance.getId()));

			workflowMetadata.getProcesses().add(workflowProcessMetadata);
		}

		return new AsyncResult<>(workflowMetadata);
	}

	/**
	 * Gets definition by running instances
	 */
	private <T extends AbstractDto, E extends AbstractEntity> void getDefinitionsByRunningInstances(ReadManager<T, E> manager, T dto, Map<String, WorkflowProcessInstanceDto> instances, Map<String, WorkflowProcessDefinitionDto> definitions) {
		List<WorkflowProcessInstanceDto> processInstances = workflowProcessInstanceService.list(manager.getEntityClass().getName(), dto.getId());
		for (WorkflowProcessInstanceDto processInstance : processInstances) {
			instances.put(processInstance.getProcessDefinitionKey(), processInstance);
			if (!definitions.containsKey(processInstance.getProcessDefinitionKey())) {
				definitions.put(processInstance.getProcessDefinitionKey(), workflowProcessDefinitionService.get(processInstance.getProcessDefinitionId()));
			}
		}
	}

	/**
	 * Gets definition by historic instances
	 */
	private <T extends AbstractDto, E extends AbstractEntity> void getDefinitionsByHistoricInstances(ReadManager<T, E> manager, T dto, Map<String, WorkflowHistoricProcessInstanceDto> histroricInstances, Map<String, WorkflowProcessDefinitionDto> definitions) {
		List<WorkflowHistoricProcessInstanceDto> processInstances = workflowHistoryService.list(manager.getEntityClass().getName(), dto.getId());
		for (WorkflowHistoricProcessInstanceDto processInstance : processInstances) {
			WorkflowProcessDefinitionDto processDefinition = workflowProcessDefinitionService.get(processInstance.getProcessDefinitionId());
			histroricInstances.put(processDefinition.getKey(), processInstance);
			if (!definitions.containsKey(processDefinition.getKey())) {
				definitions.put(processDefinition.getKey(), processDefinition);
			}
		}
	}

	/**
	 * Gets definitions by relations to this entity
	 */
	private <T extends AbstractDto, E extends AbstractEntity> void getDefinitionsByEntity(ReadManager<T, E> manager, Map<String, WorkflowProcessDefinitionDto> definitions, T dto) {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addFilter(FilterValue.single(WorkflowProcessDefinitionEntity_.entityClassName.getName(), manager.getEntityClass().getName()));
		List<WorkflowProcessDefinitionEntityDto> processDefinitionEntities = workflowProcessDefinitionEntityManager.searchWithoutSecurity(searchParameters);

		for (WorkflowProcessDefinitionEntityDto processDefinitionEntity : processDefinitionEntities) {
			WorkflowProcessDefinitionDto processDefinition = workflowProcessDefinitionService.getByKey(processDefinitionEntity.getProcessDefinitionKey());
			if (!manager.isDefinitionValid(processDefinition, dto)) {
				continue;
			}
			definitions.put(processDefinitionEntity.getProcessDefinitionKey(), processDefinition);
		}
	}
}
