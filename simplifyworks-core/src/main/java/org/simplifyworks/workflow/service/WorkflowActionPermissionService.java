package org.simplifyworks.workflow.service;

import java.util.List;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.workflow.model.domain.WorkflowPermissionDto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowActionPermissionService {

	public List<CorePermission> getCorePermissions(WorkflowPermissionDto actionPermission, AbstractEntity entity, Long relId, String relType);

	public boolean isActionAllowed(WorkflowPermissionDto actionPermission, AbstractEntity entity);
}
