/**
 *
 */
package org.simplifyworks.workflow.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultWorkflowProcessDefinitionService implements WorkflowProcessDefinitionService {

	@Autowired
	private RepositoryService repositoryService;

	@Override
	public List<WorkflowProcessDefinitionDto> getAll() {
		List<WorkflowProcessDefinitionDto> processDefinitions = new ArrayList<>();

		for (ProcessDefinition processDefinition : repositoryService.createProcessDefinitionQuery().latestVersion().orderByProcessDefinitionName().asc().list()) {
			processDefinitions.add(new WorkflowProcessDefinitionDto(processDefinition));
		}

		return processDefinitions;
	}

	@Override
	public WorkflowProcessDefinitionDto get(String id) {
		return new WorkflowProcessDefinitionDto(repositoryService.getProcessDefinition(id));
	}

	@Override
	public WorkflowProcessDefinitionDto getByKey(String key) {
		ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery().latestVersion().processDefinitionKey(key);
		ProcessDefinition result = query.singleResult();
		return result != null ? new WorkflowProcessDefinitionDto(result) : null;
	}

	@Override
	public InputStream getImage(String id) {
		ProcessDefinition processDefinition = repositoryService.getProcessDefinition(id);

		return repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), processDefinition.getDiagramResourceName());
	}
}
