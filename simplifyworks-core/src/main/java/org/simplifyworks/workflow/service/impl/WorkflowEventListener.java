package org.simplifyworks.workflow.service.impl;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import org.activiti.engine.FormService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.event.ActivitiEntityEvent;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.FormType;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.task.Task;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.EntityEvent.EntityEventOperation;
import org.simplifyworks.uam.model.domain.PermissionFor;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.model.entity.CorePermission_;
import org.simplifyworks.workflow.model.domain.WorkflowActionFormType;
import org.simplifyworks.workflow.model.domain.WorkflowPermissionDto;
import org.simplifyworks.workflow.service.WorkflowActionPermissionService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class WorkflowEventListener implements ActivitiEventListener {

	public static final String REL_TYPE = "workflowTask";

	@Autowired
	private ApplicationContext context;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private FormService formService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;

	@Override
	@Transactional
	public void onEvent(ActivitiEvent event) {
		if (ActivitiEventType.TASK_CREATED.equals(event.getType()) || ActivitiEventType.TASK_COMPLETED.equals(event.getType())) {
			try {
				TaskEntity task = (TaskEntity) ((ActivitiEntityEvent) event).getEntity();

				ExecutionEntity processInstance = task.getProcessInstance();

				Long id = (Long) processInstance.getVariable(WorkflowService.PARAMETER_ENTITY_ID);
				String type = (String) processInstance.getVariable(WorkflowService.PARAMETER_ENTITY_TYPE);
				AbstractEntity entity = (AbstractEntity) entityManager.find(Class.forName(type), id);

				switch (event.getType()) {
					case TASK_CREATED:
						createActionPermissions(task, entity);

						eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.UPDATE, entity));

						break;
					case TASK_COMPLETED:
						deleteActionPermissions(task, entity);

						eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.UPDATE, entity));

						break;
					default:
						break;
				}
			} catch (Exception e) {
				throw new CoreException(e);
			}
		}
	}

	@Override
	public boolean isFailOnException() {
		// we prefer fail over bad permissions/actions
		return true;
	}

	private void createActionPermissions(Task task, AbstractEntity entity) {
		for (FormProperty formProperty : formService.getTaskFormData(task.getId()).getFormProperties()) {
			FormType formType = formProperty.getType();

			if (formType != null && WorkflowActionFormType.NAME.equals(formType.getName())) {
				WorkflowActionFormType actionFormType = (WorkflowActionFormType) formType;

				for (WorkflowPermissionDto actionPermission : actionFormType.getPermissions()) {
					Collection<WorkflowActionPermissionService> actionPermissionServices = context.getBeansOfType(WorkflowActionPermissionService.class)
							.values();

					for (WorkflowActionPermissionService actionPermissionService : actionPermissionServices) {
						for (CorePermission permission : actionPermissionService.getCorePermissions(actionPermission, entity, Long.parseLong(task.getId()), formProperty.getId())) {
							entityManager.persist(permission);
						}
					}
				}
			}
		}
	}

	private void deleteActionPermissions(Task task, AbstractEntity entity) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		CriteriaDelete<CorePermission> delete = builder.createCriteriaDelete(CorePermission.class);
		Root<CorePermission> root = delete.from(CorePermission.class);
		delete.where(builder.and(builder.equal(root.get(CorePermission_.relId), task.getId()), builder.equal(root.get(CorePermission_.permissionFor), PermissionFor.WORKFLOW)));

		entityManager.createQuery(delete).executeUpdate();
	}
}
