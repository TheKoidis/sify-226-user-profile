package org.simplifyworks.workflow.service.impl;

import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionEntityDto;
import org.simplifyworks.workflow.model.entity.WorkflowProcessDefinitionEntity;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionEntityManager;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-07
 */
@Service
public class WorkflowProcessDefinitionEntityManagerImpl extends DefaultReadWriteManager<WorkflowProcessDefinitionEntityDto, WorkflowProcessDefinitionEntity> implements WorkflowProcessDefinitionEntityManager {

	@Autowired
	private WorkflowProcessDefinitionService workflowProcessDefinitionService;

	@Override
	public WorkflowProcessDefinitionEntityDto toDto(WorkflowProcessDefinitionEntity entity) {
		WorkflowProcessDefinitionEntityDto dto = super.toDto(entity);
//		//TODO rebuild index when definition changes
		dto.setWorkflowProcessDefinition(workflowProcessDefinitionService.getByKey(dto.getProcessDefinitionKey()));
		return dto;
	}

}
