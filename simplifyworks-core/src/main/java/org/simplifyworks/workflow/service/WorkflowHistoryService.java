package org.simplifyworks.workflow.service;

import java.io.InputStream;
import java.util.List;
import org.simplifyworks.core.model.domain.SortOrder;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricActivityInstanceDto;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricProcessInstanceDto;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricTaskInstanceDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public interface WorkflowHistoryService {

	public List<WorkflowHistoricTaskInstanceDto> historicUserTasks(String processInstanceId, SortOrder sortOrder);

	public List<WorkflowHistoricActivityInstanceDto> historicActivities(String processInstanceId, SortOrder sortOrder);

	public WorkflowHistoricProcessInstanceDto get(String entityType, Long entityId, String processDefinitionId);

	public List<WorkflowHistoricProcessInstanceDto> list(String entityType, Long entityId);

	public InputStream getImage(String processInstanceId);

	public InputStream getImageCurrentPosition(String processInstanceId);

	public void deleteProcessInstance(String processInstanceId);
}
