package org.simplifyworks.workflow.service;

import java.util.concurrent.Future;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.ReadManager;
import org.simplifyworks.workflow.web.domain.WorkflowMetadata;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowMetadataService {

	public <T extends AbstractDto, E extends AbstractEntity> Future<WorkflowMetadata> getWorkflowMetadata(ReadManager<T, E> manager, T dto);
}
