package org.simplifyworks.workflow.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.PermissionFor;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreOrganizationWithParentDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.service.CoreOrganizationManager;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.simplifyworks.workflow.model.domain.WorkflowPermissionDto;
import org.simplifyworks.workflow.service.WorkflowActionPermissionService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Last parameter is path to organization id, other parametrs are role names
 *
 * Example: roleInOrg(role1,role2,role3,path.to.organization.id) to allow acces
 * to all users with any role in organization, which has the same id as filled
 * in current entity in the property path.to.organization.id
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultRoleInOrgActionPermissionService implements WorkflowActionPermissionService {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultRoleInOrgActionPermissionService.class);

	public static final String PERMISSION_TYPE = "roleInOrg";

	@Autowired
	private SecurityService securityService;
	@Autowired
	private CoreRoleManager coreRoleManager;
	@Autowired
	private CoreOrganizationManager coreOrganizationManager;

	@Override
	public List<CorePermission> getCorePermissions(WorkflowPermissionDto actionPermission, AbstractEntity entity, Long relId, String relType) {
		List<CorePermission> permissions = new ArrayList<>();
		if (!PERMISSION_TYPE.equals(actionPermission.getType())) {
			return permissions;
		}

		if (actionPermission.getParameters().length < 2) {
			throw new CoreException("Workflow permission type " + PERMISSION_TYPE + " requires 2 or more arguments!");
		}

		String orgIdPath = actionPermission.getParameters()[actionPermission.getParameters().length - 1];

		CoreOrganizationWithParentDto organization = null;

		try {
			Long orgId = (Long) PropertyUtils.getProperty(entity, orgIdPath);

			if (orgId == null) {
				return permissions;
			}

			organization = coreOrganizationManager.get(orgId);
		} catch (NestedNullException e) {
			//null value of some field on path
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
			throw new CoreException("Cannot get permissions for entity (orgIdPath=" + orgIdPath + ", entity=" + entity + ")", ex);
		}

		if (organization == null) {
			return permissions;
		}

		for (String roleName : Arrays.asList(actionPermission.getParameters()).subList(0, actionPermission.getParameters().length - 1)) {
			CoreRoleDto role = coreRoleManager.searchUsingName(roleName);
			if (role == null) {
				return permissions;
			}

			CorePermission corePermission = new CorePermission();
			corePermission.setActive(Boolean.TRUE);
			corePermission.setCanRead(Boolean.TRUE);
			corePermission.setCanWrite(Boolean.TRUE);
			corePermission.setCanDelete(Boolean.FALSE);
			corePermission.setObjectIdentifier(String.valueOf(entity.getId()));
			corePermission.setObjectType(entity.getClass().getName());
			corePermission.setPermissionFor(PermissionFor.WORKFLOW);
			corePermission.setRole(coreRoleManager.toEntity(role));
			corePermission.setOrganization(coreOrganizationManager.toEntity(organization));
			corePermission.setRelId(relId);
			corePermission.setRelType(relType);

			permissions.add(corePermission);
		}

		return permissions;
	}

	@Override
	public boolean isActionAllowed(WorkflowPermissionDto actionPermission, AbstractEntity entity) {
		if (!PERMISSION_TYPE.equals(actionPermission.getType())) {
			return false;
		}

		if (actionPermission.getParameters().length < 2) {
			throw new CoreException("Workflow permission type " + PERMISSION_TYPE + " requires 2 or more arguments!");
		}

		String orgIdPath = actionPermission.getParameters()[actionPermission.getParameters().length - 1];

		try {
			Long orgId = (Long) PropertyUtils.getProperty(entity, orgIdPath);

			if (orgId == null) {
				return false;
			}

			CoreOrganizationDto organization = coreOrganizationManager.get(orgId);

			if (organization == null) {
				return false;
			}

			for (String roleName : Arrays.asList(actionPermission.getParameters()).subList(0, actionPermission.getParameters().length - 1)) {
				if (securityService.hasRoleInOrganization(roleName, organization.getName())) {
					return true;
				}
			}
		} catch (NestedNullException e) {
			//null value of some field on path
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
			LOG.error("Cannot get permissions for entity (orgIdPath=" + orgIdPath + ", entity=" + entity + ")", ex);
		}

		return false;
	}
}
