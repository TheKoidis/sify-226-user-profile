/**
 * 
 */
package org.simplifyworks.workflow.service.impl;

import com.google.common.collect.Lists;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.workflow.model.domain.WorkflowActionDto;
import org.simplifyworks.workflow.model.domain.WorkflowProcessInstanceDto;
import org.simplifyworks.workflow.service.WorkflowActionService;
import org.simplifyworks.workflow.service.WorkflowProcessInstanceService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultWorkflowProcessInstanceService implements WorkflowProcessInstanceService {

	@Autowired
	private RuntimeService runtimeService;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private WorkflowActionService workflowActionService;
	
	@Autowired
	private SecurityService securityService;
	
	@Override
	public WorkflowProcessInstanceDto get(String entityType, Long entityId, String processDefinitionId) {
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
			.variableValueEquals(WorkflowService.PARAMETER_ENTITY_ID, entityId)
			.variableValueEquals(WorkflowService.PARAMETER_ENTITY_TYPE, entityType)
			.processDefinitionId(processDefinitionId)
			.includeProcessVariables()
			.singleResult();

		return processInstance == null ? null : new WorkflowProcessInstanceDto(processInstance);
	}

	@Override
	public boolean isRunning(String processInstanceId) {
		return runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).count() == 1;
	}
	
	@Override
	public List<WorkflowProcessInstanceDto> getAll(String processDefinitionId) {
		List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery()
				.processDefinitionId(processDefinitionId)
				.includeProcessVariables()
				.list();

		return WorkflowProcessInstanceDto.map(processInstances);
	}
	
	@Override
	public List<WorkflowProcessInstanceDto> list(String entityType, Long entityId) {
		List<ProcessInstance> list = runtimeService.createProcessInstanceQuery()
				.variableValueEquals(WorkflowService.PARAMETER_ENTITY_ID, entityId)
				.variableValueEquals(WorkflowService.PARAMETER_ENTITY_TYPE, entityType)
				.includeProcessVariables()
				.list();

		List<WorkflowProcessInstanceDto> processInstances = Lists.newArrayList();

		for (ProcessInstance processInstance : list) {
			processInstances.add(new WorkflowProcessInstanceDto(processInstance));
		}

		return processInstances;
	}

	@Override
	public WorkflowProcessInstanceDto create(String entityType, Long entityId, String processDefinitionId) {
		ProcessInstance processInstance = runtimeService.createProcessInstanceBuilder()
			.addVariable(PARAMETER_ENTITY_TYPE, entityType)
			.addVariable(PARAMETER_ENTITY_ID, entityId)
			.processDefinitionId(processDefinitionId)
			.start();
		
		return new WorkflowProcessInstanceDto(processInstance);
	}
	
	@Override
	public void executeAction(String processInstanceId, String actionId) {
		Map<String, Object> variables = new LinkedHashMap<>();
				
		for(WorkflowActionDto action : workflowActionService.getAll(processInstanceId)) {
			variables.put(action.getId(), actionId.equals(action.getId()));
		}
		
		Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
		taskService.setAssignee(task.getId(), securityService.getUsername());
		taskService.complete(task.getId(), variables);
	}

	@Override
	public void deleteProcessInstance(String processInstanceId) {
		runtimeService.deleteProcessInstance(processInstanceId, null);
	}
}
