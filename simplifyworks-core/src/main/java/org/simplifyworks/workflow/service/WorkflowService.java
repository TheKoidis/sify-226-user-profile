package org.simplifyworks.workflow.service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowService {

	public static final String PARAMETER_ENTITY = "entity";
    public static final String PARAMETER_NEW = "new";
	public static final String PARAMETER_WF_ACTION = "action";
    public static final String PARAMETER_ENTITY_ID = "entityId";
    public static final String PARAMETER_ENTITY_TYPE = "entityType";
    public static final String PARAMETER_PROCESS_INSTANCE = "processInstanceId";
	public static final String PARAMETER_PROCESS_DEFINITION_ID = "processDefinitionId";
	public static final String VALIDATION_FAILS_KEY = "validationFails";
	public static final String VALIDATION_STATE_KEY = "state";
	public static final String VALIDATION_STATE_VALID = "valid";
	public static final String VALIDATION_STATE_ERRORS = "errors";
	public static final String VALIDATION_STATE_WARNINGS = "warnings";
}
