package org.simplifyworks.workflow.service;

import java.util.List;

import org.simplifyworks.workflow.model.domain.WorkflowActionDto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowActionService extends WorkflowService {

	public List<WorkflowActionDto> getAll(String processInstanceId);
}
