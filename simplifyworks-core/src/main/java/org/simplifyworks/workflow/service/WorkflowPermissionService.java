package org.simplifyworks.workflow.service;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public interface WorkflowPermissionService {

	public void setPermissions(Long id, String type, String expression, String relType);
}
