package org.simplifyworks.workflow.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;
import org.simplifyworks.core.model.domain.SortOrder;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricActivityInstanceDto;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricProcessInstanceDto;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.service.WorkflowHistoryService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Service
public class DefaultWorkflowHistoryService implements WorkflowHistoryService {

	@Autowired
	private HistoryService historyService;

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private RuntimeService runtimeService;

	private final ProcessDiagramGenerator processDiagramGenerator = new DefaultProcessDiagramGenerator();

	@Override
	public List<WorkflowHistoricTaskInstanceDto> historicUserTasks(String processInstanceId, SortOrder sortOrder) {
		HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery().processInstanceId(processInstanceId).orderByHistoricTaskInstanceEndTime();
		if (sortOrder == SortOrder.ASC) {
			query.asc();
		} else {
			query.desc();
		}

		List<WorkflowHistoricTaskInstanceDto> result = Lists.newArrayList();
		query.list().stream().forEach((historicTaskInstance) -> {
			result.add(new WorkflowHistoricTaskInstanceDto(historicTaskInstance));
		});
		return result;
	}

	@Override
	public List<WorkflowHistoricActivityInstanceDto> historicActivities(String processInstanceId, SortOrder sortOrder) {
		List<HistoricActivityInstance> list = getHistoricActivities(processInstanceId, sortOrder);

		List<WorkflowHistoricActivityInstanceDto> result = Lists.newArrayList();
		list.stream().forEach((historicActivityInstance) -> {
			result.add(new WorkflowHistoricActivityInstanceDto(historicActivityInstance));
		});
		return result;

	}

	private List<HistoricActivityInstance> getHistoricActivities(String processInstanceId, SortOrder sortOrder) {
		HistoricActivityInstanceQuery query = historyService
				.createHistoricActivityInstanceQuery()
				.processInstanceId(processInstanceId)
				.orderByHistoricActivityInstanceStartTime();
		if (sortOrder == SortOrder.ASC) {
			query.asc();
		} else {
			query.desc();
		}
		return query.list();
	}

	@Override
	public WorkflowHistoricProcessInstanceDto get(String entityType, Long entityId, String processDefinitionId) {
		HistoricProcessInstance result = historyService.createHistoricProcessInstanceQuery()
				.variableValueEquals(WorkflowService.PARAMETER_ENTITY_ID, entityId)
				.variableValueEquals(WorkflowService.PARAMETER_ENTITY_TYPE, entityType)
				.processDefinitionId(processDefinitionId)
				.includeProcessVariables()
				.singleResult();
		return result != null ? new WorkflowHistoricProcessInstanceDto(result) : null;
	}

	@Override
	public List<WorkflowHistoricProcessInstanceDto> list(String entityType, Long entityId) {
		List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery()
				.variableValueEquals(WorkflowService.PARAMETER_ENTITY_ID, entityId)
				.variableValueEquals(WorkflowService.PARAMETER_ENTITY_TYPE, entityType)
				.includeProcessVariables()
				.list();

		List<WorkflowHistoricProcessInstanceDto> processInstances = Lists.newArrayList();

		for (HistoricProcessInstance processInstance : list) {
			processInstances.add(new WorkflowHistoricProcessInstanceDto(processInstance));
		}

		return processInstances;
	}

	@Override
	public InputStream getImage(String processInstanceId) {
		HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
		ProcessDefinition processDefinition = repositoryService.getProcessDefinition(processInstance.getProcessDefinitionId());

		BpmnModel bpmnModel = repositoryService.getBpmnModel(processInstance.getProcessDefinitionId());
		Set<String> historicActivityKeys = Sets.newHashSet();
		List<HistoricActivityInstance> historicActivities = getHistoricActivities(processInstanceId, SortOrder.ASC);

		for (HistoricActivityInstance activiti : historicActivities) {
			historicActivityKeys.add(activiti.getActivityId());
		}

		return processDiagramGenerator.generateDiagram(
				bpmnModel,
				"png",
				Lists.newArrayList(historicActivityKeys),
				getHighLightedFlows(historicActivities, ((ProcessDefinitionEntity) processDefinition).getActivities(), -1));
	}

	@Override
	public InputStream getImageCurrentPosition(String processInstanceId) {
		HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();

		BpmnModel bpmnModel = repositoryService.getBpmnModel(processInstance.getProcessDefinitionId());

		List<HistoricActivityInstance> historicActivities = getHistoricActivities(processInstanceId, SortOrder.ASC);

		List<String> nodes;
		if (historicActivities.isEmpty()) {
			nodes = Lists.newArrayList();
		} else {
			nodes = Lists.newArrayList(historicActivities.get(historicActivities.size() - 1).getActivityId());
		}

		return processDiagramGenerator.generateDiagram(
				bpmnModel,
				"png",
				nodes,
				Lists.newArrayList());
	}

	private List<String> getHighLightedFlows(List<HistoricActivityInstance> activities, List<ActivityImpl> activityList, int count) {
		List<String> highLightedFlows = Lists.newArrayList();

		Map<String, ActivityImpl> definedActivities = Maps.newHashMap();
		for (ActivityImpl activity : activityList) {
			definedActivities.put(activity.getId(), activity);
		}
		if (count == -1 || count > activities.size()) {
			count = activities.size() - 1;
		}
		if (count == 0) {
			return highLightedFlows;
		}
		for (int i = 1; i < count + 1; i++) {
			ActivityImpl activity = definedActivities.get(activities.get(i).getActivityId());
			if (activity == null) {
				continue;
			}
			List<PvmTransition> pvmTransitionList = activity.getIncomingTransitions();
			for (PvmTransition pvmTransition : pvmTransitionList) {
				if (pvmTransition.getSource().getId().equals(activities.get(i - 1).getActivityId())) {
					highLightedFlows.add(pvmTransition.getId());
				}
			}
		}
		return highLightedFlows;
	}

	@Override
	public void deleteProcessInstance(String processInstanceId) {
		historyService.deleteHistoricProcessInstance(processInstanceId);
	}
}
