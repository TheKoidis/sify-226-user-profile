package org.simplifyworks.workflow.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.service.CorePermissionManager;
import org.simplifyworks.workflow.model.domain.WorkflowPermissionDto;
import org.simplifyworks.workflow.model.domain.WorkflowUtils;
import org.simplifyworks.workflow.service.WorkflowActionPermissionService;
import org.simplifyworks.workflow.service.WorkflowPermissionService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Service
public class DefaultWorkflowPermissionService implements WorkflowPermissionService {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultWorkflowPermissionService.class);

	@Autowired
	private ApplicationContext context;

	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;

	@Autowired
	private CorePermissionManager corePermissionManager;

	@Override
	public void setPermissions(Long id, String type, String expression, String relType) {
		List<WorkflowPermissionDto> permissions = WorkflowUtils.getPermissions(expression);

		AbstractEntity entity;
		try {
			entity = (AbstractEntity) entityManager.find(Class.forName(type), id);

			Collection<WorkflowActionPermissionService> actionPermissionServices = context.getBeansOfType(WorkflowActionPermissionService.class).values();
			List<CorePermission> corePermissions = new ArrayList<>();

			for (WorkflowPermissionDto permission : permissions) {
				for (WorkflowActionPermissionService actionPermissionService : actionPermissionServices) {
					corePermissions.addAll(actionPermissionService.getCorePermissions(permission, entity, null, relType));
				}
			}

			List<CorePermission> oldPermissions = corePermissionManager.search(entity.getClass().getName(), String.valueOf(entity.getId()), relType);
			for (CorePermission oldPermission : oldPermissions) {
				corePermissionManager.remove(oldPermission.getId());
			}

			for (CorePermission corePermission : corePermissions) {
				corePermissionManager.create(corePermission);
			}

		} catch (ClassNotFoundException ex) {
			LOG.error("Cannot get entity for id=" + id + " and type=" + type, ex);
		}
	}

}
