package org.simplifyworks.workflow.service.impl;

import java.lang.reflect.InvocationTargetException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.beanutils.PropertyUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.AfterCommitTransactionSynchronization;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.EntityEvent.EntityEventOperation;
import org.simplifyworks.workflow.service.WorkflowStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultWorkflowStateService implements WorkflowStateService {

	@Autowired
	private RuntimeService runtimeService;
	
	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	private AfterCommitTransactionSynchronization afterCommitTransactionSynchronization;
	
	@Override
	@Transactional
	public void setState(String processInstanceId, String property, String state) {
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
				.processInstanceId(processInstanceId)
				.includeProcessVariables()
				.singleResult();
		
		Long id = (Long) processInstance.getProcessVariables().get(PARAMETER_ENTITY_ID);
		String type = (String) processInstance.getProcessVariables().get(PARAMETER_ENTITY_TYPE);
		
		setState(id, type, property, state);
	}

	@Override
	@Transactional
	public void setState(Long id, String type, String property, String state) {
		if (id != null && type != null) {
			try {
				AbstractEntity entity = (AbstractEntity) entityManager.find(Class.forName(type), id);
				Class<?> propertyType = PropertyUtils.getPropertyType(entity, property);
				if (propertyType.isEnum()) {
					Enum enumState = Enum.valueOf((Class<Enum>) propertyType, state);

					PropertyUtils.setProperty(entity, property, enumState);
				} else {
					PropertyUtils.setProperty(entity, property, state);
				}

				afterCommitTransactionSynchronization.executeAfterCommit(new Runnable() {
	
					@Override
					public void run() {
						eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.UPDATE, entity));
					}
				});
			} catch(ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
				throw new CoreException(e);
			}
		}
	}
}