/**
 *
 */
package org.simplifyworks.workflow.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.activiti.engine.FormService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.FormType;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.workflow.model.domain.WorkflowActionDto;
import org.simplifyworks.workflow.model.domain.WorkflowActionFormType;
import org.simplifyworks.workflow.model.domain.WorkflowPermissionDto;
import org.simplifyworks.workflow.service.WorkflowActionPermissionService;
import org.simplifyworks.workflow.service.WorkflowActionService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultWorkflowActionService implements WorkflowActionService {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultWorkflowActionService.class);

	@Autowired
	private TaskService taskService;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private FormService formService;

	@Autowired
	private ApplicationContext context;

	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;

	@Override
	public List<WorkflowActionDto> getAll(String processInstanceId) {
		Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
		TaskFormData taskFormData = formService.getTaskFormData(task.getId());

		List<WorkflowActionDto> actions = new ArrayList<>();

		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).includeProcessVariables().singleResult();

		Long id = (Long) processInstance.getProcessVariables().get(WorkflowService.PARAMETER_ENTITY_ID);
		String type = (String) processInstance.getProcessVariables().get(WorkflowService.PARAMETER_ENTITY_TYPE);
		AbstractEntity entity;
		try {
			entity = (AbstractEntity) entityManager.find(Class.forName(type), id);

			for (FormProperty formProperty : taskFormData.getFormProperties()) {
				FormType formType = formProperty.getType();

				if (formType != null && WorkflowActionFormType.NAME.equals(formType.getName())) {
					WorkflowActionFormType actionFormType = (WorkflowActionFormType) formType;

					if (isActionAllowed(actionFormType, entity)) {
						actions.add(new WorkflowActionDto(formProperty.getId(), actionFormType));
					}
				}
			}
		} catch (ClassNotFoundException ex) {
			LOG.error("Cannot get entity for id=" + id + " and type=" + type, ex);
		}

		return actions;
	}

	private boolean isActionAllowed(WorkflowActionFormType actionFormType, AbstractEntity entity) {
		List<WorkflowPermissionDto> permissions = actionFormType.getPermissions();

		if (permissions.isEmpty()) {
			return true;
		}

		Collection<WorkflowActionPermissionService> actionPermissionServices = context.getBeansOfType(WorkflowActionPermissionService.class).values();

		for (WorkflowPermissionDto permission : permissions) {
			for (WorkflowActionPermissionService actionPermissionService : actionPermissionServices) {
				if (actionPermissionService.isActionAllowed(permission, entity)) {
					return true;
				}
			}
		}

		return false;
	}
}
