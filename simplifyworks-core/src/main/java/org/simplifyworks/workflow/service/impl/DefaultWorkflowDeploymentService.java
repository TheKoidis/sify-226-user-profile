package org.simplifyworks.workflow.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.workflow.model.domain.WorkflowDeploymentDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionEntityDto;
import org.simplifyworks.workflow.model.entity.WorkflowProcessDefinitionEntity_;
import org.simplifyworks.workflow.service.WorkflowDeploymentService;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultWorkflowDeploymentService implements WorkflowDeploymentService {

	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private WorkflowProcessDefinitionEntityManager processDefinitionEntityManager;
	
	@Override
	public WorkflowDeploymentDto create(String deploymentName, String fileName, InputStream inputStream) {
		Deployment deployment = repositoryService.createDeployment().addInputStream(fileName, inputStream).name(deploymentName).deploy();
			
		return new WorkflowDeploymentDto(deployment);
	}
	
	@Override
	public void delete(String id) {
		removeProcessDefinitionEntities(id);
		
		repositoryService.deleteDeployment(id);
	}
	
	private void removeProcessDefinitionEntities(String deploymentId) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue(WorkflowProcessDefinitionEntity_.processDefinitionKey.getName(), FilterOperator.EQUALS, getProcessDefinitionKeys(deploymentId)));
		for(WorkflowProcessDefinitionEntityDto processDefinitionEntity: processDefinitionEntityManager.search(parameters)) {
			processDefinitionEntityManager.remove(processDefinitionEntity.getId());
		}
	}
	
	private List<Object> getProcessDefinitionKeys(String deploymentId) {
		List<Object> processDefinitionKeys = new ArrayList<>();
		
		for(ProcessDefinition processDefinition : repositoryService.createProcessDefinitionQuery().deploymentId(deploymentId).list()) {
			processDefinitionKeys.add(processDefinition.getKey());
		}
		
		return processDefinitionKeys;
	}
}
