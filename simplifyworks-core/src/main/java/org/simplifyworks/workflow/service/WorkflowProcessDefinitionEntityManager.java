package org.simplifyworks.workflow.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionEntityDto;
import org.simplifyworks.workflow.model.entity.WorkflowProcessDefinitionEntity;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-07
 */
public interface WorkflowProcessDefinitionEntityManager extends ReadWriteManager<WorkflowProcessDefinitionEntityDto, WorkflowProcessDefinitionEntity> {

}
