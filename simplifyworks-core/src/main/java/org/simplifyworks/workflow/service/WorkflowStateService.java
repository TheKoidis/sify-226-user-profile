package org.simplifyworks.workflow.service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowStateService extends WorkflowService {

	public void setState(String processInstanceId, String property, String state);

	public void setState(Long id, String type, String property, String state);
}
