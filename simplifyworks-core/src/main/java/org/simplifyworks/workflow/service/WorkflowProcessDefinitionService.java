package org.simplifyworks.workflow.service;

import java.io.InputStream;
import java.util.List;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowProcessDefinitionService extends WorkflowService {

	public List<WorkflowProcessDefinitionDto> getAll();

	public WorkflowProcessDefinitionDto get(String id);

	public WorkflowProcessDefinitionDto getByKey(String key);

	public InputStream getImage(String id);
}
