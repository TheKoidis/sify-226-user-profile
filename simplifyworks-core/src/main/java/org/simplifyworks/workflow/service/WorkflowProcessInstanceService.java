package org.simplifyworks.workflow.service;

import java.util.List;

import org.simplifyworks.workflow.model.domain.WorkflowProcessInstanceDto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowProcessInstanceService extends WorkflowService {

	public WorkflowProcessInstanceDto get(String entityType, Long entityId, String processDefinitionId);
	
	public List<WorkflowProcessInstanceDto> getAll(String processDefinitionId);

	public boolean isRunning(String processInstanceId);

	public List<WorkflowProcessInstanceDto> list(String entityType, Long entityId);

	public WorkflowProcessInstanceDto create(String entityType, Long entityId, String processDefinitionId);
	
	public void executeAction(String processInstanceId, String actionId);

	public void deleteProcessInstance(String processInstanceId);
}
