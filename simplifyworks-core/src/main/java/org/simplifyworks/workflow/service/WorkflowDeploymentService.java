package org.simplifyworks.workflow.service;

import java.io.InputStream;

import org.simplifyworks.workflow.model.domain.WorkflowDeploymentDto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowDeploymentService extends WorkflowService {

	public WorkflowDeploymentDto create(String deploymentName, String fileName, InputStream inputStream);
	
	public void delete(String id);
}
