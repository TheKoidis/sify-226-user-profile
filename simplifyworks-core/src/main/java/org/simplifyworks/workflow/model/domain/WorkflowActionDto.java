package org.simplifyworks.workflow.model.domain;

import java.util.List;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class WorkflowActionDto {
	
	private String id;
	
	private String label;
	private String tooltip;
	private String style;
	private List<WorkflowFunctionDto> functions;
	
	private boolean confirmationRequired;
	
	public WorkflowActionDto(String id, WorkflowActionFormType formType) {		
		this.id = id;
		this.label = formType.getLabel();
		this.tooltip = formType.getTooltip();
		this.style = formType.getStyle();
		this.functions = formType.getFunctions();
		this.confirmationRequired = formType.isConfirmationRequired();
	}
	
	public String getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getTooltip() {
		return tooltip;
	}
	
	public String getStyle() {
		return style;
	}

	public List<WorkflowFunctionDto> getFunctions() {
		return functions;
	}

	public boolean isConfirmationRequired() {
		return confirmationRequired;
	}
}
