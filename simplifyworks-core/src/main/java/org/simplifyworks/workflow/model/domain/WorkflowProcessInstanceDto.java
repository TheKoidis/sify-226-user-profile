package org.simplifyworks.workflow.model.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class WorkflowProcessInstanceDto extends WorkflowExecutionDto {
	
	private String processDefinitionId;
	private String processDefinitionName;
	private String processDefinitionKey;
	private Integer processDefinitionVersion;
	private String deploymentId;
	private String businessKey;
	private Map<String, Object> processVariables;
	private String name;

	public WorkflowProcessInstanceDto() {
	}

	public WorkflowProcessInstanceDto(ProcessInstance processInstance) {
		super(processInstance);
		
		this.processDefinitionId = processInstance.getProcessDefinitionId();
		this.processDefinitionName = processInstance.getProcessDefinitionName();
		this.processDefinitionKey = processInstance.getProcessDefinitionKey();
		this.processDefinitionVersion = processInstance.getProcessDefinitionVersion();
		this.deploymentId = processInstance.getDeploymentId();
		this.businessKey = processInstance.getBusinessKey();
		this.processVariables = processInstance.getProcessVariables();
		this.name = processInstance.getName();
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessDefinitionName() {
		return processDefinitionName;
	}

	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}

	public String getProcessDefinitionKey() {
		return processDefinitionKey;
	}

	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}

	public Integer getProcessDefinitionVersion() {
		return processDefinitionVersion;
	}

	public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
		this.processDefinitionVersion = processDefinitionVersion;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	
	public Map<String, Object> getProcessVariables() {
		return processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static List<WorkflowProcessInstanceDto> map(List<ProcessInstance> input) {
		List<WorkflowProcessInstanceDto> result = new ArrayList<>();
		
		for(ProcessInstance processInstance : input) {
			result.add(new WorkflowProcessInstanceDto(processInstance));
		}
		
		return result;
	}
}