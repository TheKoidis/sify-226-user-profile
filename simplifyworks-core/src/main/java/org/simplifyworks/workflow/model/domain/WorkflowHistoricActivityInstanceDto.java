package org.simplifyworks.workflow.model.domain;

import java.util.Date;
import org.activiti.engine.history.HistoricActivityInstance;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class WorkflowHistoricActivityInstanceDto {

	private String assignee;
	private Date startTime;
	private Date endTime;
	private String activityName;
	private String processDefinitionId;

	public WorkflowHistoricActivityInstanceDto(HistoricActivityInstance historicActivityInstance) {
		assignee = historicActivityInstance.getAssignee();
		startTime = historicActivityInstance.getStartTime();
		endTime = historicActivityInstance.getEndTime();
		activityName = historicActivityInstance.getActivityName();
		processDefinitionId = historicActivityInstance.getProcessDefinitionId();
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

}
