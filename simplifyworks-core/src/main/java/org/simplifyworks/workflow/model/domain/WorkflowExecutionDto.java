package org.simplifyworks.workflow.model.domain;

import org.activiti.engine.runtime.Execution;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class WorkflowExecutionDto {

	private String id;
	private boolean suspended;
	private boolean ended;
	private String activityId;
	private String processInstanceId;
	private String parentId;
	private String tenantId;

	public WorkflowExecutionDto() {
	}

	public WorkflowExecutionDto(Execution execution) {
		this.id = execution.getId();
		this.suspended = execution.isSuspended();
		this.ended = execution.isEnded();
		this.activityId = execution.getActivityId();
		this.processInstanceId = execution.getProcessInstanceId();
		this.parentId = execution.getParentId();
		this.tenantId = execution.getTenantId();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}