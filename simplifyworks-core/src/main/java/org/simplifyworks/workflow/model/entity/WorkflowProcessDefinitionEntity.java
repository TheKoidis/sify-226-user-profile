package org.simplifyworks.workflow.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "CORE_WF_PROC_DEF_ENTITY", uniqueConstraints = @UniqueConstraint(columnNames = {"PROCESS_DEFINITION_KEY", "ENTITY_CLASS_NAME"}))
public class WorkflowProcessDefinitionEntity extends AbstractEntity {

	@NotNull
	@Size(max = 255)
	@Column(name = "PROCESS_DEFINITION_KEY", nullable = false, length = 255)
	private String processDefinitionKey;
	@NotNull
	@Size(max = 255)
	@Column(name = "ENTITY_CLASS_NAME", nullable = false, length = 255)
	private String entityClassName;

	public String getProcessDefinitionKey() {
		return processDefinitionKey;
	}

	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}

	public String getEntityClassName() {
		return entityClassName;
	}

	public void setEntityClassName(String entityClassName) {
		this.entityClassName = entityClassName;
	}

}
