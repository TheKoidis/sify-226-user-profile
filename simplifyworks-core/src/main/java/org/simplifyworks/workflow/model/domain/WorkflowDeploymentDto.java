package org.simplifyworks.workflow.model.domain;

import java.util.Date;

import org.activiti.engine.repository.Deployment;

public class WorkflowDeploymentDto {

	private String id;
	private String name;
	private Date deploymentTime;
	
	public WorkflowDeploymentDto() {
	}
	
	public WorkflowDeploymentDto(Deployment deployment) {
		this.id = deployment.getId();
		this.name = deployment.getName();
		this.deploymentTime = deployment.getDeploymentTime();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDeploymentTime() {
		return deploymentTime;
	}

	public void setDeploymentTime(Date deploymentTime) {
		this.deploymentTime = deploymentTime;
	}
}