package org.simplifyworks.workflow.model.domain;

import java.util.Date;
import org.activiti.engine.history.HistoricProcessInstance;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class WorkflowHistoricProcessInstanceDto {

	private Date startTime;
	private Date endTime;
	private String name;
	private String processDefinitionId;
	private String id;

	public WorkflowHistoricProcessInstanceDto(HistoricProcessInstance historicProcessInstance) {
		startTime = historicProcessInstance.getStartTime();
		endTime = historicProcessInstance.getEndTime();
		name = historicProcessInstance.getName();
		processDefinitionId = historicProcessInstance.getProcessDefinitionId();
		id = historicProcessInstance.getId();
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
