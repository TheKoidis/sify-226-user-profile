package org.simplifyworks.workflow.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class WorkflowProcessDefinitionEntityDto extends AbstractDto {

	private String processDefinitionKey;
	private String entityClassName;
	private WorkflowProcessDefinitionDto workflowProcessDefinition;

	public String getProcessDefinitionKey() {
		return processDefinitionKey;
	}

	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}

	public String getEntityClassName() {
		return entityClassName;
	}

	public void setEntityClassName(String entityClassName) {
		this.entityClassName = entityClassName;
	}

	public WorkflowProcessDefinitionDto getWorkflowProcessDefinition() {
		return workflowProcessDefinition;
	}

	public void setWorkflowProcessDefinition(WorkflowProcessDefinitionDto workflowProcessDefinition) {
		this.workflowProcessDefinition = workflowProcessDefinition;
	}

}
