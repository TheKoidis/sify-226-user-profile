/**
 * 
 */
package org.simplifyworks.core.service;

import org.springframework.transaction.support.TransactionSynchronization;

/**
 * Interface for after transaction commit synchronization component
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface AfterCommitTransactionSynchronization extends TransactionSynchronization {

	/**
	 * Executes task after commit (succesful).
	 * 
	 * @param task task to execute
	 */
	public void executeAfterCommit(Runnable task);
}
