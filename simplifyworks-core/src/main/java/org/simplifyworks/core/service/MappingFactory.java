/**
 * 
 */
package org.simplifyworks.core.service;

import java.io.IOException;

import org.elasticsearch.common.xcontent.XContentBuilder;

/**
 * Factory for creating index mapping
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface MappingFactory {

	/**
	 * Creates mapping for specified type
	 * 
	 * @param type type for mapping
	 * @return index mapping
	 * @throws IOException if mapping cannot be created (type structure is mismatched)
	 * @throws StackOverflowError if mapping cannot be created (circular dependency found)
	 */
	public XContentBuilder createMapping(Class<?> type) throws IOException;
}
