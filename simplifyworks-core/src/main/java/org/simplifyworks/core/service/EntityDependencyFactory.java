package org.simplifyworks.core.service;

import java.util.Map;
import java.util.Set;

import org.simplifyworks.core.model.domain.EntityDependency;

/**
 * Interface for entity dependency factory
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface EntityDependencyFactory {

	/**
	 * Initializes this factory
	 */
	public void init();
	
	/**
	 * Returns dependencies using dto class
	 * @param dtoClass dto class
	 * @return dependencies grouped by entity class
	 */
	public Map<Class<?>, Set<EntityDependency>> getDependencies(Class<?> dtoClass);
}
