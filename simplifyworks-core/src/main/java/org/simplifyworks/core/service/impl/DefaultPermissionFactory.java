/**
 * 
 */
package org.simplifyworks.core.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.PermissionFactory;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.domain.PermissionFor;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Default implementation of {@link PermissionFactory}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultPermissionFactory implements PermissionFactory {
	
	@Autowired
	private CoreRoleManager roleManager;
	
	@Override
	public List<CorePermission> createPermissions(Permission permission, AbstractEntity entity) {
		CoreOrganization organization;
		
		try {
			organization = StringUtils.isEmpty(permission.workplaceField()) ? null : (CoreOrganization) PropertyUtils.getProperty(entity, permission.workplaceField());
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			throw new CoreException(e);
		}
		
		Set<String> readRoleNames = permission.readRoles() == null ? Collections.emptySet() : new HashSet<>(Arrays.asList(permission.readRoles()));
		Set<String> writeRoleNames = permission.writeRoles() == null ? Collections.emptySet() : new HashSet<>(Arrays.asList(permission.writeRoles())); 
		Set<String> deleteRoleNames = permission.deleteRoles() == null ? Collections.emptySet() : new HashSet<>(Arrays.asList(permission.deleteRoles()));
		
		Set<String> allRoleNames = new HashSet<>();
		allRoleNames.addAll(readRoleNames);
		allRoleNames.addAll(writeRoleNames);
		allRoleNames.addAll(deleteRoleNames);
		
		List<CorePermission> permissions = new ArrayList<>();
		
		for(String roleName : allRoleNames) {
			permissions.add(createPermission(entity, roleName, organization, readRoleNames.contains(roleName), writeRoleNames.contains(roleName), deleteRoleNames.contains(roleName)));
		}
		
		return permissions;
	}
	
	private CorePermission createPermission(AbstractEntity entity, String roleName, CoreOrganization organization, boolean canRead, boolean canWrite, boolean canDelete) {
		CorePermission permission = new CorePermission();
		
		permission.setObjectType(entity.getClass().getName());
		permission.setObjectIdentifier(String.valueOf(entity.getId()));
		
		CoreRoleDto role = roleManager.searchUsingName(roleName);
		if(role == null) {
			throw new CoreException("Role with name '" + roleName + "' not found");
		}
		permission.setRole(roleManager.toEntity(role));
		permission.setOrganization(organization);
		
		permission.setCanRead(canRead);
		permission.setCanWrite(canWrite);
		permission.setCanDelete(canDelete);
		
		permission.setActive(true);
		permission.setPermissionFor(PermissionFor.ENTITY);
		
		return permission;
	}
}
