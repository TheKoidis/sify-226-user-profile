/**
 *
 */
package org.simplifyworks.core.service;

import java.util.List;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 * Interface for all read and write managers based on index
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface ReadWriteManager<T extends AbstractDto, E extends AbstractEntity> extends ReadManager<T, E> {

	/**
	 * Maps transfer object to entity
	 *
	 * @param mapped object
	 * @return entity created from object
	 */
	E toEntity(T object);

	/**
	 * Maps list of transfer objects to list of entities
	 *
	 * @param mapped objects
	 * @return list of entities created from list of objects
	 */
	List<E> toEntityList(List<T> objects);

	/**
	 * Creates and persists entity using transfer object
	 *
	 * @param mapped object
	 * @return mapped object
	 */
	T create(T object);

	/**
	 * Updates existing entity using transfer object
	 *
	 * @param object mapped object
	 * @return mapped object
	 */
	T update(T object);

	/**
	 * Removes existing entity using id
	 *
	 * @param id id
	 */
	void remove(long id);

	void createInitialPermissions(E entity);

	void updatePermissions(E entity);

	void removePermissions(E entity);

}
