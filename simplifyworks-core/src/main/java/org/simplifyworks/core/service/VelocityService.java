package org.simplifyworks.core.service;

import org.apache.velocity.context.Context;

/**
 * Created by Svanda on 13.8.2015.
 */
public interface VelocityService {
    String evaluate(Context context, String template);
}
