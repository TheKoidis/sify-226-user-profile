package org.simplifyworks.core.service.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.PropertyUtils;
import org.dozer.Mapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.EntityDependency;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.core.service.EntityDependencyFactory;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.EntityEvent.EntityEventOperation;
import org.simplifyworks.core.service.MappingFactory;
import org.simplifyworks.core.service.PermissionElement;
import org.simplifyworks.core.service.PermissionElementFactory;
import org.simplifyworks.core.service.ReadManager;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.service.CorePermissionManager;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class DefaultReadManager<T extends AbstractDto, E extends AbstractEntity> implements ReadManager<T, E> {

	public static final String INDEX_EVENT_METHOD_NAME_PREFIX = "on";
	public static final String INDEX_EVENT_METHOD_NAME_SUFFIX = "Event";

	protected static final Logger LOG = LoggerFactory.getLogger(DefaultReadManager.class);

	protected final Class<T> dtoClass;

	protected final Class<E> entityClass;

	@Autowired
	protected Mapper mapper;

	protected ObjectMapper jsonMapper;

	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;

	@Autowired
	protected Client client;

	@Autowired
	private ElasticsearchTemplate template;

	@Autowired
	private MappingFactory mappingFactory;

	@Autowired
	protected ElasticsearchService elasticsearchService;

	@Autowired
	protected PermissionElementFactory permissionElementFactory;

	@Autowired
	protected EntityDependencyFactory entityDependencyFactory;

	@Autowired
	protected CorePermissionManager permissionManager;

	@Autowired
	protected SecurityService securityService;

	@SuppressWarnings("unchecked")
	public DefaultReadManager() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		dtoClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
		entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];

		jsonMapper = new ObjectMapper();
		jsonMapper.setDateFormat(ElasticsearchService.DATE_TIME_FORMAT);
	}

	@Override
	public Class<T> getDtoClass() {
		return dtoClass;
	}

	@Override
	public Class<E> getEntityClass() {
		return entityClass;
	}

	@Override
	public T toDto(E entity) {
		return entity == null ? null : mapper.map(entity, dtoClass);
	}

	@Override
	public List<T> toDtoList(List<E> entities) {
		if (entities == null) {
			return null;
		}

		List<T> objects = new ArrayList<>();

		entities.stream().forEach((e) -> {
			objects.add(toDto(e));
		});

		return objects;
	}

	@Override
	/**
	 * Activiti needs separate method (different name than search) for search
	 * entity by ID.
	 */
	public T get(long id) {
		return search(id);
	}

	@Override
	public void modifySearchParameters(SearchParameters parameters) {		
	}
	
	@Override
	public T search(long id) {
		return elasticsearchService.search(dtoClass, id, AccessType.READ);
	}
	
	@Override
	public T searchForSingle(SearchParameters parameters) {
		modifySearchParameters(parameters);
		
		return elasticsearchService.searchForSingleResult(dtoClass, parameters, AccessType.READ);
	}

	@Override
	public List<T> search(SearchParameters parameters) {
		modifySearchParameters(parameters);
		
		return elasticsearchService.search(dtoClass, parameters, AccessType.READ);
	}

	@Override
	public List<T> searchWithoutSecurity(SearchParameters parameters) {
		modifySearchParameters(parameters);
		
		return elasticsearchService.search(dtoClass, parameters, null);
	}

	@Override
	public long count(SearchParameters parameters) {
		modifySearchParameters(parameters);
		
		return elasticsearchService.count(dtoClass, parameters, AccessType.READ);
	}

	@Override
	public void onApplicationEvent(EntityEvent event) {
		Map<Class<?>, Set<EntityDependency>> dependencies = entityDependencyFactory.getDependencies(dtoClass);

		Class<?> entityClass = event.getEntity().getClass();
		if (entityClass.equals(this.entityClass)) {
			reindexItself(event.getSource(), event.getOperation(), event.getEntity().getId());
		} else if (dependencies.containsKey(entityClass)) {
			BoolFilterBuilder filterBuilder = FilterBuilders.boolFilter();

			for (EntityDependency dependency : dependencies.get(entityClass)) {
				try {
					filterBuilder.should(FilterBuilders.termFilter(dependency.getPath(), PropertyUtils.getProperty(event.getEntity(), dependency.getInversePath())));
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					throw new CoreException(e);
				}
			}

			reindexChangedDtos(filterBuilder);
		} else if (entityClass.equals(CorePermission.class)) {
			if(!event.getOperation().equals(EntityEventOperation.REMOVE)){
				CorePermission permission = (CorePermission) event.getEntity();

				if (permission.getObjectType().equals(getEntityClass().getName())) {
					reindexItself(event.getSource(), EntityEventOperation.UPDATE, Long.parseLong(permission.getObjectIdentifier()));
				}
			}  	
		}
	}

	@Override
	@Transactional
	public void refresh() {
		ObjectMapper objMapper = new ObjectMapper();
		objMapper.setDateFormat(ElasticsearchService.DATE_TIME_FORMAT);
		String typeName = dtoClass.getSimpleName();

		try {
			XContentBuilder oldMapping = getOldMapping(typeName);
			XContentBuilder newMapping = mappingFactory.createMapping(dtoClass);

			// JSON tree comparison must be used
			// because of missing equals implementation in XContentBuilder
			if (oldMapping == null || !objMapper.readTree(newMapping.string()).equals(objMapper.readTree(oldMapping.string()))) {
				template.deleteType(ElasticsearchService.INDEX_NAME, typeName);
				template.putMapping(ElasticsearchService.INDEX_NAME, typeName, newMapping);

				reindexAll();

				if (LOG.isInfoEnabled()) {
					LOG.info("Mapping created/replaced for " + typeName);
				}
			} else if (LOG.isInfoEnabled()) {
				LOG.info("Mapping unchanged for " + typeName);
			}
		} catch (Throwable ex) {
			LOG.error(typeName + " is mismatched, mapping refresh skipped", ex);
		}
	}

	@SuppressWarnings("unchecked")
	private XContentBuilder getOldMapping(String typeName) throws IOException {
		if (template.typeExists(ElasticsearchService.INDEX_NAME, typeName)) {
			if (LOG.isInfoEnabled()) {
				LOG.info(typeName + " mapping exists in index");
			}

			return XContentFactory.jsonBuilder().map(template.getMapping(ElasticsearchService.INDEX_NAME, typeName));
		} else {
			if (LOG.isInfoEnabled()) {
				LOG.info(typeName + " mapping does not exist in index");
			}

			return null;
		}
	}

//	@Override
//	@Transactional
//	public long reindexAll() {
//		int batchSize = reindexAllBatchSize();
//		LOG.info("start reindex all '" + dtoClass.getSimpleName() + "; batchSize: " + batchSize);
//		int deleteCount = deleteAllIndexes(batchSize);
//		int indexCount = createAllIndexes(batchSize);
//		LOG.info("end reindex all '" + dtoClass.getSimpleName() + "': " + deleteCount + " delete req.; " + indexCount + " index req.");
//		return indexCount;
//	}
	@Override
	@Transactional
	public long reindexAll() {
		int batchSize = reindexAllBatchSize();
		LOG.info("start reindex all '" + dtoClass.getSimpleName() + "; batchSize: " + batchSize);
		int deleteCount = deleteAllIndexes(batchSize);
		int indexCount = createAllIndexes(batchSize);
		LOG.info("end reindex all '" + dtoClass.getSimpleName() + "': " + deleteCount + " delete req.; " + indexCount + " index req.");
		return indexCount;
	}

	/**
	 * number of records in the batch
	 *
	 * @return
	 */
	protected int reindexAllBatchSize() {
		return 5000;
	}

	/**
	 * delete all indexes
	 *
	 * @param batchSize
	 * @return
	 */
	private int deleteAllIndexes(int batchSize) {
		SearchHits searchHits;
		int count = 0;
		long time = 0;
		do {
			searchHits = client.prepareSearch(ElasticsearchService.INDEX_NAME).setTypes(dtoClass.getSimpleName()).setSize(batchSize).get().getHits();
			BulkRequestBuilder bulkRequest = client.prepareBulk();
			for (SearchHit searchHit : searchHits.getHits()) {
				bulkRequest.add(client.prepareDelete(searchHit.getIndex(), searchHit.getType(), searchHit.getId()));
				count++;
			}
			if (bulkRequest.numberOfActions() > 0) {
				BulkResponse resp = bulkRequest.setRefresh(true).get();
				time += resp.getTookInMillis();
				if (resp.hasFailures()) {
					LOG.warn("delete indexes with failures: " + resp.buildFailureMessage());
				} else {
					LOG.debug("deleted index 'page': " + (count / batchSize) + "; ms: " + resp.getTookInMillis());
				}
			}
		} while (searchHits.totalHits() > 0);
		LOG.info("Deleted all indexes '" + dtoClass.getSimpleName() + "': " + count + " delete req.; es. time: " + time + " ms");
		return count;
	}

	private Long getEntityRecCount() {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
		countQuery.select(criteriaBuilder.count(countQuery.from(entityClass)));
		Long countEnt = entityManager.createQuery(countQuery).getSingleResult();
		return countEnt;
	}

	/**
	 * create all indexes
	 *
	 * @param batchSize
	 * @return
	 */
	private int createAllIndexes(int batchSize) {
		Long countEnt = getEntityRecCount();
		LOG.info("rec. count '" + dtoClass.getSimpleName() + "': " + countEnt);

		int count = 0;
		long inxTime = 0;
		long sqlTime = 0;
		long dtoTime = 0;
		//
		if (countEnt > 0) {
			int curr = 1;
			Map<Long, List<PermissionElement>> permissionElementsForObjects = permissionElementFactory.createPermissionElements(entityClass, null);
			List<PermissionElement> defaultPermissionElements = createDefaultPermissionElements();
			BulkRequestBuilder bulkRequest;
			TypedQuery<E> typedQuery = entityManager.createQuery(allCriteriaQuery());
			//
			while (curr - 1 < countEnt.intValue()) {
				typedQuery.setFirstResult(curr - 1);
				typedQuery.setMaxResults(batchSize);
				long startSqlTime = new Date().getTime();
				List<E> entities = typedQuery.getResultList();
				sqlTime += new Date().getTime() - startSqlTime;
				curr += batchSize;
				//
				bulkRequest = client.prepareBulk();
				for (E entity : entities) {
					long startDtoTime = new Date().getTime();
					T dto = toDto(entity);
					computeAdditionalFieldsForIndex(dto, entity);
					dto.setPermissions(permissionElementsForObjects.containsKey(dto.getId()) ? permissionElementsForObjects.get(dto.getId()) : defaultPermissionElements);
					dtoTime += new Date().getTime() - startDtoTime;
					try {
						bulkRequest.add(client.prepareIndex(ElasticsearchService.INDEX_NAME, dtoClass.getSimpleName(), String.valueOf(dto.getId()))
								.setSource(jsonMapper.writeValueAsBytes(dto)));
					} catch (JsonProcessingException e) {
						throw new CoreException(e);
					}
					count++;
				}
				BulkResponse resp = bulkRequest.setRefresh(true).get();
				inxTime += resp.getTookInMillis();
				if (resp.hasFailures()) {
					LOG.warn("indexed with failures: " + resp.buildFailureMessage());
				} else {
					LOG.debug("idexed 'page': " + (count / batchSize) + "; ms: " + resp.getTookInMillis());
				}
			}
		}
		LOG.info("Created all indexes '" + dtoClass.getSimpleName() + "': " + count + " index req.; es. time: " + inxTime + " ms; sql time: " + sqlTime + " ms; dto time: " + dtoTime + " ms");
		return count;
	}

	private int addDeleteRequests(BulkRequestBuilder bulkRequest) {
		int counter = 0;
		SearchHits searchHits = client.prepareSearch(ElasticsearchService.INDEX_NAME).setTypes(dtoClass.getSimpleName()).setSize(Integer.MAX_VALUE).get().getHits();

		for (SearchHit searchHit : searchHits.getHits()) {
			bulkRequest.add(client.prepareDelete(searchHit.getIndex(), searchHit.getType(), searchHit.getId()));
			counter++;
		}

		return counter;
	}

	protected int addIndexRequests(BulkRequestBuilder bulkRequest) {
		int counter = 0;

		List<E> entities = loadEntities();
		if (!entities.isEmpty()) {
			Map<Long, List<PermissionElement>> permissionElementsForObjects = permissionElementFactory.createPermissionElements(entityClass, null);

			for (E entity : entities) {
				T dto = toDto(entity);
				computeAdditionalFieldsForIndex(dto, entity);

				dto.setPermissions(permissionElementsForObjects.containsKey(dto.getId()) ? permissionElementsForObjects.get(dto.getId()) : createDefaultPermissionElements());

				try {
					bulkRequest.add(client.prepareIndex(ElasticsearchService.INDEX_NAME, dtoClass.getSimpleName(), String.valueOf(dto.getId()))
							.setSource(jsonMapper.writeValueAsBytes(dto)));
				} catch (JsonProcessingException e) {
					throw new CoreException(e);
				}

				counter++;
			}
		}

		return counter;
	}

	/**
	 * @deprecated instead of this, use allCriteriaQuery() method
	 * @return
	 */
	protected List<E> loadEntities() {
		return entityManager.createQuery(allCriteriaQuery()).getResultList();
	}

	protected CriteriaQuery<E> allCriteriaQuery() {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		Root<E> from = criteriaQuery.from(entityClass);
		criteriaQuery = criteriaQuery.select(from);
		return criteriaQuery;
	}

	private List<PermissionElement> createDefaultPermissionElements() {
		PermissionElement element = new PermissionElement();

		element.setRoleName("admin");

		element.setCanRead(true);
		element.setCanUpdate(true);
		element.setCanRemove(true);

		return Collections.singletonList(element);
	}

	protected void computeAdditionalFieldsForIndex(T dto, E entity) {
	}

	protected void reindexChangedDtos(FilterBuilder builder) {
		elasticsearchService.search(dtoClass, builder).stream().forEach((T dto) -> {
			E entity = entityManager.find(entityClass, dto.getId());
			if (entity != null) {
				dto = toDto(entity);
				computeAdditionalFieldsForIndex(dto, entity);

				elasticsearchService.indexWithPermissions(dto, entityClass, dto.getId());
			}
		});
	}

	protected void reindexItself(Object source, EntityEventOperation operation, Long entityId) {
		if (this.getClass().equals(source.getClass())) {
			return;
		}

		switch (operation) {
			case CREATE:
			case UPDATE:
				E entity = entityManager.find(entityClass, entityId);
				T dto = toDto(entity);
				computeAdditionalFieldsForIndex(dto, entity);

				elasticsearchService.indexWithPermissions(dto, entityClass, entityId);
				break;
			case REMOVE:
				elasticsearchService.removeFromIndex(dtoClass, entityId);
				break;
			default:
				break;
		}
	}
	
	@Override
	public boolean isDefinitionValid(WorkflowProcessDefinitionDto processDefinition, T dto) {
		return true;
	}
}
