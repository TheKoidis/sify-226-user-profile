/**
 *
 */
package org.simplifyworks.core.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.AfterCommitTransactionSynchronization;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.EntityEvent.EntityEventOperation;
import org.simplifyworks.core.service.JpaService;
import org.simplifyworks.core.service.PermissionFactory;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class DefaultReadWriteManager<T extends AbstractDto, E extends AbstractEntity>
		extends DefaultReadManager<T, E> implements ReadWriteManager<T, E> {

	@Autowired
	protected JpaService jpaService;

	@Autowired
	private PermissionFactory permissionFactory;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	private AfterCommitTransactionSynchronization afterCommitTransactionSynchronization;

	@SuppressWarnings("unchecked")
	public DefaultReadWriteManager() {
		super();
	}

	@Override
	public E toEntity(T object) {
		return object == null ? null : mapper.map(object, entityClass);
	}

	@Override
	public List<E> toEntityList(List<T> objects) {
		if (objects == null) {
			return null;
		}

		List<E> entities = new ArrayList<>();
		objects.stream().forEach((e) -> {
			entities.add(toEntity(e));
		});

		return entities;
	}

	@Override
	@Transactional
	public T create(T object) {
		Assert.notNull(object, "object has to be filled");

		Permission permission = entityClass.getAnnotation(Permission.class);
		String[] requiredRoleNames = (permission == null ? new String[]{"admin"} : permission.createRoles());

		if (!securityService.hasAnyRole(requiredRoleNames)) {
			throw new CoreException(
					"Create failed. You don't have required roles \"" + Arrays.toString(requiredRoleNames) + "\"");
		}

		E entity = jpaService.create(toEntity(object));
		E newEntity = jpaService.refresh(entity);
		object.setId(newEntity.getId());

		createInitialPermissions(newEntity);

		T newDto = toDto(entity);
		afterCommitTransactionSynchronization.executeAfterCommit(new Runnable() {

			@Override
			public void run() {
				elasticsearchService.indexWithPermissions(newDto, entityClass, newEntity.getId());
				eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.CREATE, newEntity));
			}
		});

		return newDto;
	}

	@Override
	public void createInitialPermissions(E entity) {
		Permission annotation = AnnotationUtils.findAnnotation(entityClass, Permission.class);

		if (annotation != null) {
			for (CorePermission permission : permissionFactory.createPermissions(annotation, entity)) {
				permissionManager.create(permission);
			}
		}
	}

	@Override
	public void updatePermissions(E entity) {
		// @todo: like DefaultPermissionElementFactory.loadPermissions > ???
	}

	@Override
	public void removePermissions(E entity) {
		// @todo: like DefaultPermissionElementFactory.loadPermissions > delete ???
	}

	@Override
	@Transactional
	public T update(T object) {
		Assert.notNull(object, "object has to be filled");
		Assert.notNull(object.getId(), "id has to be filled");

		checkAccess(object.getId(), AccessType.UPDATE);

		E entity = jpaService.update(toEntity(object));
		E newEntity = jpaService.refresh(entity);
		// TODO add permissions using annotation Permission

		updatePermissions(newEntity);

		T newDto = toDto(newEntity);
		afterCommitTransactionSynchronization.executeAfterCommit(new Runnable() {

			@Override
			public void run() {
				elasticsearchService.indexWithPermissions(newDto, entityClass, newEntity.getId());
				eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.UPDATE, newEntity));
			}
		});

		return newDto;
	}

	@Override
	@Transactional
	public void remove(long id) {
		checkAccess(id, AccessType.DELETE);

		E entity = jpaService.remove(entityClass, id);

		removePermissions(entity);

		afterCommitTransactionSynchronization.executeAfterCommit(new Runnable() {

			@Override
			public void run() {
				elasticsearchService.removeFromIndex(dtoClass, id);
				eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.REMOVE, entity));
			}
		});
	}

	protected void checkAccess(long id, AccessType accessType) {
		if (!isAccessible(id, accessType)) {
			throw new CoreException("Record with id [" + id + "] does not exist or is not accessible");
		}
	}

	protected boolean isAccessible(long id, AccessType accessType) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("id", FilterOperator.EQUALS, Collections.singletonList(id)));

		return elasticsearchService.count(dtoClass, parameters, accessType) == 1;
	}
}
