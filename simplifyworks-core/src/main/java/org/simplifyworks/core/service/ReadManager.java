/**
 *
 */
package org.simplifyworks.core.service;

import java.util.List;

import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;
import org.springframework.context.ApplicationListener;

/**
 * Interface for all read-only managers based on index
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface ReadManager<T extends AbstractDto, E extends AbstractEntity> extends ApplicationListener<EntityEvent> {

	/**
	 * Returns type of managed objects
	 *
	 * @return type of managed objects
	 */
	public Class<T> getDtoClass();
	
	/**
	 * Returns type of managed entities
	 *
	 * @return type of managed entities
	 */
	Class<E> getEntityClass();

	/**
	 * Maps entity to transfer object
	 *
	 * @param mapped entity
	 * @return transfer object created from entity
	 */
	T toDto(E entity);
	
	/**
	 * Maps list of entities to list of transfer objects
	 *
	 * @param mapped entities
	 * @return list of transfer objects created from list of entities
	 */
	List<T> toDtoList(List<E> entities);

	/**
	 * Activiti needs separate method (different name than search) for search entity by ID.
	 */
	public T get(long id);

	public void modifySearchParameters(SearchParameters parameters);
	
	/**
	 * Finds single object (automatically adds security)
	 *
	 * @param id id of searched object
	 * @return object or null if does not exist
	 */
	public T search(long id);

	/**
	 * Finds single object (automatically adds security)
	 *
	 * @param parameters search parameters
	 * @return object or null if does not exist
	 */
	public T searchForSingle(SearchParameters parameters);
	
	/**
	 * Search for objects using parameters (automatically adds security)
	 *
	 * @param parameters search parameters
	 * @return all objects which satisfy parameters
	 */
	public List<T> search(SearchParameters parameters);

	/**
	 * Search for objects using parameters without security check
	 *
	 * @param parameters search parameters
	 * @return all objects which satisfy parameters
	 */
	public List<T> searchWithoutSecurity(SearchParameters parameters);

	/**
	 * Search for count of objects using parameters (automatically adds security)
	 *
	 * @param parameters search parameters
	 * @return count of all objects which satisfy parameters
	 */
	public long count(SearchParameters parameters);

	/**
	 * Refreshes invalid mapping and related records
	 */
	public void refresh();
	
	/**
	 * Rebuilds index for all objects
	 *
	 * @return number of updated objects
	 */
	public long reindexAll();

	/**
	 * Accepts events of {@link EntityEvent} type and refreshes index accordingly.
	 *
	 * @param event incoming event
	 */
	public void onApplicationEvent(EntityEvent event);
	
	public boolean isDefinitionValid(WorkflowProcessDefinitionDto processDefinition, T dto);
}
