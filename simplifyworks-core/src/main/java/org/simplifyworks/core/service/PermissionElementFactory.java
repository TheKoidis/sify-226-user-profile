package org.simplifyworks.core.service;

import java.util.List;
import java.util.Map;

/**
 * Factory that creates elementary permissions for indexed records using role and organization structure.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface PermissionElementFactory {
	
	/**
	 * Creates permission elements for specified object
	 * 
	 * @param objectType type of object
	 * @param objectIdentifier identifier of object (all objects are usedn if not present)
	 * @return map where keys represent identifiers and values represent list of permission elements (permissions are evaluated using role and organization structure)
	 */
	public Map<Long, List<PermissionElement>> createPermissionElements(Class<?> objectType, Long objectIdentifier);
}
