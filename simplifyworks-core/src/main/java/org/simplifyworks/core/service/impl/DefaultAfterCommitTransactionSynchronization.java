/**
 * 
 */
package org.simplifyworks.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.simplifyworks.core.service.AfterCommitTransactionSynchronization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Default implementation of {@link AfterCommitTransactionSynchronization}.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Component
class DefaultAfterCommitTransactionSynchronization extends TransactionSynchronizationAdapter implements AfterCommitTransactionSynchronization {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultAfterCommitTransactionSynchronization.class);
	private static final ThreadLocal<List<Runnable>> TASKS = new ThreadLocal<>();
	
	@Override
	public void executeAfterCommit(Runnable task) {		
		if(TransactionSynchronizationManager.isSynchronizationActive()) {
			LOG.debug("Transaction synchronization is active, task will be registered");
			
			List<Runnable> threadTasks = TASKS.get();
			
			if(threadTasks == null) {
				threadTasks = new ArrayList<>();
				TASKS.set(threadTasks);
				TransactionSynchronizationManager.registerSynchronization(this);
			}
			
			threadTasks.add(task);
			
			LOG.debug("Task successfully registered");
		} else {
			LOG.debug("Transaction synchronization is NOT active, task is going to be started immediately");
			
			task.run();
			
			LOG.debug("Task completed successfully");
		}
	}
	
	@Override
	public void afterCommit() {
		LOG.debug("Transaction commited successfully, tasks are going to be started");
		
		List<Runnable> threadTasks = TASKS.get();
		
		if(threadTasks != null) {
			for(Runnable task : threadTasks) {
				task.run();
			}
		}
		
		LOG.debug("All tasks completed successfully");
	}
	
	@Override
	public void afterCompletion(int status) {
		LOG.debug("Transaction completed, tasks are going to be removed");
		
		TASKS.remove();
	}
}