package org.simplifyworks.core.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.simplifyworks.core.model.domain.Dependencies;
import org.simplifyworks.core.model.domain.Dependency;
import org.simplifyworks.core.model.domain.EntityDependency;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.service.EntityDependencyFactory;
import org.simplifyworks.core.service.ReadManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Default implementation of {@link EntityDependencyFactory}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Component
public class DefaultEntityDependencyFactory implements EntityDependencyFactory {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultEntityDependencyFactory.class);
	
	@Autowired
	private ApplicationContext context;
	
	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;
	
	private Map<Class<?>, Class<?>> dtoToEntityMapping;
	private Map<Class<?>, Map<Class<?>, Set<EntityDependency>>> dependencies;
	
	@Override
	public void init() {
		if(LOG.isInfoEnabled()) {
			LOG.info("Initializing " + DefaultEntityDependencyFactory.class.getSimpleName());
		}
		
		createMapping();		
		createDependencies();
		
		if(LOG.isInfoEnabled()) {
			LOG.info(DefaultEntityDependencyFactory.class.getSimpleName() + " initialized successfully");
		}
	}
	
	/**
	 * Creates mapping from dtos to entities using definition of managers
	 */
	@SuppressWarnings("rawtypes")
	private void createMapping() {
		LOG.debug("Creating mapping from dtos to entities");
		
		dtoToEntityMapping = new HashMap<>();
		
		for(ReadManager manager : context.getBeansOfType(ReadManager.class).values()) {
			Class<?> dtoClass = manager.getDtoClass();
			
			do {
				dtoToEntityMapping.put(dtoClass, manager.getEntityClass());
			} while(!(dtoClass = dtoClass.getSuperclass()).equals(AbstractDto.class));
		}
		
		LOG.debug("Mapping from dtos to entities creates successfully");
	}
	
	/**
	 * Creates dependencies using definition of managers
	 */
	@SuppressWarnings("rawtypes")
	private void createDependencies() {
		LOG.debug("Creating dependencies from dtos to entities");
		
		dependencies = new HashMap<>();
		
		for(ReadManager manager : context.getBeansOfType(ReadManager.class).values()) {
			Class<?> dtoClass = manager.getDtoClass();
			
			dependencies.put(dtoClass, createDependencies(dtoClass));
		}
		
		LOG.debug("Dependencies from dtos to entities created successfully");
	}	
	
	/**
	 * Creates dependencies for specified dto
	 * 
	 * @param dtoClass dto class
	 * @return entity dependencies grouped by entity classes 
	 */
	private Map<Class<?>, Set<EntityDependency>> createDependencies(Class<?> dtoClass) {
		if(LOG.isDebugEnabled()) {
			LOG.debug("Creating dependencies for " + dtoClass.getName());
		}
		
		Map<Class<?>, Set<EntityDependency>> dependencies = new HashMap<>();		
		addImplicitDependencies(dependencies, dtoClass, "", new HashSet<>());		
		
		if(LOG.isDebugEnabled()) {
			LOG.debug("Dependencies for " + dtoClass.getName() + " created successfully ({})", dependencies);
		}
		
		return dependencies;
	}
	
	/**
	 * Creates and adds implicit dependencies to the set using specified dto class
	 * 
	 * @param dependencies set for adding dependencies
	 * @param dtoClass dto class
	 * @param path property path string (uses dot as separator)
	 * @param dtoClassStack stack of currently visited dto classes (to avoid cycles)
	 */
	private void addImplicitDependencies(Map<Class<?>, Set<EntityDependency>> dependencies, Class<?> dtoClass, String path, Set<Class<?>> dtoClassStack) {
		if(!Object.class.equals(dtoClass.getSuperclass())) {
			addImplicitDependencies(dependencies, dtoClass.getSuperclass(), path, dtoClassStack);
			addExplicitDependencies(dependencies, dtoClass, path);
		}
		
		for(Field field : dtoClass.getDeclaredFields()) {
			if(!Modifier.isStatic(field.getModifiers())) {			
				addImplicitDependencies(dependencies, field, path, dtoClassStack);
			}
		}
	}
	
	/**
	 * Creates and adds implicit dependencies to the set using specified field
	 * 
	 * @param dependencies set for adding dependencies
	 * @param field field to process
	 * @param path property path string (uses dot as separator)
	 * @param dtoClassStack stack of currently visited dto classes (to avoid cycles)
	 */
	private void addImplicitDependencies(Map<Class<?>, Set<EntityDependency>> dependencies, Field field, String path, Set<Class<?>> dtoClassStack) {
		Class<?> fieldTypeClass = getFieldTypeClass(field);
		
		if(fieldTypeClass != null && !dtoClassStack.contains(fieldTypeClass)) {
			dtoClassStack.add(fieldTypeClass);
			
			addElementaryImplicitDependencies(dependencies, fieldTypeClass, (path.isEmpty() ? "" : path + ".") + field.getName(), dtoClassStack);
			
			dtoClassStack.remove(fieldTypeClass);
		}
	}
	
	/**
	 * Returns field type class for field
	 * 
	 * @param field field to process
	 * @return field type if assignable from AbstractDto, generic type (must be assignable form AbstractDto) of field if assignable from collection 
	 */
	private Class<?> getFieldTypeClass(Field field) {
		Class<?> fieldClass = field.getType();
		
		if(AbstractDto.class.isAssignableFrom(fieldClass)) {
			return fieldClass;
		} else if(Collection.class.isAssignableFrom(fieldClass)) {
			Class<?> fieldTypeClass = (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
			
			if(AbstractDto.class.isAssignableFrom(fieldTypeClass)) {
				return fieldTypeClass;
			}
		}
		
		return null;
	}
	
	/**
	 * Adds elementary implicit dependencies to the set
	 * 
	 * @param dependencies set for adding dependencies
	 * @param dtoClass dto class
	 * @param path property path string (uses dot as separator)
	 * @param dtoClassStack stack of currently visited dto classes (to avoid cycles)
	 */
	private void addElementaryImplicitDependencies(Map<Class<?>, Set<EntityDependency>> dependencies, Class<?> dtoClass, String path, Set<Class<?>> dtoClassStack) {
		if(dtoToEntityMapping.containsKey(dtoClass)) {
			Class<?> entityClass = dtoToEntityMapping.get(dtoClass);
	
			if(!dependencies.containsKey(entityClass)) {
				dependencies.put(entityClass, new HashSet<>());
			}
					
			dependencies.get(entityClass).add(new EntityDependency(entityClass, path + ".id", "id"));
			
			addImplicitDependencies(dependencies, dtoClass, path, dtoClassStack);
		}		
	}
	
	/**
	 * Creates and adds explicit dependencies to the set using specified dto class
	 * 
	 * @param dependencies set for adding dependencies
	 * @param dtoClass dto class
	 */
	private void addExplicitDependencies(Map<Class<?>, Set<EntityDependency>> dependencies, Class<?> dtoClass, String path) {
		if(!Object.class.equals(dtoClass.getSuperclass())) {
			addExplicitDependencies(dependencies, dtoClass.getSuperclass(), path);
		}
		
		for(Dependency annotation : getAnnotations(dtoClass)) {
			Class<?> entityClass = annotation.entityClass();
			
			if(!dependencies.containsKey(entityClass)) {
				dependencies.put(entityClass, new HashSet<>());
			}
			
			dependencies.get(entityClass).add(new EntityDependency(entityClass, (path.isEmpty() ? "" : path + ".") + annotation.path(), annotation.inversePath()));
		}
	}
	
	/**
	 * Returns all dependency annotations for specified dto class
	 * 
	 * @param dtoClass dto class
	 * @return all dependency annotations
	 */
	private Set<Dependency> getAnnotations(Class<?> dtoClass) {
		Set<Dependency> annotations = new HashSet<>();
		
		if(dtoClass.isAnnotationPresent(Dependencies.class)) {
			annotations.addAll(Arrays.asList(dtoClass.getAnnotation(Dependencies.class).value()));
		}
		
		if(dtoClass.isAnnotationPresent(Dependency.class)) {
			annotations.add(dtoClass.getAnnotation(Dependency.class));
		}
		
		return annotations;
	}

	@Override
	public Map<Class<?>, Set<EntityDependency>> getDependencies(Class<?> dtoClass) {
		return dependencies.get(dtoClass);
	}
}
