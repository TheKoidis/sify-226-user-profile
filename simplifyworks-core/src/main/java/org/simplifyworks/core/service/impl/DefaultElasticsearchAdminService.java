package org.simplifyworks.core.service.impl;

import java.util.Collection;

import org.simplifyworks.core.service.ElasticsearchAdminService;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.core.service.ReadManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Default implementation of {@link ElasticsearchAdminService}
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultElasticsearchAdminService implements ElasticsearchAdminService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultElasticsearchAdminService.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ElasticsearchTemplate template;

    private final ObjectMapper jsonMapper;

    public DefaultElasticsearchAdminService() {
        jsonMapper = new ObjectMapper();
        jsonMapper.setDateFormat(ElasticsearchService.DATE_TIME_FORMAT);
    }

    @Override
    public void initIndex() {
    	if (!template.indexExists(ElasticsearchService.INDEX_NAME)) {
            template.createIndex(ElasticsearchService.INDEX_NAME, createIndexSettings());            
		}
    }
    
    private Object createIndexSettings() {
    	return new org.simplifyworks.MapBuilder()
    		.put("analysis")
    			.put("analyzer")
    				.put("without_accent")
    					.put("tokenizer", "keyword")
    					.put("filter", new Object[]{"standard", "lowercase_filter", "accent_filter"})
    				.close()
    			.close()
    			.put("filter")
    				.put("lowercase_filter")
    					.put("type", "lowercase")
    				.close()
    				.put("accent_filter")
    					.put("type", "asciifolding")
    					.put("preserve_original", true)
    				.close()
    			.close()
    		.close()
    	.build();
    }
    
    @SuppressWarnings("rawtypes")
    public void refreshAll() {
        Collection<ReadManager> managers = applicationContext.getBeansOfType(ReadManager.class).values();
        if (LOG.isInfoEnabled()) {
            LOG.info("Initialize index for " + managers.size() + " manager(s).");
        }
        
        for (ReadManager manager : managers) {
        	manager.refresh();
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void reindexAll() {
        Collection<ReadManager> managers = applicationContext.getBeansOfType(ReadManager.class).values();
        if (LOG.isInfoEnabled()) {
            LOG.info("Reindexing all [managers:" + managers.size() + "].");
        }
        for (ReadManager manager : managers) {
            long reindexed = manager.reindexAll();
            if (LOG.isInfoEnabled()) {
                LOG.info("Reindexed all [" + manager.getDtoClass().getSimpleName() + ":" + reindexed + "].");
            }
        }
    }
}