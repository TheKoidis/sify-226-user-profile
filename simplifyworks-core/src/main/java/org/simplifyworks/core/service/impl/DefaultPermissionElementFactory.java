package org.simplifyworks.core.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.service.PermissionElement;
import org.simplifyworks.core.service.PermissionElementFactory;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.model.entity.CorePermission_;
import org.simplifyworks.uam.service.OrganizationStructureService;
import org.simplifyworks.uam.service.RoleStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link PermissionElementFactory}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
class DefaultPermissionElementFactory implements PermissionElementFactory {

	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;

	@Autowired
	private RoleStructureService roleStructureService;

	@Autowired
	private OrganizationStructureService organizationStructureService;

	@Override
	public Map<Long, List<PermissionElement>> createPermissionElements(Class<?> objectType, Long objectIdentifier) {
		Map<Long, List<PermissionElement>> permissionElementsForObjects = new HashMap<>();
		Map<String, List<CorePermission>> permissionsForObjects = loadPermissionsForObjects(objectType, objectIdentifier);

		for (Map.Entry<String, List<CorePermission>> permissions : permissionsForObjects.entrySet()) {
			List<PermissionElement> permissionElements = new ArrayList<>();

			// iterate through object related permissions and create appropriate elementary permissions for indexed record
			for (CorePermission permission : permissions.getValue()) {
				addPermissionElements(permissionElements, permission);
			}

			permissionElementsForObjects.put(Long.parseLong(permissions.getKey()), new ArrayList<PermissionElement>(permissionElements));
		}

		if (objectIdentifier != null && !permissionElementsForObjects.containsKey(objectIdentifier)) {
			permissionElementsForObjects.put(objectIdentifier, new ArrayList<>());
			permissionElementsForObjects.get(objectIdentifier).addAll(createDefaultPermissionElements());
		}

		return permissionElementsForObjects;
	}

	private List<PermissionElement> createDefaultPermissionElements() {
		PermissionElement element = new PermissionElement();

		element.setRoleName("admin");

		element.setCanRead(true);
		element.setCanUpdate(true);
		element.setCanRemove(true);

		return Collections.singletonList(element);
	}

	private Map<String, List<CorePermission>> loadPermissionsForObjects(Class<?> objectType, Long objectIdentifier) {
		Map<String, List<CorePermission>> permissionsForObjects = new HashMap<>();

		for (CorePermission permission : loadPermissions(objectType, objectIdentifier)) {
			if (!permissionsForObjects.containsKey(permission.getObjectIdentifier())) {
				permissionsForObjects.put(permission.getObjectIdentifier(), new ArrayList<>());
			}

			permissionsForObjects.get(permission.getObjectIdentifier()).add(permission);
		}

		return permissionsForObjects;
	}

	private List<CorePermission> loadPermissions(Class<?> objectType, Long objectIdentifier) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CorePermission> criteria = cb.createQuery(CorePermission.class);
		Root<CorePermission> from = criteria.from(CorePermission.class);

		if (objectIdentifier == null) {
			criteria.where(cb.equal(from.get(CorePermission_.objectType), objectType.getName()));
		} else {
			criteria.where(cb.equal(from.get(CorePermission_.objectIdentifier), objectIdentifier), cb.equal(from.get(CorePermission_.objectType), objectType.getName()));
		}

		return entityManager.createQuery(criteria).getResultList();
	}

	/**
	 * Adds elementary permissions according to permission
	 *
	 * @param permissionElements target set for elementary permissions
	 * @param permission permission
	 */
	private void addPermissionElements(List<PermissionElement> permissionElements, CorePermission permission) {
		if (permission.getUser() != null) { // user permission
			addUserPermissionElements(permissionElements, permission);
		} else if (permission.getRole() != null) {
			if (permission.getOrganization() != null) { // role in organization permission
				addRoleInOrganizationPermissionElements(permissionElements, permission);
			} else { // role permission
				addRolePermissions(permissionElements, permission);
			}
		} else {
			throw new CoreException("Invalid permission " + permission);
		}
	}

	/**
	 * Adds user related elementary permissions using specified permission as
	 * template
	 *
	 * @param permissionElements target set for created elementary permissions
	 * @param permission user related permission
	 */
	private void addUserPermissionElements(List<PermissionElement> permissionElements, CorePermission permission) {
		PermissionElement indexRecordPermission = new PermissionElement();

		indexRecordPermission.setUserName(permission.getUser().getUsername());
		addValidFromAndValidTill(permission, indexRecordPermission);
		copyFlags(permission, indexRecordPermission);

		permissionElements.add(indexRecordPermission);
	}

	/**
	 * Adds role in organization related elementary permissions using specified
	 * permission as template
	 *
	 * @param permissionElements target set for created elementary permissions
	 * @param permission role in organization related permission
	 */
	private void addRoleInOrganizationPermissionElements(List<PermissionElement> permissionElements, CorePermission permission) {
		List<String> roleNames = findAllAncestorRoles(permission.getRole().getName());

		for (String roleName : roleNames) {
			List<String> organizationNames = findAllAncestorOrganizations(permission.getOrganization().getName());

			for (String organizationName : organizationNames) {
				PermissionElement indexRecordPermission = new PermissionElement();

				indexRecordPermission.setRoleName(roleName);
				indexRecordPermission.setOrganizationName(organizationName);
				addValidFromAndValidTill(permission, indexRecordPermission);
				copyFlags(permission, indexRecordPermission);

				permissionElements.add(indexRecordPermission);
			}
		}
	}

	/**
	 * Adds role related elementary permissions using specified permission as
	 * template
	 *
	 * @param permissionElements target set for created elementary permissions
	 * @param permission role related permission
	 */
	private void addRolePermissions(List<PermissionElement> permissionElements, CorePermission permission) {
		List<String> roleNames = findAllAncestorRoles(permission.getRole().getName());

		for (String roleName : roleNames) {
			PermissionElement indexRecordPermission = new PermissionElement();

			indexRecordPermission.setRoleName(roleName);
			addValidFromAndValidTill(permission, indexRecordPermission);
			copyFlags(permission, indexRecordPermission);

			permissionElements.add(indexRecordPermission);
		}
	}

	/**
	 * Copies access flags from permissions (source) to elementary permission
	 * (target)
	 *
	 * @param source permission
	 * @param target elementary permission
	 */
	private void copyFlags(CorePermission source, PermissionElement target) {
		target.setCanRead(source.getCanRead());
		target.setCanUpdate(source.getCanWrite());
		target.setCanRemove(source.getCanDelete());
	}

	/**
	 * Add date of valid to permission
	 *
	 * @param permission permission
	 * @param permissionElement elementary permission
	 */
	private void addValidFromAndValidTill(CorePermission permission, PermissionElement permissionElement) {
		if (permission.getValidFrom() != null) {
			permissionElement.setValidFrom(permission.getValidFrom());
		}
		if (permission.getValidTill() != null) {
			permissionElement.setValidTill(permission.getValidTill());
		}
	}

	/**
	 * Finds all ancestor role names
	 *
	 * @param roleName name of role where to start searching
	 * @return all ancestor role names (including roleName)
	 */
	private List<String> findAllAncestorRoles(String roleName) {
		return roleStructureService.findAllAncestorRoles(roleName);
	}

	/**
	 * Finds all ancestor organization names
	 *
	 * @param organizationName name of organization where to start searching
	 * @return all ancestor organization names (including organizationName)
	 */
	private List<String> findAllAncestorOrganizations(String organizationName) {
		return organizationStructureService.findAllAncestorOrganizations(organizationName);
	}
}
