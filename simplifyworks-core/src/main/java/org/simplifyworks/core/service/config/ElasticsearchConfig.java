package org.simplifyworks.core.service.config;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.collect.ImmutableList;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.NodeBuilder;
import org.simplifyworks.core.exception.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * Elasticsearch configuration
 *
 * @author Radek Tomiška <tomiska@simplifyworks.org>
 * @author Jirka Pech <pech@simplifyworks.org>
 */
@Configuration
@EnableConfigurationProperties(ElasticsearchProperties.class)
public class ElasticsearchConfig implements DisposableBean {

	private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchConfig.class);

	// TODO: Implement ElasticsearchProperties
	// @Autowired
	// private ElasticsearchProperties properties;
	@Autowired
	private Environment env;

	@Value("${spring.data.elasticsearch.cluster-nodes:}")
	private String localNodes;
	private String envNodes;

	@Value("${spring.data.elasticsearch.path.data:}")
	private String localPathData;

	@Value("${spring.data.elasticsearch.path.logs:}")
	private String localPathLogs;

	@Value("${spring.data.elasticsearch.index.number_of_shards:}")
	private String localConfigShards;
	private Byte localShards;

	@Value("${spring.data.elasticsearch.index.number_of_replicas:}")
	private String localConfigReplicas;
	private Byte localReplicas;

	@Value("${spring.data.elasticsearch.transport.tcp.port:}")
	private String localConfigPort;
	private Integer localPort;

	private Client client;

	// TODO: Allow overwriting of the cluster name
	private final String clusterName = "SimplifyworksES";

	private Byte getShards(String aNumericValue) {
		try {
			return (!StringUtils.isBlank(aNumericValue) ? (Byte.valueOf(aNumericValue)) : (1));
		} catch (NumberFormatException e) {
			LOG.warn("Number of local shards should be ... a number. Significantly small. Positive.");
			return (1);
		}
	}

	private Byte getReplicas(String aNumericValue) {
		try {
			return (!StringUtils.isBlank(aNumericValue) ? (Byte.valueOf(aNumericValue)) : (1));
		} catch (NumberFormatException e) {
			LOG.warn("Number of local replicas should be ... a number. Significantly small. Positive.");
			return (1);
		}
	}

	private Integer getPort(String aNumericValue) {
		try {
			return (!StringUtils.isBlank(aNumericValue) ? (Integer.valueOf(aNumericValue)) : (9300));
		} catch (NumberFormatException e) {
			LOG.warn("Elasticsearch can't bind to transport port " + aNumericValue);
			return (9300);
		}
	}

	@Override
	public void destroy() throws Exception {
		LOG.debug("Destroying the esClient.");
		if (this.client != null) {
			try {
				if (this.client != null) {
					this.client.close();
				}
			} catch (final Exception ex) {
				LOG.error("Something went terribly wrong when destroying the esClient:" + ex);
			}
		}
	}

	@Bean
	public Client esClient() throws Exception {
		if (StringUtils.isBlank(localNodes)) {
			envNodes = env.getProperty("elasticsearch.cluster-nodes");
			if (!StringUtils.isBlank(envNodes)) {
				localNodes = envNodes;
			}
		}

		if (StringUtils.isBlank(localNodes)) {
			//  embedded client
			LOG.warn("Starting embedded elesticsearch server, please configure ElasticSearch server before production usage [parameter: spring.data.elasticsearch.cluster-nodes]");
			if (StringUtils.isBlank(localPathData)) {
				localPathData = (!StringUtils.isBlank(env.getProperty("elasticsearch.path"))) ? (env.getProperty("elasticsearch.path")) : ("/tmp/localES/data");
			}
			if (StringUtils.isBlank(localPathLogs)) {
				localPathLogs = (!StringUtils.isBlank(env.getProperty("elasticsearch.logs"))) ? (env.getProperty("elasticsearch.logs")) : ("/tmp/localES/logs");
			}
			localShards = (StringUtils.isBlank(localConfigShards)) ? (getShards(env.getProperty("elasticsearch.shards"))) : (getShards(localConfigShards));
			localReplicas = (StringUtils.isBlank(localConfigReplicas)) ? (getReplicas(env.getProperty("elasticsearch.replicas"))) : (getReplicas(localConfigReplicas));
			localPort = (StringUtils.isBlank(localConfigPort)) ? (getPort(env.getProperty("elasticsearch.port"))) : (getPort(localConfigPort));

			// create settings for the local instance and get client
			this.client = NodeBuilder.nodeBuilder().clusterName(clusterName).local(true)
							.settings(ImmutableSettings.settingsBuilder()
											.put("path.logs", localPathLogs)
											.put("path.data", localPathData)
											.put("transport.tcp.port", localPort)
											.put("index.number_of_shards", localShards)
											.put("index.number_of_replicas", localReplicas)
											.build()).node().client();
		} else {
			TransportClient transportClient;
			final String[] splitNodes = localNodes.split(",");
			try {
				transportClient = new TransportClient(ImmutableSettings.settingsBuilder()
								.put("client.transport.ignore_cluster_name", true)
								.put("client.transport.sniff", false)
								//asi ne go .put("client.transport.sniff", true).put("sniffOnConnectionFault", true).put("maxRetries", 5)
								.build());

				for (String server : splitNodes) {
					String[] serverPort = server.trim().split(":");
					transportClient.addTransportAddress(new InetSocketTransportAddress(serverPort[0], Integer.valueOf(serverPort[1])));
					LOG.info("Added transport client node on [" + serverPort[0] + ":" + serverPort[1] + "].");
				}
			} catch (ElasticsearchException | NumberFormatException | IndexOutOfBoundsException ex) {
				throw new CoreException("ElasticSearch nodes [" + localNodes + "] are not properly configured (use syntax: server1:port,server2:port). Please fix the configuration file and restart the application.", ex);
			}

			final ImmutableList<DiscoveryNode> currentNodes = transportClient.connectedNodes();
			if (currentNodes.isEmpty()) {
				final String errorMessage = "Error connecting to the elasticsearch cluster.";
				transportClient.close();
				LOG.error(errorMessage);
				throw new RuntimeException(errorMessage);
			} else if (currentNodes.size() < splitNodes.length) {
				LOG.warn((splitNodes.length - currentNodes.size()) + " nodes(s) are not connected, you may experience performance issues.");
			}
			this.client = transportClient;
		}

		return (this.client);
	}

	@Bean
	public ElasticsearchTemplate elasticsearchTemplate() throws Exception {
		return new ElasticsearchTemplate(esClient());
	}
}
