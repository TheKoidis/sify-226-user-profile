package org.simplifyworks.core.service;

import java.util.List;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.entity.CorePermission;

/**
 * Factory that creates permissions using annotation {@link Permission}.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface PermissionFactory {

	/**
	 * Creates permissions using annotation and entity
	 * 
	 * @param permission annotation
	 * @param entity entity
	 * @return permissions
	 */
	public List<CorePermission> createPermissions(Permission permission, AbstractEntity entity);
}
