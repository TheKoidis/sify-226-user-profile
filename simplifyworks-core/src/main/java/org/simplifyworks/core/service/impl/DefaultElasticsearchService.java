package org.simplifyworks.core.service.impl;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.count.CountRequestBuilder;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterGroup;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.SortValue;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.core.service.PermissionElementFactory;
import org.simplifyworks.core.util.EntityUtils;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.elasticsearch.action.ActionRequestBuilder;

/**
 * Default implementation of {@link ElasticsearchService}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
@DependsOn("defaultElasticsearchAdminService")
public class DefaultElasticsearchService implements ElasticsearchService {

	protected static final Logger LOG = LoggerFactory.getLogger(DefaultElasticsearchService.class);

	@Autowired
	private Client client;

	@Autowired
	private SecurityService securityService;

	@Autowired
	protected PermissionElementFactory permissionElementFactory;

	protected final ObjectMapper jsonMapper;

	public DefaultElasticsearchService() {
		jsonMapper = new ObjectMapper();
		jsonMapper.setDateFormat(DATE_TIME_FORMAT);
	}

	@Override
	public <T extends AbstractDto> void indexWithPermissions(T object, Class<?> objectType, long objectIdentifier) {
		Assert.notNull(object, "object is required");
		Assert.notNull(object.getId(), "object.id is required");
		Assert.notNull(objectType, "objectType is required");

		try {
			object.setPermissions(permissionElementFactory.createPermissionElements(objectType, objectIdentifier).get(objectIdentifier));
			int i = 0;
			while (i < 3) {
				i++;
				try {
					client.prepareIndex(INDEX_NAME, object.getClass().getSimpleName(), String.valueOf(object.getId()))
									.setSource(jsonMapper.writeValueAsBytes(object))
									.setRefresh(true)
									.get();
					break;
				} catch (org.elasticsearch.client.transport.NoNodeAvailableException ex) {
					if (i < 3) {
						LOG.info("es-client not available > wait to next try (" + i + ")");
						try {
							Thread.sleep(5000);
							continue;
						} catch (InterruptedException ie) {
							LOG.error("es-client request interrupted", ie);
							throw new CoreException(ie);
						}
					} else {
						throw new CoreException(ex);
					}
				}
			}
		} catch (ElasticsearchException | JsonProcessingException e) {
			throw new CoreException(e);
		}
	}

	@Override
	public <T extends AbstractDto> void removeFromIndex(Class<T> type, long id) {
		Assert.notNull(type, "type is required");
		int i = 0;
		while (i < 3) {
			i++;
			try {
				client.prepareDelete(INDEX_NAME, type.getSimpleName(), String.valueOf(id))
								.setRefresh(true)
								.get();
				break;
			} catch (org.elasticsearch.client.transport.NoNodeAvailableException ex) {
				if (i < 3) {
					LOG.info("es-client not available > wait to next try (" + i + ")");
					try {
						Thread.sleep(5000);
						continue;
					} catch (InterruptedException ie) {
						LOG.error("es-client request interrupted", ie);
						throw new CoreException(ie);
					}
				} else {
					throw new CoreException(ex);
				}
			}
		}
	}

	@Override
	public <T extends AbstractDto> T search(Class<T> type, long id, AccessType accessType) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("id", FilterOperator.EQUALS, Collections.singletonList(id)));

		return searchForSingleResult(type, parameters, Collections.emptyMap(), accessType);
	}

	@Override
	public <T extends AbstractDto> T searchForSingleResult(Class<T> type, SearchParameters parameters, AccessType accessType) {
		return searchForSingleResult(type, parameters, Collections.emptyMap(), accessType);
	}

	@Override
	public <T extends AbstractDto> T searchForSingleResult(Class<T> type, SearchParameters parameters, Map<FilterValue, FilterBuilder> customFilterMapping, AccessType accessType) {
		List<T> result = search(type, parameters, customFilterMapping, accessType);

		switch (result.size()) {
			case 0:
				return null;
			case 1:
				return result.get(0);
			default:
				throw new CoreException("More results found");
		}
	}

	@Override
	public <T extends AbstractDto> List<T> search(Class<T> type, SearchParameters parameters, AccessType accessType) {
		return search(type, parameters, Collections.emptyMap(), accessType);
	}

	@Override
	public <T extends AbstractDto> List<T> search(Class<T> type, SearchParameters searchParameters, Map<FilterValue, FilterBuilder> customFilterMapping, AccessType accessType) {
		SearchRequestBuilder searchRequestBuilder = client.prepareSearch(INDEX_NAME).setTypes(type.getSimpleName());

		// result limits
		searchRequestBuilder.setFrom(searchParameters.getRange() != null ? searchParameters.getRange().getFirstRow() : 0);
		searchRequestBuilder.setSize(searchParameters.getRange() != null ? searchParameters.getRange().getRows() : DEFAULT_RESULTS_COUNT);

		// ordering
		for (SortValue sortValue : searchParameters.getSorts().values()) {
			PropertyDescriptor propertyDescriptor = EntityUtils.getPropertyDescriptor(type, sortValue.getPropertyName());

			String field = sortValue.getPropertyName();
			if (String.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
				field = field + ".raw";
			} else if(BigInteger.class.isAssignableFrom(propertyDescriptor.getPropertyType()) || BigDecimal.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
				field = field + ".double";
			}

			if (sortValue.getSortOrder() == null || sortValue.getSortOrder() == org.simplifyworks.core.model.domain.SortOrder.ASC) {
				searchRequestBuilder.addSort(field, SortOrder.ASC);
			} else {
				searchRequestBuilder.addSort(field, SortOrder.DESC);
			}
		}

		// criteria
		FilterBuilder filter = createFilter(type, searchParameters, customFilterMapping, accessType);

		return search(type, filter, searchRequestBuilder);
	}

	@Override
	public <T extends AbstractDto> List<T> search(Class<T> type, FilterBuilder builder, AccessType accessType) {
		return search(type, FilterBuilders.andFilter(createSecurityFilter(accessType), builder));
	}

	@Override
	public <T extends AbstractDto> List<T> search(Class<T> type, FilterBuilder filter) {
		SearchRequestBuilder searchRequestBuilder = client.prepareSearch(INDEX_NAME).setTypes(type.getSimpleName());

		// result limits
		searchRequestBuilder.setSize(DEFAULT_RESULTS_COUNT);

		return search(type, filter, searchRequestBuilder);
	}

	private <T extends AbstractDto> List<T> search(Class<T> type, FilterBuilder filter, SearchRequestBuilder searchRequestBuilder) {
		//filter = checkConcept(filter);
		if (filter != null) {
			searchRequestBuilder.setPostFilter(filter);
		}
		SearchResponse scrollResp = (SearchResponse) tryThreeTimes(searchRequestBuilder);
		List results = Lists.newArrayList();
		if (scrollResp != null) {
			for (SearchHit hit : scrollResp.getHits().getHits()) {
				try {
					// TODO: question is ... return (and store) data in index or read results from database
					results.add(jsonMapper.readValue(hit.getSourceAsString(), type));
				} catch (IOException ex) {
					LOG.error("Searched result [" + hit.getId() + "] cant be converted to dto [" + type.getSimpleName() + "]", ex);
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Paged search result - [" + results.size() + "] records of type [" + type.getSimpleName() + "]");
		}
		return results;
	}

	@Override
	public <T extends AbstractDto> long count(Class<T> type, SearchParameters parameters, AccessType accessType) {
		return count(type, parameters, Collections.emptyMap(), accessType);
	}

	@Override
	public <T extends AbstractDto> long count(Class<T> type, SearchParameters parameters, Map<FilterValue, FilterBuilder> customFilterMapping, AccessType accessType) {
		CountRequestBuilder countRequestBuilder = client.prepareCount(INDEX_NAME).setTypes(type.getSimpleName());
		FilterBuilder filter = createFilter(type, parameters, customFilterMapping, accessType);
		//filter = checkConcept(filter);
		if (filter != null) {
			countRequestBuilder.setQuery(QueryBuilders.constantScoreQuery(filter));
		}
		CountResponse response = (CountResponse) tryThreeTimes(countRequestBuilder);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Counted [" + response.getCount() + "] records of type [" + type.getSimpleName() + "]");
		}
		return response.getCount();
	}

	@Override
	public <T extends AbstractDto> long count(Class<T> type, FilterBuilder builder, AccessType accessType) {
		return count(type, FilterBuilders.andFilter(createSecurityFilter(accessType), builder));
	}

	@Override
	public <T extends AbstractDto> long count(Class<T> type, FilterBuilder filter) {
		CountRequestBuilder countRequestBuilder = client.prepareCount(INDEX_NAME).setTypes(type.getSimpleName());
		if (filter != null) {
			countRequestBuilder.setQuery(QueryBuilders.constantScoreQuery(filter));
		}
		CountResponse response = (CountResponse) tryThreeTimes(countRequestBuilder);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Counted [" + response.getCount() + "] records of type [" + type.getSimpleName() + "]");
		}
		return response.getCount();
	}

	private Object tryThreeTimes(ActionRequestBuilder req) {
		Object actionGet = null;
		int i = 0;
		while (i < 3) {
			i++;
			try {
				actionGet = req.execute().actionGet();
				break;
			} catch (org.elasticsearch.client.transport.NoNodeAvailableException ex) {
				if (i < 3) {
					LOG.info("es-client not available > wait to next try (" + i + ")");
					try {
						Thread.sleep(5000);
						continue;
					} catch (InterruptedException ie) {
						LOG.error("es-client request interrupted", ie);
						throw new CoreException(ie);
					}
				} else {
					throw new CoreException(ex);
				}
			}
		}
		return actionGet;
	}

	/*
	 private FilterBuilder checkConcept(FilterBuilder filter) {
	 if (IConceptEntity.class.isAssignableFrom(dtoClass)) {
	 OrFilterBuilder concept = FilterBuilders.orFilter(
	 FilterBuilders.termFilter(IConceptEntity.CONCEPT_AUTHOR_FIELD_NAME, securityService.getUsername()),
	 FilterBuilders.termFilter(IConceptEntity.CONCEPT_FIELD_NAME, false)
	 );
	 if (filter != null) {
	 filter = FilterBuilders.andFilter(filter, concept);
	 } else {
	 filter = concept;
	 }
	 }
	 return filter;
	 }*/

	/**
	 * Return search query bulded from parameters
	 *
	 * @param <T>
	 * @param type
	 * @param searchParameters
	 * @param customFilterMapping
	 * @param accessType
	 * @return
	 */
	protected <T extends AbstractDto> FilterBuilder createFilter(Class<T> type, SearchParameters searchParameters, Map<FilterValue, FilterBuilder> customFilterMapping, AccessType accessType) {
		Assert.notNull(searchParameters, "searchParameters has to be filled");

		BoolFilterBuilder filterAll = FilterBuilders.boolFilter();

		// security
		if (accessType != null && securityService.isAuthenticated()) {			 // TODO do not check when invoked by system
			filterAll.must(createSecurityFilter(accessType));
		} else {
			filterAll.must(FilterBuilders.existsFilter("id"));
		}

		//		filterAll.must(FilterBuilders.existsFilter("id"));
		if (!searchParameters.isEmptyFilters()) {
			// full text
			if (StringUtils.isNotBlank(searchParameters.getFulltext())) {
				filterAll.must(FilterBuilders.queryFilter(QueryBuilders.matchQuery("_all", searchParameters.getFulltext())));
			}

			// other filters
			for (FilterGroup filterGroup : searchParameters.getFilterGroups().values()) {
				if (filterGroup.isEmpty()) {
					continue;
				}
				BoolFilterBuilder filterPart = FilterBuilders.boolFilter();
				for (FilterValue filterValue : filterGroup.getFilters().values()) {
					if (filterValue.isEmpty()) {
						continue;
					}
					if (filterGroup.getGroupOperator().equals(FilterGroup.GroupOperatorType.AND)) { // TODO: Pro zpetnou kompatibilitu - defaultni skupina neumi or => pridat operator do skupiny
						filterPart.must(createFieldFilter(type, filterValue, customFilterMapping));
					} else {
						filterPart.should(createFieldFilter(type, filterValue, customFilterMapping));
					}
				}
				filterAll.must(filterPart);
			}
		}

		return filterAll;
	}

	/**
	 * Search query for single field
	 *
	 * @param <T>
	 * @param type
	 * @param filterValue
	 * @param customFilterMapping
	 * @return
	 */
	protected <T extends AbstractDto> FilterBuilder createFieldFilter(Class<T> type, FilterValue filterValue, Map<FilterValue, FilterBuilder> customFilterMapping) {
		return customFilterMapping.containsKey(filterValue) ? customFilterMapping.get(filterValue) : filterValue.getOperator().createFilter(type, filterValue);
	}

	private FilterBuilder createSecurityFilter(AccessType accessType) {
		BoolFilterBuilder builder = FilterBuilders.boolFilter();
		Date now = new Date(); // TODO universal date creation bean

		builder.should(FilterBuilders.boolFilter()
						.must(FilterBuilders.termFilter(accessType.getPropertyName(), true))
						.must(FilterBuilders.termFilter("permissions.userName.raw", securityService.getUsername()))
						.must(createValidityFilter(now))
                );

		for (GrantedAuthority authority : securityService.getAuthentication().getAuthorities()) {
			if (authority instanceof RoleOrganizationGrantedAuthority) {
				RoleOrganizationGrantedAuthority defaultGrantedAuthority = (RoleOrganizationGrantedAuthority) authority;
				String roleName = defaultGrantedAuthority.getRoleName();
				String organizationName = defaultGrantedAuthority.getOrganizationName();

				FilterBuilder organizationFilter = FilterBuilders.notFilter(FilterBuilders.existsFilter("permissions.organizationName.raw"));

				if (organizationName != null) {
                                    organizationFilter = FilterBuilders.orFilter(organizationFilter, FilterBuilders.termFilter("permissions.organizationName.raw", organizationName));
				}

				builder.should(FilterBuilders.boolFilter()
								.must(FilterBuilders.termFilter(accessType.getPropertyName(), true))
								.must(FilterBuilders.termFilter("permissions.roleName.raw", roleName))
								.must(createValidityFilter(now))
								.must(organizationFilter));
			}
		}

		return FilterBuilders.nestedFilter("permissions", builder);
	}

	private FilterBuilder createValidityFilter(Date now) {
		FilterBuilder emptyValidFrom = FilterBuilders.notFilter(FilterBuilders.existsFilter("permissions.validFrom"));
		FilterBuilder validFromBefore = FilterBuilders.rangeFilter("permissions.validFrom").to(ElasticsearchService.DATE_TIME_FORMAT.format(now));

		FilterBuilder emptyValidTill = FilterBuilders.notFilter(FilterBuilders.existsFilter("permissions.validTill"));
		FilterBuilder validTillAfter = FilterBuilders.rangeFilter("permissions.validTill").from(ElasticsearchService.DATE_TIME_FORMAT.format(now));

		return FilterBuilders.andFilter(FilterBuilders.orFilter(emptyValidFrom, validFromBefore), FilterBuilders.orFilter(emptyValidTill, validTillAfter));
	}
}
