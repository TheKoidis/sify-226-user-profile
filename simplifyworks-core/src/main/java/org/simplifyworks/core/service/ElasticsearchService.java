/**
 * 
 */
package org.simplifyworks.core.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.elasticsearch.index.query.FilterBuilder;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * Service for common tasks with ES index
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface ElasticsearchService {

	// TODO name of index should be in different place
	public static final String INDEX_NAME = "dto";
	
	/**
	 * Common date format used by index
	 */
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
	
	/**
	 * Common date and time format used by index
	 * 
	 * More info here: basic_date_time https://www.elastic.co/guide/en/elasticsearch/reference/1.6/mapping-date-format.html
	 */
	public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMdd'T'HHmmss.SSSZ");
	
	/**
	 * Default count of results (applied when not present in request)
	 */
	public static final int DEFAULT_RESULTS_COUNT = 500;
		
	/**
	 * Indexes object with permissions (permissions are automatically searched and computed).
	 * 
	 * This method does create or update depending on id attribute of object.
	 * This method does NOT any security checks.
	 * 
	 * @param object object to index
	 * @param objectType type of object used for permission search
	 * @param objectIdentifier identifier of object used for permission search
	 * @see PermissionElementFactory
	 */
	public <T extends AbstractDto> void indexWithPermissions(T object, Class<?> objectType, long objectIdentifier);
	
	/**
	 * Removes object from index.
	 * 
	 * This method does NOT any security checks.
	 * 
	 * @param type type of removed object
	 * @param id id of removed object
	 */
	public <T extends AbstractDto> void removeFromIndex(Class<T> type, long id);
		
	/**
	 * Searches for object.
	 * 
	 * This method applies current user permissions in search filter.
	 * 
	 * @param type type of object
	 * @param id id of object
	 * @param accessType type of required access to object
	 * @return specified object or null if not accessible (i.e. if does not exist or is not visible to current user)
	 */
	public <T extends AbstractDto> T search(Class<T> type, long id, AccessType accessType);
	
	/**
	 * Searches for object using parameters.
	 * 
	 * This method applies current user permissions in search filter.
	 * This method may throw exception when more objects are found.
	 * 
	 * @param type type of object
	 * @param parameters search parameters
	 * @param accessType type of required access to object
	 * @return specified object or null if not accessible (i.e. if does not exist or is not visible to current user)
	 */
	public <T extends AbstractDto> T searchForSingleResult(Class<T> type, SearchParameters parameters, AccessType accessType);
	
	/**
	 * Searches for object using parameters.
	 * 
	 * This method applies current user permissions in search filter.
	 * This method may throw exception when more objects are found.
	 * 
	 * @param type type of object
	 * @param parameters search parameters
	 * @param customFilterMapping custom mapping for filter values
	 * @param accessType type of required access to object
	 * @return specified object or null if not accessible (i.e. if does not exist or is not visible to current user)
	 */
	public <T extends AbstractDto> T searchForSingleResult(Class<T> type, SearchParameters parameters, Map<FilterValue, FilterBuilder> customFilterMapping, AccessType accessType);
	
	/**
	 * Search for objects using parameters.
	 *
	 * This method applies current user permissions in search filter.
	 *
	 * @param type type of objects
	 * @param parameters search parameters
	 * @param accessType type of required access to object
	 * @return all accessible objects which satisfy parameters
	 */
	public <T extends AbstractDto> List<T> search(Class<T> type, SearchParameters parameters, AccessType accessType);
	
	/**
	 * Search for objects using parameters.
	 *
	 * This method applies current user permissions in search filter.
	 *
	 * @param type type of objects
	 * @param parameters search parameters
	 * @param customFilterMapping custom mapping for filter values
	 * @param accessType type of required access to object
	 * @return all accessible objects which satisfy parameters
	 */
	public <T extends AbstractDto> List<T> search(Class<T> type, SearchParameters parameters, Map<FilterValue, FilterBuilder> customFilterMapping, AccessType accessType);
	
	/**
	 * Search for objects using builder.
	 *
	 * This method applies current user permissions in search filter.
	 * 
	 * @param type type of objects
	 * @param buider filter builder
	 * @param accessType type of required access to object
	 * @return all accessible objects which satisfy filter builder
	 */
	public <T extends AbstractDto> List<T> search(Class<T> type, FilterBuilder builder, AccessType accessType);
	
	/**
	 * Search for objects using builder.
	 *
	 * This method does NOT any security checks.
	 * 
	 * @param type type of objects
	 * @param buider filter builder
	 * @return all objects which satisfy filter builder
	 */
	public <T extends AbstractDto> List<T> search(Class<T> type, FilterBuilder builder);
	
	/**
	 * Returns count of objects using parameters.
	 *
	 * This method applies current user permissions in search filter.
	 *
	 * @param type type of objects
	 * @param parameters search parameters
	 * @param accessType type of required access to object
	 * @return count of all accessible objects which satisfy parameters
	 */
	public <T extends AbstractDto> long count(Class<T> type, SearchParameters parameters, AccessType accessType);
	
	/**
	 * Returns count of objects using parameters.
	 *
	 * This method applies current user permissions in search filter.
	 *
	 * @param type type of objects
	 * @param parameters search parameters
	 * @param customFilterMapping custom mapping for filter values
	 * @param accessType type of required access to object
	 * @return count of all accessible objects which satisfy parameters
	 */
	public <T extends AbstractDto> long count(Class<T> type, SearchParameters parameters, Map<FilterValue, FilterBuilder> customFilterMapping, AccessType accessType);
	
	/**
	 * Returns count of objects using builder.
	 *
	 * This method applies current user permissions in search filter.
	 * 
	 * @param type type of objects
	 * @param buider filter builder
	 * @param accessType type of required access to object
	 * @return count of all accessible objects which satisfy filter builder
	 */
	public <T extends AbstractDto> long count(Class<T> type, FilterBuilder builder, AccessType accessType);
	
	/**
	 * Returns count of objects using builder.
	 *
	 * This method does NOT any security checks.
	 * 
	 * @param type type of objects
	 * @param buider filter builder
	 * @return count of all objects which satisfy filter builder
	 */
	public <T extends AbstractDto> long count(Class<T> type, FilterBuilder builder);
}
