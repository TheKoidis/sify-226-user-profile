package org.simplifyworks.core.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.jdbc.ReturningWork;

public class JpaUtils {

	private JpaUtils() {
	}

	public static boolean isMicrosoftSQLServer(EntityManager entityManager) {
		return "Microsoft SQL Server".equalsIgnoreCase(getDatabaseProductName(entityManager));
	}

	public static boolean isMySQL(EntityManager entityManager) {
		return "MySQL".equalsIgnoreCase(getDatabaseProductName(entityManager));
	}

	public static boolean isOracle(EntityManager entityManager) {
		return "Oracle".equalsIgnoreCase(getDatabaseProductName(entityManager));
	}

	public static String resolveDateFunction(EntityManager entityManager) {
		return (isMySQL(entityManager) ? ("date") : ("trunc"));
	}

	public static String getDatabaseProductName(EntityManager entityManager) {
		Session session = entityManager.unwrap(Session.class);
		return session.doReturningWork(new ReturningWork<String>() {

			@Override
			public String execute(Connection connection) throws SQLException {
				DatabaseMetaData metaData = connection.getMetaData();
				return metaData.getDatabaseProductName();
			}
		});
	}

	public static Dialect getDialect(EntityManager entityManager) {
		Session session = entityManager.unwrap(Session.class);
		SessionFactoryImplementor sessionFactory = (SessionFactoryImplementor) session.getSessionFactory();
		return sessionFactory.getDialect();
	}
}
