/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

/**
 * Reprezentace sortu pro predavani mezi MVC
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public enum SortOrder implements FilterableEnum {

	ASC,
	DESC;

	@Override
	public String getMessage() {
		return "sort." + this.name();
	}

	@Override
	public String getValue() {
		return this.name();
	}
}
