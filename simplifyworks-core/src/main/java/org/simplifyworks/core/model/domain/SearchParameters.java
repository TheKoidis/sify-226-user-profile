/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;

/**
 * Reprezentuje nastaveni filtru, sortu atd pro nacitani a vyhledavani dat
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchParameters implements Serializable {

	public static final String DEFAULT_GROUP = "default_group"; // skupina obsahujici uni filtrz generovane pro tabulky a podobne

	private final Map<String, SortValue> sorts = Maps.newLinkedHashMap(); // propertyName - sort

	private final Map<String, FilterGroup> filterGroups = Maps.newLinkedHashMap(); // "skupina" + filtry - propertyName - filter

	private RecordRange range = null; // "limit"
	@JsonIgnore
	private Locale locale = null; // locale pro filtrovani a sort
	@JsonIgnore
	private boolean distinct = false; // distinct data
	@JsonIgnore
	private String currentUsername;
	private String fulltext;

	public SearchParameters() {
	}

	public SearchParameters(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Vraci defaultni skupinu pro zpetnou kompatibilitu - neumoznuje OR
	 * clausule, proto byl pridano filterGroups
	 *
	 * @return
	 */
	@JsonIgnore
	public Map<String, FilterValue> getFilters() {
		return getFilterGroup(DEFAULT_GROUP).getFilters();
	}

	@JsonIgnore
	public Map<String, FilterValue> getFilters(String groupName) {
		return getFilterGroup(groupName).getFilters();
	}

	public FilterGroup getFilterGroup(String groupName) {
		if (!filterGroups.containsKey(groupName)) {
			filterGroups.put(groupName, new FilterGroup(groupName));
		}
		return filterGroups.get(groupName);
	}

	@JsonIgnore
	public Map<String, FilterGroup> getFilterGroups() {
		return filterGroups;
	}

	/**
	 * Only for JSON serialization
	 *
	 * @return
	 */
	@JsonProperty(value = "filterGroups")
	public Collection<FilterGroup> getFilterGroupsList() {
		return getFilterGroups().values();
	}

	/**
	 * Only for JSON serialization
	 *
	 * @param collection
	 */
	public void setFilterGroupsList(Collection<FilterGroup> collection) {
		getFilterGroups().clear();
		collection.stream().forEach((group) -> {
			getFilterGroups().put(group.getName(), group);
		});
	}

	public FilterGroup addFilterGroup(FilterGroup filterGroup) {
		getFilterGroups().put(filterGroup.getName(), filterGroup);
		return filterGroup;
	}

	@JsonIgnore
	public Map<String, SortValue> getSorts() {
		return sorts;
	}

	/**
	 * Only for JSON serializ
	 *
	 * @return ation
	 */
	@JsonProperty(value = "sorts")
	public Collection<SortValue> getSortList() {
		return getSorts().values();
	}

	/**
	 * Only for JSON serialization
	 *
	 * @param collection
	 */
	public void setSortList(Collection<SortValue> collection) {
		getSorts().clear();
		collection.stream().forEach((sort) -> {
			getSorts().put(sort.getPropertyName(), sort);
		});
	}

	public void addSort(String propertyName, SortOrder sortOrder) {
		addSort(new SortValue(propertyName, sortOrder));
	}

	public void addSort(SortValue sortValue) {
		if (sortValue == null || StringUtils.isBlank(sortValue.getPropertyName())) {
			return;
		}
		if (sortValue.getSortOrder() == null) {
			sorts.remove(sortValue.getPropertyName());
		} else {
			sorts.put(sortValue.getPropertyName(), sortValue);
		}
	}

	public void initSort(String propertyName, SortOrder sortOrder) {
		if (!sorts.containsKey(propertyName)) {
			addSort(propertyName, sortOrder);
		}
	}

	/**
	 * Nastavi filter pro property POZOR: property muze byt ve filtrech jen
	 * jednou - vychazi to z logiky setable tabulek, ale moc logicke to neni :)
	 *
	 * @param filterValue
	 * @return
	 */
	public FilterValue addFilter(FilterValue filterValue) {
		return addFilter(DEFAULT_GROUP, filterValue);
	}

	public FilterValue addFilter(String groupName, FilterValue filterValue) {
		return getFilterGroup(groupName).addFilter(filterValue);
	}

	public FilterValue addInternalFilter(FilterValue filterValue) {
		return getFilterGroup(DEFAULT_GROUP).addInternalFilter(filterValue);
	}

	public FilterValue addManualFilter(FilterValue filterValue) {
		return getFilterGroup(DEFAULT_GROUP).addManualFilter(filterValue);
	}

	public FilterValue initFilter(FilterValue filterValue) {
		return getFilterGroup(DEFAULT_GROUP).initFilter(filterValue);
	}

	public FilterValue initManualFilter(FilterValue filterValue) {
		return getFilterGroup(DEFAULT_GROUP).initManualFilter(filterValue);
	}

	public FilterValue initInternalFilter(FilterValue filterValue) {
		return getFilterGroup(DEFAULT_GROUP).initInternalFilter(filterValue);
	}

	public void clear() {
		clearSorts();
		clearFilters();
		range = null;
		fulltext = null;
	}

	@JsonIgnore
	public boolean isEmpty() {
		return isEmptyFilters() && sorts.isEmpty();
	}

	@JsonIgnore
	public boolean isEmptyFilters() {
		if(StringUtils.isNotBlank(fulltext)) {
			return false;
		}
		for (FilterGroup filterGroup : filterGroups.values()) {
			if (!filterGroup.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	public void clearSorts() {
		sorts.clear();
	}

	public void clearFilters() {
		// TODO: tohle mi pripada, ze nefunguje uplne nejlepe - po clearFilters by melo byt logicky isEmpty true, coz neni ... cistit cele skupinz, nebo porovnavat hodnoty?
		for (FilterGroup filterGroup : filterGroups.values()) {
			filterGroup.clearFilters();
		}
	}
	
	public void clearNonDefaultFilters() {
		FilterGroup defaultGroup = filterGroups.get("default_group");
		
		filterGroups.clear();
		
		if(defaultGroup != null) {
			filterGroups.put("default_group", defaultGroup);
		}
	}

	public RecordRange getRange() {
		return range;
	}

	public void setRange(RecordRange range) {
		this.range = range;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public String getCurrentUsername() {
		return currentUsername;
	}

	public void setCurrentUsername(String currentUsername) {
		this.currentUsername = currentUsername;
	}

	public void setFulltext(String fulltext) {
		this.fulltext = fulltext;
	}

	public String getFulltext() {
		return fulltext;
	}	
}
