package org.simplifyworks.core.model.domain.filteroperator;

import java.beans.PropertyDescriptor;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.util.EntityUtils;

import com.google.common.base.Strings;

/**
 * v podstate "instr" hleda se jako podretezec
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class LikeFilterOperator extends FilterOperator {

	public static final String NAME = "LIKE";

	@Override
	public String getLabel() {
		return "≅";
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;
		for (Object obj : filter.getFilledValues()) {
			Predicate subPredicate;
			if (Date.class.isAssignableFrom(expression.getJavaType())) {
				if (obj instanceof String) {
					obj = new Date(Long.valueOf((String) obj));
				}
				Calendar dateCalendar = Calendar.getInstance();
				dateCalendar.setTime((Date) obj);
				//porovnani jen rok/mesic/den, cas se ignoruje
				subPredicate = criteriaBuilder.and(
						criteriaBuilder.equal(criteriaBuilder.function("year", Integer.class, expression), dateCalendar.get(Calendar.YEAR)),
						criteriaBuilder.equal(criteriaBuilder.function("month", Integer.class, expression), dateCalendar.get(Calendar.MONTH) + 1),
						criteriaBuilder.equal(criteriaBuilder.function("day", Integer.class, expression), dateCalendar.get(Calendar.DATE)));
				if (predicate == null) {
					predicate = subPredicate;
				} else {
					predicate = criteriaBuilder.or(predicate, subPredicate);
				}
			} else {
//						System.out.println("xx + 7");
				// CAST(odberne_misto.prijmeni AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci LIKE '%$prijmeni%'
				// criteriaBuilder.function(resolveDateFunction(), Date.class, validFrom)
				String stringFilterValue = String.valueOf(obj);
				if (!Strings.isNullOrEmpty(stringFilterValue)) {
					// criteriaBuilder.function("CAST(odberne_misto.prijmeni AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci LIKE", S.class, validFrom);
					//  '%$prijmeni%'"
					Expression<Integer> locator = criteriaBuilder.locate(criteriaBuilder.upper((Path<String>) expression), stringFilterValue.toUpperCase(locale), 1);
					subPredicate = criteriaBuilder.gt(locator, 0);
					//ParameterExpression<String> p = cb.parameter(String.class,"param0" );
//							subPredicate = criteriaBuilder.like(criteriaBuilder.literal("CAST(rem AS CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci"), stringFilterValue);
					if (predicate == null) {
						predicate = subPredicate;
					} else {
						predicate = criteriaBuilder.or(predicate, subPredicate);
					}
				}
			}
		}

		return predicate;
	}
	
	@Override
	public FilterBuilder createFilter(Class<? extends AbstractDto> dtoClass, FilterValue filterValue) {
		BoolFilterBuilder valuesFilter = FilterBuilders.boolFilter();
		PropertyDescriptor propertyDescriptor = EntityUtils.getPropertyDescriptor(dtoClass, filterValue.getPropertyName());
		// like for dates is not needed
		if (Date.class.isAssignableFrom(propertyDescriptor.getPropertyType())
				|| Number.class.isAssignableFrom(propertyDescriptor.getPropertyType()) // TODO: wildcard dont work on numbers ... better solution?
				|| propertyDescriptor.getPropertyType().equals(int.class)
				|| propertyDescriptor.getPropertyType().equals(long.class)) {
			return FilterOperator.EQUALS.createFilter(dtoClass, filterValue);
		}
		for (Object obj : filterValue.getFilledValues()) {
			FilterBuilder queryFilter = FilterBuilders.queryFilter(QueryBuilders.wildcardQuery(filterValue.getPropertyName() + ".without_accent", "*" + obj.toString().toLowerCase() + "*"));
			
			queryFilter = computeNestedFilters(dtoClass, filterValue, queryFilter);
			
			valuesFilter.should(queryFilter);
		}
		return valuesFilter;
	}

}
