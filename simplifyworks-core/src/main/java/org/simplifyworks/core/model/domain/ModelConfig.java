/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

/**
 * Global model constants
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface ModelConfig {
	
	public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
}
