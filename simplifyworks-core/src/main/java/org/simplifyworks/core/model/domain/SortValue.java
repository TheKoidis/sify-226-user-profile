/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

import java.io.Serializable;

/**
 * Nastaveni sortu pro property
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class SortValue implements Serializable {

	private String propertyName;
	private SortOrder sortOrder;

	public SortValue() {
	}

	public SortValue(String propertyName, SortOrder sortOrder) {
		this.propertyName = propertyName;
		this.sortOrder = sortOrder;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
}
