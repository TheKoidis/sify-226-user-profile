/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

/**
 * Enumerace ve fitlrech, selectech item-value apod.
 * @author RT
 */
public interface FilterableEnum {
  /**
   * Klic k lokalizaci
   */
  public String getMessage();
  /**
   * Hodnota
   * @return 
   */
  public String getValue();
  
}
