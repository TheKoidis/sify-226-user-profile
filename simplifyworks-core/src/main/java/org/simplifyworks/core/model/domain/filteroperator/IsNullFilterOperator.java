package org.simplifyworks.core.model.domain.filteroperator;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Locale;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class IsNullFilterOperator extends FilterOperator {

	public static final String NAME = "IS_NULL";

	@Override
	public String getLabel() {
		return "O";
	}

	@Override
	public boolean isEmpty(List<Object> values) {
		return false;
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		return criteriaBuilder.isNull(expression);
	}
	
	@Override
	public FilterBuilder createFilter(Class<? extends AbstractDto> dtoClass, FilterValue filterValue) {
		BoolFilterBuilder mustNot = FilterBuilders.boolFilter().mustNot(FilterOperator.IS_NOT_NULL.createFilter(dtoClass, filterValue));
		return computeNestedFilters(dtoClass, filterValue, mustNot);
	}

}
