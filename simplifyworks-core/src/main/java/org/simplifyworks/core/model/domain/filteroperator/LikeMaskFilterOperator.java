package org.simplifyworks.core.model.domain.filteroperator;

import com.google.common.base.Strings;
import java.beans.PropertyDescriptor;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.Locale;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.RegexpFilterBuilder;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.util.EntityUtils;

/**
 * SQL like vcetne wildcard
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class LikeMaskFilterOperator extends FilterOperator {

	public static final String NAME = "LIKE_MASK";

	@Override
	public String getLabel() {
		return "≈";
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;
		for (Object obj : filter.getFilledValues()) {
			if (!String.class.isAssignableFrom(expression.getJavaType())) {
				throw new CoreException("filter.operator.likeMask.error.type", new Object[]{filter.getPropertyName(), expression.getJavaType().getSimpleName()});
			}
			String stringFilterValue = String.valueOf(obj);
			if (!Strings.isNullOrEmpty(stringFilterValue)) {
				Predicate subPredicate = criteriaBuilder.like(criteriaBuilder.upper((Path<String>) expression), stringFilterValue.toUpperCase(locale));
				if (predicate == null) {
					predicate = subPredicate;
				} else {
					predicate = criteriaBuilder.or(predicate, subPredicate);
				}
			}
		}

		return predicate;
	}

	@Override
	public FilterBuilder createFilter(Class<? extends AbstractDto> dtoClass, FilterValue filterValue) {
		PropertyDescriptor propertyDescriptor = EntityUtils.getPropertyDescriptor(dtoClass, filterValue.getPropertyName());
		if (!String.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
			throw new CoreException("filter.operator.likeMask.error.type", new Object[]{filterValue.getPropertyName(), propertyDescriptor.getPropertyType().getSimpleName()});
		}
		BoolFilterBuilder filter = FilterBuilders.boolFilter();
		for (Object obj : filterValue.getFilledValues()) {
			FilterBuilder regexpFilter = FilterBuilders.regexpFilter(filterValue.getPropertyName(), obj.toString());
			
			regexpFilter = computeNestedFilters(dtoClass, filterValue, regexpFilter);
			
			filter.should(regexpFilter);
		}
		return filter;
	}

}
