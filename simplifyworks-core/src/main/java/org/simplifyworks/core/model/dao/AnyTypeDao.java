/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.simplifyworks.core.model.domain.FieldInfo;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.entity.BaseEntity;

/**
 * Basic dao extending simple spring data repository TODO: nepouzijeme spodni
 * vrstvu z jenero as it is?
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 * @param <T>
 */
@Deprecated
public interface AnyTypeDao<T extends BaseEntity> {

	public static final String FILTER_VALID = "core_valid";

	/**
	 * Vraci entitu
	 *
	 * @param id
	 * @return
	 */
	T findById(Serializable id);

	/**
	 * Vsechny entity daneho typu
	 *
	 * @return
	 */
	List<T> findAll();

	/**
	 * Ulozi novou ci edituje existujici entitu dle idcka (respektive PK)
	 *
	 * @param entity
	 * @return
	 */
	T saveOrUpdate(T entity);

	/**
	 * Edituje existujici entitu dle idcka (respektive PK)
	 *
	 * @param entity
	 * @return
	 */
	T update(T entity);

	/**
	 * Ulozeni entity s vycistenim contextu - hodi se pro hromadne operace,
	 * importy apod, kdz nechceme entity
	 *
	 * @param entity
	 * @return
	 */
	T batch(T entity);

	void remove(Serializable id);

	/**
	 * Standardni filtr
	 *
	 * @param searchParameters
	 * @return
	 */
	List<T> search(SearchParameters searchParameters);

	/**
	 * Pocet zaznamu vzhovujicich standardnimu filtru
	 *
	 * @param searchParameters
	 * @return
	 */
	long count(SearchParameters searchParameters);

	/**
	 * vrati primitivni persistovane atributy entitni tridy + primitivni
	 * persistovane atributy navazanych entit (vztah many-to-one nebo
	 * one-to-one) a subentit dalsich levelech.
	 *
	 * Hodi se predevsim pro validaci rucne pridanych entit.
	 *
	 * @param level
	 * @return
	 */
	List<FieldInfo> getSingularAttributes(int level);

	/**
	 * Vrati PK od aktualni classy entity
	 *
	 * @return
	 */
	SingularAttribute<? super T, ?> getIdAttribute();

	/**
	 * Vraci genericky typ entity
	 *
	 * @return
	 */
	Class<T> getEntityClass();
}
