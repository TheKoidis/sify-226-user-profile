/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.entity;

import java.util.Date;

/**
 * Base audit info (who, when,...)
 *
 * @author hanak
 */
public interface AuditInfo {

	String getCreator();

	void setCreator(String creator);

	Date getCreated();

	void setCreated(Date created);

	String getModifier();

	void setModifier(String modifier);

	Date getModified();

	void setModified(Date modified);
}
