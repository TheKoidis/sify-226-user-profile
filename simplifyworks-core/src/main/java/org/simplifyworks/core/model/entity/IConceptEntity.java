package org.simplifyworks.core.model.entity;

/**
 * Created by Siroky on 25.6.2015.
 */
public interface IConceptEntity {

    public static final String CONCEPT_FIELD_NAME = "concept";
    public static final String CONCEPT_AUTHOR_FIELD_NAME = "conceptAuthor";

    public boolean isConcept();

    public String getConceptAuthor();

    public void setConcept(boolean concept);

    public void setConceptAuthor(String conceptAuthor);
}
