/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.entity;

import java.util.Date;

/**
 * Entita s platnosti
 * 
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public interface IValidableEntity {
	
	/**
	 * Vraci platnost od
	 * @return 
	 */
	Date getValidFrom();
	
	/**
	 * Vraci platnost do
	 * @return 
	 */
	Date getValidTill();
	
}
