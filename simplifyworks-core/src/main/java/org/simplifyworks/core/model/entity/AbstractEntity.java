/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Version;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.util.Assert;

/**
 * Base entity
 *
 * TODO: optimisticke zamky vzdy, nebo jen u vybranych entit? TODO: audit -
 * remove new Date()
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@MappedSuperclass
public class AbstractEntity implements BaseEntity, AuditInfo {

	@Id
	@Column(name = "ID", precision = 18, scale = 0)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "CREATED")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date created = new Date();
	@Column(name = "MODIFIED")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date modified = new Date();
	@Size(max = 50)
	@Column(name = "CREATOR", length = 50)
	private String creator;
	@Size(max = 50)
	@Column(name = "MODIFIER", length = 50)
	private String modifier;
	private transient boolean locked = false;
	@Max(value = 9999999999L)
	@Version
	@Column(name = "VERSION", precision = 10, scale = 0, nullable = false)
	private int version;

	public AbstractEntity() {
	}

	public AbstractEntity(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Serializable id) {
		if (id != null) {
			Assert.isInstanceOf(Long.class, id, "AbstractEntity supports only Long identifier. For different identifier type generalize BaseEntity.");
		}
		setId((Long) id);
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Date getCreated() {
		return created;
	}

	@Override
	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public Date getModified() {
		return modified;
	}

	@Override
	public void setModified(Date modified) {
		this.modified = modified;
	}

	@Override
	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Override
	public String getCreator() {
		return creator;
	}

	@Override
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	@Override
	public String getModifier() {
		return modifier;
	}

	@Override
	public String toString() {
		return getClass().getCanonicalName() + "[ id=" + getId() + " ]";
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null || !object.getClass().equals(getClass())) {
			return false;
		}

		AbstractEntity other = (AbstractEntity) object;
		if ((this.getId() == null && other.getId() != null)
				|| (this.getId() != null && !this.getId().equals(other.getId()))
				|| (this.getId() == null && other.getId() == null && this != other)) {
			return false;
		}

		return true;
	}

	@Override
	public boolean isLocked() {
		return locked;
	}

	@Override
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Override
	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int getVersion() {
		return version;
	}
}
