package org.simplifyworks.core.model.domain;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Dependencies annotation
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dependencies {

	public Dependency[] value();
}
