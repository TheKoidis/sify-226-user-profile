/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

import java.io.Serializable;

/**
 * "limit / rownum"
 * 
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class RecordRange implements Serializable {
	
	private int firstRow;
	private int rows;

	public RecordRange(int firstRow, int rows) {
		this.firstRow = firstRow;
		this.rows = rows;
	}

	public RecordRange() {
	}

	public int getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(int firstRow) {
		this.firstRow = firstRow;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	@Override
	public String toString() {
		return "firstRow [" + firstRow + "] - rows [" + rows + "]";
	}
}
