package org.simplifyworks.core.model.domain;

import java.lang.reflect.Field;

/**
 *
 * @author hanak
 */
public class FieldInfo {

	private Class entityClass;
	private Field field;
	private String propertyName;
	private Field subEntity;
	private Boolean isSubEntityIdentifier;

	public FieldInfo(Class entityClass, Field field, String propertyName) {
		this(entityClass, field, propertyName, null, null);
	}

	public FieldInfo(Class entityClass, Field field, String propertyName, Field subEntity, Boolean isSubEntityIdentifier) {
		this.entityClass = entityClass;
		this.field = field;
		this.propertyName = propertyName;
		this.subEntity = subEntity;
		this.isSubEntityIdentifier = isSubEntityIdentifier;
	}

	/**
	 * @return the field
	 */
	public Field getField() {
		return field;
	}

	/**
	 * @return the propertyName
	 */
	public String getPropertyName() {
		return propertyName;
	}

	/**
	 * @return the subEntity
	 */
	public Field getSubEntity() {
		return subEntity;
	}

	/**
	 * atribut je identifikator (id) v subentite (vazba na subentitu)
	 *
	 * @return the isSubEntityIdentifier
	 */
	public Boolean isSubEntityIdentifier() {
		return isSubEntityIdentifier;
	}

	public Class getEntityClass() {
		return entityClass;
	}
}
