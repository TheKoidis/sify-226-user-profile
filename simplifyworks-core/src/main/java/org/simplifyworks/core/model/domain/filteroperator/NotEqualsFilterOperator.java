package org.simplifyworks.core.model.domain.filteroperator;

import org.apache.commons.lang3.time.DateUtils;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.util.JpaUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class NotEqualsFilterOperator extends FilterOperator {

	public static final String NAME = "NOT_EQUALS";

	@Override
	public String getLabel() {
		return "≠";
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;

		for (Object obj : filter.getFilledValues()) {
			Predicate subPredicate;
			if (Date.class.isAssignableFrom(expression.getJavaType())) {
				subPredicate = criteriaBuilder.notEqual(criteriaBuilder.function(JpaUtils.resolveDateFunction(entityManager), Date.class, expression), DateUtils.truncate(obj, Calendar.DATE));
			} else {
				subPredicate = criteriaBuilder.notEqual(expression, obj);
			}
			if (predicate == null) {
				predicate = subPredicate;
			} else {
				predicate = criteriaBuilder.and(predicate, subPredicate);
			}
		}

		return predicate;
	}
	
	@Override
	public FilterBuilder createFilter(Class<? extends AbstractDto> dtoClass, FilterValue filterValue) {
		FilterBuilder createFilter = FilterOperator.EQUALS.createFilter(dtoClass, filterValue);
		
		createFilter = computeNestedFilters(dtoClass, filterValue, createFilter);
		
		return FilterBuilders.boolFilter().mustNot(createFilter);
	}

}
