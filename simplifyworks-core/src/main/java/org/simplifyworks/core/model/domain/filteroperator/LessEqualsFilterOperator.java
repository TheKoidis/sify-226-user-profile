package org.simplifyworks.core.model.domain.filteroperator;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.time.DateUtils;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.util.EntityUtils;
import org.simplifyworks.core.util.JpaUtils;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class LessEqualsFilterOperator extends FilterOperator {

	public static final String NAME = "LESS_EQUALS";

	@Override
	public String getLabel() {
		return "≤";
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;
		for (Object obj : filter.getFilledValues()) {
			Comparable comp = (Comparable) obj;
			Predicate subPredicate;
			if (String.class.isAssignableFrom(expression.getJavaType())) {
				subPredicate = criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.upper((Path<String>) expression), ((String) comp).toUpperCase());
			} else if (Date.class.isAssignableFrom(expression.getJavaType())) {
				subPredicate = criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.function(JpaUtils.resolveDateFunction(entityManager), Date.class, expression), DateUtils.truncate(comp, Calendar.DATE));
			} else {
				subPredicate = criteriaBuilder.lessThanOrEqualTo((Path<Comparable>) expression, comp);
			}
			if (predicate == null) {
				predicate = subPredicate;
			} else {
				predicate = criteriaBuilder.or(predicate, subPredicate);
			}
		}

		return predicate;
	}
	
	@Override
	public FilterBuilder createFilter(Class<? extends AbstractDto> dtoClass, FilterValue filterValue) {
		BoolFilterBuilder filter = FilterBuilders.boolFilter();
		PropertyDescriptor propertyDescriptor = EntityUtils.getPropertyDescriptor(dtoClass, filterValue.getPropertyName());
		for (Object obj : filterValue.getFilledValues()) {
			FilterBuilder filterPart;
			if (Date.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
				filterPart = FilterBuilders.rangeFilter(filterValue.getPropertyName()).lte(toElasticsearchDate(obj));
			} else if(BigInteger.class.isAssignableFrom(propertyDescriptor.getPropertyType()) || BigDecimal.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
				filterPart = FilterBuilders.rangeFilter(filterValue.getPropertyName() + ".double").lte(toDouble(obj, Double.MIN_VALUE));
			} else {
				// simple data types
				filterPart = FilterBuilders.rangeFilter(filterValue.getPropertyName()).lte(obj);
			}
                        filterPart = computeNestedFilters(dtoClass, filterValue, filterPart);
			filter.should(filterPart);
		}
		return filter;
	}

}
