package org.simplifyworks.core.model.domain;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.EntityDependencyFactory;

/**
 * Annotation represents entity dependency.
 * 
 * {@link EntityDependencyFactory} uses this annotation for meta model construction.
 * Path is used in current data transfer object (which owns this annotation) to determine <strong>attribute for comparison</strong>.
 * Inverse path is used in entity to determine <strong>value of attribute for comparison</strong>.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dependency {

	/**
	 * Class of dependent entity
	 * 
	 * @return class
	 */
	public Class<? extends AbstractEntity> entityClass();
	
	/**
	 * Path in data transfer object (used in left side of expression)
	 * 
	 * @return path
	 */
	public String path() default "id";
	
	/**
	 * Path in entity (used in right side of expression)
	 * 
	 * @return path
	 */
	public String inversePath() default "id";
}
