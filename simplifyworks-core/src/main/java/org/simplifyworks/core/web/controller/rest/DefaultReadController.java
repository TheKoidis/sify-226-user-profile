package org.simplifyworks.core.web.controller.rest;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.exception.EntityNotFoundException;
import org.simplifyworks.core.model.domain.FilterGroup;
import org.simplifyworks.core.model.domain.RecordRange;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.SortValue;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.PermissionElement;
import org.simplifyworks.core.service.ReadManager;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.core.web.domain.table.Metadata;
import org.simplifyworks.core.web.domain.table.RequestParser;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.workflow.model.domain.WorkflowActionDto;
import org.simplifyworks.workflow.service.WorkflowMetadataService;
import org.simplifyworks.workflow.web.domain.WorkflowMetadata;
import org.simplifyworks.workflow.web.domain.WorkflowProcessMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class DefaultReadController<T extends AbstractDto, E extends AbstractEntity> {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultReadController.class);
	public static final String ENCODING = "UTF-8";
	public static final String PARAMETER_INCLUDE_METADATA = "includeMetadata";

	@Autowired
	protected SecurityService securityService;

	@Autowired
	private WorkflowMetadataService workflowMetadataService;

	protected abstract ReadManager<T, E> getManager();

	/**
	 * Return filtered dto page
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResourcesWrapper<ResourceWrapper<T>> getPage(HttpServletRequest request) {
		return getData(request, null);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ResourcesWrapper<ResourceWrapper<T>> getPage(HttpServletRequest request, @RequestBody SearchParameters jsonSearchParameters) {
		SearchParametersParser searchParametersParser = new SearchParametersParser();
		searchParametersParser.process(jsonSearchParameters);
		return getData(request, jsonSearchParameters);
	}

	/**
	 * TODO: return json message?
	 *
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/reindex")
	public long reindex() {
		long reindexAll = getManager().reindexAll();

		if (LOG.isDebugEnabled()) {
			LOG.debug("Reindexed all records [" + reindexAll + "] for [" + getManager().getDtoClass().getSimpleName()
					+ "] by user [" + securityService.getUsername() + "]");
		}
		return reindexAll;
	}

	private ResourcesWrapper<ResourceWrapper<T>> getData(HttpServletRequest request, SearchParameters jsonSearchParameters) {
		RequestParser requestParser = new RequestParser(request);
		SearchParameters dataTablesSearchParameters = requestParser.toSearchParameters();

		RestSimpleFilterRequestParser restSimpleFilterRequestParser = new RestSimpleFilterRequestParser(request);
		SearchParameters restSimpleSearchParameters = restSimpleFilterRequestParser.toSearchParameters();

		// merge all search-parameters
		SearchParameters searchParameters = mergeSearchParameters(jsonSearchParameters, restSimpleSearchParameters,
				dataTablesSearchParameters);

		if (searchParameters.getRange() == null) {
			RecordRange recordRange = new RecordRange();
			recordRange.setFirstRow(0);
			recordRange.setRows(1000);
			searchParameters.setRange(recordRange);
		}

		searchParameters.setLocale(LocaleContextHolder.getLocale());

		Long recordsFiltered = getManager().count(searchParameters);

		ResourcesWrapper<ResourceWrapper<T>> response;

		if (recordsFiltered > 0) {
			response = new ResourcesWrapper<ResourceWrapper<T>>(toResources(getManager().search(searchParameters),
					request.getParameter(PARAMETER_INCLUDE_METADATA) != null));
		} else {
			response = new ResourcesWrapper<ResourceWrapper<T>>(Lists.newArrayList());
		}

		searchParameters.clearNonDefaultFilters();
		Long recordsTotal = getManager().count(searchParameters);

		response.setRecordsFiltered(recordsFiltered);
		response.setRecordsTotal(recordsTotal);
		response.setDraw(requestParser.getDraw());

		return response;
	}

	/**
	 * Merge searchParametersList values using "AND"
	 */
	private SearchParameters mergeSearchParameters(SearchParameters... searchParametersList) {
		SearchParameters result = new SearchParameters();
		for (SearchParameters searchParameters : searchParametersList) {
			if (searchParameters == null) {
				continue;
			}

			for (FilterGroup group : searchParameters.getFilterGroups().values()) {
				result.addFilterGroup(group);
			}

			// TODO: this is not good, becose last searchParameters wins
			if (searchParameters.getRange() != null) {
				result.setRange(searchParameters.getRange());
			}
			if (StringUtils.isNotBlank(searchParameters.getFulltext())) {
				result.setFulltext(searchParameters.getFulltext());
			}
		}

		for (int index = searchParametersList.length - 1; index >= 0; index--) {
			if (searchParametersList[index] == null) {
				continue;
			}
			ArrayList<SortValue> values = Lists.newArrayList(searchParametersList[index].getSorts().values());
			for (SortValue sort : values) {
				result.addSort(sort);
			}
		}

		return result;
	}

	/**
	 * Load record by id
	 *
	 * @param id
	 * @param includeMetadata
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResourceWrapper<T> get(@PathVariable Long id) {
		T result = getManager().search(id);
		if (result == null) {
			throw new EntityNotFoundException(id);
		}
		return toResource(result, true);
	}

	/**
	 * HATEOAS resources construction
	 *
	 * @param data
	 * @param includeMetadata
	 * @return
	 */
	protected List<ResourceWrapper<T>> toResources(List<T> data, boolean includeMetadata) {
		List<ResourceWrapper<T>> resources = Lists.newArrayList();
		data.stream().forEach((abstractDto) -> {
			resources.add(toResource(abstractDto, includeMetadata));
		});
		return resources;
	}

	/*
	 * private DtoResource<T> toResource(T dto) { return toResource(dto, false);
	 * }
	 */
	/**
	 * HATEOAS resource construction
	 */
	protected ResourceWrapper<T> toResource(T dto, boolean includeMetadata) {
		ResourceWrapper<T> dtoResource = new ResourceWrapper<>(dto);
		if (includeMetadata) {
			Metadata metadata = getMetadata(dto);
			dtoResource.setMetadata(metadata);

			try {
				WorkflowMetadata workflowMetadata;
				workflowMetadata = workflowMetadataService.getWorkflowMetadata(getManager(), dto).get();
				dtoResource.setWorkflowMetadata(workflowMetadata);
			} catch (Exception e) {
				throw new CoreException(e);
			}
		}

		return dtoResource;
	}

	/**
	 * Creates metadata for given object. Can be overridden.
	 */
	protected Metadata getMetadata(T dto) {
		boolean canDelete = false;
		boolean canEdit = false;

		List<PermissionElement> permissions = dto.getPermissions();
		for (PermissionElement p : permissions) {

			//LOG.info(p.getUserName() + " " + p.getRoleName() + " " + p.getOrganizationName() + " " + String.valueOf(p.isCanRemove()) + " " + String.valueOf(p.isCanUpdate()));
			if ((p.getUserName() != null && p.getUserName().equals(securityService.getUsername()))
					|| (p.getRoleName() != null && p.getOrganizationName() == null && securityService.hasAnyRole(p.getRoleName()))
					|| (p.getRoleName() != null && p.getOrganizationName() != null && securityService.hasExplicitRoleInOrganization(p.getRoleName(), p.getOrganizationName()))) {
				if (!canDelete) {
					canDelete = p.isCanRemove();
				}
				if (!canEdit) {
					canEdit = p.isCanUpdate();
				}
			}
		}

		Metadata metadata = new Metadata();
		metadata.setCanDelete(canDelete);
		metadata.setCanEdit(canEdit);
		return metadata;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/action-permit/{action}")
	public boolean isActionEnabled(@PathVariable String action, @RequestBody List<Long> ids) throws InterruptedException, ExecutionException {
		List<Future<WorkflowMetadata>> metadata = new ArrayList<>();

		for (Long id : ids) {
			metadata.add(workflowMetadataService.getWorkflowMetadata(getManager(), getManager().search(id)));
		}

		for (Future<WorkflowMetadata> m : metadata) {
			if (!isActionEnabled(action, m.get())) {
				return false;
			}
		}

		return true;
	}

	private boolean isActionEnabled(String action, WorkflowMetadata metadata) {
		List<WorkflowProcessMetadata> processes = metadata.getProcesses();

		if (processes != null) {
			for (WorkflowProcessMetadata process : processes) {
				List<WorkflowActionDto> actions = process.getActions();

				if (actions != null) {
					for (WorkflowActionDto a : actions) {
						if (action.equals(a.getId())) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}
}
