package org.simplifyworks.core.web.domain;

import org.simplifyworks.core.web.domain.table.Metadata;
import org.simplifyworks.workflow.web.domain.WorkflowMetadata;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Svanda on 1.7.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Relation(value = "resource", collectionRelation = "resources")
public class ResourceWrapper<T> extends ResourceSupport {

	private final T resource;
	
	@JsonProperty
	private Metadata metadata;

	@JsonProperty
	private WorkflowMetadata workflowMetadata;
	
	public ResourceWrapper(T resource) {
		this.resource = resource;
	}
	
	public T getResource() {
		return resource;
	}
	
	public Metadata getMetadata() {
		return metadata;
	}
	
	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}
	
	public WorkflowMetadata getWorkflowMetadata() {
		return workflowMetadata;
	}
	
	public void setWorkflowMetadata(WorkflowMetadata workflowMetadata) {
		this.workflowMetadata = workflowMetadata;
	}
}
