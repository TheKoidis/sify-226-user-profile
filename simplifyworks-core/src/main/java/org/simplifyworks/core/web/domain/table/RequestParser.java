/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.domain.table;

import com.google.common.collect.Lists;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.RecordRange;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.SortOrder;
import org.simplifyworks.core.model.domain.filteroperator.LikeFilterOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.ServletRequestUtils;

/**
 * DataTables request representation
 * <p>
 * https://www.datatables.net/manual/server-side
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public class RequestParser {

    private static final Logger LOG = LoggerFactory.getLogger(RequestParser.class);
    private final String draw;
    private final String start;
    private final String length;
    private final String searchValue;
    private final String searchRegex;
    private final List<BasicColumn> columns;
    private final List<BasicColumnOrder> orders;

    public RequestParser(HttpServletRequest httpRequest) {
        this.columns = parseColumns(httpRequest);
        this.orders = parseOrders(httpRequest);
        this.start = httpRequest.getParameter("start");
        this.draw = httpRequest.getParameter("draw");
        this.length = httpRequest.getParameter("length");
        this.searchValue = ServletRequestUtils.getStringParameter(httpRequest, "search[value]", null);
        this.searchRegex = ServletRequestUtils.getStringParameter(httpRequest, "search[regex]", null);
    }

    /**
     * TODO: http://docs.spring.io/spring-framework/docs/4.1.6.RELEASE/javadoc-api/org/springframework/web/util/WebUtils.html
     *
     * @param request
     * @return
     */
    private List<BasicColumnOrder> parseOrders(HttpServletRequest request) {
        Map<String, BasicColumnOrder> assocArray = new LinkedHashMap<>();
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            String parameterName = entry.getKey();
            if (parameterName.startsWith("order[")) {
                String key = parameterName.substring(parameterName.indexOf('[') + 1, parameterName.indexOf(']'));
                if (!assocArray.containsKey(key)) {
                    assocArray.put(key, new BasicColumnOrder());
                }
                String fieldSuffix = parameterName.substring(parameterName.indexOf(']'));
                String orderField = fieldSuffix.substring(fieldSuffix.indexOf('[') + 1, fieldSuffix.lastIndexOf(']'));
                switch (orderField) {
                    case "column": {
                        try {
                            assocArray.get(key).setColumnIndex(Integer.valueOf(entry.getValue()[0]));
                        } catch (NumberFormatException ex) {
                            LOG.warn("Wrong column index [" + key + ":" + entry.getValue()[0] + "], skipping order", ex);
                            assocArray.get(key).setColumnIndex(-1);
                        }
                        break;
                    }
                    case "dir": {
                        try {
                            assocArray.get(key).setSortOrder(SortOrder.valueOf(entry.getValue()[0].toUpperCase()));
                        } catch (IllegalArgumentException ex) {
                            assocArray.get(key).setSortOrder(SortOrder.ASC);
                            LOG.warn("Wrong direction [" + key + ":" + entry.getValue()[0] + "], using default [asc]", ex);
                        }
                        break;
                    }
                }
            }
        }
        // remove invalid orders
        List<BasicColumnOrder> results = Lists.newArrayList();
        assocArray.values().stream().filter((basicColumnOrder) -> (basicColumnOrder.getColumnIndex() > -1)).forEach((basicColumnOrder) -> {
            results.add(basicColumnOrder);
        });
        return results;
    }

    private List<BasicColumn> parseColumns(HttpServletRequest request) {
        Map<String, BasicColumn> assocArray = new LinkedHashMap<>();
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            String parameterName = entry.getKey();
            if (parameterName.startsWith("columns[")) {
                String key = parameterName.substring(parameterName.indexOf('[') + 1, parameterName.indexOf(']'));
                if (!assocArray.containsKey(key)) {
                    assocArray.put(key, new BasicColumn());
                }
                String fieldSuffix = parameterName.substring(parameterName.indexOf(']'));
                String orderField = fieldSuffix.substring(fieldSuffix.indexOf('[') + 1, fieldSuffix.lastIndexOf(']'));
                switch (orderField) {
                    case "data": {
                        assocArray.get(key).setProperty(entry.getValue()[0]);
                        break;
                    }
                    case "name": {
                        assocArray.get(key).setName(entry.getValue()[0]);
                        break;
                    }
                    case "searchable": {
                        assocArray.get(key).setSearchable(Boolean.valueOf(entry.getValue()[0]));
                        break;
                    }
                    case "orderable": {
                        assocArray.get(key).setOrderable(Boolean.valueOf(entry.getValue()[0]));
                        break;
                    }
                    case "search][value": {
                        assocArray.get(key).setSearchValue(entry.getValue()[0]);
                        break;
                    }
                    case "search][regex": {
                        assocArray.get(key).setSearchRegex(entry.getValue()[0]);
                        break;
                    }
                }
            }
        }
        return Lists.newArrayList(assocArray.values());
    }

    public List<BasicColumn> getColumns() {
        return columns;
    }

    public BasicColumn findColumn(int index) {
        if (index < 0 || columns.size() < index) {
            return null;
        }
        return columns.get(index);
    }

    public List<BasicColumnOrder> getOrders() {
        return orders;
    }

    public String getLength() {
        return length;
    }

    public String getStart() {
        return start;
    }

    public Integer getDraw() {
        return draw != null ? Integer.valueOf(draw) : 1;
    }

    public String getSearchRegex() {
        return searchRegex;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public SearchParameters toSearchParameters() {
        SearchParameters searchParameters = new SearchParameters();
        if (length != null || start != null) {
            RecordRange range = new RecordRange();
            range.setRows(length != null ? Integer.valueOf(length) : 1000);
            range.setFirstRow(start != null ? Integer.valueOf(start) : 0);
            searchParameters.setRange(range);
        }

        for (BasicColumn column : getColumns()) {
            if (!column.isSearchable()) {
                continue;
            }
            if (StringUtils.isBlank(column.getSearchValue())) {
                continue;
            }
            // TODO: operators
            searchParameters.addFilter(FilterValue.single(column.getProperty(), new LikeFilterOperator(), column.getSearchValue()));
        }
        for (BasicColumnOrder columnOrder : getOrders()) {
            BasicColumn column = findColumn(columnOrder.getColumnIndex());
            if (column == null || !column.isOrderable()) {
                continue;
            }
            searchParameters.addSort(column.getProperty(), columnOrder.getSortOrder());
        }
        // fulltext
        searchParameters.setFulltext(searchValue);
        return searchParameters;
    }
}
