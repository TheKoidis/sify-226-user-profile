/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.controller.rest;

import java.util.List;

import org.simplifyworks.core.exception.EntityNotFoundException;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricProcessInstanceDto;
import org.simplifyworks.workflow.service.WorkflowHistoryService;
import org.simplifyworks.workflow.service.WorkflowProcessInstanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Base rest controler for table data = pagination, ordering, filters
 * <p>
 * TODO: security annotation
 * TODO rename to DefaultReadWriteController
 *
 * @param <T> dTo class
 * @param <E> Entity class
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public abstract class BasicTableController<T extends AbstractDto, E extends AbstractEntity> extends DefaultReadController<T, E> {

    private static final Logger LOG = LoggerFactory.getLogger(BasicTableController.class);
    
    @Autowired
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Autowired
	private WorkflowHistoryService workflowHistoryService;
    
    protected abstract ReadWriteManager<T, E> getManager();

    /**
     * Create new record
     *
     * @param dto
     * @param includeMetadata
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResourceWrapper<T> create(@RequestBody T dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
        return toResource(getManager().create(dto), includeMetadata != null);
    }

    /**
     * @param dto
     * @param includeMetadata
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResourceWrapper<T> update(@RequestBody T dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("update entity [" + dto.getId() + "]");
        }
        T result = getManager().update(dto);
        if (result == null) {
            throw new EntityNotFoundException(dto.getId());
        }
        return toResource(result, includeMetadata != null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Long id) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("remove entity [" + id + "]");
        }
		getManager().remove(id);

		//clear all running or finished workflow processes
		List<WorkflowHistoricProcessInstanceDto> historicProcesses = workflowHistoryService.list(getManager().getEntityClass().getName(), id);
		for (WorkflowHistoricProcessInstanceDto historicProcess : historicProcesses) {
			if (workflowProcessInstanceService.isRunning(historicProcess.getId())) {
				workflowProcessInstanceService.deleteProcessInstance(historicProcess.getId());
			}
			workflowHistoryService.deleteProcessInstance(historicProcess.getId());
		}
    }
}
