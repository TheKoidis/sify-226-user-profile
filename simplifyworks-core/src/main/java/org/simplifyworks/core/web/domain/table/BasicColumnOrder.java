/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.domain.table;

import org.simplifyworks.core.model.domain.SortOrder;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public class BasicColumnOrder {

	private Integer columnIndex; // column index
	private SortOrder sortOrder; // TODO: Enumeration - jenero sort

	public BasicColumnOrder() {
	}

	public BasicColumnOrder(Integer columnIndex, SortOrder sortOrder) {
		this.columnIndex = columnIndex;
		this.sortOrder = sortOrder;
	}

	public void setColumnIndex(Integer columnIndex) {
		this.columnIndex = columnIndex;
	}

	public Integer getColumnIndex() {
		return columnIndex;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	@Override
	public String toString() {
		return "[" + columnIndex + ":" + sortOrder + "]";
	}
}
