package org.simplifyworks.core.web.domain.table;

/**
 * Created by Siroky on 2.6.2015.
 */
public class Metadata {
    private Boolean canDelete;
    private Boolean canEdit;

    public Boolean getCanDelete() {
        return canDelete;
    }

	public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }
}
