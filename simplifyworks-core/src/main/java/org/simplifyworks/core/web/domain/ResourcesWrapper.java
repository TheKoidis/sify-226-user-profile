/**
 * 
 */
package org.simplifyworks.core.web.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Relation(value = "resource", collectionRelation = "resources")
public class ResourcesWrapper<T> extends ResourceSupport {

	private final Collection<T> resources;
	
	private int draw = 1;
	private long recordsTotal = 0;
	private long recordsFiltered = 0;
	private long recordsReturned = 0;
	
	public ResourcesWrapper(Collection<T> resources) {
		this.resources = resources;
	
		this.recordsTotal = resources.size();
		this.recordsFiltered = resources.size();
		this.recordsReturned = resources.size();
	}
	
	public ResourcesWrapper(Collection<T> resources, long recordsTotal, long recordsFiltered) {
		this.resources = resources;		
		this.recordsReturned = resources.size();
		
		this.recordsTotal = recordsTotal;
		this.recordsFiltered = recordsFiltered;
	}

	public Collection<T> getResources() {
		return resources;
	}
	
	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public long getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public long getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(long recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public long getRecordsReturned() {
		return recordsReturned;
	}

	public void setRecordsReturned(long recordsReturned) {
		this.recordsReturned = recordsReturned;
	}
	
	public static <V> ResourcesWrapper<ResourceWrapper<V>> createUsingValues(Collection<V> values) {
		List<ResourceWrapper<V>> content = new ArrayList<>();
		
		for(V value : values) {
			content.add(new ResourceWrapper<V>(value));
		}
		
		return new ResourcesWrapper<>(content, values.size(), values.size());
	}
}
