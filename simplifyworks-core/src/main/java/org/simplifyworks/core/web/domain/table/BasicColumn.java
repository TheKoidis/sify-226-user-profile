/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.domain.table;

/**
 * Data table column representation
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public class BasicColumn {

	private String property;
	private String name;
	private boolean searchable;
	private boolean orderable;
	private String searchValue;
	private String searchRegex;

	public void setProperty(String property) {
		this.property = property;
	}

	public String getProperty() {
		return property;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOrderable(boolean orderable) {
		this.orderable = orderable;
	}

	public boolean isSearchable() {
		return searchable;
	}

	public boolean isOrderable() {
		return orderable;
	}

	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getSearchRegex() {
		return searchRegex;
	}

	public void setSearchRegex(String searchRegex) {
		this.searchRegex = searchRegex;
	}
}
