package org.simplifyworks.core.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.VndErrors;
import org.springframework.hateoas.VndErrors.VndError;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class ValidationException extends CoreException {

	public ValidationException(String message, ValidationFail[] details) {
		super(message, details);
	}

	@Override
	public ValidationFail[] getDetails() {
		return (ValidationFail[]) super.getDetails();
	}
	
	public VndError toVndError() {
		List<VndError> causes = new ArrayList<>();
		
		for(ValidationFail fail : getDetails()) {
			causes.add(fail.toVndError());
		}
		
		return new CompositeVndError("validation_failed", getMessage(), new VndErrors(causes));
	}
}
