package org.simplifyworks.core.exception;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.VndErrors;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class CompositeVndError extends VndErrors.VndError {

	private VndErrors causes;
	
	public CompositeVndError(String logRef, String message, VndErrors causes, Link... links) {
		super(logRef, message, links);
		
		this.causes = causes;
	}
	
	public VndErrors getCauses() {
		return causes;
	}
}
