package org.simplifyworks.core.exception;

/**
 * Zakladni vyjimka pracujici s lokalizaci
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class CoreException extends RuntimeException {

	private Object[] details;
	
	public CoreException() {
	}

	public CoreException(Throwable cause) {
		super(cause);
	}

	public CoreException(String message, Throwable cause) {
		super(message, cause);
	}

	public CoreException(String message) {
		super(message);
	}
	
	public CoreException(String message, Object[] details) {
		super(message);
		this.details = details;
	}
	
	public CoreException(String message, Object[] details, Throwable cause) {
		super(message, cause);
		this.details = details;
	}

	public Object[] getDetails() {
		return details;
	}

	public void setDetails(Object[] details) {
		this.details = details;
	}
}
