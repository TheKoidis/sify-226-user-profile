package org.simplifyworks.core.exception;

import java.io.Serializable;
import java.util.Map;
import org.springframework.hateoas.VndErrors.VndError;

/**
 * @author Martin Široký <siroky@ders.cz>
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class ValidationFail implements Comparable<ValidationFail>, Serializable {

	private ValidationFailType type;
	private String humanMessageKey;
	private String logMessage;
	private Map<String, Object> params;

	public ValidationFail(String humanMessageKey, String logMessagePattern, Map<String, Object> params) {
		this(humanMessageKey, logMessagePattern, params, ValidationFailType.MANDATORY);
	}

	public ValidationFail(String humanMessageKey, String logMessagePattern, Map<String, Object> params, ValidationFailType type) {
		this.humanMessageKey = humanMessageKey;
		this.logMessage = processLogMessage(logMessagePattern, params);
		this.params = params;
		this.type = type;
	}

	private String processLogMessage(String logMessagePattern, Map<String, Object> params) {
		String logMessage = logMessagePattern;

		for (Map.Entry<String, Object> paramEntry : params.entrySet()) {
			logMessage = logMessage.replace("{" + paramEntry.getKey() + "}", "\"" + paramEntry.getValue() + "\"");
		}

		return logMessage;
	}

	public String getHumanMessageKey() {
		return humanMessageKey;
	}

	public void setHumanMessageKey(String humanMessageKey) {
		this.humanMessageKey = humanMessageKey;
	}

	public String getLogMessage() {
		return logMessage;
	}

	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public ValidationFailType getType() {
		return type;
	}

	public void setType(ValidationFailType type) {
		this.type = type;
	}

	public VndError toVndError() {
		return new ParametrizedVndError("validation_failed", logMessage, humanMessageKey, params, type.name());
	}

	@Override
	public int compareTo(ValidationFail o) {
		int result = this.type.compareTo(o.getType());

		if (result != 0) {
			return result;
		}

		return this.humanMessageKey.compareTo(o.getHumanMessageKey());
	}
}
