/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.exception;

import java.io.Serializable;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public class EntityNotFoundException extends CoreException {
	
	private final Serializable entityId;
	
	public EntityNotFoundException(Serializable entityId) {
		super("Entity [identifier: " + entityId + "] not found");
		this.entityId = entityId;
	}

	public Serializable getEntityId() {
		return entityId;
	}

}
