package org.simplifyworks.core.exception;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum ValidationFailType {

	MANDATORY, PROVIDED, OPTIONAL;
}
