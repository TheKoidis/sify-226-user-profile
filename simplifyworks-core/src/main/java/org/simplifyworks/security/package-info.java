/**
 * Security support package
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
package org.simplifyworks.security;