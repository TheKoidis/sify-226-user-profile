package org.simplifyworks.security.domain;

import java.io.Serializable;
import java.text.MessageFormat;

/**
 * Wrapper object for authentication details
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class AuthenticationDetails implements Serializable {
	
	private final String currentUsername;
	private final String originalUsername;
	
	public AuthenticationDetails(String currentUsername, String originalUsername) {		
		this.currentUsername = currentUsername;
		this.originalUsername = originalUsername;
	}
	
	public String getCurrentUsername() {
		return currentUsername;
	}
	
	public String getOriginalUsername() {
		return originalUsername;
	}
	
	@Override
	public int hashCode() {
		return getCurrentUsername().hashCode() + 47 * getOriginalUsername().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof AuthenticationDetails) {
			AuthenticationDetails that = (AuthenticationDetails) obj;
			
			return this.currentUsername.equals(that.getCurrentUsername()) && this.getOriginalUsername().equals(that.getOriginalUsername());
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return MessageFormat.format("{0}/{1}", currentUsername, originalUsername);
	}
}
