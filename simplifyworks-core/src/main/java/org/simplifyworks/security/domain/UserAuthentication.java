package org.simplifyworks.security.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * Immutable implementation of {@link Authentication}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class UserAuthentication implements Authentication {

	private final String currentUsername;
	private final String originalUsername;
	private final Collection<? extends GrantedAuthority> authorities;
	private final AuthenticationDetails details;
	
	public UserAuthentication(String currentUsername, String originalUsername, Collection<? extends GrantedAuthority> authorities, AuthenticationDetails details) {
		this.currentUsername = currentUsername;
		this.originalUsername = originalUsername;
		this.authorities = Collections.unmodifiableList(new ArrayList<>(authorities));
		this.details = details;
	}
	
	@Override
	public String getName() {
		return currentUsername;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
        
        public Collection<RoleOrganizationGrantedAuthority> getRoleOrganizationAuthorities() {
                Collection<RoleOrganizationGrantedAuthority> roleOrganizationAuthorities = new ArrayList<>();
                for (GrantedAuthority authority : authorities) {
                    if (authority instanceof RoleOrganizationGrantedAuthority) {
                        roleOrganizationAuthorities.add((RoleOrganizationGrantedAuthority) authority);
                    }
                }
		return roleOrganizationAuthorities;
	}               

	@Override
	public Object getCredentials() {
		return "";
	}

	@Override
	public Object getDetails() {
		return details;
	}

	@Override
	public Object getPrincipal() {
		return currentUsername;
	}

	@Override
	public boolean isAuthenticated() {
		return true;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		throw new IllegalArgumentException("Authentication flag cannot be changed");
	}
	
	public String getCurrentUsername() {
		return currentUsername;
	}
	
	public String getOriginalUsername() {
		return originalUsername;
	}
}
