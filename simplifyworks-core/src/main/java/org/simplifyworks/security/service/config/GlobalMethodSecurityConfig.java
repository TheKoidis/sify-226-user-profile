package org.simplifyworks.security.service.config;

import org.simplifyworks.uam.service.CoreRoleHierarchy;
import org.simplifyworks.uam.service.PermissionEvaluator;
import org.simplifyworks.uam.service.impl.CorePermissionEvaluatorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 *
 * @author Svanda
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class GlobalMethodSecurityConfig extends GlobalMethodSecurityConfiguration {

	@Autowired(required = true)
	private CoreRoleHierarchy coreRoleHierarchy;

	@Override
	protected MethodSecurityExpressionHandler createExpressionHandler() {
		DefaultMethodSecurityExpressionHandler methodSecurityExpressionHandler = new DefaultMethodSecurityExpressionHandler();
		methodSecurityExpressionHandler.setRoleHierarchy(coreRoleHierarchy);
		methodSecurityExpressionHandler.setPermissionEvaluator(permissionEvaluator());
		return methodSecurityExpressionHandler;
	}

	@Bean
	public PermissionEvaluator permissionEvaluator() {
		return new CorePermissionEvaluatorImpl();
	}

	@Bean
	public RoleHierarchyVoter roleVoter() {
		RoleHierarchyVoter roleHierarchyVoter = new RoleHierarchyVoter(coreRoleHierarchy);
		return roleHierarchyVoter;
	}

}
