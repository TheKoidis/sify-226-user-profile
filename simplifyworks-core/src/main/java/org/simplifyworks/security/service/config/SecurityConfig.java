package org.simplifyworks.security.service.config;

import org.simplifyworks.security.service.impl.DefaultAuthenticationDetailsSource;
import org.simplifyworks.security.service.impl.DefaultAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.NullSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;

/**
 * Security configuration
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        
        http.httpBasic()
        	.authenticationDetailsSource(defaultAuthenticationDetailsSource())
        	.realmName("simplifyworks")
            .and()
                .authorizeRequests()
                .antMatchers("/api/**")
                .fullyAuthenticated()
            .and()
            	.authorizeRequests()
            	.antMatchers("/auth")
            	.anonymous();
        
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
    
    @Bean
    public DefaultAuthenticationDetailsSource defaultAuthenticationDetailsSource() {
    	return new DefaultAuthenticationDetailsSource();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(defaultAuthenticationProvider());
    }
    
    @Bean
    public DefaultAuthenticationProvider defaultAuthenticationProvider() {
    	return new DefaultAuthenticationProvider();
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
            .antMatchers("/js/**", "/css/**", "/img/**", "/libs/**", "/favicons/**"); // TODO: remove api/event
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityContextRepository securityContextRepository() {
        return new NullSecurityContextRepository();
    }
}
