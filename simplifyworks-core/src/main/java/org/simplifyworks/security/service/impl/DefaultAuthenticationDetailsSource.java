package org.simplifyworks.security.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.simplifyworks.security.domain.AuthenticationDetails;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.util.Assert;

/**
 * Default implementation of {@link AuthenticationDetailsSource}. This implementation
 * uses two required headers (<strong>Current-Username and Original-Username</strong>) from HTTP request.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, AuthenticationDetails> {

	/**
	 * Current username header name
	 */
	public static final String CURRENT_USERNAME_HEADER = "Current-Username";
	
	/**
	 * Original username header name
	 */
	public static final String ORIGINAL_USERNAME_HEADER = "Original-Username";
	
	@Override
	public AuthenticationDetails buildDetails(HttpServletRequest context) {
		String currentUsername = context.getHeader(CURRENT_USERNAME_HEADER);
		String originalUsername = context.getHeader(ORIGINAL_USERNAME_HEADER);
		
		Assert.notNull(currentUsername, "Current username cannot be null");
		Assert.notNull(originalUsername, "Original username cannot be null (can be the same as current username)");
		
		return new AuthenticationDetails(currentUsername, originalUsername);
	}
}