package org.simplifyworks.security.service;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;

/**
 * Factory which returns granted authorities for users
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface GrantedAuthoritiesFactory {

	/**
	 * Returns granted authorities for user
	 * 
	 * @param username username
	 * @return granted authorities (never null)
	 */
	public List<GrantedAuthority> getGrantedAuthorities(String username);
}
