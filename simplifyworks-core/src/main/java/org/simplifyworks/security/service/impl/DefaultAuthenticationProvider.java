package org.simplifyworks.security.service.impl;

import java.util.List;

import org.simplifyworks.security.domain.AuthenticationDetails;
import org.simplifyworks.security.domain.UserAuthentication;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

/**
 * Default implementation of {@link AuthenticationProvider}. This implementation does authentication in this way:
 * <ol>
 * 	<li>Client authentication (for example front-end)
 * 	 <ol>
 * 	  <li>takes name and password from authentication</li>
 * 	  <li>checks the name and password using configuration attributes</li>
 *   </ol>
 *  </li>
 *  <li>User authentication
 *   <ol>
 * 	  <li>takes authentication details</li>
 *    <li>searches for user/roles using authentication details</li>
 *    <li>returns user/roles authentication</li>
 *   </ol>
 *  </li>
 * </ol>
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultAuthenticationProvider implements AuthenticationProvider {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultAuthenticationProvider.class);
	
	@Value("${security.authentication.client.name:sify}")
	private String clientName;
	
	@Value("${security.authentication.client.password:pass}")
	private String clientPassword;
	
	@Autowired
	private GrantedAuthoritiesFactory grantedAuthoritiesFactory;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		authenticateClient(authentication.getName(), (String) authentication.getCredentials());
		
		return authenticateUser((AuthenticationDetails) authentication.getDetails());
	}
	
	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
	
	/**
	 * Authenticates client using name and password
	 * 
	 * @param name authenticating client name
	 * @param password authenticating client password
	 */
	private void authenticateClient(String name, String password) {
		if(LOG.isDebugEnabled()) {
			LOG.debug("Authenticating client [" + name + "]");
		}
		
		if(!clientName.equals(name) || !clientPassword.equals(password)) {
			LOG.debug("Bad credentials provided");
			
			throw new BadCredentialsException("Bad credentials provided");
		}
		
		if(LOG.isDebugEnabled()) {
			LOG.debug("Client [" + name + "] authenticated");
		}
	}
	
	/**
	 * Authenticates user using details or throws exception, never returns null.
	 * Users without roles cannot be authenticated (user has to have at least one role)!
	 * 
	 * @param details details
	 * @return user authentication
	 */
	private Authentication authenticateUser(AuthenticationDetails details) {
		if(LOG.isDebugEnabled()) {
			LOG.debug("Authenticating user [" + details + "]");
		}
		
		List<GrantedAuthority> currentUserGrantedAuthorities = grantedAuthoritiesFactory.getGrantedAuthorities(details.getCurrentUsername());		
		
		if(currentUserGrantedAuthorities.isEmpty()) {
			if(LOG.isDebugEnabled()) {
				LOG.debug("User [" + details.getCurrentUsername() + "] does not exist or has no roles");
			}
		
			throw new DisabledException("User [" + details.getCurrentUsername() + "] does not exist or has no roles");
		}
		
		List<GrantedAuthority> originalUserGrantedAuthorities = grantedAuthoritiesFactory.getGrantedAuthorities(details.getOriginalUsername());
		
		if(originalUserGrantedAuthorities.isEmpty()) {
			if(LOG.isDebugEnabled()) {
				LOG.debug("User [" + details.getOriginalUsername() + "] does not exist or has no roles");
			}
		
			throw new DisabledException("User [" + details.getOriginalUsername() + "] does not exist or has no roles");
		}
		
		if(LOG.isDebugEnabled()) {
			LOG.debug("User [" + details + "] authenticated");
		}
		
		return new UserAuthentication(details.getCurrentUsername(), details.getOriginalUsername(), currentUserGrantedAuthorities, details);
	}
}
