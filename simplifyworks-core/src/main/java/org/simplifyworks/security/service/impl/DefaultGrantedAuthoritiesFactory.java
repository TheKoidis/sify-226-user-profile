package org.simplifyworks.security.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreUserRole_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Component
public class DefaultGrantedAuthoritiesFactory implements GrantedAuthoritiesFactory {

	@Autowired
	private ElasticsearchService elasticsearchService;
	
	@Override
	public List<GrantedAuthority> getGrantedAuthorities(String username) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue(CoreUserRole_.user.getName() + "." + CoreUser_.username.getName(), FilterOperator.EQUALS, Collections.singletonList(username)));
		
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		
		for(CoreUserRoleWithUserDto coreUserRole : elasticsearchService.search(CoreUserRoleWithUserDto.class, parameters, null)) {
			CoreRoleDto role = coreUserRole.getRole();
			CoreOrganizationDto organization = coreUserRole.getOrganization();
				
			grantedAuthorities.add(new RoleOrganizationGrantedAuthority(role.getName(), organization == null ? null : organization.getName()));
		}
		
		return grantedAuthorities;
	}
}
