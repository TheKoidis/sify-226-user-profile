package org.simplifyworks.security.service;

/**
 * Interface for service which verifies credentials of user (in case when no other authentication like SSO is possible)
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface UserVerificationService {

	/**
	 * Verifies account existence using username and password
	 * 
	 * @param username username
	 * @param password password
	 * @return return true if account exists and has specified credentials and at least one role; otherwise false
	 */
	public boolean verify(String username, String password);
}
