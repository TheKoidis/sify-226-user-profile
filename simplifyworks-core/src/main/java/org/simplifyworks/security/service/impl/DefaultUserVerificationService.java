package org.simplifyworks.security.service.impl;

import java.util.Collections;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.simplifyworks.security.service.UserVerificationService;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link UserVerificationService}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultUserVerificationService implements UserVerificationService {
    
	@Autowired
	private ElasticsearchService elasticsearchService;
	
	@Autowired
	private GrantedAuthoritiesFactory grantedAuthoritiesFactory;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public boolean verify(String username, String password) {                
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue(CoreUser_.username.getName(), FilterOperator.EQUALS, Collections.singletonList(username)));
		
		CoreUserWithPersonDto user = elasticsearchService.searchForSingleResult(CoreUserWithPersonDto.class, parameters, null);
                              
		if(user == null || !passwordEncoder.matches(password, new String(user.getPassword()))) {
			return false;
		}
		
		if(grantedAuthoritiesFactory.getGrantedAuthorities(username).isEmpty()) {
			return false;
		}
		
		return true;
	}
}
