package org.simplifyworks.security.web.controller.rest;

import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.security.service.UserVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping(value = "/auth")
public class UserVerificationController {

	@Autowired
	private UserVerificationService userVerificationService;
	
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResourceWrapper<Boolean> verify(@RequestParam(value = "username") String username, @RequestParam(value = "password") String password) {
		return new ResourceWrapper<Boolean>(userVerificationService.verify(username, password));
	}
}
