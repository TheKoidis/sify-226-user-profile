package org.simplifyworks.conf.service.impl;

import java.util.List;

import org.simplifyworks.conf.model.dto.CoreAppSettingDto;
import org.simplifyworks.conf.model.entity.CoreAppSetting;
import org.simplifyworks.conf.model.entity.CoreAppSetting_;
import org.simplifyworks.conf.service.CoreAppSettingManager;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.springframework.stereotype.Service;

/**
 * Nastaveni aplikace
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreAppSettingManagerImpl extends DefaultReadWriteManager<CoreAppSettingDto, CoreAppSetting> implements CoreAppSettingManager {

	@Override
	public String getValueByKey(String key) {
		SearchParameters searchParameters = new SearchParameters();
		FilterValue filterValue = new FilterValue();
		filterValue.setPropertyName(CoreAppSetting_.settingKey.getName());
		filterValue.setValue(key);
		filterValue.setOperator(FilterOperator.EQUALS);
		searchParameters.addFilter(filterValue);

		List<CoreAppSettingDto> search = search(searchParameters);
		if (search == null || search.isEmpty()) {
			return null;
		}
		if (search.size() > 1) {
			throw new CoreException("To many items found for key " + key + "!");
		}
		return search.get(0).getValue();

	}

}
