package org.simplifyworks.conf.service;

import org.simplifyworks.conf.model.dto.CoreAppSettingDto;
import org.simplifyworks.conf.model.entity.CoreAppSetting;
import org.simplifyworks.core.service.ReadWriteManager;

/**
 * Application setting
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CoreAppSettingManager extends ReadWriteManager<CoreAppSettingDto, CoreAppSetting> {
    String getValueByKey(String key);
}
