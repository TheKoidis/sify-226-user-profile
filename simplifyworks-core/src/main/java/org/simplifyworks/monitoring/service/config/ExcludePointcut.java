package org.simplifyworks.monitoring.service.config;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.support.ComposablePointcut;

/**
 * Exclude classes extending or implementing any of given classes
 *
 * @author Martin Široký <siroky@ders.cz>
 */
class ExcludePointcut extends ComposablePointcut {

	private final Class<?>[] excluded;

	public ExcludePointcut(Class<?>... excluded) {
		this.excluded = excluded;
	}

	@Override
	public ClassFilter getClassFilter() {
		return new ClassFilter() {
			@Override
			public boolean matches(Class<?> clazz) {
				for (Class<?> exclude : excluded) {
					if (exclude.isAssignableFrom(clazz)) {
						return false;
					}
				}
				return true;
			}
		};
	}

}
