alter table core_permission drop column wf_action_type;

alter table core_permission drop column wf_button_key;

alter table core_permission drop column wf_definition_id;

alter table core_permission drop column wf_user_task;
