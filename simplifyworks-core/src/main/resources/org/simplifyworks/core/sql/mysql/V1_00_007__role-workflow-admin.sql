insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'workflow_admin', 'reset running or finished processes');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='workflow_admin'));
