insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_user', 'Core records reading');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user'));
