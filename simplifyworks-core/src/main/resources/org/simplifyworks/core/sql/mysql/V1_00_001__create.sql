--------------------------------------------------------------------------------
-- Core Objects
--------------------------------------------------------------------------------

drop table if exists core_app_setting, core_template, core_attachment,
  core_role_composition, core_email_record, core_wf_proc_def_entity,
  core_permission, core_user_role, core_person_organization_ext,
  core_person_organization, core_organization_ext, core_organization, 
  core_user, core_person_ext, core_person, core_role, core_print_module;

create table core_app_setting (
    id                      bigint(18) unsigned auto_increment not null,
    setting_key             varchar(255) not null,
    value                   text,
    system                  tinyint(1) unsigned not null default 0 comment ' 0 - can be changed; 1 - system record, can not be changed',
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_app_setting
  add constraint system_core_app_setting_c check (system in (0,1));

create unique index core_app_setting_uq on core_app_setting (setting_key);

create table core_attachment (
    id                      bigint(18) unsigned auto_increment not null,
    attachment_type         varchar(50),
    content_guid            varchar(36) not null,
    content_path            varchar(255),
    description             varchar(255),
    encoding                varchar(100) not null,
    filesize                bigint(18) unsigned not null,
    mimetype                varchar(255) not null,
    name                    varchar(255) not null,
    object_identifier       varchar(36) not null,
    object_status           varchar(50),
    object_type             varchar(128) not null,
    version_label           varchar(10) not null,
    version_number          int(10) unsigned not null,
    next_version_id         bigint(18) unsigned,
    parent_id               bigint(18) unsigned,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

create index fk_core_attachment_nvid on core_attachment (next_version_id);
create index core_attachment_pid on core_attachment (parent_id);

alter table core_attachment
  add constraint fk_core_attachment_parent_id
    foreign key (parent_id) references core_attachment (id);

alter table core_attachment
  add constraint fk_core_attach_next_version_id
    foreign key (next_version_id) references core_attachment (id);

-- some columns here are actually longer in Oracle 
create table core_email_record (
    id                      bigint(18) unsigned auto_increment not null,
    bcc                     varchar(255),
    cc                      varchar(255),
    email_from              varchar(255),
    encoding                varchar(25) not null,
    message                 text,
    recipients              varchar(255),
    sent                    datetime,
    sent_log                text,
    subject                 varchar(255),
    parent_id               bigint(18) unsigned,
    previous_version        bigint(18) unsigned,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_email_record
  add constraint fk_core_email_record_parent_id
    foreign key (parent_id) references core_email_record (id);
alter table core_email_record
  add constraint fk_core_emai_previous_version
    foreign key (previous_version) references core_email_record (id);
  
create index fk_core_email_record_pid on core_email_record (parent_id);
create index fk_core_email_record_pv on core_email_record (previous_version);

create table core_organization (
    id                      bigint(18) unsigned auto_increment not null,
    abbreviation            varchar(50),
    code                    varchar(50),
    name                    varchar(100) not null,
    parent_id               bigint(18) unsigned,
    valid_from              datetime,
    valid_to                datetime,
    active                  tinyint(1) unsigned not null default 1,
    unit                    bigint(18),
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_organization
  add constraint fk_core_organization_pid
    foreign key (parent_id) references core_organization (id);
alter table core_organization add constraint core_organization_active_c check (active in (0,1));

create table core_organization_ext (
    id                      bigint(18) unsigned auto_increment not null,
    core_organization_id    bigint(18) unsigned,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_organization_ext
  add constraint fk_core_organization_id foreign key (core_organization_id)
    references core_organization (id);
create index core_orga_core_organizatio_ix on core_organization_ext (core_organization_id);

create table core_person (
    id                      bigint(18) unsigned auto_increment not null,
    addressing_phrase       varchar(100),
    email                   varchar(255),
    firstname               varchar(100),
    note                    varchar(200),
    personal_number         varchar(100),
    phone                   varchar(30),
    surname                 varchar(100) not null,
    title_after             varchar(100),
    title_before            varchar(100),
    valid_from              datetime,
    valid_to                datetime,
    active                  tinyint(1) unsigned not null default 1,
    date_of_birth           date,
    unit                    bigint(18),
    sex                     char(1) not null default 'M',
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_person
  add constraint core_person_sex check (sex in ('F', 'M', 'T', '?'));
alter table core_person add constraint core_person_active_c check (active in (0,1));

create table core_person_ext (
    id                      bigint(18) unsigned auto_increment not null,
    core_person_id          bigint(18) unsigned,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_person_ext
  add constraint fk_core_person_id foreign key (core_person_id)
    references core_person (id);
create index core_person_core_person_id_ix on core_person_ext (core_person_id);

create table core_person_organization (
    id                      bigint(18) unsigned auto_increment not null,
    category                varchar(50),
    obligation              decimal(3,2) unsigned,
    valid_from              date,
    valid_to                date,
    core_organization_id    bigint(18) unsigned not null,
    core_person_id          bigint(18) unsigned not null,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_person_organization add check (obligation >= 0 and obligation <= 1);

create index core_person_core_person_id_ix on core_person_organization (core_person_id);
create index core_perso_core_org_ix on core_person_organization (core_organization_id);

alter table core_person_organization
  add constraint fk_core_person_core_person_id
    foreign key (core_person_id) references core_person (id);
alter table core_person_organization
  add constraint fk_core_perso_core_organizatio
    foreign key (core_organization_id) references core_organization (id);

create table core_person_organization_ext (
    id                          bigint(18) unsigned auto_increment not null,
    core_person_organization_id bigint(18) unsigned,
    created                     datetime,
    creator                     varchar(50),
    modified                    datetime,
    modifier                    varchar(50),
    version                     int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

alter table core_person_organization_ext
  add constraint fk_core_person_organization foreign key (core_person_organization_id)
    references core_person_organization (id);
create index core_perso_core_person_org_ix on core_person_organization_ext (core_person_organization_id);

create table core_print_module (
    id                      bigint(18) unsigned auto_increment not null,
    name                    varchar(255) not null,
    template                varchar(255) not null,
    type                    varchar(255) not null,
    object_type             varchar(128) not null,
    wf_state                varchar(255),    
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;
    
create table core_role (
    id                      bigint(18) unsigned auto_increment not null,
    name                    varchar(64) not null,
    description             varchar(255),
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

create unique index uk_core_role_name on core_role (name);

create table core_role_composition (
    id                      bigint(18) unsigned auto_increment not null,
    sub_role_id             bigint(18) unsigned not null,
    superior_role_id        bigint(18) unsigned not null,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

create index core_role_comp_sub_role_id_ix on core_role_composition (sub_role_id);
create index core_role_comp_super_ro_id_ix on core_role_composition (superior_role_id);

alter table core_role_composition
  add constraint fk_core_role_compo_sub_role_id
    foreign key (sub_role_id) references core_role (id);
alter table core_role_composition
  add constraint fk_core_role_comp_super_ro_id
    foreign key (superior_role_id) references core_role (id);

create table core_user (
    id                      bigint(18) unsigned auto_increment not null,
    last_login              datetime,
    password                varchar(100),
    username                varchar(50) not null,
    core_person_id          bigint(18) unsigned,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

create unique index uk_core_user_username on core_user(username);
create index core_user_core_person_id_ix on core_user (core_person_id);

alter table core_user
  add constraint fk_core_user_core_person_id
    foreign key (core_person_id) references core_person (id);

create table core_permission (
    id                      bigint(18) unsigned auto_increment not null,
    discriminator           varchar(31) not null,
    active                  tinyint(1) unsigned not null,
    can_delete              tinyint(1) unsigned not null,
    can_read                tinyint(1) unsigned not null,
    can_write               tinyint(1) unsigned not null,
    object_identifier       varchar(100),
    object_type             varchar(150) not null,
    rel_id                  bigint(18) unsigned,
    rel_type                varchar(150),
    permission_for          varchar(255) not null,
    wf_action_type          varchar(255) not null default 'NONE',
    wf_button_key           varchar(255),
    wf_definition_id        varchar(255),
    wf_user_task            varchar(255),
    core_organization_id    bigint(18) unsigned,
    core_role_id            bigint(18) unsigned,
    core_user_id            bigint(18) unsigned,
    valid_from              datetime,
    valid_till              datetime,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

create index core_permissi_core_user_id_ix on core_permission (core_user_id);
create index core_permissi_core_role_id_ix on core_permission (core_role_id);
create index core_perm_core_organizatio_ix on core_permission (core_organization_id);
create index core_permission_ti on core_permission (object_type, object_identifier);
create unique index core_permission_ticore on core_permission (object_type, object_identifier, core_organization_id, core_role_id, core_user_id, rel_type, rel_id);

alter table core_permission
  add constraint core_permission_can_delete_c check (can_delete in (0,1));
  
alter table core_permission
  add constraint core_permission_can_write_c check (can_write in (0,1));

alter table core_permission
  add constraint core_permission_can_read_c check (can_read in (0,1));

alter table core_permission
  add constraint core_permission_active_c check (active in (0,1));

alter table core_permission
  add constraint core_permissi_wf_action_type_c check (wf_action_type in ('FORWARD', 'BACKWARDS','NONE'));

alter table core_permission
  add constraint core_permissi_permission_for_c check (permission_for in ('ENTITY','SECTION', 'SCREEN'));

alter table core_permission
  add constraint core_permission_trio_chk check (core_user_id is not null or core_role_id is not null or (core_organization_id is not null and core_role_id is not null));

alter table core_permission
  add constraint fk_core_permissi_core_role_id
    foreign key (core_role_id) references core_role (id);

alter table core_permission
  add constraint fk_core_permissi_core_user_id
    foreign key (core_user_id) references core_user (id);

alter table core_permission
  add constraint fk_core_perm_core_organizatio
    foreign key (core_organization_id) references core_organization (id);

create table core_template (
    id                      bigint(18) unsigned auto_increment not null,
    body                    text not null,
    code                    varchar(50) not null,
    description             varchar(255),
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

create table core_user_role (
    id                      bigint(18) unsigned auto_increment not null,
    valid_from              date,
    valid_till              date,
    core_organization_id    bigint(18) unsigned,
    core_role_id            bigint(18) unsigned not null,
    core_user_id            bigint(18) unsigned not null,
    created                 datetime,
    creator                 varchar(50),
    modified                datetime,
    modifier                varchar(50),
    version                 int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

create index core_user_role_core_user_id_ix on core_user_role (core_user_id);
create index core_user_role_core_role_id_ix on core_user_role (core_role_id);
create index core_user_core_org_ix on core_user_role (core_organization_id);

alter table core_user_role
  add constraint fk_core_user_core_organizatio
    foreign key (core_organization_id) references core_organization (id);

alter table core_user_role
  add constraint fk_core_user_role_core_role_id
    foreign key (core_role_id) references core_role (id);

alter table core_user_role
  add constraint fk_core_user_role_core_user_id
    foreign key (core_user_id) references core_user (id);

--------------------------------------------------------------------------------
-- Quartz
--------------------------------------------------------------------------------

drop table if exists QRTZ_SIMPLE_TRIGGERS, QRTZ_CRON_TRIGGERS,
  QRTZ_SIMPROP_TRIGGERS, QRTZ_BLOB_TRIGGERS, QRTZ_CALENDARS, QRTZ_PAUSED_TRIGGER_GRPS,
  QRTZ_FIRED_TRIGGERS, QRTZ_SCHEDULER_STATE, QRTZ_LOCKS, QRTZ_TRIGGERS, QRTZ_JOB_DETAILS;

create table QRTZ_JOB_DETAILS (
    SCHED_NAME              varchar(120) not null ,
    JOB_NAME                varchar(200) not null,
    JOB_GROUP               varchar(200) not null,
    DESCRIPTION             varchar(250),
    JOB_CLASS_NAME          varchar(250) not null,
    IS_DURABLE              varchar(1) not null,
    IS_NONCONCURRENT        varchar(1) not null,
    IS_UPDATE_DATA          varchar(1) not null,
    REQUESTS_RECOVERY       varchar(1) not null,
    JOB_DATA                blob,
    constraint QRTZ_JOB_DETAILS_PK primary key (SCHED_NAME, JOB_NAME, JOB_GROUP)) engine=innodb default charset=utf8 collate utf8_bin;

create index IDX_QRTZ_J_REQ_RECOVERY on QRTZ_JOB_DETAILS (SCHED_NAME, REQUESTS_RECOVERY);
create index IDX_QRTZ_J_GRP on QRTZ_JOB_DETAILS (SCHED_NAME, JOB_GROUP);

create table QRTZ_TRIGGERS (
    SCHED_NAME              varchar(120) not null ,
    TRIGGER_NAME            varchar(200) not null,
    TRIGGER_GROUP           varchar(200) not null,
    JOB_NAME                varchar(200) not null,
    JOB_GROUP               varchar(200) not null,
    DESCRIPTION             varchar(250),
    NEXT_FIRE_TIME          bigint(13),
    PREV_FIRE_TIME          bigint(13),
    PRIORITY                bigint(13),
    TRIGGER_STATE           varchar(16) not null,
    TRIGGER_TYPE            varchar(8) not null ,
    START_TIME              bigint(13) not null,
    END_TIME                bigint(13),
    CALENDAR_NAME           varchar(200),
    MISFIRE_INSTR           smallint(2),
    JOB_DATA                blob,
    constraint QRTZ_TRIGGERS_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint QRTZ_TRIGGER_TO_JOBS_FK
      foreign key ( SCHED_NAME,JOB_NAME, JOB_GROUP)
        references QRTZ_JOB_DETAILS(SCHED_NAME, JOB_NAME,JOB_GROUP)) engine=innodb default charset=utf8 collate utf8_bin;

create index IDX_QRTZ_T_J on QRTZ_TRIGGERS (SCHED_NAME, JOB_NAME, JOB_GROUP);
create index IDX_QRTZ_T_JG on QRTZ_TRIGGERS (SCHED_NAME, JOB_GROUP);
create index IDX_QRTZ_T_C on QRTZ_TRIGGERS (SCHED_NAME, CALENDAR_NAME);
create index IDX_QRTZ_T_G on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_GROUP);
create index IDX_QRTZ_T_STATE on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_STATE);
create index IDX_QRTZ_T_N_STATE on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, TRIGGER_STATE);
create index IDX_QRTZ_T_N_G_STATE on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_GROUP, TRIGGER_STATE);
create index IDX_QRTZ_T_NEXT_FIRE_TIME on QRTZ_TRIGGERS (SCHED_NAME, NEXT_FIRE_TIME);
create index IDX_QRTZ_T_NFT_ST on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_STATE, NEXT_FIRE_TIME);
create index IDX_QRTZ_T_NFT_MISFIRE on QRTZ_TRIGGERS (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME);
create index IDX_QRTZ_T_NFT_ST_MISFIRE on QRTZ_TRIGGERS (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_STATE);
create index IDX_QRTZ_T_NFT_ST_MISFIRE_GRP on QRTZ_TRIGGERS (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_GROUP, TRIGGER_STATE);

create table QRTZ_SIMPLE_TRIGGERS (
    SCHED_NAME      varchar(120) not null ,
    TRIGGER_NAME    varchar(200) not null,
    TRIGGER_GROUP   varchar(200) not null,
    REPEAT_COUNT    int(7) not null,
    REPEAT_INTERVAL bigint(12) not null,
    TIMES_TRIGGERED int(10) not null,
    constraint QRTZ_SIMPLE_TRIG_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint QRTZ_SIMPLE_TRIG_TO_TRIG_FK
      foreign key ( SCHED_NAME, TRIGGER_NAME,TRIGGER_GROUP)
        references QRTZ_TRIGGERS( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)) engine=innodb default charset=utf8 collate utf8_bin;

create table QRTZ_CRON_TRIGGERS (
    SCHED_NAME      varchar(120) not null ,
    TRIGGER_NAME    varchar(200) not null,
    TRIGGER_GROUP   varchar(200) not null,
    CRON_EXPRESSION varchar(120) not null,
    TIME_ZONE_ID    varchar(80),
    constraint QRTZ_CRON_TRIG_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint QRTZ_CRON_TRIG_TO_TRIG_FK
      foreign key ( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
        references QRTZ_TRIGGERS(SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
        )  engine=innodb default charset=utf8 collate utf8_bin;

create table QRTZ_SIMPROP_TRIGGERS (
    SCHED_NAME    varchar(120) not null ,
    TRIGGER_NAME  varchar(200) not null,
    TRIGGER_GROUP varchar(200) not null,
    STR_PROP_1    varchar(255) ,
    STR_PROP_2    varchar(255) ,
    STR_PROP_3    varchar(255) ,
    INT_PROP_1    int(10),
    INT_PROP_2    int(10),
    LONG_PROP_1   bigint(13),
    LONG_PROP_2   bigint(13),
    DEC_PROP_1    decimal(13,4),
    DEC_PROP_2    decimal(13,4),
    BOOL_PROP_1   varchar(1),
    BOOL_PROP_2   varchar(1),
    constraint QRTZ_SIMPROP_TRIG_PK primary key (SCHED_NAME , TRIGGER_NAME, TRIGGER_GROUP),
    constraint QRTZ_SIMPROP_TRIG_TO_TRIG_FK
      foreign key ( SCHED_NAME, TRIGGER_NAME,TRIGGER_GROUP)
        references QRTZ_TRIGGERS( SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)) engine=innodb default charset=utf8 collate utf8_bin;

create table QRTZ_BLOB_TRIGGERS (
    SCHED_NAME    varchar(120) not null,
    TRIGGER_NAME  varchar(200) not null,
    TRIGGER_GROUP varchar(200) not null,
    BLOB_DATA     blob,
    constraint QRTZ_BLOB_TRIG_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint QRTZ_BLOB_TRIG_TO_TRIG_FK
      foreign key ( SCHED_NAME,TRIGGER_NAME, TRIGGER_GROUP)
        references QRTZ_TRIGGERS(SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)) engine=innodb default charset=utf8 collate utf8_bin;

create table QRTZ_CALENDARS (
    SCHED_NAME    varchar(120) not null ,
    CALENDAR_NAME varchar(200) not null,
    CALENDAR      blob not null,
    constraint QRTZ_CALENDARS_PK primary key (SCHED_NAME, CALENDAR_NAME)) engine=innodb default charset=utf8 collate utf8_bin;

create table QRTZ_PAUSED_TRIGGER_GRPS (
    SCHED_NAME    varchar(120) not null ,
    TRIGGER_GROUP varchar(200) not null,
    constraint QRTZ_PAUSED_TRIG_GRPS_PK primary key (SCHED_NAME, TRIGGER_GROUP)) engine=innodb default charset=utf8 collate utf8_bin;

create table QRTZ_FIRED_TRIGGERS (
    SCHED_NAME        varchar(120)  not null,
    ENTRY_ID          varchar(95)   not null,
    TRIGGER_NAME      varchar(200)  not null,
    TRIGGER_GROUP     varchar(200)  not null,
    INSTANCE_NAME     varchar(200)  not null,
    FIRED_TIME        bigint(13)    not null,
    SCHED_TIME        bigint(13)    not null,
    PRIORITY          bigint(13)    not null,
    STATE             varchar(16)   not null,
    JOB_NAME          varchar(200),
    JOB_GROUP         varchar(200),
    IS_NONCONCURRENT  varchar(1),
    REQUESTS_RECOVERY varchar(1),
    constraint QRTZ_FIRED_TRIGGER_PK primary key (SCHED_NAME, ENTRY_ID)) engine=innodb default charset=utf8 collate utf8_bin;

create index IDX_QRTZ_FT_TRIG_INST_NAME on QRTZ_FIRED_TRIGGERS (SCHED_NAME, INSTANCE_NAME);
create index IDX_QRTZ_FT_INST_JOB_REQ_RCVRY on QRTZ_FIRED_TRIGGERS (SCHED_NAME, INSTANCE_NAME, REQUESTS_RECOVERY);
create index IDX_QRTZ_FT_J_G on QRTZ_FIRED_TRIGGERS (SCHED_NAME, JOB_NAME, JOB_GROUP);
create index IDX_QRTZ_FT_JG on QRTZ_FIRED_TRIGGERS (SCHED_NAME, JOB_GROUP);
create index IDX_QRTZ_FT_T_G on QRTZ_FIRED_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);
create index IDX_QRTZ_FT_TG on QRTZ_FIRED_TRIGGERS (SCHED_NAME, TRIGGER_GROUP);

create table QRTZ_SCHEDULER_STATE (
    SCHED_NAME        varchar(120) not null ,
    INSTANCE_NAME     varchar(200) not null,
    LAST_CHECKIN_TIME bigint(13) not null,
    CHECKIN_INTERVAL  bigint(13) not null,
    constraint QRTZ_SCHEDULER_STATE_PK primary key ( SCHED_NAME,INSTANCE_NAME)) engine=innodb default charset=utf8 collate utf8_bin;

create table QRTZ_LOCKS (
    SCHED_NAME varchar(120) not null ,
    LOCK_NAME  varchar(40) not null,
    constraint QRTZ_LOCKS_PK primary key (SCHED_NAME, LOCK_NAME)) engine=innodb default charset=utf8 collate utf8_bin;

create table core_wf_proc_def_entity (
   	id bigint(18) unsigned auto_increment not null,
	process_definition_key varchar(255) not null, 
	entity_class_name varchar(255) not null, 
	created datetime, 
	creator varchar(50), 
	modified datetime, 
	modifier varchar(50), 
	version int(10) unsigned not null default 0,
    primary key(id)) engine=innodb default charset=utf8 collate utf8_bin;

--------------------------------------------------------------------------------
-- Finale
--------------------------------------------------------------------------------
insert into core_app_setting (setting_key, value, system, created)
  values ('CORE_VERSION', '0.3-SNAPSHOT (row 0)', 1, curdate());

commit;
