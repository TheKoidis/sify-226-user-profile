INSERT INTO "CORE_ORGANIZATION" (ID, ABBREVIATION, CODE, NAME, VERSION) VALUES ('-1', 'DEMO organization', '99999999999999', 'DEMO organization', '0');

update core_user_role set core_organization_id=-1 where core_organization_id is null;
alter table core_user_role modify core_organization_id not null;
