insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_user', 'Core records reading');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user'));
