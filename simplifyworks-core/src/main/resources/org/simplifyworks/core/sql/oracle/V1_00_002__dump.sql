-- Default roles
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'admin', 'administrator');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'admin_scheduler', 'scheduling administrator');

--Admin user with prson, roles (password is "defaultpassword")
INSERT INTO CORE_PERSON(ID,CREATED, CREATOR, VERSION,surname) values(-1, sysdate, 'system', '0','admin');
INSERT INTO CORE_USER (ID, CREATED, CREATOR, VERSION, PASSWORD, USERNAME, CORE_PERSON_ID) VALUES (-1, sysdate, 'system', '0', '$2a$10$1Jy.xKaSDe.d5eGN0AUzz.p0ASZmxLo/mvR/h0SY.T5miMc9kZg/m', 'admin', (select id from core_person where surname='admin'));
INSERT INTO CORE_USER_ROLE (ID, CREATED, CREATOR, VERSION, CORE_USER_ID, CORE_ROLE_ID) VALUES (-1, sysdate, 'system', '0', (select id from core_user where username='admin'), (select id from core_role where name='admin'));
INSERT INTO CORE_USER_ROLE (ID, CREATED, CREATOR, VERSION, CORE_USER_ID, CORE_ROLE_ID) VALUES (-2, sysdate, 'system', '0', (select id from core_user where username='admin'), (select id from core_role where name='admin_scheduler'));

COMMIT;
