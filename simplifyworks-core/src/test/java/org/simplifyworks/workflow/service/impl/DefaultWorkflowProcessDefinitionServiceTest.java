package org.simplifyworks.workflow.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.workflow.model.domain.WorkflowProcessDefinitionDto;

/**
 * Test for {@link DefaultWorkflowProcessDefinitionService}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultWorkflowProcessDefinitionServiceTest extends AbstractTest{

	@Mock
	private ProcessDefinitionQuery processDefinitionQuery;
	
	@Mock
	private RepositoryService repositoryService;
	
	@InjectMocks
	private DefaultWorkflowProcessDefinitionService processDefinitionService;
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetAll() {
		// mocks and data initialization
		List<ProcessDefinition> processDefinitions = new ArrayList<>();
		
		processDefinitions.add(new ProcessDefinitionEntity());
		processDefinitions.add(new ProcessDefinitionEntity());
				
		when(repositoryService.createProcessDefinitionQuery()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.latestVersion()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.orderByProcessDefinitionName()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.asc()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.list()).thenReturn(processDefinitions);
		
		// execution of tested method
		List<WorkflowProcessDefinitionDto> result = processDefinitionService.getAll();
				
		// result verification
		assertEquals(2, result.size());
				
		verify(repositoryService).createProcessDefinitionQuery();
		verify(processDefinitionQuery).latestVersion();
		verify(processDefinitionQuery).orderByProcessDefinitionName();
		verify(processDefinitionQuery).asc();
		verify(processDefinitionQuery).list();
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testGet() {
		// mocks and data initialization
		String id = "Hello world";
						
		when(repositoryService.getProcessDefinition(id)).thenReturn(new ProcessDefinitionEntity());
				
		// execution of tested method
		WorkflowProcessDefinitionDto result = processDefinitionService.get(id);
						
		// result verification
		assertNotNull(result);
						
		verify(repositoryService).getProcessDefinition(id);
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetByKey1() {
		// mocks and data initialization
		String key = "Hello world";
							
		when(repositoryService.createProcessDefinitionQuery()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.latestVersion()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.processDefinitionKey(key)).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.singleResult()).thenReturn(new ProcessDefinitionEntity());
						
		// execution of tested method
		WorkflowProcessDefinitionDto result = processDefinitionService.getByKey(key);
								
		// result verification
		assertNotNull(result);
								
		verify(repositoryService).createProcessDefinitionQuery();
		verify(processDefinitionQuery).latestVersion();
		verify(processDefinitionQuery).processDefinitionKey(key);
		verify(processDefinitionQuery).singleResult();
	}
	
	/**
	 * Missing process definition
	 */
	@Test
	public void testGetByKey2() {
		// mocks and data initialization
		String key = "Hello world";
							
		when(repositoryService.createProcessDefinitionQuery()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.latestVersion()).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.processDefinitionKey(key)).thenReturn(processDefinitionQuery);
		when(processDefinitionQuery.singleResult()).thenReturn(null);
						
		// execution of tested method
		WorkflowProcessDefinitionDto result = processDefinitionService.getByKey(key);
								
		// result verification
		assertNull(result);
								
		verify(repositoryService).createProcessDefinitionQuery();
		verify(processDefinitionQuery).latestVersion();
		verify(processDefinitionQuery).processDefinitionKey(key);
		verify(processDefinitionQuery).singleResult();
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetImage() {
		// mocks and data initialization
		String id = "Hello world";
		
		String deploymentId = "deployment";
		String diagramResourceName = "diagram";
		
		ProcessDefinitionEntity processDefinition = new ProcessDefinitionEntity();
		processDefinition.setDeploymentId(deploymentId);
		processDefinition.setDiagramResourceName(diagramResourceName);
		
		when(repositoryService.getProcessDefinition(id)).thenReturn(processDefinition);
		when(repositoryService.getResourceAsStream(deploymentId, diagramResourceName)).thenReturn(new ByteArrayInputStream(new byte[10]));
						
		// execution of tested method
		InputStream result = processDefinitionService.getImage(id);
								
		// result verification
		assertNotNull(result);
								
		verify(repositoryService).getProcessDefinition(id);
		verify(repositoryService).getResourceAsStream(deploymentId, diagramResourceName);
	}
}
