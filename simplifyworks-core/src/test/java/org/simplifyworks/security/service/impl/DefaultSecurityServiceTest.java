package org.simplifyworks.security.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.security.domain.UserAuthentication;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.service.OrganizationStructureService;
import org.simplifyworks.uam.service.RoleStructureService;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Test for {@link DefaultSecurityService}
 * 
 * @author kakrda
 */
public class DefaultSecurityServiceTest extends AbstractTest {
        
        private static final String CURRENT_USERNAME = "current";
        private static final String ORIGINAL_USERNAME = "original";
        private static final String ROLE_NAME = "testAdmin";
        private static final String ORGANIZATION_NAME = "testOrganization";
	private static final UserAuthentication authentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, Collections.EMPTY_LIST, null);
        
        @Mock
        private SecurityContext contextMock;
       
        @Mock
        private RoleStructureService roleStructureServiceMock;

        @Mock
        private OrganizationStructureService organizationStructureServiceMock;
    
    	@InjectMocks
	private DefaultSecurityService defaultSecurityService = new DefaultSecurityService();
        
        @Before
        public void init() {
            SecurityContextHolder.setContext(contextMock);
        }
    
    	/**
	 * Happy day scenario
	 */
	@Test
	public void testIsAuthenticated() {
                // mocks and data initialization
                when(contextMock.getAuthentication()).thenReturn(authentication);                                
                        
		// execution of tested method
                boolean result = defaultSecurityService.isAuthenticated();         
              
		// result verification
                assertTrue(result);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Happy day scenario
	 */
	@Test
	public void testGetAuthentication() {
                // mocks and data initialization  
                when(contextMock.getAuthentication()).thenReturn(authentication);                                
                                        
		// execution of tested method
                UserAuthentication result = defaultSecurityService.getAuthentication();         
              
		// result verification               
                assertEquals(result.getCurrentUsername(), authentication.getCurrentUsername());
                assertEquals(result.getOriginalUsername(), authentication.getOriginalUsername());
                assertEquals(result.getAuthorities(), authentication.getAuthorities());
                assertEquals(result.getDetails(), authentication.getDetails());
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Happy day scenario
	 */
	@Test
	public void testGetUsername1() {
                // mocks and data initialization  
                when(contextMock.getAuthentication()).thenReturn(authentication);
                        
		// execution of tested method
                String result = defaultSecurityService.getUsername();         
              
		// result verification               
                assertEquals(result, authentication.getCurrentUsername());
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * No authentication
	 */
	@Test
	public void testGetUsername2() {
                // mocks and data initialization
                when(contextMock.getAuthentication()).thenReturn(null);
                        
		// execution of tested method
                String result = defaultSecurityService.getUsername();         
              
		// result verification               
                assertEquals(result, null);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Happy day scenario
	 */
	@Test
	public void testGetOriginalUsername1() {
                // mocks and data initialization  
                when(contextMock.getAuthentication()).thenReturn(authentication);
                        
		// execution of tested method
                String result = defaultSecurityService.getOriginalUsername();         
              
		// result verification               
                assertEquals(result, authentication.getOriginalUsername());
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * No authentication
	 */
	@Test
	public void testGetOriginalUsername2() {
                // mocks and data initialization                
                when(contextMock.getAuthentication()).thenReturn(null);
                        
		// execution of tested method
                String result = defaultSecurityService.getOriginalUsername();         
              
		// result verification               
                assertEquals(result, null);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Happy day scenario
	 */
	@Test
	public void testIsSwitchedUser() {
                // mocks and data initialization  
                when(contextMock.getAuthentication()).thenReturn(authentication);
                        
		// execution of tested method
                boolean result = defaultSecurityService.isSwitchedUser();         
              
		// result verification               
                assertTrue(result);
                
                verify(contextMock).getAuthentication();
        }

        /**
	 * Happy day scenario
	 */        
	@Test
	public void testGetAllRoleNames1() {
                // mocks and data initialization
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);
                
                List<String> roleNames = new ArrayList<>();
                roleNames.add(ROLE_NAME);                           
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllDescendantRoles(roleNames)).thenReturn(roleNames);
                        
		// execution of tested method
                Set<String> result = defaultSecurityService.getAllRoleNames();
                              
		// result verification               
                assertTrue(!result.isEmpty());
                
                verify(contextMock).getAuthentication();
                verify(roleStructureServiceMock).findAllDescendantRoles(roleNames);
        }
        
        /**
	 * No role names
	 */
	@Test
	public void testGetAllRoleNames2() {
                // mocks and data initialization
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);
                
                List<String> roleNames = new ArrayList<>();
                roleNames.add(ROLE_NAME);
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllDescendantRoles(roleNames)).thenReturn(Collections.EMPTY_LIST);
                        
		// execution of tested method
                Set<String> result = defaultSecurityService.getAllRoleNames();
                              
		// result verification               
                assertTrue(result.isEmpty());
                                
                verify(contextMock).getAuthentication();
                verify(roleStructureServiceMock).findAllDescendantRoles(roleNames);
        }
                
        /**
	 * Happy day scenario
	 */
	@Test
	public void testHasAnyRole1() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);
                
                List<String> roleNames = new ArrayList<>();
                roleNames.add(ROLE_NAME);                           
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllDescendantRoles(roleNames)).thenReturn(roleNames);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasAnyRole(ROLE_NAME);         
              
		// result verification
                assertTrue(result);
                
                verify(contextMock).getAuthentication();
                verify(roleStructureServiceMock).findAllDescendantRoles(roleNames);
        }
        
        /**
	 * Empty role names
	 */
	@Test
	public void testHasAnyRole2() {                        
                try {
                    // execution of tested method
		    defaultSecurityService.hasAnyRole(new String[]{});
                    fail();
		} catch (IllegalArgumentException e) {
                    //e.printStackTrace();
		}                      
        }
        
        /**
	 * Happy day scenario
	 */
	@Test
	public void testHasAllRoles1() {
            	// mocks and data initialization
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);
                
                List<String> roleNames = new ArrayList<>();
                roleNames.add(ROLE_NAME);                           
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllDescendantRoles(roleNames)).thenReturn(roleNames);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasAllRoles(ROLE_NAME);         
              
		// result verification
                assertTrue(result);
                
                verify(contextMock).getAuthentication();
                verify(roleStructureServiceMock).findAllDescendantRoles(roleNames);
        }
        
        /**
	 * Empty role names
	 */
	@Test
	public void testHasAllRoles2() {                 
                try {
                    // execution of tested method
		    defaultSecurityService.hasAllRoles(new String[]{});
                    fail();
		} catch (IllegalArgumentException e) {
                    //e.printStackTrace();
		}                      
        }
        
        /**
	 * Happy day scenario
	 */
	@Test
	public void testHasExplicitRoleInOrganization1() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);         
              
		// result verification               
                assertTrue(result);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * No role name
	 */
	@Test
	public void testHasExplicitRoleInOrganization2() {                        
                try {
                    // execution of tested method
		    defaultSecurityService.hasExplicitRoleInOrganization(null, ORGANIZATION_NAME);
                    fail();
		} catch (IllegalArgumentException e) {
                    //e.printStackTrace();
		}                      
        }
        
        /**
	 * No organization name
	 */
	@Test
	public void testHasExplicitRoleInOrganization3() {                        
                try {
                    // execution of tested method
		    defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, null);
                    fail();
		} catch (IllegalArgumentException e) {
                    //e.printStackTrace();
		}                      
        }
        
        /**
	 * No authority
	 */
	@Test
	public void testHasExplicitRoleInOrganization4() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();                
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Wrong role name
	 */
	@Test
	public void testHasExplicitRoleInOrganization5() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasExplicitRoleInOrganization("wrongName", ORGANIZATION_NAME);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Wrong organization name
	 */
	@Test
	public void testHasExplicitRoleInOrganization6() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, "wrongName");         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Wrong role name and organization name
	 */
	@Test
	public void testHasExplicitRoleInOrganization7() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasExplicitRoleInOrganization("wrongName", "wrongName");         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Happy day scenario
	 */
	@Test
	public void testHasRoleInOrganization1() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);
                
                List<String> roleNames = Collections.singletonList(ROLE_NAME);
                List<String> organizationNames = Collections.singletonList(ORGANIZATION_NAME);
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllAncestorRoles(ROLE_NAME)).thenReturn(roleNames);
                when(organizationStructureServiceMock.findAllAncestorOrganizations(ORGANIZATION_NAME)).thenReturn(organizationNames);
                
                // execution of tested method
                boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);         
              
		// result verification               
                assertTrue(result);
                
                verify(contextMock).getAuthentication();                
                verify(roleStructureServiceMock).findAllAncestorRoles(ROLE_NAME);
                verify(organizationStructureServiceMock).findAllAncestorOrganizations(ORGANIZATION_NAME);
        }
        
        /**
	 * No role name
	 */
	@Test
	public void testHasRoleInOrganization2() {                        
                try {
                    // execution of tested method
		    defaultSecurityService.hasRoleInOrganization(null, ORGANIZATION_NAME);
                    fail();
		} catch (IllegalArgumentException e) {
                    //e.printStackTrace();
		}                      
        }
        
        /**
	 * No organization name
	 */
	@Test
	public void testHasRoleInOrganization3() {                        
                try {
                    // execution of tested method
		    defaultSecurityService.hasRoleInOrganization(ROLE_NAME, null);
                    fail();
		} catch (IllegalArgumentException e) {
                    //e.printStackTrace();
		}                      
        }
        
        /**
	 * No authority
	 */
	@Test
	public void testHasRoleInOrganization4() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();
        }
        
        /**
	 * Wrong role name
	 */
	@Test
	public void testHasRoleInOrganization5() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                String wrongRoleName = "wrongRoleName";
                List<String> roleNames = Collections.singletonList(wrongRoleName);
                List<String> organizationNames = Collections.singletonList(ORGANIZATION_NAME);
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllAncestorRoles(wrongRoleName)).thenReturn(roleNames);
                when(organizationStructureServiceMock.findAllAncestorOrganizations(ORGANIZATION_NAME)).thenReturn(organizationNames);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasRoleInOrganization(wrongRoleName, ORGANIZATION_NAME);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();                
                verify(roleStructureServiceMock).findAllAncestorRoles(wrongRoleName);
                verify(organizationStructureServiceMock).findAllAncestorOrganizations(ORGANIZATION_NAME);
        }
        
        /**
	 * Wrong organization name
	 */
	@Test
	public void testHasRoleInOrganization6() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                                
                String wrongOrganizationName = "wrongOrganizationName";
                List<String> roleNames = Collections.singletonList(ROLE_NAME);
                List<String> organizationNames = Collections.singletonList(wrongOrganizationName);
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllAncestorRoles(ROLE_NAME)).thenReturn(roleNames);
                when(organizationStructureServiceMock.findAllAncestorOrganizations(wrongOrganizationName)).thenReturn(organizationNames);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, wrongOrganizationName);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();                
                verify(roleStructureServiceMock).findAllAncestorRoles(ROLE_NAME);
                verify(organizationStructureServiceMock).findAllAncestorOrganizations(wrongOrganizationName);
        }
        
        /**
	 * Wrong role name and organization name
	 */
	@Test
	public void testHasRoleInOrganization7() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
                
                String wrongRoleName = "wrongRoleName";
                String wrongOrganizationName = "wrongOrganizationName";
                List<String> roleNames = Collections.singletonList(wrongRoleName);
                List<String> organizationNames = Collections.singletonList(wrongOrganizationName);
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllAncestorRoles(wrongRoleName)).thenReturn(roleNames);
                when(organizationStructureServiceMock.findAllAncestorOrganizations(wrongOrganizationName)).thenReturn(organizationNames);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasRoleInOrganization(wrongRoleName, wrongOrganizationName);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();                
                verify(roleStructureServiceMock).findAllAncestorRoles(wrongRoleName);
                verify(organizationStructureServiceMock).findAllAncestorOrganizations(wrongOrganizationName);
        }
        
        /**
	 * No role names
	 */
	@Test
	public void testHasRoleInOrganization8() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
               
                List<String> roleNames = new ArrayList<>();
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllAncestorRoles(ROLE_NAME)).thenReturn(roleNames);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();
                verify(roleStructureServiceMock).findAllAncestorRoles(ROLE_NAME);
        }
        
        /**
	 * No organization names
	 */
	@Test
	public void testHasRoleInOrganization9() {
            	// mocks and data initialization                
                Collection<RoleOrganizationGrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
                UserAuthentication userAuthentication = new UserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, authorities, null);              
               
                List<String> roleNames = Collections.singletonList(ROLE_NAME);
                List<String> organizationNames = new ArrayList<>();
                
                when(contextMock.getAuthentication()).thenReturn(userAuthentication);
                when(roleStructureServiceMock.findAllAncestorRoles(ROLE_NAME)).thenReturn(roleNames);
                when(organizationStructureServiceMock.findAllAncestorOrganizations(ORGANIZATION_NAME)).thenReturn(organizationNames);
                                
                // execution of tested method
                boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);         
              
		// result verification               
                assertFalse(result);
                
                verify(contextMock).getAuthentication();
                verify(roleStructureServiceMock).findAllAncestorRoles(ROLE_NAME);
                verify(organizationStructureServiceMock).findAllAncestorOrganizations(ORGANIZATION_NAME);
        }
        
}
