package org.simplifyworks.security.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.jpa.internal.metamodel.SingularAttributeImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.filteroperator.EqualsFilterOperator;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUserRole;
import org.simplifyworks.uam.model.entity.CoreUserRole_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.springframework.security.core.GrantedAuthority;

/**
 * Test for {@link DefaultGrantedAuthoritiesFactory}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultGrantedAuthoritiesFactoryTest extends AbstractTest {

	@Mock
	private ElasticsearchService elasticsearchServiceMock;

	@InjectMocks
	private DefaultGrantedAuthoritiesFactory defaultGrantedAuthoritiesFactory = new DefaultGrantedAuthoritiesFactory();
	
	/**
	 * Initializes static meta model (normally is initialized by Hibernate at start)
	 */
	@Before
	public void initializeMetaModel() {
		CoreUser_.username = new SingularAttributeImpl<CoreUser, String>("username", String.class,
				null, null, false, false, false, null, null);
		CoreUserRole_.user = new SingularAttributeImpl<CoreUserRole, CoreUser>("user", CoreUser.class,
				null, null, false, false, false, null, null);
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetGrantedAuthorities1() {
		// mocks and data initialization
		String username = "test-user";

		String firstRoleName = "first-role";
		CoreRoleDto firstRole = new CoreRoleDto();
		firstRole.setName(firstRoleName);

		String secondRoleName = "second-role";
		CoreRoleDto secondRole = new CoreRoleDto();
		secondRole.setName(secondRoleName);

		String organizationName = "organization";
		CoreOrganizationDto organization = new CoreOrganizationDto();
		organization.setName(organizationName);

		CoreUserRoleWithUserDto firstUserRole = new CoreUserRoleWithUserDto();
		firstUserRole.setRole(firstRole);

		CoreUserRoleWithUserDto secondUserRole = new CoreUserRoleWithUserDto();
		secondUserRole.setRole(secondRole);
		secondUserRole.setOrganization(organization);

		List<CoreUserRoleWithUserDto> roles = new ArrayList<>();
		roles.add(firstUserRole);
		roles.add(secondUserRole);

		when(elasticsearchServiceMock.search(eq(CoreUserRoleWithUserDto.class), any(SearchParameters.class), isNull(AccessType.class))).thenReturn(roles);

		// execution of tested method
		List<GrantedAuthority> result = defaultGrantedAuthoritiesFactory.getGrantedAuthorities(username);

		// result verification
		assertEquals(2, result.size());

		Iterator<? extends GrantedAuthority> authorityIterator = result.iterator();

		RoleOrganizationGrantedAuthority firstAuthority = (RoleOrganizationGrantedAuthority) authorityIterator.next();
		assertEquals(firstRoleName, firstAuthority.getRoleName());
		assertNull(firstAuthority.getOrganizationName());

		RoleOrganizationGrantedAuthority secondAuthority = (RoleOrganizationGrantedAuthority) authorityIterator.next();
		assertEquals(secondRoleName, secondAuthority.getRoleName());
		assertEquals(organizationName, secondAuthority.getOrganizationName());

		ArgumentCaptor<SearchParameters> parametersCaptor = ArgumentCaptor.forClass(SearchParameters.class);

		verify(elasticsearchServiceMock).search(eq(CoreUserRoleWithUserDto.class), parametersCaptor.capture(), isNull(AccessType.class));
		Map<String, FilterValue> filters = parametersCaptor.getValue().getFilters();

		assertEquals(1, filters.size());

		FilterValue filter = filters.values().iterator().next();
		assertEquals(CoreUserRole_.user.getName() + "." + CoreUser_.username.getName(), filter.getPropertyName());
		assertTrue(filter.getOperator() instanceof EqualsFilterOperator);
		assertEquals(Collections.singletonList(username), filter.getValues());
	}
	
	/**
	 * No data scenario
	 */
	@Test
	public void testGetGrantedAuthorities2() {
		// mocks and data initialization
		String username = "test-user";

		when(elasticsearchServiceMock.search(eq(CoreUserRoleWithUserDto.class), any(SearchParameters.class), isNull(AccessType.class))).thenReturn(Collections.emptyList());

		// execution of tested method
		List<GrantedAuthority> result = defaultGrantedAuthoritiesFactory.getGrantedAuthorities(username);

		// result verification
		assertTrue(result.isEmpty());

		ArgumentCaptor<SearchParameters> parametersCaptor = ArgumentCaptor.forClass(SearchParameters.class);

		verify(elasticsearchServiceMock).search(eq(CoreUserRoleWithUserDto.class), parametersCaptor.capture(), isNull(AccessType.class));
		Map<String, FilterValue> filters = parametersCaptor.getValue().getFilters();

		assertEquals(1, filters.size());

		FilterValue filter = filters.values().iterator().next();
		assertEquals(CoreUserRole_.user.getName() + "." + CoreUser_.username.getName(), filter.getPropertyName());
		assertTrue(filter.getOperator() instanceof EqualsFilterOperator);
		assertEquals(Collections.singletonList(username), filter.getValues());
	}
}
