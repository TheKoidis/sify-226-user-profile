package org.simplifyworks.security.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.security.domain.AuthenticationDetails;

/**
 * Test for {@link DefaultAuthenticationDetailsSource}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultAuthenticationDetailsSourceTest extends AbstractTest {

	@Mock
	private HttpServletRequest contextMock;

	@InjectMocks
	private DefaultAuthenticationDetailsSource defaultAuthenticationDetailsSource = new DefaultAuthenticationDetailsSource();

	/**
	 * Happy day scenario
	 */
	@Test
	public void testBuildDetails1() {
		// mocks and data initialization
		String currentUsername = "current";
		String originalUsername = "original";

		when(contextMock.getHeader(DefaultAuthenticationDetailsSource.CURRENT_USERNAME_HEADER))
				.thenReturn(currentUsername);
		when(contextMock.getHeader(DefaultAuthenticationDetailsSource.ORIGINAL_USERNAME_HEADER))
				.thenReturn(originalUsername);

		// execution of tested method
		AuthenticationDetails result = defaultAuthenticationDetailsSource.buildDetails(contextMock);

		// result verification
		assertEquals(currentUsername, result.getCurrentUsername());
		assertEquals(originalUsername, result.getOriginalUsername());

		verify(contextMock).getHeader(DefaultAuthenticationDetailsSource.CURRENT_USERNAME_HEADER);
		verify(contextMock).getHeader(DefaultAuthenticationDetailsSource.ORIGINAL_USERNAME_HEADER);
	}

	/**
	 * Missing current user header
	 */
	@Test
	public void testBuildDetails2() {
		// mocks and data initialization
		when(contextMock.getHeader(DefaultAuthenticationDetailsSource.CURRENT_USERNAME_HEADER)).thenReturn(null);
		when(contextMock.getHeader(DefaultAuthenticationDetailsSource.ORIGINAL_USERNAME_HEADER)).thenReturn("original");

		try {
			// execution of tested method
			defaultAuthenticationDetailsSource.buildDetails(contextMock);

			fail();
		} catch (IllegalArgumentException e) {
			// result verification
			verify(contextMock).getHeader(DefaultAuthenticationDetailsSource.CURRENT_USERNAME_HEADER);
			verify(contextMock).getHeader(DefaultAuthenticationDetailsSource.ORIGINAL_USERNAME_HEADER);
		}
	}
	
	/**
	 * Missing original user header
	 */
	@Test
	public void testBuildDetails3() {
		// mocks and data initialization
		when(contextMock.getHeader(DefaultAuthenticationDetailsSource.CURRENT_USERNAME_HEADER)).thenReturn("current");
		when(contextMock.getHeader(DefaultAuthenticationDetailsSource.ORIGINAL_USERNAME_HEADER)).thenReturn(null);

		try {
			// execution of tested method
			defaultAuthenticationDetailsSource.buildDetails(contextMock);

			fail();
		} catch (IllegalArgumentException e) {
			// result verification
			verify(contextMock).getHeader(DefaultAuthenticationDetailsSource.CURRENT_USERNAME_HEADER);
			verify(contextMock).getHeader(DefaultAuthenticationDetailsSource.ORIGINAL_USERNAME_HEADER);
		}
	}
}
