package org.simplifyworks.security.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.security.domain.AuthenticationDetails;
import org.simplifyworks.security.domain.UserAuthentication;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Test for {@link DefaultAuthenticationProvider}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultAuthenticationProviderTest extends AbstractTest {
	
	private static final String CLIENT_NAME = "sify";
	private static final String CLIENT_PASSWORD = "pass";
	
	@Mock
	private Authentication authenticationMock;
	
	@Mock
	private GrantedAuthoritiesFactory grantedAuthoritiesFactoryMock;

	@InjectMocks
	private DefaultAuthenticationProvider defaultAuthenticationProvider = new DefaultAuthenticationProvider();

	/**
	 * Initializes client name and password (this normally does Spring using configuration)
	 */
	@Before
	public void initializeClientNameAndPassword() {
		ReflectionTestUtils.setField(defaultAuthenticationProvider, "clientName", CLIENT_NAME);
		ReflectionTestUtils.setField(defaultAuthenticationProvider, "clientPassword", CLIENT_PASSWORD);
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testAuthenticate1() {
		// mocks and data initialization
		String currentUsername = "current";
		String originalUsername = "original";
		AuthenticationDetails details = new AuthenticationDetails(currentUsername, originalUsername);
		
		String roleName = "role";
		String organizationName = "organization";
		
		List<GrantedAuthority> currentUserGrantedAuthorities = Collections.singletonList(new RoleOrganizationGrantedAuthority(roleName, organizationName));
		List<GrantedAuthority> originalUserGrantedAuthorities = Collections.singletonList(new RoleOrganizationGrantedAuthority(roleName, organizationName));
		
		when(authenticationMock.getName()).thenReturn(CLIENT_NAME);
		when(authenticationMock.getCredentials()).thenReturn(CLIENT_PASSWORD);
		when(authenticationMock.getDetails()).thenReturn(details);
		
		when(grantedAuthoritiesFactoryMock.getGrantedAuthorities(currentUsername)).thenReturn(currentUserGrantedAuthorities);
		when(grantedAuthoritiesFactoryMock.getGrantedAuthorities(originalUsername)).thenReturn(originalUserGrantedAuthorities);
		
		// execution of tested method
		Authentication result = defaultAuthenticationProvider.authenticate(authenticationMock);

		// result verification
		assertTrue(result instanceof UserAuthentication);
		assertEquals(currentUsername, result.getName());
		assertEquals(currentUserGrantedAuthorities, result.getAuthorities());
		assertEquals(details, result.getDetails());
		assertEquals(currentUsername, ((UserAuthentication) result).getCurrentUsername());
		assertEquals(originalUsername, ((UserAuthentication) result).getOriginalUsername());
		
		verify(authenticationMock).getName();
		verify(authenticationMock).getCredentials();
		verify(authenticationMock).getDetails();
		
		verify(grantedAuthoritiesFactoryMock).getGrantedAuthorities(currentUsername);
		verify(grantedAuthoritiesFactoryMock).getGrantedAuthorities(originalUsername);
	}
	
	/**
	 * Bad client credentials
	 */
	@Test
	public void testAuthenticate2() {
		// mocks and data initialization
		when(authenticationMock.getName()).thenReturn("bad");
		when(authenticationMock.getCredentials()).thenReturn("credentials");
		
		try {
			// execution of tested method
			defaultAuthenticationProvider.authenticate(authenticationMock);
			
			fail();
		} catch (BadCredentialsException e) {
			// result verification
			verify(authenticationMock).getName();
			verify(authenticationMock).getCredentials();
		}	
	}
	
	/**
	 * Bad client credentials
	 */
	@Test
	public void testAuthenticate3() {
		// mocks and data initialization
		when(authenticationMock.getName()).thenReturn(CLIENT_NAME);
		when(authenticationMock.getCredentials()).thenReturn("credentials");
		
		try {
			// execution of tested method
			defaultAuthenticationProvider.authenticate(authenticationMock);
			
			fail();
		} catch (BadCredentialsException e) {
			// result verification
			verify(authenticationMock).getName();
			verify(authenticationMock).getCredentials();
		}	
	}
	
	/**
	 * Bad client credentials
	 */
	@Test
	public void testAuthenticate4() {
		// mocks and data initialization
		when(authenticationMock.getName()).thenReturn("bad");
		when(authenticationMock.getCredentials()).thenReturn(CLIENT_PASSWORD);
		
		try {
			// execution of tested method
			defaultAuthenticationProvider.authenticate(authenticationMock);
			
			fail();
		} catch (BadCredentialsException e) {
			// result verification
			verify(authenticationMock).getName();
			verify(authenticationMock).getCredentials();
		}	
	}
	
	/**
	 * Current user does not exist or has no roles
	 */
	@Test
	public void testAuthenticate5() {
		// mocks and data initialization
		String currentUsername = "current";
		String originalUsername = "original";
		AuthenticationDetails details = new AuthenticationDetails(currentUsername, originalUsername);
											
		when(authenticationMock.getName()).thenReturn(CLIENT_NAME);
		when(authenticationMock.getCredentials()).thenReturn(CLIENT_PASSWORD);
		when(authenticationMock.getDetails()).thenReturn(details);
						
		when(grantedAuthoritiesFactoryMock.getGrantedAuthorities(currentUsername)).thenReturn(Collections.emptyList());
				
		try {
			// execution of tested method
			defaultAuthenticationProvider.authenticate(authenticationMock);
					
			fail();
		} catch (DisabledException e) {
			verify(authenticationMock).getName();
			verify(authenticationMock).getCredentials();
			verify(authenticationMock).getDetails();
					
			verify(grantedAuthoritiesFactoryMock).getGrantedAuthorities(currentUsername);
		}	
	}
	
	/**
	 * Original user does not exist or has no roles
	 */
	@Test
	public void testAuthenticate6() {
		// mocks and data initialization
		String currentUsername = "current";
		String originalUsername = "original";
		AuthenticationDetails details = new AuthenticationDetails(currentUsername, originalUsername);
				
		String roleName = "role";
		String organizationName = "organization";
			
		List<GrantedAuthority> currentUserGrantedAuthorities = Collections.singletonList(new RoleOrganizationGrantedAuthority(roleName, organizationName));
				
		when(authenticationMock.getName()).thenReturn(CLIENT_NAME);
		when(authenticationMock.getCredentials()).thenReturn(CLIENT_PASSWORD);
		when(authenticationMock.getDetails()).thenReturn(details);
				
		when(grantedAuthoritiesFactoryMock.getGrantedAuthorities(currentUsername)).thenReturn(currentUserGrantedAuthorities);
		when(grantedAuthoritiesFactoryMock.getGrantedAuthorities(originalUsername)).thenReturn(Collections.emptyList());
		
		try {
			// execution of tested method
			defaultAuthenticationProvider.authenticate(authenticationMock);
			
			fail();
		} catch (DisabledException e) {
			verify(authenticationMock).getName();
			verify(authenticationMock).getCredentials();
			verify(authenticationMock).getDetails();
			
			verify(grantedAuthoritiesFactoryMock).getGrantedAuthorities(currentUsername);
			verify(grantedAuthoritiesFactoryMock).getGrantedAuthorities(originalUsername);
		}		
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testSupports() {
		// execution of tested method
		boolean result = defaultAuthenticationProvider.supports(UsernamePasswordAuthenticationToken.class);
		
		// result verification
		assertTrue(result);
	}
}