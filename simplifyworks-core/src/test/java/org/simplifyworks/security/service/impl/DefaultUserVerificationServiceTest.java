package org.simplifyworks.security.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.hibernate.jpa.internal.metamodel.SingularAttributeImpl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.filteroperator.EqualsFilterOperator;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Test for {@link DefaultUserVerificationService}
 * 
 * @author kakrda
 */
public class DefaultUserVerificationServiceTest extends AbstractTest {
    
        private static final String USERNAME = "username";
        private static final String PASSWORD = "password";
    
        @Mock
    	private ElasticsearchService elasticsearchServiceMock;
        
        @Mock
	private GrantedAuthoritiesFactory grantedAuthoritiesFactoryMock;
        
        @Mock
	private PasswordEncoder passwordEncoderMock;
        
        @InjectMocks
	private DefaultUserVerificationService defaultUserVerificationService = new DefaultUserVerificationService();
        
        /**
	 * Initializes static meta model (normally is initialized by Hibernate at start)
	 */
	@Before
	public void initializeMetaModel() {
		CoreUser_.username = new SingularAttributeImpl<CoreUser, String>("username", String.class,
				null, null, false, false, false, null, null);
	}
    
        /**
	 * Happy day scenario
	 */
	@Test
	public void testVerify1() {            
                // mocks and data initialization
                CoreUserWithPersonDto user = new CoreUserWithPersonDto(USERNAME, PASSWORD.toCharArray());                
                List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
                grantedAuthorities.add(new RoleOrganizationGrantedAuthority("testRole", null));
                
                when(elasticsearchServiceMock.searchForSingleResult(eq(CoreUserWithPersonDto.class), any(SearchParameters.class), isNull(AccessType.class))).thenReturn(user);
                when(passwordEncoderMock.matches(PASSWORD, PASSWORD)).thenReturn(true);
                when(grantedAuthoritiesFactoryMock.getGrantedAuthorities(USERNAME)).thenReturn(grantedAuthorities);

		// execution of tested method
		boolean result = defaultUserVerificationService.verify(USERNAME, PASSWORD);

		// result verification
                assertTrue(result);

                verify(passwordEncoderMock).matches(PASSWORD, PASSWORD);
                verify(grantedAuthoritiesFactoryMock).getGrantedAuthorities(USERNAME);
                                
                ArgumentCaptor<SearchParameters> parametersCaptor = ArgumentCaptor.forClass(SearchParameters.class);
		verify(elasticsearchServiceMock).searchForSingleResult(eq(CoreUserWithPersonDto.class), parametersCaptor.capture(), isNull(AccessType.class));
		Map<String, FilterValue> filters = parametersCaptor.getValue().getFilters();
                
		assertEquals(1, filters.size());
                
		FilterValue filter = filters.values().iterator().next();
		assertEquals(CoreUser_.username.getName(), filter.getPropertyName());
		assertTrue(filter.getOperator() instanceof EqualsFilterOperator);
		assertEquals(Collections.singletonList(USERNAME), filter.getValues());            
        }
        
        /**
	 * No user
	 */
	@Test
	public void testVerify2() {            
                // mocks and data initialization                
                when(elasticsearchServiceMock.searchForSingleResult(eq(CoreUserWithPersonDto.class), any(SearchParameters.class), isNull(AccessType.class))).thenReturn(null);

		// execution of tested method
		boolean result = defaultUserVerificationService.verify(USERNAME, PASSWORD);

		// result verification
                assertFalse(result);
                  
                ArgumentCaptor<SearchParameters> parametersCaptor = ArgumentCaptor.forClass(SearchParameters.class);
		verify(elasticsearchServiceMock).searchForSingleResult(eq(CoreUserWithPersonDto.class), parametersCaptor.capture(), isNull(AccessType.class));
		Map<String, FilterValue> filters = parametersCaptor.getValue().getFilters();

		assertEquals(1, filters.size());

		FilterValue filter = filters.values().iterator().next();
		assertEquals(CoreUser_.username.getName(), filter.getPropertyName());
		assertTrue(filter.getOperator() instanceof EqualsFilterOperator);
		assertEquals(Collections.singletonList(USERNAME), filter.getValues());            
        }
        
        /**
	 * Wrong password
	 */
	@Test
	public void testVerify3() {            
                // mocks and data initialization
                String wrongPassword = "wrong";
                CoreUserWithPersonDto user = new CoreUserWithPersonDto(USERNAME, PASSWORD.toCharArray());
                
                when(elasticsearchServiceMock.searchForSingleResult(eq(CoreUserWithPersonDto.class), any(SearchParameters.class), isNull(AccessType.class))).thenReturn(user);
                when(passwordEncoderMock.matches(wrongPassword, PASSWORD)).thenReturn(false);

		// execution of tested method
		boolean result = defaultUserVerificationService.verify(USERNAME, wrongPassword);

		// result verification
                assertFalse(result);

                verify(passwordEncoderMock).matches(wrongPassword, PASSWORD);
                                
                ArgumentCaptor<SearchParameters> parametersCaptor = ArgumentCaptor.forClass(SearchParameters.class);
		verify(elasticsearchServiceMock).searchForSingleResult(eq(CoreUserWithPersonDto.class), parametersCaptor.capture(), isNull(AccessType.class));
		Map<String, FilterValue> filters = parametersCaptor.getValue().getFilters();

		assertEquals(1, filters.size());
                
		FilterValue filter = filters.values().iterator().next();
		assertEquals(CoreUser_.username.getName(), filter.getPropertyName());
		assertTrue(filter.getOperator() instanceof EqualsFilterOperator);
		assertEquals(Collections.singletonList(USERNAME), filter.getValues());            
        }
        
        /**
	 * No authorities
	 */
	@Test
	public void testVerify4() {            
                // mocks and data initialization
                CoreUserWithPersonDto user = new CoreUserWithPersonDto(USERNAME, PASSWORD.toCharArray());                
                List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
                
                when(elasticsearchServiceMock.searchForSingleResult(eq(CoreUserWithPersonDto.class), any(SearchParameters.class), isNull(AccessType.class))).thenReturn(user);
                when(passwordEncoderMock.matches(PASSWORD, PASSWORD)).thenReturn(true);
                when(grantedAuthoritiesFactoryMock.getGrantedAuthorities(USERNAME)).thenReturn(grantedAuthorities);

		// execution of tested method
		boolean result = defaultUserVerificationService.verify(USERNAME, PASSWORD);

		// result verification
                assertFalse(result);

                verify(passwordEncoderMock).matches(PASSWORD, PASSWORD);
                verify(grantedAuthoritiesFactoryMock).getGrantedAuthorities(USERNAME);
                                
                ArgumentCaptor<SearchParameters> parametersCaptor = ArgumentCaptor.forClass(SearchParameters.class);
		verify(elasticsearchServiceMock).searchForSingleResult(eq(CoreUserWithPersonDto.class), parametersCaptor.capture(), isNull(AccessType.class));
		Map<String, FilterValue> filters = parametersCaptor.getValue().getFilters();

		assertEquals(1, filters.size());
                
		FilterValue filter = filters.values().iterator().next();
		assertEquals(CoreUser_.username.getName(), filter.getPropertyName());
		assertTrue(filter.getOperator() instanceof EqualsFilterOperator);
		assertEquals(Collections.singletonList(USERNAME), filter.getValues());            
        }
    
}
