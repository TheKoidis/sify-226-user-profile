package org.simplifyworks.core.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.dozer.Mapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.core.service.PermissionElement;
import org.simplifyworks.core.service.PermissionElementFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.mockito.Mockito;

/**
 * Test for {@link DefaultReadManager}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Ignore
public class DefaultReadManagerTest extends AbstractTest {

	@Mock
	protected Mapper mapperMock;

	@Mock
	protected ObjectMapper jsonMapper;

	@Mock
	protected AbstractDto abstractDtoMock;

	@Mock
	protected EntityManager entityManagerMock;

	@Mock
	protected CriteriaBuilder criteriaBuilderMock;

	@Mock
	protected CriteriaQuery<AbstractEntity> criteriaQueryMock;

	@Mock
	protected TypedQuery<AbstractEntity> queryMock;

	@Mock
	protected AbstractEntity abstractEntityMock;

	@Mock
	protected Client clientMock;

	@Mock
	protected BulkRequestBuilder bulkBuilderMock;

	@Mock
	protected BulkResponse bulkResponseMock;

	@Mock
	protected DeleteRequestBuilder deleteBuilderMock;

	@Mock
	protected IndexRequestBuilder indexBuilderMock;

	@Mock
	protected SearchRequestBuilder searchBuilderMock;

	@Mock
	protected SearchResponse searchResponseMock;

	@Mock
	protected SearchHits searchHitsMock;

	@Mock
	protected SearchHit searchHitMock;

	@Mock
	protected PermissionElementFactory permissionElementFactoryMock;

	@Mock
	protected CriteriaQuery<Long> criteriaCountQueryMock;

	@Mock
	protected TypedQuery<Long> queryCountMock;

	@InjectMocks
	private DefaultReadManager<AbstractDto, AbstractEntity> defaultReadManager = new DefaultReadManager<AbstractDto, AbstractEntity>() {};

	/**
	 * Reindex with empty index and empty DB too.
	 */
	@Test
	public void testReindexAll1() {
		// mocks and data initialization
		when(clientMock.prepareBulk()).thenReturn(bulkBuilderMock);

		when(clientMock.prepareSearch(ElasticsearchService.INDEX_NAME)).thenReturn(searchBuilderMock);
		when(searchBuilderMock.setTypes(AbstractDto.class.getSimpleName())).thenReturn(searchBuilderMock);
		when(searchBuilderMock.setSize(Mockito.anyInt())).thenReturn(searchBuilderMock);
		when(searchBuilderMock.get()).thenReturn(searchResponseMock);
		when(searchResponseMock.getHits()).thenReturn(searchHitsMock);
		when(searchHitsMock.getHits()).thenReturn(new SearchHit[0]);

		when(entityManagerMock.getCriteriaBuilder()).thenReturn(criteriaBuilderMock);
		when(criteriaBuilderMock.createQuery(Mockito.any(Class.class))).thenReturn(criteriaCountQueryMock,criteriaQueryMock);
		when(entityManagerMock.createQuery(Mockito.any(CriteriaQuery.class))).thenReturn(queryCountMock,queryMock);
		when(queryMock.getResultList()).thenReturn(Collections.emptyList());

//		when(criteriaBuilderMock.createQuery(Long.class)).thenReturn(criteriaCountQueryMock);
//		when(entityManagerMock.createQuery(criteriaCountQueryMock)).thenReturn(queryCountMock);
	  when(queryCountMock.getSingleResult()).thenReturn(new Long(Collections.emptyList().size()));
//		doReturn(0L).when(defaultReadManager.getEntityRecCount());

		when(bulkBuilderMock.numberOfActions()).thenReturn(0);

		// execution of tested method
		long result = defaultReadManager.reindexAll();

		// result verification
		assertEquals(0L, result);

		verify(clientMock).prepareBulk();
		verify(clientMock).prepareSearch(ElasticsearchService.INDEX_NAME);
		verify(searchBuilderMock).setTypes(AbstractDto.class.getSimpleName());
		verify(searchBuilderMock).setSize(Mockito.anyInt());
		verify(searchBuilderMock).get();
		verify(searchResponseMock).getHits();
		verify(searchHitsMock).getHits();

		verify(entityManagerMock).getCriteriaBuilder();
		verify(criteriaBuilderMock).createQuery(Mockito.any(Class.class));
		verify(criteriaQueryMock).from(AbstractEntity.class);
		verify(entityManagerMock).createQuery(criteriaQueryMock);
		verify(queryMock).getResultList();

		verify(bulkBuilderMock).numberOfActions();
	}

	/**
	 * Reindex with non empty index and empty DB.
	 */
	@Test
	public void testReindexAll2() {
		// mocks and data initialization
		when(clientMock.prepareBulk()).thenReturn(bulkBuilderMock);

		when(clientMock.prepareSearch(ElasticsearchService.INDEX_NAME)).thenReturn(searchBuilderMock);
		when(searchBuilderMock.setTypes(AbstractDto.class.getSimpleName())).thenReturn(searchBuilderMock);
		when(searchBuilderMock.setSize(Mockito.anyInt())).thenReturn(searchBuilderMock);
		when(searchBuilderMock.get()).thenReturn(searchResponseMock);
		when(searchResponseMock.getHits()).thenReturn(searchHitsMock);
		when(searchHitsMock.getHits()).thenReturn(new SearchHit[] {searchHitMock});
		when(searchHitMock.getIndex()).thenReturn(ElasticsearchService.INDEX_NAME);
		when(searchHitMock.getType()).thenReturn(AbstractDto.class.getSimpleName());
		when(searchHitMock.getId()).thenReturn("1234");
		when(clientMock.prepareDelete(ElasticsearchService.INDEX_NAME, AbstractDto.class.getSimpleName(), "1234")).thenReturn(deleteBuilderMock);

		when(entityManagerMock.getCriteriaBuilder()).thenReturn(criteriaBuilderMock);
		when(criteriaBuilderMock.createQuery(AbstractEntity.class)).thenReturn(criteriaQueryMock);
		when(entityManagerMock.createQuery(criteriaQueryMock)).thenReturn(queryMock);
		when(queryMock.getResultList()).thenReturn(Collections.emptyList());

		when(bulkBuilderMock.numberOfActions()).thenReturn(1);
		when(bulkBuilderMock.get()).thenReturn(bulkResponseMock);
		when(bulkResponseMock.getTook()).thenReturn(new TimeValue(1000));

		// execution of tested method
		long result = defaultReadManager.reindexAll();

		// result verification
		assertEquals(0L, result);

		verify(clientMock).prepareBulk();
		verify(clientMock).prepareSearch(ElasticsearchService.INDEX_NAME);
		verify(searchBuilderMock).setTypes(AbstractDto.class.getSimpleName());
		verify(searchBuilderMock).setSize(Mockito.anyInt());
		verify(searchBuilderMock).get();
		verify(searchResponseMock).getHits();
		verify(searchHitsMock).getHits();
		verify(searchHitMock).getIndex();
		verify(searchHitMock).getType();
		verify(searchHitMock).getId();
		verify(clientMock).prepareDelete(ElasticsearchService.INDEX_NAME, AbstractDto.class.getSimpleName(), "1234");
		verify(bulkBuilderMock).add(deleteBuilderMock);

		verify(entityManagerMock).getCriteriaBuilder();
		verify(criteriaBuilderMock).createQuery(AbstractEntity.class);
		verify(criteriaQueryMock).from(AbstractEntity.class);
		verify(entityManagerMock).createQuery(criteriaQueryMock);
		verify(queryMock).getResultList();

		verify(bulkBuilderMock).numberOfActions();
		verify(bulkBuilderMock).get();
		verify(bulkResponseMock).getTook();
	}

	/**
	 * Reindex with empty index and non empty DB.
	 */
	@Test
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void testReindexAll3() throws JsonProcessingException {
		// mocks and data initialization
		when(clientMock.prepareBulk()).thenReturn(bulkBuilderMock);

		when(clientMock.prepareSearch(ElasticsearchService.INDEX_NAME)).thenReturn(searchBuilderMock);
		when(searchBuilderMock.setTypes(AbstractDto.class.getSimpleName())).thenReturn(searchBuilderMock);
		when(searchBuilderMock.setSize(Mockito.anyInt())).thenReturn(searchBuilderMock);
		when(searchBuilderMock.get()).thenReturn(searchResponseMock);
		when(searchResponseMock.getHits()).thenReturn(searchHitsMock);
		when(searchHitsMock.getHits()).thenReturn(new SearchHit[0]);

		when(entityManagerMock.getCriteriaBuilder()).thenReturn(criteriaBuilderMock);
		when(criteriaBuilderMock.createQuery(AbstractEntity.class)).thenReturn(criteriaQueryMock);
		when(entityManagerMock.createQuery(criteriaQueryMock)).thenReturn(queryMock);
		when(queryMock.getResultList()).thenReturn(Collections.singletonList(abstractEntityMock));
		when(mapperMock.map(abstractEntityMock, AbstractDto.class)).thenReturn(abstractDtoMock);
		when(abstractDtoMock.getId()).thenReturn(1234L);

		when(clientMock.prepareIndex(ElasticsearchService.INDEX_NAME, AbstractDto.class.getSimpleName(), "1234")).thenReturn(indexBuilderMock);
		when(indexBuilderMock.setSource("test".getBytes())).thenReturn(indexBuilderMock);
		when(jsonMapper.writeValueAsBytes(abstractDtoMock)).thenReturn("test".getBytes());

		when(bulkBuilderMock.numberOfActions()).thenReturn(1);
		when(bulkBuilderMock.get()).thenReturn(bulkResponseMock);
		when(bulkResponseMock.getTook()).thenReturn(new TimeValue(1000));

		// execution of tested method
		long result = defaultReadManager.reindexAll();

		// result verification
		assertEquals(1L, result);

		verify(clientMock).prepareBulk();
		verify(clientMock).prepareSearch(ElasticsearchService.INDEX_NAME);
		verify(searchBuilderMock).setTypes(AbstractDto.class.getSimpleName());
		verify(searchBuilderMock).setSize(Mockito.anyInt());
		verify(searchBuilderMock).get();
		verify(searchResponseMock).getHits();
		verify(searchHitsMock).getHits();

		verify(entityManagerMock).getCriteriaBuilder();
		verify(criteriaBuilderMock).createQuery(AbstractEntity.class);
		verify(criteriaQueryMock).from(AbstractEntity.class);
		verify(entityManagerMock).createQuery(criteriaQueryMock);
		verify(queryMock).getResultList();
		verify(permissionElementFactoryMock).createPermissionElements(AbstractEntity.class, null);
		verify(mapperMock).map(abstractEntityMock, AbstractDto.class);
		verify(abstractDtoMock, atLeast(1)).getId();

		ArgumentCaptor<List> permissionsCaptor = ArgumentCaptor.forClass(List.class);
		verify(abstractDtoMock).setPermissions(permissionsCaptor.capture());
		assertEquals(1, permissionsCaptor.getValue().size());
		PermissionElement permission = (PermissionElement) permissionsCaptor.getValue().get(0);
		assertEquals("admin", permission.getRoleName());
		assertNull(permission.getOrganizationName());
		assertNull(permission.getUserName());
		assertTrue(permission.isCanRead());
		assertTrue(permission.isCanUpdate());
		assertTrue(permission.isCanRemove());

		verify(clientMock).prepareIndex(ElasticsearchService.INDEX_NAME, AbstractDto.class.getSimpleName(), "1234");
		verify(jsonMapper).writeValueAsBytes(abstractDtoMock);
		verify(indexBuilderMock).setSource("test".getBytes());
		verify(bulkBuilderMock).add(indexBuilderMock);

		verify(bulkBuilderMock).numberOfActions();
		verify(bulkBuilderMock).get();
		verify(bulkResponseMock).getTook();
	}
}
