/**
 * 
 */
package org.simplifyworks.ecm.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;

/**
 * Test for {@link StandaloneJasperReportManager}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class EmbeddedJasperReportManagerTest extends AbstractTest {

	private static final String templateBaseDir;
	
	private static final String dataBaseDir = System.getProperty("java.io.tmpdir") + File.separator + "data";
	private static final String reportBaseDir = System.getProperty("java.io.tmpdir") + File.separator + "report";
	
	static {
		try {
			templateBaseDir = new File(EmbeddedJasperReportManagerTest.class.getResource("/").toURI()).getAbsolutePath();
		} catch (URISyntaxException e) {
			throw new Error(e);
		}
	}
	
	private EmbeddedJasperReportManager jasperReportManager = new EmbeddedJasperReportManager();
	
	@Before
	public void initProperties() {
		jasperReportManager.setTemplateBaseDir(templateBaseDir);
		
		jasperReportManager.setDataBaseDir(dataBaseDir);
		jasperReportManager.setReportBaseDir(reportBaseDir);
		
		jasperReportManager.cleanDirs();
	}
	
	/**
	 * PDF scenario
	 */
	@Test
	public void testGenerateReport1() {
		// mocks and data initialization
		String name = "Test";
		ReportType type = ReportType.PDF;
		Object object = createDataObject();
				
		// execution of tested method
		Report result = jasperReportManager.generateReport(name, type, object);
					
		// result verification
		assertNotNull(result);
		assertEquals(name, result.getName());
		assertEquals(type, result.getType());
		assertNotNull(result.getDataUri());
		assertNotNull(result.getReportUri());
		
		assertTrue(result.getReportUri().toString().endsWith(type.getSuffix()));
	}
	
	/**
	 * HTML scenario
	 */
	@Test
	public void testGenerateReport2() {
		// mocks and data initialization
		String name = "Test";
		ReportType type = ReportType.HTML;
		Object object = createDataObject();
				
		// execution of tested method
		Report result = jasperReportManager.generateReport(name, type, object);
					
		// result verification
		assertNotNull(result);
		assertEquals(name, result.getName());
		assertEquals(type, result.getType());
		assertNotNull(result.getDataUri());
		assertNotNull(result.getReportUri());
		
		assertTrue(result.getReportUri().toString().endsWith(type.getSuffix()));
	}
	
	/**
	 * XML scenario
	 */
	@Test
	public void testGenerateReport3() {
		// mocks and data initialization
		String name = "Test";
		ReportType type = ReportType.XML;
		Object object = createDataObject();
				
		// execution of tested method
		Report result = jasperReportManager.generateReport(name, type, object);
					
		// result verification
		assertNotNull(result);
		assertEquals(name, result.getName());
		assertEquals(type, result.getType());
		assertNotNull(result.getDataUri());
		assertNotNull(result.getReportUri());
		
		assertTrue(result.getReportUri().toString().endsWith(type.getSuffix()));
	}
	
	/**
	 * Missing template scenario
	 */
	@Test
	public void testGenerateReport4() {
		// mocks and data initialization
		String name = "Boo";
		ReportType type = ReportType.XML;
		Object object = createDataObject();
			
		try {
			// execution of tested method
			jasperReportManager.generateReport(name, type, object);
		
			fail();
		} catch(CoreException e) {
			// result verification
			assertNotNull(e);
		}
	}
	
	private Data createDataObject() {
		Data data = new Data();
		
		data.persons.add(new Person("John", "England"));
		data.persons.add(new Person("Jan", "Czech Republic"));
		
		return data;
	}
}
