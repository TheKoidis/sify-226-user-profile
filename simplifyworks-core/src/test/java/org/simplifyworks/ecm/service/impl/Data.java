/**
 * 
 */
package org.simplifyworks.ecm.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data object for report tests
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@XmlRootElement
class Data {
	
	@XmlElement(name = "persons")
	List<Person> persons = new ArrayList<>();
	
	public Data() {
	}
}
