/**
 * 
 */
package org.simplifyworks.ecm.service.impl;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Person object for report tests
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@XmlRootElement
class Person {
	
	@XmlElement
	String name;
	
	@XmlElement
	String country;
	
	public Person() {
	}
	
	public Person(String name, String country) {
		this.name = name;
		this.country = country;
	}
}
