# README #

## What is this repository for? ##

This repository holds a Simplifyworks framework and the example client implementation. 

Simplifyworks is an open source platform providing a bunch of ready made components with main focus on making your document workflow really simple. You can read more about it on the [project website](http://www.simplifyworks.org).

## Repository organization ##

The basic structure of this repo is divided in folders:

Folder | What's in it?
------ | --------------
/simplifyworks-client-example | Java Example client (aka example backend)
/simplifyworks-client-example-ui |  JavaScript Example client (aka example frontend)
/simplifyworks-core | Java core of the framework
/simplifyworks-core-ui | JavaScript core of the framework
/simplifyworks-test | Java project containing the test classes
/sql | Scripts used to create/modify the database objects

## Branches ##

Branches are simply **master**, where the stable version should reside and **devel**, where the development is done. Only hotfixes and critical bugs are pushed to master, everything else is in devel branch.

## How do I get set up? ##

**Please hold on, we are working on this topic.**

You can request access to [this document](https://drive.google.com/open?id=1zXfSMYZNv6oiCcxDgiDs4ZT3daUlfZpKLKNVDg1kYWE) [Czech language only] in the meantime and [contact us](mailto:getstarted@simplifyworks.org) in case you get stuck.

## Summary of set up ##
### JRE & Backend Servers ###

There are no unusual tools needed, but you'll obviously need

* Java 8 ([download latest](https://java.com/download/))
* Tomcat 8 ([download latest](http://tomcat.apache.org/download-80.cgi))
* Any modern JDBC compliant SQL database (preferably [MySQL](https://dev.mysql.com/downloads/mysql/) or [Oracle](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html))

You may optionally create your own instance of [Elasticsearch](https://www.elastic.co/downloads/elasticsearch). If you don't, the framework summons an instance of the embedded Elasticsearch node (see ElasticsearchConfig.java for the details).

#### ElasticSearch configuration ####

We've added some parameters giving you control over Elasticsearch client behavior. You can modify your application.properties or use the JVM parameters with -D.

Application.properties | JVM Parameters | Default Value | Meaning
---------------------- | -------------- | ------------- | -------
spring.data.elasticsearch.cluster-nodes | elasticsearch.nodes | N/A | Where to find the elasticsearch transport client port. You specify many of them delimited by comma.
spring.data.elasticsearch.localPathData | elasticsearch.path | /tmp/localES/data | Where do we put the data files for embedded Elasticsearch node.
spring.data.elasticsearch.localPathLogs | elasticsearch.logs | /tmp/localES/logs | Where do we put the logs for embedded Elasticsearch node. Note that we are not logging anything as of v0.3.
spring.data.elasticsearch.localShards | elasticsearch.shards | 1 | Number of local shards.
spring.data.elasticsearch.localReplicas | elasticsearch.replicas | 0 | Number of local replicas.


### Downloading the source ###
Assuming you have git installed, you can do

```
#!bash

git clone -b master --single-branch  git@bitbucket.org:simplifyworks/sify.git simplifyworks
```

### Compiling and packaging the backend ###

All you need is properly configured [Maven](https://maven.apache.org/download.cgi). You step into the simplifyworks-client-example and package the war file.

```
#!bash

cd simplifyworks-client-example
mvn clean
mvn package -Dmaven.test.skip=true
```

As the result, you should find your simplifyworks-client-example/target/simplifyworks-client-example-*.war in a few seconds. If you want to run the test suite, which is not necessary when you're downloading from the master branch, it will take much longer.

Note that we normally use Netbeans IDE, Eclipse IDE and IntelliJ IDEA, so the build should work fine in any of these environments too.


### Preparing your database ###

You either have one or you have to create an empty database. Then go to the /sql/create directory and run create.sql and dump.sql.
This will create the basic database structures for you. If everything goes fine with no errors, just go to the /sql/change/mysql
(or /sql/change/oracle) directory and run all scripts up to the version of the framework you are using. So, for example, if you plan
to use v0.2 on MySQL, you just run /sql/change/mysql/0.2.sql.

### Deploying to Tomcat ###

Before you start the deployment, please make sure that you have the JDBC driver installed in the tomcat's lib directory.
In case you are using the MySQL, you can download the jar file [here](http://dev.mysql.com/downloads/connector/j/).

Next step is to modify conf/context.xml and put your database connection information in there. The JDBC alias is kolobehy as of v0.2.

```xml
<Resource
	name="jdbc/kolobehy"
	auth="Container"
	type="javax.sql.DataSource"
	username="youruser"
	password="yoursecret"
	driverClassName="com.mysql.jdbc.Driver"
	url="jdbc:mysql://examplehost.yourdomain/simplifyworksdb?useUnicode=true&amp;characterEncoding=UTF-8&amp;autoReconnect=true"
	maxActive="10"
	maxIdle="8"/>
```

Feel free to change _youruser_, _yoursecret_, _examplehost.yourdomain_ and _simplifyworksdb_ to whatever you need.

We also recommend to start tomcat on different port than the default tcp/8080 (because the frontend runs on that port by default). Find your server.xml and change this

```xml
<Connector
	port="8080"
	protocol="HTTP/1.1"
	connectionTimeout="20000"
	redirectPort="8443" />
```

into this

```xml
<Connector
	port="8084"
	protocol="HTTP/1.1"
	connectionTimeout="5000"
	redirectPort="8443" />
```

Then start your tomcat and use the manager application to deploy the app to /simplifyworks context.
You can test that your application backend is running by going to [http://localhost:8084/simplifyworks/](http://localhost:8084/simplifyworks/) or any other host the tomcat is running on.

### Node.js and the frontend configuration ###

All you need on the frontend side is [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/).
At first, you have to compile the core module

```
#!bash

cd simplifyworks-core-ui
[sudo] npm install
[sudo] npm link
```

Then you go to the /simplifyworks-client-example-ui and link the core module in there, install remaining modules and run gulp.

```
#!bash

cd simplifyworks-client-example-ui
[sudo] npm link simplifyworks-core
[sudo] npm install
gulp
```

That should do the trick and if everything goes fine, you can find your frontend on [http://localhost:8080](http://localhost:8080).
It expects the backend running on localhost:8084/simplifyworks, so in case you run the backend on other machine, you can specify it to gulp

```
#!bash

gulp --backend http://someothermachine:1234
```

You should now be able to log in using username _admin_ and password _defaultpassword_ which should be immediately
changed in case the port is accessible from other machines.

You can also configure the frontend port by using --port parameter.

## Contribution guidelines ##

### Basics ###

If you want to contribute, please clone the devel branch using

```
#!bash

git clone -b devel --single-branch git@bitbucket.org:simplifyworks/sify.git sify-devel
```

When pulling our changes from repository, we always rebase by calling

```
#!bash

git pull --rebase
```

That basically means no other branch is created by merging our local changes. On the other hand you should fork the repo,
create your own branch (or request one being created for you) and use the pull request facility to push your work **to the devel branch**.
Please note that no changes to the master branch will be accepted if you are not a member of the core development team.

Also, please read the [Pull requests tutorial](https://www.atlassian.com/git/tutorials/making-a-pull-request/example) before starting
any work on the project. It can save you hours of wondering how to do things properly.

**Please never forget to use proper [JIRA](https://simplifyworks.atlassian.net/secure/RapidBoard.jspa?rapidView=5) issue key in the commit comments!**

Example:
```
#!bash

git checkout mybranch
git commit -a -m "SIFY-159 I have changed this and that, because it will be much greater."
git push
```

## Who do I talk to? ##

If you need help getting started, please do not hesitate to [contact us](mailto:getstarted@simplifyworks.org).