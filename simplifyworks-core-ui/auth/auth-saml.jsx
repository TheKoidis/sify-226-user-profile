var Strategy = require.options.defaultRequire('passport-saml').Strategy;
var fs = require('fs');

/**
*   AuthSaml represents configuration of saml (shibboleth) authentication strategy.
*/
var AuthSaml = {

    // initialize strategy
    init: function(passport, app, backendUrl) {
        console.log('SAML', process.env.AUTH_SAML_ENABLED);

        if(process.env.AUTH_SAML_ENABLED === 'true') {
            // configuration attributes
            var ssoUrl = process.env.AUTH_SAML_SSO_URL ? process.env.AUTH_SAML_SSO_URL : 'https://idp2.ders.cz/idp/profile/SAML2/Redirect/SSO';
            var ssoCallbackUrl = process.env.AUTH_SAML_SSO_CALLBACK_URL ?  process.env.AUTH_SAML_SSO_CALLBACK_URL : 'http://javadev1.ders.cz:8081/login/Shibboleth/callback';

            var issuer = process.env.AUTH_SAML_ISSUER ? process.env.AUTH_SAML_ISSUER : 'simplify.ders.cz';

            var idpCert = fs.readFileSync(process.env.AUTH_SAML_IDP_CERT ? process.env.AUTH_SAML_IDP_CERT : './certs/saml-idp-cert.pem', 'utf-8');
            var spKey = fs.readFileSync(process.env.AUTH_SAML_SP_KEY ? process.env.AUTH_SAML_SP_KEY : './certs/saml-sp-key.pem', 'utf-8');
            var spCert = fs.readFileSync(process.env.AUTH_SAML_SP_CERT ? process.env.AUTH_SAML_SP_CERT : './certs/saml-sp-cert.pem', 'utf-8');

            var identifierFormat = 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified';
            var acceptedClockSkewMs = 5 * 60 * 1000;
            var usernameMetadataIdentifier = 'urn:oid:0.9.2342.19200300.100.1.1';
            var sloUrl = process.env.AUTH_SAML_SLO_URL ? process.env.AUTH_SAML_SLO_URL : 'https://idp2.ders.cz/logout?return=http://javadev1.ders.cz:8081/login';

            // prepare strategy
            var strategy = new Strategy(
                {
                    // IDP SSO URL
                    entryPoint: ssoUrl,
                    // application SSO callback URL
                    callbackUrl: ssoCallbackUrl,

                    // IDP SLO URL (currently SLO is not implemented in this way)
                    // logoutUrl: 'https://idp2.ders.cz/idp/profile/SAML2/Redirect/SLO',
                    // application SLO callback URL (currently SLO is not implemented in this way)
                    // logoutCallbackUrl: 'http://javadev1.ders.cz:8081/logout/Shibboleth/callback',

                    // application domain
                    issuer: issuer,

                    // IDP cert
                    cert: idpCert,

                    // SP private cert
                    privateCert: spKey,
                    decryptionPvk: spKey,

                    // we do not suppose any format of identifier
                    identifierFormat: identifierFormat,

                    // we accept 5 minutes clock skew
                    acceptedClockSkewMs: acceptedClockSkewMs
                },
                function(profile, done) {
                    // check profile (response from IDP)
                    if(profile && profile[usernameMetadataIdentifier]) {
                        var expiration = Date.now() + 1000 * (process.env.SESSION_EXPIRATION_TIMEOUT ? parseInt(process.env.SESSION_EXPIRATION_TIMEOUT) : 3600);
                        var expirationWarning = Date.now() + 1000 * (process.env.SESSION_EXPIRATION_WARNING_TIMEOUT ? parseInt(process.env.SESSION_EXPIRATION_WARNING_TIMEOUT) : 3300)

                        // use callback with user info
                        return done(null, {
                            strategy: 'saml',
                            currentUsername: profile[usernameMetadataIdentifier],
                            originalUsername: profile[usernameMetadataIdentifier],
                            logoutUrl: sloUrl,

                            expirationUrl: '/login?fail=session_expired',
                            expiration: expiration,
                            expirationWarning: expirationWarning
                        });
                    } else {
                        return done(null, false);
                    }
                }
            );

            // enable strategy in passport
            passport.use(strategy);

            // metadata handling
            console.log('Registering Shibboleth metadata');

            app.get('/Shibboleth.sso/Metadata', function(req, res) {
                res.type('application/xml');
                res.status(200);
                res.send(strategy.generateServiceProviderMetadata(spCert));
            });

            // login handling
            app.get('/login/Shibboleth', passport.authenticate('saml', {failureRedirect: '/login'}));

            // login callback handling
            app.post('/login/Shibboleth/callback', passport.authenticate('saml', {failureRedirect: '/login'}),
                function(req, res) {
                    // we must redirect to after-login page, where user info is loaded to browser session
                    res.redirect('/after-login');
                }
            );

            /* SLO handling (currently SLO is not implemented in this way)
            app.get('/logout/Shibboleth', function(req, res) {
                req.user.nameID = req.user.username;
                req.user.nameIDFormat = usernameMetadataIdentifier;

                strategy.logout(req, function(err, request){
                    if(!err) {
                        //redirect to the IdP Logout URL
                        res.redirect(request);
                    }
                });
            });

            app.post('/logout/Shibboleth/callback', function(req, res) {
                req.logout();
                res.redirect('/login/Shibboleth');
            });
            */
        }
    }
};

module.exports = AuthSaml;
