var passport = require.options.defaultRequire('passport');
var Request = require('request');

/**
*   Auth represents base authentication class. Authentication strategies can be enabled/disabled here.
*/
var Auth = {
    init: function(app, backendServerUrl) {
        // enable passport in express
        app.use(passport.initialize());
        app.use(passport.session());

        // set serialization function (key == value)
        passport.serializeUser(function(user, done) {
            done(null, user);
        });

        // set deserialization function (key == value)
        passport.deserializeUser(function(id, done) {
            done(null, id);
        });

        // enable form authentication
        var AuthForm = require('./auth-form.jsx');
        AuthForm.init(passport, app, backendServerUrl);

        // enable saml (shibboleth) authentication
        var AuthSaml = require('./auth-saml.jsx');
        AuthSaml.init(passport, app, backendServerUrl);

        // logout handling
        app.get('/logout', function(req, res) {
            // get logout url from user (i.e. from session)
            var logoutUrl = req.user.logoutUrl;

            // log out user from app
            req.logout();

            // redirect user to required url (to welcome page, to IDP etc.)
            res.redirect(logoutUrl);
        });

        app.post('/superuser', function(req, res) {
            if(req.body.currentUsername === req.user.originalUsername) {
                // go back to original user (no check needed)
                req.user.currentUsername = req.user.originalUsername;

                res.status(204).end();
            } else {
                var headers = {
                    'Authorization': 'Basic c2lmeTpwYXNz',
                    'Current-Username': req.user.originalUsername,
                    'Original-Username': req.user.originalUsername
                }

                Request.get({url: backendServerUrl + '/api/core/user-profile/current', headers: headers, json: true}, function(err, response, body) {
                    if(body.resource.allRoles.indexOf('admin') !== -1) {
                        req.user.currentUsername = req.body.currentUsername;

                        res.status(204).end();
                    } else {
                        res.status(401).end();
                    }
                });
            }
        });
    }
};

module.exports = Auth;
