var Strategy = require.options.defaultRequire('passport-local').Strategy;
var Request = require('request');

/**
*   AuthForm represents configuration of form authentication strategy.
*/
var AuthForm = {

    // initialize strategy
    init: function(passport, app, backendUrl) {
        // enable strategy in passport
        passport.use(new Strategy(
            {},
            function(username, password, done) {
                // authenticate user using backend
                Request.get(backendUrl + '/auth?username=' + username + '&password=' + password, function(err, response, body) {
                    // in case of error do not set the user
                    if(err || !body) {
                        return done(err, false);
                    }

                    var json = JSON.parse(body);

                    // if user is succesfully authenticated json.resource will be true, otherwise false
                    if(json.resource) {
                        var expiration = Date.now() + 1000 * (process.env.SESSION_EXPIRATION_TIMEOUT ? parseInt(process.env.SESSION_EXPIRATION_TIMEOUT) : 3600);
                        var expirationWarning = Date.now() + 1000 * (process.env.SESSION_EXPIRATION_WARNING_TIMEOUT ? parseInt(process.env.SESSION_EXPIRATION_WARNING_TIMEOUT) : 3300)

                        // use callback with user info
                        return done(null, {
                            strategy: 'form',
                            currentUsername: username,
                            originalUsername: username,
                            logoutUrl: '/login',

                            expirationUrl: '/login?fail=session_expired',
                            expiration: expiration,
                            expirationWarning: expirationWarning
                        });
                    } else {
                        return done(null, false);
                    }
                });
            })
        );

        // login handling (we do not use common redirect after success/fail but http status)
        app.post('/login/form', function(req, res, next) {
            passport.authenticate('local', function(err, user, info) {
                if (err) {
                    return next(err);
                }

                if (!user) {
                    // user is not allowed to continue
                    res.redirect('/login?fail=login_bad_credentials');
                    return;
                }

                req.logIn(user, function(err) {
                    if (err) {
                        return next(err);
                    }

                    // user is successfully authenticated
                    res.redirect('/after-login');
                    return;
                });
            })(req, res, next);
        });
    }
};

module.exports = AuthForm;
