var FlashConstants = {
    FLASH_ADD: "FLASH_ADD",
    FLASH_TOAST: "FLASH_TOAST",
    FLASH_CLEAR: "FLASH_CLEAR",
    FLASH_AJAX_ERROR: "FLASH_AJAX_ERROR"
};

module.exports = FlashConstants;
