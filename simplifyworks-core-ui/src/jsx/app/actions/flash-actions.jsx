var constants = require('./flash-constants.jsx');

var FlashActions = {
    add: function(options) {
        this.dispatch(constants.FLASH_ADD, options);
    },

    toast: function(options) {
        this.dispatch(constants.FLASH_TOAST, options);
    },

    clear: function(container) {
        this.dispatch(constants.FLASH_CLEAR, container);
    },

    ajaxError: function(xhr, status, error, options) {
        this.dispatch(constants.FLASH_AJAX_ERROR, {
            xhr: xhr,
            status: status,
            error: error,
            options: options
        });
    }
};

module.exports = FlashActions;
