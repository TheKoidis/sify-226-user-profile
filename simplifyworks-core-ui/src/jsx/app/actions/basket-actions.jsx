var constants = require('./basket-constants.jsx');

var BasketActions = {
    add: function(id, type) {
        this.dispatch(constants.BASKET_ADD, id, type);
    },

    remove: function(id, type) {
        this.dispatch(constants.BASKET_REMOVE, id, type);
    },

    contains: function(id, type) {
        this.dispatch(constants.BASKET_CONTAINS, id, type);
    },

    clear: function(type) {
        this.dispatch(constants.BASKET_CLEAR, type);
    },

    get(type) {
        this.dispatch(constants.BASKET_GET, type);
    },

    getString(type) {
        this.dispatch(constants.BASKET_GET_STRING, type);
    },
};

module.exports = BasketActions;
