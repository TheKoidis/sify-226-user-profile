var constants = require('./security-constants.jsx');

var SecurityActions = {
    reloadContext: function(callback) {
        this.dispatch(constants.SECURITY_RELOAD_CONTEXT, {callback: callback});
    },

    changeCurrentUser: function(username, callback) {
        this.dispatch(constants.SECURITY_CHANGE_CURRENT_USER, {username: username, callback: callback});
    }
};

module.exports = SecurityActions;
