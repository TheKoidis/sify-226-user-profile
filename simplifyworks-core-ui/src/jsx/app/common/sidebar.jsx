var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var Sidebar = React.createClass({

    mixins: [ReactRouter.State, FluxMixin, Fluxxor.StoreWatchMixin("SecurityStore")],

    getStateFromFlux: function () {
        return this.getFlux().store("SecurityStore").getState();
    },

    render: function () {
        var isAdmin = this.getFlux().store("SecurityStore").hasRole('admin');
        var currentUsername = this.state.currentUser ? this.state.currentUser.username : undefined;
        var originalUsername = this.state.originalUser ? this.state.originalUser.username : undefined;
        var showSystem = isAdmin || (currentUsername !== originalUsername);

        return (
            showSystem ?
                (
                    <SidebarNavItem glyph='icon-simple-line-icons-settings'
                                    name={
                                <span><Entity entity='core_navigation' data={{item: 'system'}} /></span>
                            }>

                        <SidebarNav>
                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-simple-line-icons-user'
                                        name={<Entity entity='users'/>}
                                        href='/system/user-list'/>
                                :   null
                            }

                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-dripicons-user'
                                        name={<Entity entity='persons'/>}
                                        href='/system/person-list'/>
                                :   null
                            }

                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-simple-line-icons-users'
                                        name={<Entity entity='roles'/>}
                                        href='/system/role-list'/>
                                :   null
                            }

                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-dripicons-home'
                                        name={<Entity entity='organizations'/>}
                                        href='/system/organization-list'/>
                                :   null
                            }

                            <SidebarNavItem glyph='icon-fontello-users'
                                name={<Entity entity='superuser'/>}
                                href='/system/superuser-detail'/>


                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-simple-line-icons-settings'
                                        name={<Entity entity='core_navigation' data={{item: 'setting'}}/>}
                                        href='/system/setting-list'/>
                                :   null
                            }

                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-dripicons-attachment'
                                        name={<Entity entity='core_navigation' data={{item: 'attachment'}}/>}
                                        href='/system/attachment-list'/>
                                :   null
                            }

                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-fontello-flow-branch'
                                        name={<Entity entity='core_navigation' data={{item: 'workflow'}}/>}
                                        href='/system/workflow-list'/>
                                :   null
                            }

                            {isAdmin
                                ?   <SidebarNavItem glyph='icon-simple-line-icons-grid'
                                        name={<Entity entity='core_navigation' data={{item: 'template'}}/>}
                                        href='/system/template-list'/>
                                :   null
                            }

                            {isAdmin
							    ?   <SidebarNavItem glyph='icon-fontello-docs'
                                        name={<Entity entity='core_navigation' data={{item: 'printModules'}}/>}
                                        href='/system/print-module-list'/>
                                :   null
                            }

							{this.getFlux().store("SecurityStore").hasRole('admin_scheduler')
                            	?	<SidebarNavItem glyph='icon-fontello-clock'
                                    	name={<Entity entity='tasks'/>}
                                        href='/system/task-list'/>
                                :	null
                            }
                        </SidebarNav>
                    </SidebarNavItem>

                )
                : null
        );
    }
});

module.exports = Sidebar;
