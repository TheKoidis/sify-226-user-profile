/**
 * Common REST util class
 */
var RestUtil = React.createClass({

    statics: {
        escapeFilterParams(value) {
            return value.replace(/_/g, '_underscore_').replace(/:/g, '_colon_').replace(/,/g, '_comma_').replace(/;/g, '_semicolon_');
        },

    	unescapeFilterParams(value) {
    		return value.replace(/_semicolon_/g, ';').replace(/_comma_/g, ',').replace(/_colon_/, ':').replace(/_underscore_/g, '_');
        },

        toSearchParameters(simpleFilter) {

    		if (!simpleFilter) {
                return {};
            }

            var filters = [];
            var searchParameters = {groupOperator: 'AND', name: 'default_group', filters: filters};

            var splittedFilter = simpleFilter.split(",");

            for (filterKey in splittedFilter) {
              if (splittedFilter[filterKey] === '') continue;

				        var filter = splittedFilter[filterKey].split(":");

                var filterOperator = 'EQUALS';
                if (filter.length > 2) {
                    filterOperator = filter[filter.length - 1].toUpperCase();
                }

                if (filterOperator == 'INTERVAL') {
                    var filterValues = [];
                    filterValues.push(this.unescapeFilterParams(filter[1]));
                    filterValues.push(this.unescapeFilterParams(filter[2]));

                    filters.push({propertyName: this.unescapeFilterParams(filter[0]), operator: {'@type': filterOperator}, values: filterValues});
                } else {
                    var filterValues = [];
                    var splittedValues = filter[1].split(";");
					for (valueKey in splittedValues) {
                        filterValues.push(this.unescapeFilterParams(splittedValues[valueKey]));
                    }

                    filters.push({propertyName: this.unescapeFilterParams(filter[0]), operator: {'@type': filterOperator}, values: filterValues});
                }
            }

            return searchParameters;
        }
    },

    render: function () {

    }


});

module.exports = RestUtil;
