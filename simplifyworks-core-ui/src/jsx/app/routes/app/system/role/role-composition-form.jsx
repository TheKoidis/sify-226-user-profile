var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var RoleCompositionForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    propTypes: {		
		recordId: React.PropTypes.object, // id of record
		modalCloseFunc: React.PropTypes.func, // modal close function
		
		superiorRole: React.PropTypes.object, // predefined superior role
		subRole: React.PropTypes.object // predefined sub role
	},
	
	render: function() {
		return (
			<AdvancedForm id='role-composition-form' ref='form'
				recordId={this.props.recordId} modalCloseFunc={this.props.modalCloseFunc} labelKeyPrefix='roleComposition'
				rest='/api/core/role-compositions' cancelRoute='role-list' flux={this.props.flux}>
				
				<AdvancedForm.Object size={6} property='superiorRole' predefinedValue={this.props.superiorRole} required={true} objectRenderFunc={RenderUtil.userRoleNiceLabel} tableRenderFunc={this.renderRoleTable}/>
				<AdvancedForm.Object size={6} property='subRole' predefinedValue={this.props.subRole} required={true} objectRenderFunc={RenderUtil.userRoleNiceLabel} tableRenderFunc={this.renderRoleTable}/>			
			</AdvancedForm>
		);
	},
	
	renderRoleTable: function(flux, recordChosedFunc) {
		return (
			<AdvancedTable id='role-table' rest='/api/core/roles' labelKeyPrefix='role' flux={flux} recordChosedFunc={recordChosedFunc}>
				<AdvancedTable.Column property='name'/>
			</AdvancedTable>
		);
	}
});

module.exports = RoleCompositionForm;
