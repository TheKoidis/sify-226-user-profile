var restEndpoint = '/api/core/roles/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var RoleForm = require('./role-form.jsx');
var UserRoleForm = require('./../user/user-role-form.jsx');
var RoleCompositionForm = require('./role-composition-form.jsx');

var RoleDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],

    render: function () {
        var id = this.getParams().id;

        return (
            <Container id='body'>
                <AdvancedDetail id='role-detail' rest='/api/core/roles' ref='role_detail' renderHeadFunc={this.renderHead}>
                    <AdvancedDetail.Section id='role-form' labelKey='basic_info' className='primary'>
                        <RoleForm />
                    </AdvancedDetail.Section>

                    <AdvancedDetail.Section id='users' labelKey='role_users' className='primary'>
                        <Section iconGlyph='icon-simple-line-icons-users' labelKey='users'>
			               	<AdvancedTable id='user-role-table'
		               					    rest='/api/core/user-roles'
			                                filter={'role.id:'+(id ? id:'0')}
			                                labelKeyPrefix='userRole'
			                                createNewDetailModalFunc={this.getUserRoleModalDialog}
		              						actions={[AdvancedTable.defaultDeleteAction]}>

			                                <AdvancedTable.Column property='id' face='detail' detailRest='/api/core/user-roles' detailModalFunc={this.getUserRoleModalDialog} width='2.5%' />
			                                <AdvancedTable.Column property='user.username' detail='user-detail' detailProperty='user.id' filterType='none'/>
			                                <AdvancedTable.Column property='organization.name' detail='organization-detail' detailProperty='organization.id' filterType='none' width='25%'/>
			                                <AdvancedTable.Column property='validFrom' face='date' filterType='none' width='10%'/>
			                                <AdvancedTable.Column property='validTill' face='date' filterType='none' width='10%'/>
		                    </AdvancedTable>
		                </Section>
		            </AdvancedDetail.Section>

	    		    <AdvancedDetail.Section id='role-hierarchy' labelKey='role_hierarchy' className='primary'>
		    			<Section iconGlyph='icon-simple-line-icons-users' labelKey='role_superiorRoles'>
		    				<AdvancedTable id='role-superior-roles-table'
	               						    rest={'/api/core/role-compositions'}
	               							filter={'subRole.id:' + (id ? id : '0')}
	               							labelKeyPrefix='role_superiorRoles'
	               							createNewDetailModalFunc={this.getSuperiorRoleModalDialog}
	               							actions={[AdvancedTable.defaultDeleteAction]}>

	               							<AdvancedTable.Column property='superiorRole.id' face='detail' detail='role-detail' detailProperty='superiorRole.id' width='2.5%' />
					                        <AdvancedTable.Column property='superiorRole.name' labelKey='role_name' searchable={false}/>
					        </AdvancedTable>
		                </Section>

                        <Section iconGlyph='icon-simple-line-icons-users' labelKey='role_subRoles'>
                            <AdvancedTable id='role-subroles-table'
                                rest={'/api/core/role-compositions'}
                                filter={'superiorRole.id:' + (id ? id : '0')}
                                labelKeyPrefix='role_subroles'
                                createNewDetailModalFunc={this.getSubRoleModalDialog}
                                actions={[AdvancedTable.defaultDeleteAction]}>

                                <AdvancedTable.Column property='subRole.id' face='detail' detail='role-detail' detailProperty='subRole.id' width='2.5%' />
                                <AdvancedTable.Column property='subRole.name' labelKey='role_name' searchable={false}/>
                            </AdvancedTable>
                        </Section>
		            </AdvancedDetail.Section>
                </AdvancedDetail>
            </Container>
        );
    },

    renderHead: function(record) {
        return record ? l20n.ctx.getSync('role_detail', {name: record.name}) : l20n.ctx.getSync('role_detail_new');
    },

    getUserRoleModalDialog: function (url, isNew, recordId, modalCloseFunc) {
    	return (
           	<UserRoleForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} role={this.refs.role_detail.state.record}/>
        );
    },

    getSuperiorRoleModalDialog: function (url, isNew, recordId, modalCloseFunc) {
    	return (
           	<RoleCompositionForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} subRole={this.refs.role_detail.state.record}/>
        );
    },

    getSubRoleModalDialog: function (url, isNew, recordId, modalCloseFunc) {
    	return (
           	<RoleCompositionForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} superiorRole={this.refs.role_detail.state.record}/>
        );
    }
});

module.exports = RoleDetail;
