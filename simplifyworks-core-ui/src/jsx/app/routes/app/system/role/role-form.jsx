var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var RoleForm = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	render: function() {
		return (
			<AdvancedForm id='form' ref='form' labelKey='basic_info' labelKeyPrefix='role' rest='/api/core/roles' cancelRoute='role-list'>
				<AdvancedForm.Text size={12} property='name' validations='required,lowercase,length:3:25'/>
				<AdvancedForm.Text size={12} property='description' type='area' validations='length:0:255'/>
		    </AdvancedForm>
		);
	}
});

module.exports = RoleForm;
