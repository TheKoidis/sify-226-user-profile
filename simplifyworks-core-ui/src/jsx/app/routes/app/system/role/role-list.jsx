var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var RoleList = React.createClass({
    mixins: [ReactRouter.State, FluxMixin],

    render: function () {
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12} md={12} lg={12}>
                        	<Section iconGlyph='icon-simple-line-icons-users' labelKey='roles'>
                        		<AdvancedTable
		                    		id='role-table'
		                    		ref='role_table'
		                    		rest='/api/core/roles'
		                            createNewDetail='role-detail'
		                            ordering={[{property: 'name', order: 'asc'}]}
		                    		labelKeyPrefix='role'
		                    		actions={[AdvancedTable.defaultDeleteAction]}>

		                            <AdvancedTable.Column property='id' face='detail' detail='role-detail' width='2.5%' />
		                            <AdvancedTable.Column property='name' width='20%'/>
		                            <AdvancedTable.Column property='description'/>
		                        </AdvancedTable>
                        	</Section>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = RoleList;
