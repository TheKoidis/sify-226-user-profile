var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var PrintModuleForm = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],
	
	propTypes: {		
		recordId: React.PropTypes.object, // id of record
		modalCloseFunc: React.PropTypes.func, // modal close function
	},
	
    render: function() {
		return (
			<AdvancedForm id='print-module-form' ref='form'
				recordId={this.props.recordId} modalCloseFunc={this.props.modalCloseFunc} labelKeyPrefix='printModule'
				rest='/api/core/print-modules' cancelRoute='print-module-list' flux={this.props.flux}>
			
				<AdvancedForm.Text size={4} property='name' validations='required' />
				<AdvancedForm.Text size={4} property='template' validations='required' />
				<AdvancedForm.Enum size={4} property='type' validations='required' options={[{value: 'PDF', label: 'PDF'}, {value: 'HTML', label: 'HTML'}, {value: 'XML', label: 'XML'}]}/>				

				<AdvancedForm.Text size={6} property='objectType' validations='required'/>
				<AdvancedForm.Text size={6} property='wfState'/>				
		    </AdvancedForm>
		);
	},
});

module.exports = PrintModuleForm;
