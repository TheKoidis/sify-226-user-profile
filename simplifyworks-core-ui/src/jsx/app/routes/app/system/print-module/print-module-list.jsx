var PrintModuleForm = require('./print-module-form.jsx');

var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var PrintModuleList = React.createClass({

    mixins: [FluxMixin],

    getPrintModuleModalDialog: function (url, isNew, recordId, modalCloseFunc) {
        return (
        	<PrintModuleForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} />
        );
    },

    render: function () {
        return (
            <Container id='body'>
            	<Grid>
            		<Row>
            			<Col sm={12} md={12} lg={12}>
	            			<Section iconGlyph='icon-fontello-docs' labelKey='printModules'>
	            				<AdvancedTable id='user-table'
			                    	rest='/api/core/print-modules'
			                    	ordering={[{property: 'name', order: 'asc'}]}
			                    	labelKeyPrefix='printModule'
			                    	createNewDetailModalFunc={this.getPrintModuleModalDialog}
    								actions={[AdvancedTable.defaultDeleteAction]}>

	            					<AdvancedTable.Column property='id' face='detail' detailRest='/api/core/print-modules' detailModalFunc={this.getPrintModuleModalDialog} width='2.5%' />
			                        <AdvancedTable.Column property='name' width='20%' />
			                        <AdvancedTable.Column property='template' width='10%' />
			                        <AdvancedTable.Column property='type' face='enum' width='10%'/>
			                        <AdvancedTable.Column property='objectType' width='25%'/>
			                        <AdvancedTable.Column property='wfState' />
			                    </AdvancedTable>
		                	</Section>
		                </Col>
			        </Row>
			    </Grid>
            </Container>
        );
    }
});

module.exports = PrintModuleList;
