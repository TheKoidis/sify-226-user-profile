var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var UserRoleForm = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	propTypes: {
		recordId: React.PropTypes.object, // id of record
		modalCloseFunc: React.PropTypes.func, // modal close function

		user: React.PropTypes.object, // predefined user
		role: React.PropTypes.object, // predefined role
		organization: React.PropTypes.object // predefined organization
	},

    render: function() {
		return (
			<AdvancedForm id='user-role-form' ref='form'
				recordId={this.props.recordId} modalCloseFunc={this.props.modalCloseFunc} labelKeyPrefix='userRole'
				rest='/api/core/user-roles' cancelRoute='user-list' flux={this.props.flux}>

				<AdvancedForm.Object size={4} property='user' predefinedValue={this.props.user} validations='required' objectRenderFunc={RenderUtil.userNiceLabel} tableRenderFunc={this.renderUserTable}/>
				<AdvancedForm.Object size={4} property='role' predefinedValue={this.props.role} validations='required' objectRenderFunc={RenderUtil.userRoleNiceLabel} tableRenderFunc={this.renderRoleTable}/>
				<AdvancedForm.Object size={4} property='organization' predefinedValue={this.props.organization} objectRenderFunc={RenderUtil.organizationNiceLabel} tableRenderFunc={this.renderOrganizationTable}/>

				<AdvancedForm.Datetime size={6} property='validFrom' timeEnabled={true}/>
				<AdvancedForm.Datetime size={6} property='validTill' timeEnabled={true}/>
		    </AdvancedForm>
		);
	},

	renderUserTable: function(flux, recordChosedFunc) {
		return (
			<AdvancedTable id='user-table' rest='/api/core/users' labelKeyPrefix='user' flux={flux} recordChosedFunc={recordChosedFunc}>
				<AdvancedTable.Column property='username'/>
				<AdvancedTable.Column property='person.surname' labelKey='person_surname' />
				<AdvancedTable.Column property='person.firstname' labelKey='person_firstname' />
			</AdvancedTable>
		);
	},

	renderRoleTable: function(flux, recordChosedFunc) {
		return (
				<AdvancedTable id='role-table' rest='/api/core/roles' labelKeyPrefix='role' flux={flux} recordChosedFunc={recordChosedFunc}>
					<AdvancedTable.Column property='name'/>
				</AdvancedTable>
			);
	},

	renderOrganizationTable: function(flux, recordChosedFunc) {
		return (
				<AdvancedTable id='organization-table' rest='/api/core/organizations' labelKeyPrefix='organization' flux={flux} recordChosedFunc={recordChosedFunc}>
					<AdvancedTable.Column property='code'/>
					<AdvancedTable.Column property='name'/>
				</AdvancedTable>
			);
	}
});

module.exports = UserRoleForm;
