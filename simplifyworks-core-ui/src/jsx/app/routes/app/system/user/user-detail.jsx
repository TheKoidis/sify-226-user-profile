// TODO: global app setting
var restEndpoint = '/api/core';

var UserForm = require('./user-form.jsx');
var RestUtil = require('../../../../domain/rest-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var UserRoleForm = require('./user-role-form.jsx');

var UserDetail = React.createClass({
    mixins: [SidebarMixin, FluxMixin, ReactRouter.State],

    render: function () {
        var idUser = this.getParams().id;
        return (
            <Container id='body'>
                <AdvancedDetail id='user-detail' rest='/api/core/users' ref='user_detail' renderHeadFunc={this.renderHead}>
                    <AdvancedDetail.Section labelKey='basic_info' className='primary'>
                        <UserForm />
                    </AdvancedDetail.Section>

                    <AdvancedDetail.Section labelKey='user_roles' className='primary'>
                        <Section iconGlyph='icon-simple-line-icons-users' labelKey='user_roles'>
                            <AdvancedTable id='user-role-table'
                                rest='/api/core/user-roles'
                                filter={'user.id:' + (idUser ? idUser : '0')}
                                labelKeyPrefix='userRole'
                                createNewDetailModalFunc={this.getUserRoleModalDialog}
                                actions={[AdvancedTable.defaultDeleteAction]}>

                                <AdvancedTable.Column property='id' face='detail' detailRest='/api/core/user-roles' detailModalFunc={this.getUserRoleModalDialog} width='2.5%' />
                                <AdvancedTable.Column property='role.name' detail='role-detail' detailProperty='role.id' filterType='none' />
                                <AdvancedTable.Column property='organization.name' detail='organization-detail' detailProperty='organization.id' filterType='none' width='25%'/>
                                <AdvancedTable.Column property='validFrom' face='date' filterType='none' width='10%' />
                                <AdvancedTable.Column property='validTill' face='date' filterType='none' width='10%' />
                            </AdvancedTable>
                        </Section>
                    </AdvancedDetail.Section>
                </AdvancedDetail>
            </Container>
        );
    },

    renderHead: function(record) {
        return record ? l20n.ctx.getSync('user_detail', {username: record.username}) : l20n.ctx.getSync('user_detail_new');
    },

    getUserRoleModalDialog: function (url, isNew, recordId, modalCloseFunc) {
        return (
        	<UserRoleForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} user={this.refs.user_detail.state.record}/>
        );
    }
});

module.exports = UserDetail;
