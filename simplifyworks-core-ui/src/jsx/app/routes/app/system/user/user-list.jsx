var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var UserList = React.createClass({

    mixins: [FluxMixin],

    render: function () {
        return (
            <Container id='body'>
            	<Grid>
            		<Row>
            			<Col sm={12} md={12} lg={12}>
			            	<Section iconGlyph='icon-simple-line-icons-user' labelKey='users'>
			            		<AdvancedTable id='user-table'
			                    	rest='/api/core/users'
			                    	createNewDetail='user-detail'
			                    	ordering={[{property: 'username', order: 'asc'}]}
			                    	labelKeyPrefix='user'
			                    	actions={[AdvancedTable.defaultDeleteAction]}>

			                    	<AdvancedTable.Column property='id' face='detail' detail='user-detail' width='2.5%' />
			                        <AdvancedTable.Column property='username' width='20%'/>
			                        <AdvancedTable.Column property='person.surname' labelKey='person_surname' width='20%'/>
			                        <AdvancedTable.Column property='person.firstname' labelKey='person_firstname' width='20%'/>
			                        <AdvancedTable.Column property='person.email' labelKey='person_email'/>
			                    </AdvancedTable>
			            	</Section>
			            </Col>
			        </Row>
			    </Grid>
            </Container>
        );
    }
});

module.exports = UserList;
