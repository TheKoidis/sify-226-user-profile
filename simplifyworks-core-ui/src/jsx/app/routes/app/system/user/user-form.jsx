var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var UserForm = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    render: function() {
		var idUser = this.getParams().id;

		return (
			<AdvancedForm id='user-form' ref='form' labelKey='basic_info' labelKeyPrefix='user' rest='/api/core/users' cancelRoute='user-list'>
				<AdvancedForm.Text size={6} property='username' disabled={idUser !== 'new'} validations='required,length:3:50'/>
				<AdvancedForm.Text size={6} property='newPassword' labelKey='user_password' type='password' validations={(idUser === 'new' ? 'required,' : '') + 'length:5:50'}/>
				<AdvancedForm.Object size={12} property='person' objectRenderFunc={RenderUtil.personNiceLabel} tableRenderFunc={this.renderPersonTable}/>
		    </AdvancedForm>
		);
	},

	renderPersonTable: function(flux, recordChosedFunc) {
		return (
			<AdvancedTable id='persons' rest='/api/core/persons' labelKeyPrefix='person' flux={flux} recordChosedFunc={recordChosedFunc}>
				<AdvancedTable.Column property='surname' />
				<AdvancedTable.Column property='firstname' />
				<AdvancedTable.Column property='email' />
			</AdvancedTable>
		);
	}
});

module.exports = UserForm;
