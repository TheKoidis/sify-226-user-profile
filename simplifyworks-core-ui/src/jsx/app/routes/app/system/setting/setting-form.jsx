var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var SettingForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    propTypes: {
		recordId: React.PropTypes.object, // id of record
		modalCloseFunc: React.PropTypes.func, // modal close function
	},

    getInitialState: function() {
        return {
            disabled: true && this.props.recordId != 'new'
        };
    },

	render: function() {
		return (
			<AdvancedForm id='setting-form' ref='form'
				recordId={this.props.recordId} modalCloseFunc={this.props.modalCloseFunc} labelKeyPrefix='setting'
				rest='/api/core/settings' cancelRoute='setting-list' flux={this.props.flux} postLoadFunc={this.changeDisability} disabled={this.state.disabled}>

				<AdvancedForm.Text size={6} property='settingKey' validations='required,length:0:50' />
				<AdvancedForm.Text size={4} property='value' />

				<AdvancedForm.Boolean size={2} property='system' disabled={true} />
			</AdvancedForm>
		);
	},

    changeDisability: function(record) {
        this.setState({
            disabled: record.system
        });
    }
});


module.exports = SettingForm;
