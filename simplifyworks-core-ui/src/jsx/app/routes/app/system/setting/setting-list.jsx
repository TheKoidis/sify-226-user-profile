var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var SettingForm = require('./setting-form.jsx');

var SettingList = React.createClass({

    mixins: [FluxMixin],

    render: function () {
    	return (
    		<Container id='body'>
               	<Grid>
               		<Row>
               			<Col sm={12} md={12} lg={12}>
    	           			<Section iconGlyph='icon-simple-line-icons-settings' labelKey='settings'>
    	           				<AdvancedTable id='setting-table'
    		                    	rest='/api/core/settings'
    		                    	ordering={[{property: 'settingKey', order: 'asc'}]}
    		                    	labelKeyPrefix='setting'
    		                    	createNewDetailModalFunc={this.getSettingForm}
        							actions={[AdvancedTable.defaultDeleteAction]}>

    	           					<AdvancedTable.Column property='id' face='detail' detailRest='/api/core/settings' detailModalFunc={this.getSettingForm} width='2.5%'/>
    		                        <AdvancedTable.Column property='settingKey' width='15%'/>
    		                        <AdvancedTable.Column property='value' />
    		                        <AdvancedTable.Column property='system' face='boolean' width='10%'/>
    		                    </AdvancedTable>
    		               	</Section>
    		            </Col>
    		        </Row>
    		    </Grid>
            </Container>
    	);
    },

    getSettingForm: function (url, isNew, recordId, modalCloseFunc) {
    	return (
           	<SettingForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} />
        );
    }
});

module.exports = SettingList;
