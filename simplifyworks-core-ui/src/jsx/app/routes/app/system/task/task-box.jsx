var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var TaskAjax = require('./task-ajax.jsx');
var RenderUtil = require('../../../../renderer/render-util.jsx');

/**
 * Class represents box for single task with its triggers.
 */
var TaskBox = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    propTypes: {
    	task: React.PropTypes.object.isRequired, // task for this box
    	reloadFunc: React.PropTypes.func.isRequired // function responsible for reloading of tasks and triggers
    },

    // renders field set using task from props
    render: function () {
    	return (
    		<fieldset id={this.props.task.name} style={{'marginBottom': '12.5px'}}>
    			<legend>
    				<Grid>
    					<Row>
		    				<Col sm={6} md={6} lg={6}>
		    					<Entity entity={'task_' + this.props.task.name} />
		    				</Col>

		    				<Col sm={6} md={6} lg={6} style={{textAlign: 'right'}}>
		    					<ButtonGroup>
		    						<Button sm bsStyle='success'
		    							onClick={ModalDialogManager.create.bind(this, this.getOneTimeTriggerModalDialog, this.props.task.name)}>

		    							<Icon glyph='icon-fontello-plus-circled'/>
		    							{' '}<Entity entity='task_add_one_time_task_trigger' />
		    						</Button>
		    						<Button sm bsStyle='success'
		    							onClick={ModalDialogManager.create.bind(this, this.getCronTriggerModalDialog, this.props.task.name)}>

		    							<Icon glyph='icon-fontello-plus-circled'/>
		    							{' '}<Entity entity='task_add_cron_task_trigger' />
		    						</Button>
		    						<Button sm bsStyle='primary' title={l20n.ctx.getSync('task_run_now')}
		    							onClick={TaskAjax.runTaskNow.bind(this, this.props.task.name, this.props.reloadFunc, this.getFlux())}>

		    		    				<Icon glyph='icon-fontello-play' />
		    		    				{' '}<Entity entity='task_run_now' />
		    		    			</Button>
		    					</ButtonGroup>
		    				</Col>
		    			</Row>
		    		</Grid>
    			</legend>

    			<Grid>
					<Row>
    					<Col sm={12} md={12} lg={12}>
							{this.props.task.triggers.length > 0
								?	this.createTriggerTable()
								:	<Entity entity='task_no_triggers' />
							}
						</Col>
					</Row>
				</Grid>
    		</fieldset>
    	);
    },

    // creates trigger table
    createTriggerTable: function() {
    	return (
    		<Table hover className='table table-striped table-bordered table-condensed' cellSpacing='0' width='100%'>
    			{this.createTriggerTableHead()}
    			{this.createTriggerTableBody()}
    		</Table>
    	);
    },

    // creates trigger table head
    createTriggerTableHead: function() {
    	return (
    		<thead>
				<tr>
					<th style={{'width': '30%'}}>
						<Entity entity='task_trigger_description' />
					</th>

					<th style={{'width': '20%'}}>
						<Entity entity='task_trigger_detail' />
					</th>

					<th style={{'width': '20%'}}>
						<Entity entity='task_trigger_nextFireTime' />
					</th>

					<th style={{'width': '20%'}}>
						<Entity entity='task_trigger_previousFireTime' />
					</th>

					<th style={{'width': '4%'}}>
						<Entity entity='task_trigger_active' />
					</th>

					<th style={{'width': '6%'}}/>
				</tr>
			</thead>
    	);
    },

    // creates trigger table body
    createTriggerTableBody: function() {
    	return (
    		<tbody>
    			{this.createTriggerTableRows()}
    		</tbody>
    	);
    },

    // creates trigger table rows
    createTriggerTableRows: function() {
    	var rows = [];

    	for(var index = 0; index < this.props.task.triggers.length; index++) {
    		var trigger = this.props.task.triggers[index];

    		rows.push(
    			<tr>
    				<td>
    					{trigger.description}
    				</td>

    				<td>
						{this.createTriggerDetail(trigger)}
					</td>

					<td>
						{trigger.nextFireTime ? new Date(trigger.nextFireTime).format(RenderUtil.getDateTimeFormat()) : ' '}
					</td>

					<td>
						{trigger.previousFireTime ? new Date(trigger.previousFireTime).format(RenderUtil.getDateTimeFormat()) : ' '}
					</td>

					<td>
						<Icon glyph={trigger.state === 'ACTIVE' ? 'icon-fontello-check-1' : 'icon-fontello-check-empty'} />
					</td>

					<td>
						{this.createTriggerActions(trigger)}
					</td>
    			</tr>
    		);
    	}

    	return rows;
    },

    // creates value for detail column using type of trigger
    createTriggerDetail: function(trigger) {
    	switch(trigger.type) {
    		case 'CronTaskTrigger':
    			return trigger.cron;
    		case 'OneTimeTaskTrigger':
    			return new Date(trigger.fireTime).format(RenderUtil.getDateTimeFormat());
    		default:
    			return trigger.type;
    	}
    },

    // creates action buttons for trigger
    createTriggerActions: function(trigger) {
    	return (
    		<ButtonGroup sm>
    			{trigger.state === 'ACTIVE'
    				?	<Button sm bsStyle='warning' title={l20n.ctx.getSync('task_trigger_pause')}
							onClick={TaskAjax.changeTriggerState.bind(this, this.props.task.name, trigger.name, 'pause', this.props.reloadFunc, this.getFlux())}>

							<Icon glyph='icon-fontello-power' />
						</Button>
					: 	<Button sm bsStyle='success' title={l20n.ctx.getSync('task_trigger_resume')}
		    				onClick={TaskAjax.changeTriggerState.bind(this, this.props.task.name, trigger.name, 'resume', this.props.reloadFunc, this.getFlux())}>

		    				<Icon glyph='icon-fontello-power' />
		    			</Button>
    			}

    			<Button sm bsStyle='danger' title={l20n.ctx.getSync('task_trigger_remove')}
    				onClick={TaskAjax.removeTrigger.bind(this, this.props.task.name, trigger.name, this.props.reloadFunc, this.getFlux())}>

    				<Icon glyph='icon-fontello-cancel-circled' />
    			</Button>
    		</ButtonGroup>
    	);
    },

    // returns modal dialog for creation of one time trigger
    getOneTimeTriggerModalDialog: function() {
    	return (
    		<ModalDialog lg modalId={this.props.task.name}>
    	    	<ModalDialogBody>
	    	    	<AdvancedForm id='one-time-trigger-form' iconGlyph='icon-fontello-plus-circled' labelKeyPrefix='task_one_time_task_trigger'
	    				rest={'/api/scheduler/tasks/' + this.props.task.name + '/triggers/one-time'} newRecord={{type: 'OneTimeTaskTrigger'}}
	    				modalCloseFunc={this.closeModalDialog} flux={this.getFlux()} recordId='new'>

	    				<AdvancedForm.Text size={6} property='description' validations='length:1:100'/>
	    				<AdvancedForm.Datetime size={6} property='fireTime' timeEnabled={true} validations='required' />
	    			</AdvancedForm>
    	        </ModalDialogBody>
    	    </ModalDialog>
    	);
    },

    // returns modal dialog for creation of cron trigger
    getCronTriggerModalDialog: function() {
    	return (
    		<ModalDialog lg modalId={this.props.task.name}>
    	    	<ModalDialogBody>
	    	    	<AdvancedForm id='cron-trigger-form' iconGlyph='icon-fontello-plus-circled' labelKeyPrefix='task_cron_task_trigger'
	    				rest={'/api/scheduler/tasks/' + this.props.task.name + '/triggers/cron'} newRecord={{type: 'CronTaskTrigger'}}
	    				modalCloseFunc={this.closeModalDialog} flux={this.getFlux()} recordId='new'>

	    				<AdvancedForm.Text size={6} property='description' validations='length:1:100'/>
	    				<AdvancedForm.Text size={6} property='cron' validations='required,length:1:100'/>
	    			</AdvancedForm>
    	        </ModalDialogBody>
    	    </ModalDialog>
    	);
    },

    // closes modal dialog and reloads tasks
    closeModalDialog: function() {
    	ModalDialogManager.remove(this.props.task.name);

    	this.props.reloadFunc();
    }
});

module.exports = TaskBox;
