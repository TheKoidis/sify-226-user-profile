var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var TaskAjax = require('./task-ajax.jsx');
var TaskBox = require('./task-box.jsx');

/**
 * This class represents list of schedulable tasks. Component refreshes automatically in specified interval.
 */
var TaskList = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	statics: {
		refreshInterval: 10000
	},
	
	// returns initial state (empty task list)
    getInitialState: function() {
    	return {
    		tasks: []
    	};
    },
    
    // sets timer
    componentWillMount: function() {
    	this.setState({
    		timerId: setInterval(this.reload, TaskList.refreshInterval)
    	});
    },
    
    // reloads tasks
    componentDidMount: function() {
    	this.reload();    	
    },
    
    // clears timer
    componentWillUnmount: function() {
    	if(this.state.timerId) {
    		clearInterval(this.state.timerId);
    	}
    },    
    
    // renders main container with task boxes inside
    render: function () {
    	return (
    		<Container id='body'>
               	<Grid>
               		<Row>
               			<Col sm={12} md={12} lg={12}>
    	           			<Section iconGlyph='icon-fontello-clock' labelKey='tasks'>
    	           				{this.state.tasks.length > 0
    	           					?	this.createTaskBoxes()
    	           					:	<Entity entity='tasks_no_tasks' />    	           					
    	           				}
    		               	</Section>
    		            </Col>
    		        </Row>
    		    </Grid>
            </Container>
    	);
    },
    
    // creates task boxes using state
    createTaskBoxes: function() {
    	var taskBoxes = [];
    	
    	for(var index = 0; index < this.state.tasks.length; index++) {
    		taskBoxes.push(
    			<TaskBox task={this.state.tasks[index]} reloadFunc={this.reload}/>
    		);
    	}
    	
    	return taskBoxes;
    },
    
    // reloads tasks and sets state
    reload: function() {
    	TaskAjax.getAllTasks(function (response) {
    		this.setState({
    			tasks: (response._embedded.taskList) ? response._embedded.taskList : []
    		});
		}.bind(this), this.getFlux());
    }
});

module.exports = TaskList;
