/**
 * Utility class for task AJAX support
 */
var TaskAjax = {
	
	// returns all tasks, invokes callback then and show message using flux
	getAllTasks: function(callback, flux) {
	    $.ajax({
			headers: {
				'Accept': 'application/json',
	 	        'Content-Type': 'application/json'
	 	    },
	 	    method: 'GET',
	 	    url: '/api/scheduler/tasks',
	 	    success: function (response) {
	 	    	callback(response);
	 	    }.bind(this),
	 	    error: function (xhr, status, error) {
	 	       	this.processError(xhr, status, error, flux);
	 	    }.bind(this)            
	    });
	},
	
	// runs specified task, invokes callback then and show message using flux
	runTaskNow: function(taskName, callback, flux) {
		$.ajax({
			headers: {
 	            'Content-Type': 'application/json'
 	        },
 	        method: 'POST',
 	        url: '/api/scheduler/tasks/' + taskName + '/triggers/one-time',
 	        data: JSON.stringify({type: 'OneTimeTaskTrigger', fireTime: new Date()}),
 	        success: function () {
 	        	this.getFlux().actions.flash.add({
 	               type: 'success',
 	               message: l20n.ctx.getSync('success')
 	           	});
 	        	
 	        	callback();
 	        }.bind(this),
 	        error: function (xhr, status, error) {
 	        	this.processError(xhr, status, error, flux);
 	        }.bind(this)            
        });
	},
	
	// removes trigger using its name and task name, invokes callback then and show message using flux
	removeTrigger: function(taskName, triggerName, callback, flux) {
    	$.ajax({
			headers: {
 	            'Content-Type': 'application/json'
 	        },
 	        method: 'DELETE',
 	        url: '/api/scheduler/tasks/' + taskName + '/triggers/' + triggerName,
 	        success: function () {
 	        	this.getFlux().actions.flash.add({
 	               type: 'success',
 	               message: l20n.ctx.getSync('success')
 	           	});
 	        	
 	        	callback();
 	        }.bind(this),
 	        error: function (xhr, status, error) {
 	        	this.processError(xhr, status, error, flux);
 	        }.bind(this)            
        });
    },
    
    // changes trigger state to specified one using its name and task name, invokes callback then and show message using flux
    changeTriggerState: function(taskName, triggerName, state, callback, flux) {
    	$.ajax({
			headers: {
 	            'Content-Type': 'application/json'
 	        },
 	        method: 'POST',
 	        url: '/api/scheduler/tasks/' + taskName + '/triggers/' + triggerName + '/' + state,
 	        success: function () {
 	        	this.getFlux().actions.flash.add({
 	               type: 'success',
 	               message: l20n.ctx.getSync('success')
 	           	});
 	        	
 	        	callback();
 	        }.bind(this),
 	        error: function (xhr, status, error) {
 	        	this.processError(xhr, status, error, flux);
 	        }.bind(this)            
        });
    },
	
	// processes error (shows message to user) of AJAX requests
    processError: function(xhr, status, error, flux) {
    	//TODO process errors
    	console.log(xhr);
    	console.log(error);
    	
    	flux.actions.flash.add({
            messageId: 'formErrors',
            type: 'error',
            message: l20n.ctx.getSync('error')
        });		
    }
}

module.exports = TaskAjax;