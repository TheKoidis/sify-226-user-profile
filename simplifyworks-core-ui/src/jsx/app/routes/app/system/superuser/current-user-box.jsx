var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var RenderUtil = require('../../../../renderer/render-util.jsx');

/**
 * Class represents box for current user info/change.
 */
var CurrentUserBox = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin, Fluxxor.StoreWatchMixin("SecurityStore")],

    getStateFromFlux: function () {
        return this.getFlux().store("SecurityStore").getState();
    },

    // renders field set using task from props
    render: function () {
        var switchedUser = this.state.currentUser.username !== this.state.originalUser.username;

    	return (
            <fieldset id='current-user' style={{'marginBottom': '12.5px'}}>
                <legend>
                    <Grid>
                        <Row>
                            <Col xs={6}>
                                {l20n.ctx.getSync('superuser_currentUser')}
                            </Col>

                            <Col xs={6} style={{textAlign: 'right'}}>
                                <ButtonGroup>
                                    {switchedUser
                                        ?   <Button sm bsStyle='success' onClick={this.changeCurrentUserBackToOriginalUser}>
                                                <Icon glyph='icon-fontello-ccw'/>
                                                {' ' + l20n.ctx.getSync('superuser_changeCurrentUserToOriginalUser')}
                                            </Button>
                                        :   <Button sm bsStyle='success' onClick={ModalDialogManager.create.bind(this, this.getCurrentUserSelectionModalDialog, 'current-user-selection')}>
                                                <Icon glyph='icon-fontello-users'/>
                                                {' ' + l20n.ctx.getSync('superuser_changeCurrentUser')}
                                            </Button>
                                    }
                                </ButtonGroup>
                            </Col>
                        </Row>
                    </Grid>
                </legend>

                <Grid>
                    <Row>
                        <Col xs={12}>
                            {RenderUtil.userWithPersonLabel(this.state.currentUser)}
                        </Col>
                    </Row>
                </Grid>
            </fieldset>
    	);
    },

    getCurrentUserSelectionModalDialog: function() {
        return (
            <ModalDialog lg modalId='current-user-selection'>
                <ModalDialogBody>
                    <Section iconGlyph='icon-fontello-users' labelKey='superuser_currentUserSelection'>
                        <AdvancedTable id='user-table'
                            rest='/api/core/users'
                            ordering={[{property: 'username', order: 'asc'}]}
                            labelKeyPrefix='user' flux={this.getFlux()} recordChosedFunc={this.currentUserChosed}>

                            <AdvancedTable.Column property='username' width='20%'/>
                            <AdvancedTable.Column property='person.surname' labelKey='person_surname' width='20%'/>
                            <AdvancedTable.Column property='person.firstname' labelKey='person_firstname' width='20%'/>
                            <AdvancedTable.Column property='person.email' labelKey='person_email'/>
                        </AdvancedTable>

                        <PanelFooter style={{'textAlign': 'right'}}>
                            <Button outlined onlyOnHover bsStyle='red' onClick={ModalDialogManager.remove.bind(this, 'current-user-selection')}>
                                <Entity entity='btn_close' />
                            </Button>
                        </PanelFooter>
                    </Section>
                </ModalDialogBody>
            </ModalDialog>
        );
    },

    currentUserChosed: function(object) {
        ModalDialogManager.remove('current-user-selection');

        this.changeCurrentUser(object.username);
    },

    changeCurrentUserBackToOriginalUser: function() {
        this.changeCurrentUser(this.state.originalUser.username);
    },

    changeCurrentUser: function(username) {
        this.getFlux().actions.security.changeCurrentUser(username, function() {
            this.transitionTo('/system/superuser-detail');
        }.bind(this));
    }
});

module.exports = CurrentUserBox;
