var restEndpoint = '/api/core/roles/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var CurrentUserBox = require('./current-user-box.jsx');
var OriginalUserBox = require('./original-user-box.jsx');

var RoleDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],

    render: function () {
        return (
            <Container id='body'>
                <Grid>
               		<Row>
               			<Col sm={12} md={12} lg={12}>
    	           			<Section iconGlyph='icon-fontello-users' labelKey='superuser'>
                                <CurrentUserBox />
                                <OriginalUserBox />
    		               	</Section>
    		            </Col>
    		        </Row>
    		    </Grid>
            </Container>
        );
    },


});

module.exports = RoleDetail;
