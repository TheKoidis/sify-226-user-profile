var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var RenderUtil = require('../../../../renderer/render-util.jsx');

/**
 * Class represents box for current user info/change.
 */
var CurrentUserBox = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin, Fluxxor.StoreWatchMixin("SecurityStore")],

    getStateFromFlux: function () {
        return this.getFlux().store("SecurityStore").getState();
    },

    // renders field set using task from props
    render: function () {
    	return (
            <fieldset id='original-user' style={{'marginBottom': '12.5px'}}>
                <legend>
                    <Grid>
                        <Row>
                            <Col xs={12}>
                                {l20n.ctx.getSync('superuser_originalUser')}
                            </Col>
                        </Row>
                    </Grid>
                </legend>

                <Grid>
                    <Row>
                        <Col xs={12}>
                            {RenderUtil.userWithPersonLabel(this.state.originalUser)}
                        </Col>
                    </Row>
                </Grid>
            </fieldset>
    	);
    }
});

module.exports = CurrentUserBox;
