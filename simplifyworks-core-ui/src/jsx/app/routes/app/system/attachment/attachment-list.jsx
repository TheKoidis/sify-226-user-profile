var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var SettingList = React.createClass({

    mixins: [FluxMixin],

    render: function () {
    	return (
    		<Container id='body'>
               	<Grid>
               		<Row>
               			<Col sm={12} md={12} lg={12}>
    	           			<Section iconGlyph='icon-dripicons-attachment' labelKey='attachments'>
    	           				<AdvancedTable id='attachment-table'
    		                    	rest='/api/attachments'
    		                    	ordering={[{property: 'name', order: 'asc'}]}
    		                    	labelKeyPrefix='attachment'
    		                    	createNewDetail='attachment-detail'
        							actions={[AdvancedTable.defaultDeleteAction]}>

    	           					<AdvancedTable.Column property='id' face='detail' detail='attachment-detail' width='2.5%'/>
    		                        <AdvancedTable.Column property='name' width='25%'/>
    		                        <AdvancedTable.Column property='mimetype' width='10%'/>
    		                        <AdvancedTable.Column property='contentGuid' width='25%'/>
    		                        <AdvancedTable.Column property='objectType' width='20%'/>
    		                        <AdvancedTable.Column property='objectIdentifier' />
    		                    </AdvancedTable>
    		               	</Section>
    		            </Col>
    		        </Row>
    		    </Grid>
            </Container>
    	);
    }
});

module.exports = SettingList;
