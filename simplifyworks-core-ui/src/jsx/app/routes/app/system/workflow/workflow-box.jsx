var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var RestUtil = require('../../../../domain/rest-util.jsx');
var WorkflowAjax = require('./workflow-ajax.jsx');
var WorkflowDefinitionEntitiesForm = require('./definition-entities-form.jsx');

/**
 * Class represents box for single workflow process definition.
 */
var WorkflowBox = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    propTypes: {
    	processDefinition: React.PropTypes.object.isRequired, // process definition for this box
        reloadFunc: React.PropTypes.func.isRequired // function responsible for reloading of processes
    },

    getWorkflowProcessDefinitionEntityForm(url, isNew, recordId, modalCloseFunc) {
        return (
            <WorkflowDefinitionEntitiesForm url={url} recordId={recordId} modalCloseFunc={modalCloseFunc} parent={this.props.processDefinition} flux={this.getFlux()}/>
        );
    },

    // renders field set using process definition from props
    render: function () {
    	return (
    		<fieldset id={this.props.processDefinition.name} style={{'marginBottom': '12.5px'}}>
    			<legend>
    				<Grid>
    					<Row>
		    				<Col sm={6} md={6} lg={6}>
		    					{this.props.processDefinition.name}
		    				</Col>

		    				<Col sm={6} md={6} lg={6} style={{textAlign: 'right'}}>
		    					<ButtonGroup>
                                    <Button bsStyle='danger' onClick={WorkflowAjax.deleteProcessDefinition.bind(this, this.props.processDefinition, this.props.reloadFunc, this.getFlux())}>
                                        {l20n.ctx.getSync('btn_delete')}
                                    </Button>
		    					</ButtonGroup>
		    				</Col>
		    			</Row>
		    		</Grid>
    			</legend>

    			<Grid>
					<Row>
    					<Col sm={12} md={12} lg={12}>
	    					<AdvancedTable id='definition-entities-table'
	                            rest='/api/wf/definition-entities'
	                            filter={'processDefinitionKey:' + RestUtil.escapeFilterParams(this.props.processDefinition.key)}
	                            labelKeyPrefix='workflowProcessDefinitionEntity'
	                            createNewDetailModalFunc={this.getWorkflowProcessDefinitionEntityForm}
                                actions={[AdvancedTable.defaultDeleteAction]}>

	                            <AdvancedTable.Column property='id' face='detail' detailRest='/api/wf/definition-entities' detailModalFunc={this.getWorkflowProcessDefinitionEntityForm} width='2.5%'/>
	                            <AdvancedTable.Column property='entityClassName' />
	                        </AdvancedTable>
						</Col>
					</Row>
				</Grid>
    		</fieldset>
    	);
    }
});

module.exports = WorkflowBox;
