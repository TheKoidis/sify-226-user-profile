var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var WorkflowAjax = require('./workflow-ajax.jsx');
var WorkflowBox = require('./workflow-box.jsx');

/**
 * This class represents list of workflow process definitions tasks.
 */
var WorkflowList = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	// returns initial state (empty process definition list)
    getInitialState: function() {
    	return {
    		processDefinitions: []
    	};
    },

    // reloads process definitions
    componentDidMount: function() {
    	this.reload();
    },

    // renders main container with process definition boxes inside
    render: function () {
    	return (
    		<Container id='body'>
               	<Grid>
               		<Row>
               			<Col sm={12} md={12} lg={12}>
    	           			<Section iconGlyph='icon-fontello-flow-branch' labelKey='workflow'>
    	           				<Dropzone ref="dropzone"
	               					multiple={true}
	               					accept='.bpmn20.xml'
	               					titleKey='workflow_upload'
	               					onDrop={this.onDrop} />

    	           				{this.state.processDefinitions.length > 0
    	           					?	this.createWorkflowBoxes()
    	           					:	null
    	           				}
    		               	</Section>
    		            </Col>
    		        </Row>
    		    </Grid>
            </Container>
    	);
    },

    // creates task boxes using state
    createWorkflowBoxes: function() {
    	var workflowBoxes = [];

    	for(var index = 0; index < this.state.processDefinitions.length; index++) {
    		workflowBoxes.push(
    			<WorkflowBox key={index} processDefinition={this.state.processDefinitions[index]} reloadFunc={this.reload}/>
    		);
    	}

    	return workflowBoxes;
    },

    // reloads process definitions and sets state
    reload: function() {
    	WorkflowAjax.getAllProcessDefinitions(function (response) {
    		this.setState({
    			processDefinitions: (response.resources) ? response.resources : []
    		});
		}.bind(this), this.getFlux());
    },

    onDrop: function (files) {
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.endsWith('bpmn20.xml')) {
                this.uploadFile(files[i]);
            } else {
                this.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'error',
                    message: l20n.ctx.getSync("workflow_ext_only_bpmn") + " (" + files[i].name + ")!"
                });
            }
        }

    },

    uploadFile: function (file) {
        var fd = new FormData();
        fd.append('data', file);
        fd.append('deploymentName', file.name);
        fd.append('fileName', file.name);

        $.ajax({
            url: '/api/workflow/deployments',
            data: fd,
            dataType: 'json',
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type: 'POST',
            error: function (xhr, status, err) {
                this.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'error',
                    message: l20n.ctx.getSync("workflow_not_deployed") + " (" + l20n.ctx.getSync(xhr.responseJSON.exception) + ")!"
                });
            }.bind(this),
            success: function (success) {
                this.getFlux().actions.flash.add({
                    messageId: 'success',
                    type: 'success',
                    message: l20n.ctx.getSync("workflow_deployed") + " (" + success.resource.name + ")."
                });
                this.reload();
            }.bind(this)
        });
    },
});

module.exports = WorkflowList;
