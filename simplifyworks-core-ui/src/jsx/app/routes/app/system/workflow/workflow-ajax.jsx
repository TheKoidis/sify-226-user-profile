/**
 * Utility class for workflow AJAX support
 */
var WorkflowAjax = {

	// returns all process definitions, invokes callback then and show message using flux
	getAllProcessDefinitions: function(callback, flux) {
	    $.ajax({
			headers: {
				'Accept': 'application/json',
	 	        'Content-Type': 'application/json'
	 	    },
	 	    method: 'GET',
	 	    url: '/api/workflow/process-definitions',
	 	    success: function (response) {
	 	    	callback(response);
	 	    }.bind(this),
	 	    error: function (xhr, status, error) {
	 	       	WorkflowAjax.processError(xhr, status, error, flux);
	 	    }.bind(this)
	    });
	},

	// deletes process definition, invokes callback then and show message using flux
	deleteProcessDefinition: function(processDefinition, callback, flux) {
		$.ajax({
			headers: {
				'Accept': 'application/json',
	 	        'Content-Type': 'application/json'
	 	    },
	 	    method: 'DELETE',
	 	    url: '/api/workflow/deployments/' + processDefinition.deploymentId,
	 	    success: function (response) {
	 	    	callback(response);
	 	    }.bind(this),
	 	    error: function (xhr, status, error) {
	 	       	WorkflowAjax.processError(xhr, status, error, flux);
	 	    }.bind(this)
	    });
	},

	// processes error (shows message to user) of AJAX requests
    processError: function(xhr, status, error, flux) {
    	//TODO process errors
    	console.log(xhr);
    	console.log(error);

    	flux.actions.flash.add({
            messageId: 'formErrors',
            type: 'error',
            message: l20n.ctx.getSync('error')
        });
    }
}

module.exports = WorkflowAjax;
