var restEndpoint = '/api/wf';

var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var WorkflowDefinitionEntitiesForm = React.createClass({
    mixins: [FluxMixin],
    propTypes: {
		recordId: React.PropTypes.object, // id of record
		modalCloseFunc: React.PropTypes.func // modal close function
	},

    render: function () {
        var newRecord = {
            processDefinitionKey: this.props.parent.key
        };
        return (
            <AdvancedForm id='form' recordId={this.props.recordId}  modalCloseFunc={this.props.modalCloseFunc} ref='form' newRecord={newRecord}
                labelKeyPrefix='workflowProcessDefinitionEntity' rest={restEndpoint+'/definition-entities'} flux={this.props.flux}>
                <AdvancedForm.Text size={12} property='entityClassName' validations="required,length:0:255"/>
            </AdvancedForm>
        );
    }
});

module.exports = WorkflowDefinitionEntitiesForm;
