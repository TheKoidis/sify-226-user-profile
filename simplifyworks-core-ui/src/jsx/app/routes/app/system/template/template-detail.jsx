var restEndpoint = '/api/core/templates/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);


var Form = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.code = component.model.code;

        this.state.data.description = component.model.description;
        this.state.data.body = $('#editor_body').trumbowyg('html');

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'template-detail');
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },

    render: function () {

            var commponent = (
                <Grid>
                    <Row>

                        <Col xs={6} xsOffset={3}>

                            <BasicForm name={'form-template-detail'+this.props.id}
                                       nameKey='template_detail'
                                       onSubmit={this.handleSubmit}
                                       formColor='blue'
                                       glyph='icon-simple-line-icons-grid'
                                       routeOnCancel='template-list'
                                       isNew={this.state.isNew}>

                                <InputText name='code'
                                           value={this.state.data.code}
                                           disabled={!this.state.isNew}
                                           required={true}
                                           labelKey='template_code'/>

                                <InputText name='description'
                                           value={this.state.data.description}
                                           labelKey='template_description'/>

                                       <InputText name='subject'
                                           value={this.state.data.subject}
                                           labelKey='template_subject'/>

                                <HtmlEditor name='body' value={this.state.data.body}/>
                            </BasicForm>
                        </Col>
                    </Row>

                </Grid>

            );

        return (
            <div>

                {commponent}
            </div>
        );
    }
});


var SettingDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],
    render: function () {

        var id = this.getParams().id;

        return (
            <Container id='body'>
                <Form url={restEndpoint + id}
                      id={id}
                      isNew={BasicForm.isNew(id)}/>
            </Container>
        );
    }
});

module.exports = SettingDetail;
