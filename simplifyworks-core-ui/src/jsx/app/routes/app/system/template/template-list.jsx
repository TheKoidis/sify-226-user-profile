var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var TemplateList = React.createClass({

    mixins: [FluxMixin],

    render: function () {
    	return (
    		<Container id='body'>
               	<Grid>
               		<Row>
               			<Col sm={12} md={12} lg={12}>
    	           			<Section iconGlyph='icon-simple-line-icons-grid' labelKey='templates'>
    	           				<AdvancedTable id='template-table'
    		                    	rest='/api/core/templates'
    		                    	ordering={[{property: 'code', order: 'asc'}]}
    		                    	labelKeyPrefix='template'
    		                    	createNewDetail='template-detail'
        							actions={[AdvancedTable.defaultDeleteAction]}>

    	           					<AdvancedTable.Column property='id' face='detail' detail='template-detail' width='2.5%' />
    		                        <AdvancedTable.Column property='code' width='10%' />
    		                        <AdvancedTable.Column property='description' />
    		                    </AdvancedTable>
    		               	</Section>
    		            </Col>
    		        </Row>
    		    </Grid>
            </Container>
    	);
    }
});

module.exports = TemplateList;
