// TODO: global app setting
var restEndpoint = '/api/core';

var PersonForm = require('../person/person-form.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var RenderUtil = require('../../../../renderer/render-util.jsx');
var PersonOrganizationForm = require('./person-organization-form.jsx');

var PersonDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    render: function () {
        var idPerson = this.getParams().id;

        return (
            <Container id='body'>
                <AdvancedDetail id='person-detail' rest='/api/core/persons' ref='person_detail' renderHeadFunc={this.renderHead}>
                    <AdvancedDetail.Section id='person-form' labelKey='basic_info' className='primary'>
                        <PersonForm ref='person_form'/>
                    </AdvancedDetail.Section>

                    <AdvancedDetail.Section id='users' labelKey='person_users' className='primary'>
            	       	<Section iconGlyph='icon-simple-line-icons-users' labelKey='users'>
			           		<AdvancedTable id='user-table'
                                            ref='user_table'
                                            rest='/api/core/users'
			                       		    filter={'person.id:'+(idPerson ? idPerson : '0')}
			                       		    createNewDetail='user-detail'
			                       		    ordering={[{property: 'username', order: 'asc'}]}
			                       		    labelKeyPrefix='user'>

			                       		    <AdvancedTable.Column property='id' face='detail' detail='user-detail' width='2.5%' />
                                            <AdvancedTable.Column property='username' filterType='none' />
                                            <AdvancedTable.Column property='lastLogin' face='datetime' filterType='none' width='10%'/>
                            </AdvancedTable>
			            </Section>
			        </AdvancedDetail.Section>

                    <AdvancedDetail.Section id='organizations' labelKey='person_personOrganizations' className='primary'>
	                   	<Section iconGlyph='icon-simple-line-icons-users' labelKey='person_personOrganizations'>
	                   		<AdvancedTable id='person-organization-table'
                                            ref='person_organization_table'
			                           		rest='/api/core/person-organizations'
			                           		filter={'person.id:'+(idPerson ? idPerson : '0')}
				                   			createNewDetailModalFunc={this.getPersonOrganizationForm}
			                           		ordering={[{property: 'organization.name', order: 'asc'}]}
			                           		labelKeyPrefix='personOrganization'>

				                   			<AdvancedTable.Column property='id' face='detail' detailRest='/api/core/person-organizations' detailModalFunc={this.getPersonOrganizationForm} width='2.5%' />
				                   			<AdvancedTable.Column property='organization.name' detail='organization-detail' detailProperty='organization.id' filterType='none' />
				                   			<AdvancedTable.Column property='validFrom' face='date' filterType='none' width='10%'/>
				                   			<AdvancedTable.Column property='validTill' face='date' filterType='none' width='10%'/>
				                   			<AdvancedTable.Column property='obligation' face='decimal' filterType='none' width='10%'/>
				            </AdvancedTable>
				        </Section>
				    </AdvancedDetail.Section>
                </AdvancedDetail>
            </Container>
        );
    },

    renderHead: function(record) {
        return record ? l20n.ctx.getSync('person_detail', {fullName: RenderUtil.personNiceLabel(record)}) : l20n.ctx.getSync('person_detail_new');
    },

    getPersonOrganizationForm: function (url, isNew, recordId, modalCloseFunc) {
    	return (
           	<PersonOrganizationForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} person={this.refs.person_detail.state.record}/>
        );
    }
});

module.exports = PersonDetail;
