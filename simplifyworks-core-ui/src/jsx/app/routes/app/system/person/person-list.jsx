var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var PersonList = React.createClass({
    mixins: [ReactRouter.State, FluxMixin],

    idSummaryExample: function(values) {
    	return this.summaryExample(values, 'id');
    },

    versionSummaryExample: function(values) {
    	return this.summaryExample(values, 'version');
    },

    summaryExample: function(values, property) {
    	var sum = 0;
    	var min = Number.MAX_SAFE_INTEGER;
    	var max = Number.MIN_SAFE_INTEGER;

    	for(var index = 0; index < values.length; index++) {
    		sum += values[index][property];
    		min = Math.min(min, values[index][property]);
    		max = Math.max(max, values[index][property]);
    	}

    	var avg = values.length === 0 ? null : (sum / values.length);

    	return (
    		<Grid>
    			<Row>
    				<Col sm={12} md={12} lg={12}>
    					Sum: {sum}
    				</Col>
    			</Row>

    			<Row>
					<Col sm={12} md={12} lg={12}>
						Avg: {avg}
					</Col>
				</Row>

				<Row>
					<Col sm={12} md={12} lg={12}>
					 	Min: {min}
					</Col>
				</Row>

				<Row>
					<Col sm={12} md={12} lg={12}>
					Max: {max}
					</Col>
				</Row>
    		</Grid>
    	);
    },

    render: function () {
    	var customNamedFilters = [];

    	customNamedFilters.push({
    		render: function() {
    			return 'With email';
    		},

    		createFilter: function() {
    			return 'email::is_not_null';
    		}
    	});

    	customNamedFilters.push({
        	render: function() {
        		return 'With name';
        	},

        	createFilter: function() {
        		return 'firstname::is_not_null';
        	}
    	});

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12} md={12} lg={12}>
                        	<Section iconGlyph='icon-dripicons-user' labelKey='persons'>
                        		<AdvancedTable
		                    		id='person-table'
		                    		ref='person_table'
		                    		rest='/api/core/persons'
		                    		createNewDetail='person-detail'
		                    		ordering={[{property: 'surname', order: 'asc'}]}
		                    		labelKeyPrefix='person'
		                    		actions={[AdvancedTable.defaultDeleteAction].concat(AdvancedTable.getWorkflowActions([{name:'approve'}, {name:'disapprove'}, {name:'to_beginning'}], function(oks, fails) {alert(oks.map(function(r) {return r.surname} ))}))}>

		                    		<AdvancedTable.Column property='id' face='detail' searchable={false} orderable={false} detail='person-detail' width='2.5%' />
		                    		<AdvancedTable.Column property='surname' tooltip='This is very long tooltip info which will be shown' width='20%' />
		                    		<AdvancedTable.Column property='firstname' width='20%' />
		                    		<AdvancedTable.Column property='email' width='25%' />
                                    <AdvancedTable.Column property='personalNumber' width='10%' />
		                    		<AdvancedTable.Column property='created' face='date' searchable={false} />
		                    	</AdvancedTable>
                        	</Section>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = PersonList;
