var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var PersonOrganizationForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    propTypes: {		
		recordId: React.PropTypes.object, // id of record
		modalCloseFunc: React.PropTypes.func, // modal close function
		
		person: React.PropTypes.object, // predefined person
		organization: React.PropTypes.object // predefined organization
	},
	
	render: function() {
		return (
			<AdvancedForm id='person-organization-form' ref='form'
				recordId={this.props.recordId} modalCloseFunc={this.props.modalCloseFunc} labelKeyPrefix='personOrganization'
				rest='/api/core/person-organizations' cancelRoute='person-list' flux={this.props.flux}>
				
				<AdvancedForm.Object size={6} property='person' predefinedValue={this.props.person} required={true} objectRenderFunc={RenderUtil.personNiceLabel} tableRenderFunc={this.renderPersonTable} validations='required'/>
				<AdvancedForm.Object size={6} property='organization' predefinedValue={this.props.organization} required={true} objectRenderFunc={RenderUtil.organizationNiceLabel} tableRenderFunc={this.renderOrganizationTable} validations='required'/>

				<AdvancedForm.Text size={6} property='category'/>
				<AdvancedForm.Number size={6} property='obligation' type='decimal' validations='required,maxScale:2' />	
				
				<AdvancedForm.Datetime size={6} property='validFrom' timeEnabled={true}/>
				<AdvancedForm.Datetime size={6} property='validTill' timeEnabled={true}/>				
			</AdvancedForm>
		);
	},
	
	renderPersonTable: function(flux, recordChosedFunc) {
		return (
			<AdvancedTable id='person-table' rest='/api/core/persons' labelKeyPrefix='person' flux={flux} recordChosedFunc={recordChosedFunc}>
				<AdvancedTable.Column property='surname'/>
				<AdvancedTable.Column property='firstname'/>
			</AdvancedTable>
		);
	},
	
	renderOrganizationTable: function(flux, recordChosedFunc) {
		return (
			<AdvancedTable id='organization-table' rest='/api/core/organizations' labelKeyPrefix='organization' flux={flux} recordChosedFunc={recordChosedFunc}>
				<AdvancedTable.Column property='code'/>
				<AdvancedTable.Column property='name'/>
			</AdvancedTable>
		);
	}
});


module.exports = PersonOrganizationForm;
