var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var CoreSexEnum = require('../../../../domain/core-sex-enum.jsx');

PersonForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    render: function() {
        var preWorkflowActionFunc = function(record, process, actionId, callback) {console.log(record); console.log(process); console.log(actionId); callback();};
		return (
			<AdvancedForm id='form' ref='form' labelKey='basic_info' labelKeyPrefix='person' rest='/api/core/persons' cancelRoute='person-list' preWorkflowActionFunc={preWorkflowActionFunc}>
				<AdvancedForm.Group name='Common' labelKey='person_common' helpKey='person_common_help'>
					<AdvancedForm.Text size={2} property='titleBefore' />
					<AdvancedForm.Text size={4} property='firstname' validations="length:0:50"/>
					<AdvancedForm.Text size={4} property='surname' validations="required,length:0:50"/>
					<AdvancedForm.Text size={2} property='titleAfter' />
				</AdvancedForm.Group>

				<AdvancedForm.Group name='Contact' labelKey='person_contact' helpKey='person_contact_help'>
					<AdvancedForm.Text size={4} property='email' validations="email,length:0:50"/>
					<AdvancedForm.Text size={4} property='phone' validations="length:0:50"/>
					<AdvancedForm.Text size={4} property='personalNumber' validations="length:0:50"/>
				</AdvancedForm.Group>

				<AdvancedForm.Group name='Other' labelKey='person_other'>
                    <AdvancedForm.Datetime size={6} property='dateOfBirth'/>
                    <AdvancedForm.Enum size={6} property='sex' options={Enumbox.enumOptions(CoreSexEnum.values,'coreSexEnum_', false)} validations="required"/>
                    <AdvancedForm.Datetime size={6} property='validFrom'/>
                    <AdvancedForm.Datetime size={6} property='validTo'/>
					<AdvancedForm.Text size={12} property='note' type='area'/>
                    <AdvancedForm.Boolean size={6} property='active'/>
				</AdvancedForm.Group>
		    </AdvancedForm>
		);
	}
});

module.exports = PersonForm;
