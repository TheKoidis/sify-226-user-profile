var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var OrganizationForm = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    render: function() {
		return (
			<AdvancedForm id='form' ref='form' labelKey='basic_info' labelKeyPrefix='organization' rest='/api/core/organizations' cancelRoute='organization-list'>
				<AdvancedForm.Text size={6} property='code' validations='length:1:50' />
				<AdvancedForm.Text size={6} property='abbreviation' validations='length:0:50'/>
				<AdvancedForm.Text size={12} property='name' validations='required,length:1:100'/>
				<AdvancedForm.Datetime size={6} property='validFrom'/>
				<AdvancedForm.Datetime size={6} property='validTo'/>
				<AdvancedForm.Object size={6} property='organization' labelKey='organization_parent_organization' objectRenderFunc={RenderUtil.organizationNiceLabel} tableRenderFunc={this.renderOrganizationTable}/>
				<AdvancedForm.Boolean size={6} property='active'/>
		    </AdvancedForm>
		);
	},

	renderOrganizationTable: function(flux, recordChosedFunc) {
		return (
			<AdvancedTable id='parent-organization' rest='/api/core/organizations' labelKeyPrefix='organization' flux={flux} recordChosedFunc={recordChosedFunc}>
				<AdvancedTable.Column property='code' />
				<AdvancedTable.Column property='name' />
				<AdvancedTable.Column property='abbreviation' />
			</AdvancedTable>
		);
	}
});

module.exports = OrganizationForm;
