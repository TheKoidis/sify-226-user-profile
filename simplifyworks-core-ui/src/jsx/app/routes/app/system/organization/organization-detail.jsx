// TODO: global app setting
var restEndpoint = '/api/core';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var OrganizationForm = require('./organization-form.jsx');
var UserRoleForm = require('./../user/user-role-form.jsx');
var PersonOrganizationForm = require('../person/person-organization-form.jsx');

var OrganizationDetail = React.createClass({

    mixins: [SidebarMixin, FluxMixin, ReactRouter.State],

    render: function () {
        var idOrganization = this.getParams().id;
        return (
            <Container id='body'>
                <AdvancedDetail id='organization-detail' rest='/api/core/organizations' ref='organization_detail' renderHeadFunc={this.renderHead}>
                    <AdvancedDetail.Section id='organization-form' labelKey='basic_info' className='primary'>
                        <OrganizationForm />
                    </AdvancedDetail.Section>

                    <AdvancedDetail.Section id='organization-subOrganizations' labelKey='organization_subOrganizations' className='primary'>
		                <Section iconGlyph='icon-dripicons-home' labelKey='organization_subOrganizations'>
			            	<AdvancedTable id='organizations-table'
	                    					rest='/api/core/organizations'
	                        				filter={'organization.id:' + (idOrganization ? idOrganization : '0')}
	                        				labelKeyPrefix='organization'
	                        				createNewDetail='organization-detail'
	                        				actions={[AdvancedTable.defaultDeleteAction]}>

	                    					<AdvancedTable.Column property='id' face='detail' detail='organization-detail' width='2.5%'/>
	                        				<AdvancedTable.Column property='code' filterType='none' width='10%'/>
	                        				<AdvancedTable.Column property='name' filterType='none' width='50%'/>
	                        				<AdvancedTable.Column property='abbreviation' filterType='none'/>
							</AdvancedTable>
		                </Section>
		            </AdvancedDetail.Section>

                    <AdvancedDetail.Section id='organization-roles' labelKey='users' className='primary'>
		                <Section iconGlyph='icon-simple-line-icons-users' labelKey='users'>
			            	<AdvancedTable id='user-role-table'
	            							rest='/api/core/user-roles'
	            							filter={'organization.id:' + (idOrganization ? idOrganization : '0')}
	            							labelKeyPrefix='userRole'
	            							createNewDetailModalFunc={this.getUserRoleModalDialog}
	            							actions={[AdvancedTable.defaultDeleteAction]}>

	            							<AdvancedTable.Column property='id' face='detail' detailRest='/api/core/user-roles' detailModalFunc={this.getUserRoleModalDialog} width='2.5%' />
	            							<AdvancedTable.Column property='user.username' labelKey='userRole_user' detail='user-detail' detailProperty='user.id' filterType='none' />
	            							<AdvancedTable.Column property='role.name' labelKey='userRole_role' detail='role-detail' detailProperty='role.id' filterType='none' width='25%'/>
	            							<AdvancedTable.Column property='validFrom' face='date' filterType='none' width='10%'/>
	            							<AdvancedTable.Column property='validTill' face='date' filterType='none' width='10%'/>
					         </AdvancedTable>
		                </Section>
		            </AdvancedDetail.Section>

                    <AdvancedDetail.Section id='organization-persons' labelKey='organization_personOrganizations' className='primary'>
		                <Section iconGlyph='icon-simple-line-icons-users' labelKey='organization_personOrganizations'>
			            	<AdvancedTable id='person-organization-table'
	            							rest='/api/core/person-organizations'
	            							filter={'organization.id:' + (idOrganization ? idOrganization : '0')}
	            							labelKeyPrefix='personOrganization'
	            							createNewDetailModalFunc={this.getPersonOrganizationForm}
	            							actions={[AdvancedTable.defaultDeleteAction]}>

	            							<AdvancedTable.Column property='id' face='detail' detailRest='/api/core/person-organizations' detailModalFunc={this.getPersonOrganizationForm} width='2.5%'/>
	            							<AdvancedTable.Column property='person' renderFunc={RenderUtil.personNiceLabel} detail='person-detail' detailProperty='person.id' filterType='none'/>
	            							<AdvancedTable.Column property='validFrom' face='date' filterType='none' width='10%'/>
	            							<AdvancedTable.Column property='validTill' face='date' filterType='none' width='10%'/>
	            							<AdvancedTable.Column property='obligation' face='decimal' filterType='none' width='10%'/>
					        </AdvancedTable>
		                </Section>
		            </AdvancedDetail.Section>
                </AdvancedDetail>
            </Container>
        );
    },

    renderHead: function(record) {
        return record ? l20n.ctx.getSync('organization_detail', {name: record.name}) : l20n.ctx.getSync('organization_detail_new');
    },

    getUserRoleModalDialog: function (url, isNew, recordId, modalCloseFunc) {
    	return (
           	<UserRoleForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} organization={this.refs.organization_detail.state.record}/>
        );
    },

    getPersonOrganizationForm: function (url, isNew, recordId, modalCloseFunc) {
    	return (
           	<PersonOrganizationForm recordId={recordId} modalCloseFunc={modalCloseFunc} flux={this.getFlux()} organization={this.refs.organization_detail.state.record}/>
        );
    }
});

module.exports = OrganizationDetail;
