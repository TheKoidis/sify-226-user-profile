var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var OrganizationList = React.createClass({
    render: function () {
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12} md={12} lg={12}>
                        	<Section iconGlyph='icon-dripicons-home' labelKey='organizations'>
	                        	<AdvancedTable id='organization-table'
	                            	rest='/api/core/organizations'
	                                createNewDetail='organization-detail'
	                                actions={[AdvancedTable.defaultDeleteAction]}
	                            	labelKeyPrefix='organization'>

	                                <AdvancedTable.Column property='id' face='detail' detail='organization-detail' width='2.5%' />
	                                <AdvancedTable.Column property='code' width='10%' />
	                                <AdvancedTable.Column property='name' width='50%'/>
	                                <AdvancedTable.Column property='abbreviation'/>
	                            </AdvancedTable>
                        	</Section>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = OrganizationList;
