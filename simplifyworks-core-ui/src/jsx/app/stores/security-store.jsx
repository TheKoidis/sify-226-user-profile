var constants = require('../actions/security-constants.jsx');
var Fluxxor = require('fluxxor');

/**
 * Store which owns security related data (current user, original user)
 */
var SecurityStore = Fluxxor.createStore({

    // initializes store
    initialize() {
        this.onReloadContext({});

        // bind actions
        this.bindActions(
            constants.SECURITY_RELOAD_CONTEXT, this.onReloadContext,
            constants.SECURITY_CHANGE_CURRENT_USER, this.onChangeCurrentUser
        );
    },

    // returns security infos
    getState() {
        return {
            currentUser: this.currentUser,
            originalUser: this.originalUser,
            allRoles: this.allRoles
        };
    },

    // reloads security context (payload can contain callback)
    onReloadContext: function(payload) {
        $.when(
            // sends request for user data to backend (frontend proxy adds security headers)
            $.ajax('/session', {})
        ).done(
            function (response) {
                // assign users
                this.currentUser = response.currentUser;
                this.originalUser = response.originalUser;
                this.allRoles = response.allRoles;

                if(response.expiration) {
                    setTimeout(function() {
                        window.location = response.expirationUrl;
                    }, response.expiration - Date.now());

                    setTimeout(function() {
                        this.flux.actions.flash.toast({
                            messageId: 'session-expiration-warning',
                            type: 'warning',
                            message: l20n.ctx.getSync('session_expiration_warning', {expiration: new Date(response.expiration).toLocaleTimeString()})
                        });
                    }.bind(this), response.expirationWarning - Date.now());
                }

                if(this.currentUser) {
                    // show flash messages
                    this.flux.actions.flash.clear();
                    this.flux.actions.flash.toast({
                        messageId: 'login-success',
                        type: 'success',
                        message: l20n.ctx.getSync('login_success', { user: this.currentUser.username })
                    });
                } else if(!localStorage.getItem('redirected-to-login')) {
                    window.location = '/login';
                    localStorage.setItem('redirected-to-login', true);
                }

                // invoke callback
                if(payload.callback) {
                    payload.callback();
                }

                // emit change of store
                this.emit('change');
            }.bind(this)
        );
    },

    // changes current user (payload must contain username and can contain callback)
    onChangeCurrentUser: function(payload) {
        $.when(
            // sends request to frontend (frontend checks security using backend and changes session state)
            $.post('/superuser', {currentUsername: payload.username})
        ).done(
            // reloads context
            function() {
                this.onReloadContext({callback: payload.callback});
            }.bind(this)
        );
    },

    // returns current user
    getCurrentUser: function() {
        return this.currentUser;
    },

    // returns role ownership flag
    hasRole: function(roleName) {
        return this.allRoles && this.allRoles.indexOf(roleName) != -1;
    }
});

module.exports = SecurityStore;
