var Fluxxor = require('fluxxor');

var BasketStore = Fluxxor.createStore({
    initialize() {
        this.ids = [];
    },

    add(id, type) {
        if (!this.ids[type]) {
            this.ids[type] = [];
        }
        this.ids[type].push(id);

        // emit change of store
        this.emit('change');
    },

    remove(id, type) {
        var index = this.ids[type].indexOf(id);
        if (index > -1) {
            this.ids[type].splice(index, 1);
        }

        // emit change of store
        this.emit('change');
    },

    contains(id, type) {
        if (!this.ids[type]) {
            return false;
        }
        var index = this.ids[type].indexOf(id);
        return (index > -1);
    },

    get(type) {
        return this.ids[type];
    },

    getString(type) {
        var idsString = "";
        if (!this.ids[type]) {
            return "";
        }

        for (var i=0;i<this.ids[type].length;i++) {
            if (i>0) {
                idsString = idsString+";";
            }
            idsString = idsString+this.ids[type][i];
        }
        //console.log("idsString",idsString);
        return idsString;
    },

    getState(type) {
        //console.log("getState",type);
        return {
            string: this.getString(type)
        }
    },

    clear(type) {
        this.ids[type] = [];

        // emit change of store
        this.emit('change');
    }
});

module.exports = BasketStore;
