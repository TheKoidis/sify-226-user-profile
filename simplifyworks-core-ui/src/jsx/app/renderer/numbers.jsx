var Numbers = {
		
	// global number formatting function (converts from 'java-big-decimal' style to string using specified separators)	
	format: function(input, outputGroupSeparator, outputDecimalSeparator) {
		// use default group separator if not specified
		if(outputGroupSeparator === undefined) {
			outputGroupSeparator = l20n.ctx.getSync('numbers_group_separator');
		}
		
		// use default decimal separator if not specified
		if(outputDecimalSeparator === undefined) {
			outputDecimalSeparator = l20n.ctx.getSync('numbers_decimal_separator');
		}		
		
		if(input) {
			// split stringified input using dot
			var parts = ('' + input).split('.');
			      
			var integerPart = '';
			var decimalPart = '';
			
			// format integer part
			if(parts.length >= 1) {
				integerPart = parts[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1" + outputGroupSeparator);
			}
			      
			// format decimal part
			if(parts.length === 2) {
				decimalPart = parts[1];
			}
			      
			// concat integer and decimal part
			return integerPart + (decimalPart ? outputDecimalSeparator + decimalPart : '');
		} else {
			return null;
		}
	},
	
	// global number parsing function (converts from string to 'java-big-decimal' style using specified separators; whitespaces in input are ignored)
	parse: function(input, inputGroupSeparator, inputDecimalSeparator) {		      
		// use default group separator if not specified
		if(inputGroupSeparator === undefined) {
			inputGroupSeparator = l20n.ctx.getSync('numbers_group_separator');
		}
		
		// use default decimal separator if not specified
		if(inputDecimalSeparator === undefined) {
			inputDecimalSeparator = l20n.ctx.getSync('numbers_decimal_separator');
		} 
		
		if(input) {
			// replace whitespaces and group separators, then parse transformed string using expression
			var parts = new RegExp('^(\\d+)(?:[' + inputDecimalSeparator + '](\\d+))?$').exec(('' + input).replace(new RegExp('\\s|' + inputGroupSeparator, 'g'), ''));
			
			// transform to final string if expression matched
			if(parts !== null) {        
				return parts[1] + (parts[2] ? ('.' + parts[2]) : ''); 
			}
		}
		
		return null;
	}
};

module.exports = Numbers;