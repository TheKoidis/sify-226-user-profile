var RenderUtil = React.createClass({

    statics: {

        idNiceLabel: function(object) {
            if(!object){
                return null;
            }
            return (object.id);
        },

        personNiceLabel: function (person) {
            if(!person){
                return null;
            }
            return (person.titleBefore ? person.titleBefore + ' ' : '')
                + (person.firstname ? person.firstname + ' ' : '')
                + (person.surname ? person.surname + ' ' : '')
                + (person.titleAfter ? person.titleAfter : '');

        },

        userNiceLabel: function (user) {
            if(!user){
                return null;
            }
            return (user.username);
        },

        userWithPersonLabel: function(user) {
        	if(!user) {
        		return null;
        	}

        	return user.username + (user.person ? ' (' + RenderUtil.personNiceLabel(user.person) + ')': '');
        },

        organizationNiceLabel: function (organization) {
            if(!organization){
                return null;
            }
            return (organization.name);

        },

        userRoleNiceLabel: function (role) {
            if(!role){
                return null;
            }
            return (role.name ? role.name: '');

        },

        personOrganizationNiceLabel: function (personOrganization) {
            if(!personOrganization){
                return null;
            }
            return this.personNiceLabel(personOrganization.person) + " / "+ this.organizationNiceLabel(personOrganization.organization);

        },

        definitionEntityNiceLabel: function (workflowProcessDefinitionEntity) {
            if(!workflowProcessDefinitionEntity){
                return null;
            }

            return workflowProcessDefinitionEntity.processDefinitionKey;

        },

        enumNiceLabel: function(prefix, value) {
            return l20n.ctx.getSync(prefix + value);
        },

        getDateTimeFormat(unix) {
            return l20n.ctx.getSync('patternDateTime'+(unix?'Unix':''));
        },

        getTimeFormat(unix) {
            return l20n.ctx.getSync('patternTime'+(unix?'Unix':''));
        },

        getDateFormat(unix) {
            return l20n.ctx.getSync('patternDate'+(unix?'Unix':''));
        },

    },

    render: function () {

    }


});

module.exports = RenderUtil;
