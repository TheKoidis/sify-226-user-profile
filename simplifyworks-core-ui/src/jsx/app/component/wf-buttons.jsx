var WfButtons = React.createClass({

    propTypes: {
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.array,
        disabled: React.PropTypes.bool,
        processFunc: React.PropTypes.func.isRequired,
        tooltip: React.PropTypes.string
    },

    getInitialState: function () {
        return {
            value: this.props.value
        };
    },


    //This method is call after reditrect with Route.
    componentWillReceiveProps: function (nextProps) {

        this.setState({
            value: nextProps.value
            // When the value changes, wait for it to propagate and
            // then validate the input
        });

    },


    processButton: function (button, e) {
        this.props.processFunc(button, e);
    },

    //return view for wf button
    getButton: function (button) {
        var label =  l20n.ctx.getSync(button.labelMessage)
        return (

            <Button
                name={button.key}
                bsStyle={button.styleClass}
                disabled={this.props.disabled || !button.editable}
                title={this.props.tooltip}
                onClick={this.processButton.bind(this,button)}
                >
                {label}
            </Button>
        );
    },


    render: function () {
        var value = this.state.value;


        var buttons = [];
        if (value) {
            for (var i = 0; i < value.length; i++) {
                buttons.push(
                    this.getButton(value[i])
                )
            }
        }

        return (
            <span>
                {buttons}
            </span>);
    }

});


module.exports = WfButtons;