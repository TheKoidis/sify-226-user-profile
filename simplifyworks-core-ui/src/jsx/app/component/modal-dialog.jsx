var classSet = React.addons.classSet;

var ModalDialogManager = {};
var ModalDialogFooter = React.createClass({
    render: function () {
        var props = React.mergeProps({
            className: 'modal-footer'
        }, this.props);

        return (
            <div {...props}>
                {this.props.children}
            </div>
        );
    }
});

var ModalDialogBody = React.createClass({
    render: function () {
        var props = React.mergeProps({
            className: 'modal-body'
        }, this.props);

        return (
            <div {...props}>
                {this.props.children}
            </div>
        );
    }
});

var ModalDialogHeader = React.createClass({
    render: function () {
        var props = React.mergeProps({
            className: 'modal-header'
        }, this.props);

        return (
            <div {...props}>
                {this.props.children}
            </div>
        );
    }
});

var ModalDialog = React.createClass({
    propTypes: {
        lg: React.PropTypes.bool,
        sm: React.PropTypes.bool,
        onShow: React.PropTypes.func,
        onShown: React.PropTypes.func,
        onHide: React.PropTypes.func,
        onHidden: React.PropTypes.func,
        modalId: React.PropTypes.string
    },
    getInitialState: function () {
        return {
            styles: {}
        };
    },
    getDefaultProps: function () {
        return {
            onShow: function () {
            },
            onShown: function () {
            },
            onHide: function () {
            },
            onHidden: function () {
            }
        };
    },

    unbindEvents: function () {
        if (ModalDialogManager.count == 1) {
            $('body').unbind('click.modal');
            $('body').unbind('keydown.modal');
            $('body').removeClass('modal-open').find('>.modal-backdrop').remove()
        }
    },
    componentWillUnmount: function () {
        this.unbindEvents();
    },
    open: function () {
        this.props.onShow();
        this.state.styles = {display: 'block'};
        this.setState(this.state, this.props.onShown);
        if (ModalDialogManager.count == 1) {
            $('html, body').css('overflow', 'hidden');
        }
    },
    close: function () {
        this.props.onHide();
        this.unbindEvents();
        this.state.styles = {display: 'none'};
        this.setState(this.state, this.props.onHidden);
        ModalDialogManager.remove(this.props.modalId);
    },
    render: function () {
        var modalDialogClasses = classSet({
            'modal-dialog': true,
            'modal-lg': this.props.lg,
            'modal-sm': this.props.sm
        });

        var modalClasses = classSet({
            'modal': true,
            'fade': true,
            'in': this.state.styles.display === 'block' ? true : false,
            'out': this.state.styles.display === 'none' ? true : false
        });

        var props = React.mergeProps({
            role: 'dialog',
            tabIndex: '-1',
            style: this.state.styles,
            className: modalClasses.trim()
        }, this.props);

        return (
            <div ref='modal' {...props}>
                <div className={modalDialogClasses.trim()}>
                    <div className='modal-content'>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
});

ModalDialogManager.count = 0;

ModalDialogManager.create = function (modal, modalId) {
    $('body').addClass('modal-open').append("<div class='modal-backdrop fade in'></div>").append("<div id='modal-container-" + modalId + "'></div>");
    ModalDialogManager.count++;

    var component = React.render(modal(), document.getElementById('modal-container-' + modalId));
    component.open();
};
ModalDialogManager.remove = function (modalId, e) {
    if (ModalDialogManager.count == 1) {
        $('html, body').css('overflow', '');
    }
    React.unmountComponentAtNode(document.getElementById('modal-container-' + modalId));
    $('body').find('>#modal-container-' + modalId).remove();
    ModalDialogManager.count--;
};

module.exports.ModalDialog = ModalDialog;
module.exports.ModalDialogBody = ModalDialogBody;
module.exports.ModalDialogHeader = ModalDialogHeader;
module.exports.ModalDialogFooter = ModalDialogFooter;
module.exports.ModalDialogManager = ModalDialogManager;
