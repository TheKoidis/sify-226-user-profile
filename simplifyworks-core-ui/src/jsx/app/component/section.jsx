/******************************************************************************************************************************************************
 *
 * Section is common component for wrapping and grouping another components.
 *
 ******************************************************************************************************************************************************/
var Section = React.createClass({

    propTypes: {
    	iconGlyph: React.PropTypes.string, // icon glyph for header
        label: React.PropTypes.string, // label string for header
    	labelKey: React.PropTypes.string, // label key string for header

    	headerClassName: React.PropTypes.string // header class name
    },

    getDefaultProps: function() {
    	return {
    		headerClassName: 'bg-darkblue fg-white'
    	}
    },

    // renders whole component
    render: function () {
    	return (
    		<PanelContainer noControls={true} bordered>
    			<Panel>
    				<PanelHeader className={this.props.headerClassName}>
    					<Grid>
    						<Row>
    							<Col sm={12} md={12} lg={12}>
    								<h3>
    									{this.props.iconGlyph
    										? <Icon glyph={this.props.iconGlyph}/>
    										: null
    									}
    									{this.props.iconGlyph
    										? ' '
    										: null
    									}
    									{this.props.label ? this.props.label : l20n.ctx.getSync(this.props.labelKey)}
    								</h3>
    							</Col>
    						</Row>
    					</Grid>
    				</PanelHeader>

    				<PanelBody>
    					<Grid>
    						<Row>
    							<Col sm={12} md={12} lg={12}>
    								{this.props.children}
    							</Col>
    						</Row>
    					</Grid>
    				</PanelBody>
    			</Panel>
    		</PanelContainer>
    	);
    }
});

module.exports = Section;
