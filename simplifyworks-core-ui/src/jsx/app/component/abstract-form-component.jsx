/**
 * Abstract form component that implements common methods (creating labels etc.)
 *
 * @type {{componentLabelMessage: Function, componentLabel: Function}}
 */
var AbstractFormComponent = {

    componentLabelMessage: function () {
        var componentLabelMessage;
        
        if (this.state.isValid === false && !this.state.disabled) {
            var invalidValidatorsLabel = '';

            var invalidValidators = this.state.invalidValidators;
            if (invalidValidators) {
                Object.keys(invalidValidators).forEach(function (name) {
                    var args = invalidValidators[name];
                    var local = l20n.ctx.getSync('validation', {item: name, arg1: args[0], arg2: args[1]});
                    invalidValidatorsLabel += (local == 'validation' ? name + args : local) + " ";
                })
            }

            var labelValidations = this.props.validationsMessage != null ? l20n.ctx.getSync(this.props.validationsMessage) : invalidValidatorsLabel;
            componentLabelMessage = (<div>
            							<Icon className={this.getValidationClassName()} glyph={this.getValidationGlyph()} />
            							<i>
            								{' '}{labelValidations}
            							</i>
            						</div>);
        }

        return componentLabelMessage;
    },
    
    getValidationClassName: function() {
    	switch(this.props.validationType) {
			case 'mandatory':
				return 'fg-danger';
			case 'provided':
				return 'fg-warning';
			case 'optional':
				return 'fg-info';
			default:
				console.log('Unknown validation type ' + this.props.validationType);
				return null;
    	}
    },
    
    getValidationGlyph: function() {
    	switch(this.props.validationType) {
    		case 'mandatory':
    			return 'icon-fontello-error';
    		case 'provided':
    			return 'icon-fontello-attention';
    		case 'optional':
    			return 'icon-fontello-info-circled';
    		default:
    			console.log('Unknown validation type ' + this.props.validationType);
    			return null;
    	}
    },

    getInitialState: function () {
        //It is first render after loading data
        //We will copy default value to value (we will validate only value).
        return {
            value: this.props.value,
            firstValidation: true,
            isValid: true,
            visible: this.props.visible
        };
    },

    componentLabel: function () {
        var componentLabel;
        var labelKey = this.props.labelKey;
        var label = this.props.label;
        var title = this.props.title;
        if (!title) {
            title=l20n.ctx.getSync(labelKey+'_tooltip');
            if (title==(labelKey+'_tooltip')) {
                title = undefined;
            }
        }
        var text = (label?label:l20n.ctx.getSync(labelKey)) + " " + (this.props.required ? ' *' : '');

        if (labelKey || label) {
            componentLabel =

                <Label
                    title={title}
                    htmlFor={this.props.name}>{text}</Label>
            
        }

        return componentLabel;
    },

    getDefaultProps: function () {
        return {
            visible: true,
            validationType: 'mandatory' // mandatory, provided, optional
        };
    },

    //This method is call after reditrect with Route.
    componentWillReceiveProps: function (nextProps) {
        //We can't create new validators and connections on Form (may be in future will be necessarily)
        //We use old (from previous props)
        nextProps.validations = this.props.validations;
        nextProps.attachToForm = this.props.attachToForm;
        nextProps.validate = this.props.validate;
        nextProps.detachToForm = this.props.detachToForm;
        nextProps.keyPressListener = this.props.keyPressListener;

        this.setState({
            value: nextProps.value,
            visible: nextProps.visible
            // When the value changes, wait for it to propagate and
            // then validate the input
        }, function () {
            if (this.props.validate) {
                this.props.validate(this);
            }
        }.bind(this));

    },

    showValidations: function () {
        if (this.state.firstValidation === true) {
            this.state.firstValidation = false;
        }
    },


    formGroup: function (component, className) {
        var result = <FormGroup className={className}>
            <span>{this.componentLabel()}</span>
            <span>{component}</span>
            <span>{this.componentLabelMessage()}</span>
        </FormGroup>


        return (
            <div>{this.state.visible === true ? result : null}</div>
        );
    }

};


module.exports = AbstractFormComponent;
