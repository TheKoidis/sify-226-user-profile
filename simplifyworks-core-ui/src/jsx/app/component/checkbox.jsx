var AbstractFormComponent = require('./abstract-form-component.jsx');

var Checkbox = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        //Input Name
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.bool,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string.isRequired
    },

    getDefaultProps: function () {
        return {
            value: false
        };
    },

    // Whenever the input changes we update the value state
    // of this component
    setValue: function (event) {
        this.showValidations();
        this.setState({
            value: event.currentTarget.checked
            // When the value changes, wait for it to propagate and
            // then validate the input
        }, function () {
          if (this.props.validate) {
              this.props.validate(this);
          }
          if (this.props.onChange) {
              this.props.onChange(this.props.name, this.state.value);
          }
        }.bind(this));
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true
        })

        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }
    },

    componentWillUnmount: function () {
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    render: function () {
        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }

        var component = <Input type='checkbox' id={this.props.name}
                               onChange={this.setValue}
                               onKeyPress={this.props.keyPressListener}
                               className={this.props.className}
                               disabled={this.state.disabled}
                               ref={this.props.name}
                               checked={this.state.value}/>

        return this.formGroup(component, className);
    }
});

module.exports = Checkbox;
