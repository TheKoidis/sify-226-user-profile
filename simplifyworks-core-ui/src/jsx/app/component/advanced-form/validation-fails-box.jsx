var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

/**
 * Component for rendering validation fails
 */
var ValidationFailsBox = React.createClass({

	propTypes: {
		label: React.PropTypes.string, // section label
		validationFails: React.PropTypes.arrayOf(React.PropTypes.object).isRequired // validation fails
	},

	getInitialState: function() {
		return {
			toggled: false
		}
	},

	render: function() {
		return (
			<div>
				{this.props.validationFails && this.props.validationFails.length > 0
					?	<Section iconGlyph='icon-fontello-attention' label={this.props.label ? l20n.ctx.getSync('advancedForm_workflow_validation_fails_list', {label: this.props.label}) : l20n.ctx.getSync('advancedForm_validation_fails_list')} headerClassName='bg-red fg-white'>
							<ListGroup>
								{this.createValidationFailSummary()}
								{this.state.toggled
									?	this.createValidationFailBoxes()
									:	null
								}
							</ListGroup>
						</Section>
					:	null
				}
			</div>
		);
	},

	createValidationFailSummary: function() {
		return (
			<ListGroupItem>
				<Grid collapse>
					<Row>
						<Col xs={10}>
							{l20n.ctx.getSync('advancedForm_workflow_validation_fails_summary', {count: this.props.validationFails.length})}
						</Col>
						<Col xs={2} style={{textAlign: 'right'}}>
							<Button sm onClick={this.toggle}>
								<Icon glyph={this.state.toggled ? 'icon-fontello-minus-circled' : 'icon-fontello-plus-circled'}/>
								{' ' + l20n.ctx.getSync(this.state.toggled ? 'advancedForm_workflow_validation_fails_show_less' : 'advancedForm_workflow_validation_fails_show_more')}
							</Button>
						</Col>
					</Row>
				</Grid>
			</ListGroupItem>
		)
	},

	createValidationFailBoxes: function() {
		var boxes = [];

		for(var index = 0; index < this.props.validationFails.length; index++) {
			var fail = this.props.validationFails[index];
			boxes.push(
				<ListGroupItem>
					<Icon glyph='icon-fontello-attention' className={fail.type == 'MANDATORY' ? 'fg-danger' : 'fg-warning'}/>{' '}
					{this.createValidationFailBox(fail)}
				</ListGroupItem>
			);
		}

		return boxes;
	},

	createValidationFailBox: function(validationFail) {
		return l20n.ctx.getSync(validationFail.humanMessageKey, validationFail.params);
	},

	toggle: function() {
		this.setState({
			toggled: !this.state.toggled
		});
	}
});

module.exports = ValidationFailsBox;
