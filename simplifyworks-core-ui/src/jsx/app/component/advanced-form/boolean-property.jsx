var AdvancedFormProperty = require('./property.jsx');

/**
 * Class represents input/output boolean property component of record.
 */
var AdvancedFormBooleanProperty = React.createClass({

	mixins: [AdvancedFormProperty],

	propTypes: {
		id: React.PropTypes.string.isRequired, // this component id

		value: React.PropTypes.bool, // initial value
		valueChangedFunc: React.PropTypes.func.isRequired, // listener for value changes

		labelKey: React.PropTypes.string.isRequired, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
        disabled: React.PropTypes.bool, // disabled field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	// returns initial state
	getInitialState: function() {
		return {
			value: this.props.value,
			validationResults: []
		};
	},

	// changes current value
	componentWillReceiveProps: function(nextProps) {
		this.setState({
			value: nextProps.value
		});
	},

	// renders form group
	render: function() {
		var component = (
			<Input id={this.props.id} type='checkbox'
				checked={this.state.value}
				onChange={this.onCheckBoxChange}
				disabled={this.props.disabled}/>
		);

		return this.renderProperty(component);
	},

	// changes value and propagates the change to the form
	onCheckBoxChange: function(event) {
		var newValue = event.currentTarget.checked;

		this.setState({
			value: newValue,
			validationResults: this.validate(newValue)
		}, this.props.valueChangedFunc.bind(this, newValue));
	}
});

module.exports = AdvancedFormBooleanProperty;
