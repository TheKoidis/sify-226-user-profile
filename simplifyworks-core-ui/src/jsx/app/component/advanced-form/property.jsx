var Validation = require('./validation.jsx');

/**
 * Class represents base for concrete property types.
 */
var AdvancedFormProperty = {

	// renders component wrapped in form group with label and validations
	renderProperty: function(component) {
		return (
			<FormGroup>
				<Label title={this.props.tooltip}>
					{this.props.label ?	this.props.label :
						<Entity entity={this.props.labelKey} />
					}
					{this.props.validations && this.props.validations.indexOf('required') > -1
						? ' *'
						: ''
					}
				</Label>

				<span>
					{component}
				</span>

				<span>
					{this.renderValidationLabels()}
				</span>
			</FormGroup>
		);
	},

	// renders validation labels
	renderValidationLabels: function() {
		return (
			<div>
				{this.renderValidationLabel('fg-danger', 'icon-fontello-error', 'mandatory')}
				{this.renderValidationLabel('fg-warning', 'icon-fontello-attention', 'provided')}
				{this.renderValidationLabel('fg-info', 'icon-fontello-info-circled', 'optional')}
			</div>
		);
	},

	// renders validation label of specified type using className and iconGlyph
	renderValidationLabel: function(className, iconGlyph, type) {
		var validationMessages = this.getValidationMessages(type);

		if(validationMessages.length > 0) {
			return (
				<div>
					<Icon className={className} glyph={iconGlyph} />
					<i>
					{' '}{validationMessages.join(' ')}
					</i>
				</div>
			);
		} else {
			return null;
		}
	},

	// returns validation messages for specified type of validation
	getValidationMessages: function(type) {
		var validationMessages = [];

		this.state.validationResults.forEach(function (validationResult) {
			if(validationResult.message && validationResult.type === type) {
				validationMessages.push(validationResult.message);
			}
		});

		return validationMessages;
	},

	// validates current value in state
	validateUsingState: function(callback) {
		var validationResults = this.validate(this.state.value);

		this.setState({
			validationResults : validationResults
		}, callback);

		return validationResults;
	},

	// validates value using validation props and returns validation results in array (each result has following properties: name, type, arguments and message)
	validate: function(value) {
		var validationResults = [];

		if(this.props.validations) {
			this.props.validations.split(',').forEach(function (validationString) {
				var validation = /([^.:]+)(?:[.]([^:]+))?(?:[:](.+))?/g.exec(validationString);

				var name = validation[1];
				var type = validation[2] ? validation[2] : 'mandatory';
				var arguments = validation[3] ? validation[3].split(':') : null;

				if(Validation[name]) {
					var validationResult = Validation[name](value, type, arguments);

					if(validationResult) {
						validationResults.push({
							name: name,
							type: type,
							arguments: arguments,
							message: validationResult
						});
					}
				} else {
					console.log('Unknown validation \'' + validationString + '\'');
				}
			});
		}

		if(this.props.validationFunc) {
			var validationResult = this.props.validationFunc(value);

			if(validationResult) {
				validationResults.push(validationResult);
			}
		}

		return validationResults;
	},

	containsMandatoryValidationError: function(validationResults) {
		for(var index = 0; index < validationResults.length; index++) {
			if(validationResults[index].type == 'mandatory' && validationResults[index].message !== null) {
				return true;
			}
		}

		return false;
	},

	notifyAboutChange: function(validationResults, newValue) {
		if(!this.containsMandatoryValidationError(validationResults)) {
			this.props.valueChangedFunc(newValue);
		}
	}
};

module.exports = AdvancedFormProperty;
