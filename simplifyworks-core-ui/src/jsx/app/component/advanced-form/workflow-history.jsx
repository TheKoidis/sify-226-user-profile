var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var AdvancedFormWorkflowAjax = require('./workflow-ajax.jsx');
var RenderUtil = require('../../renderer/render-util.jsx');

var WorkflowHistory = React.createClass({

    render() {
        var time = new Date().getTime();

        return (
            <Section iconGlyph='icon-fontello-archive' labelKey='advancedForm_workflow_history'>
                <Grid>
                    <Row>
                        <Col sm={12} md={12} lg={12}>
                            <fieldset>
                                <legend><Entity entity='advancedForm_workflow_overview' /></legend>
                                <ul>
                                    <li><Entity entity='advancedForm_workflow_overview_start' />: {new Date(this.props.historicProcessInstance.startTime).format(RenderUtil.getDateTimeFormat())}</li>
                                    <li><Entity entity='advancedForm_workflow_overview_end' />: {this.props.historicProcessInstance.endTime ? new Date(this.props.historicProcessInstance.endTime).format(RenderUtil.getDateTimeFormat()) : '-'}</li>
                                </ul>
                            </fieldset>
                        </Col>
                    </Row>

                    <Row>
                        <Col sm={6} md={6} lg={6}>
                            <fieldset>
                				<legend><Entity entity='advancedForm_workflow_history_activities' /></legend>
                                <WorkflowActivities processInstanceId={this.props.processInstanceId} flux={this.props.flux}/>
                            </fieldset>
                        </Col>
                        <Col sm={6} md={6} lg={6}>
                            <fieldset>
                				<legend><Entity entity='advancedForm_workflow_history_user_tasks' /></legend>
                                <WorkflowUserTasks processInstanceId={this.props.processInstanceId} flux={this.props.flux}/>
                            </fieldset>
                        </Col>
                    </Row>

                    <Row>
                        <Col sm={12} md={12} lg={12}>
                            <fieldset>
                				<legend><Entity entity='advancedForm_workflow_history_path' /></legend>
                                <img src={'/api/workflow/history/'+this.props.processInstanceId+'/image?time='+time} style={{width: '100%'}}/>
                            </fieldset>
                            <fieldset>
                				<legend><Entity entity='advancedForm_workflow_history_currentPosition' /></legend>
                                <img src={'/api/workflow/history/'+this.props.processInstanceId+'/image-current-position?time='+time} style={{width: '100%'}}/>
                            </fieldset>
                        </Col>
                    </Row>
                </Grid>

                <PanelFooter style={{'textAlign': 'right'}}>
                    <Button outlined onlyOnHover bsStyle='red' onClick={ModalDialogManager.remove.bind(this, 'workflow-history')}>
                        <Entity entity='btn_close' />
                    </Button>
                </PanelFooter>
            </Section>
        );
    }
});

var WorkflowActivities = React.createClass({

    getInitialState() {
        return {};
    },

    componentDidMount() {
        AdvancedFormWorkflowAjax.history('/api/workflow/history/activities/' + this.props.processInstanceId, this.fillHistory, this.props.flux);
    },

    fillHistory(response) {
        this.setState({
            data: response
        });
    },

    render() {
        if (!this.state.data) {
            return (<span/>);
        }
        var data = this.state.data.resources;
        var items = [];
        for (var i = 0; i < data.length; i++) {
            items.push(
                <li>{data[i].activityName}: <b>{data[i].assignee}</b> {new Date(data[i].startTime).format(RenderUtil.getDateTimeFormat())} </li>
            );
        }

        return (
            <ul>
                {items}
            </ul>
        );
    }
});

var WorkflowUserTasks = React.createClass({

    getInitialState() {
        return {};
    },

    componentDidMount() {
        AdvancedFormWorkflowAjax.history('/api/workflow/history/user-tasks/' + this.props.processInstanceId, this.fillHistory, this.props.flux);
    },

    fillHistory(response) {
        this.setState({
            data: response
        });
    },

    render() {
        if (!this.state.data) {
            return (<span/>);
        }
        var data = this.state.data.resources;
        var items = [];
        for (var i = 0; i < data.length; i++) {
            items.push(
                <li>{data[i].name}: <b>{data[i].assignee}</b> {new Date(data[i].startTime).format(RenderUtil.getDateTimeFormat())} </li>
            );
        }

        return (
            <ul>
                {items}
            </ul>
        );
    }
});

module.exports = WorkflowHistory;
