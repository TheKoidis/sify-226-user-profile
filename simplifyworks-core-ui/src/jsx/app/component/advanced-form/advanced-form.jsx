var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var ValidationFailsBox = require('./validation-fails-box.jsx');

var AdvancedFormProperty = require('./property.jsx');
var AdvancedFormTextProperty = require('./text-property.jsx');
var AdvancedFormNumberProperty = require('./number-property.jsx');
var AdvancedFormBooleanProperty = require('./boolean-property.jsx');
var AdvancedFormDatetimeProperty = require('./datetime-property.jsx');
var AdvancedFormEnumProperty = require('./enum-property.jsx');
var AdvancedFormObjectProperty = require('./object-property.jsx');

var AdvancedFormWorkflow = require('./workflow.jsx');

/**
 * Advanced form represents form with load/save support.
 */
var AdvancedForm = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	statics: {
		// returns property existence flag for specified object (supports 'dot convention')
	    hasProperty: function(object, property) {
	    	if(property) {
	    		var value = object;
	    		var propertyParts = property.split('.');

	    		// iterate through property name parts and updates value
	    		for(var index = 0; index < propertyParts.length; index++) {
	    			if(value) {
	    				value = value[propertyParts[index]];
	    			}
	    		}

	    		return value === undefined;
	    	} else {
	    		return null;
	    	}
	    },

		// returns property value for specified object (supports 'dot convention')
	    getPropertyValue: function(object, property) {
	    	if(property) {
	    		var value = object;
	    		var propertyParts = property.split('.');

	    		// iterate through property name parts and updates value
	    		for(var index = 0; index < propertyParts.length; index++) {
	    			if(value) {
	    				value = value[propertyParts[index]];
	    			}
	    		}

	    		return value;
	    	} else {
	    		return null;
	    	}
	    },

	    // sets property value for specified object (supports 'dot convention')
	    setPropertyValue: function(object, property, newValue) {
	    	if(property) {
	    		var value = object;
	    		var propertyParts = property.split('.');

	    		// iterate through property name parts and updates value
	    		for(var index = 0; index < propertyParts.length; index++) {
	    			if(index === propertyParts.length - 1) {
	    				value[propertyParts[index]] = newValue;
	    			} else {
	    				if(value[propertyParts[index]]) {
	    					value = value[propertyParts[index]];
	    				} else {
	    					value = {};
	    				}
	    			}
	    		}
	    	}

	    	return newValue;
	    }
	},

	propTypes: {
		id: React.PropTypes.string.isRequired, // id

        rest: React.PropTypes.string.isRequired, // REST URL for loading/saving data
        newRecord: React.PropTypes.object, // new record object for case when predefined instance must be used

        modalCloseFunc: React.PropTypes.func, // function responsible for closing modal
        cancelRoute: React.PropTypes.string, // route to show on cancel

        recordId: React.PropTypes.any, // id of record to load/save

        disabled: React.PropTypes.bool, // disabled form flag (overrides attributes in properties and disables buttons)

        postLoadFunc: React.PropTypes.func, // function invoked after load ('postLoad(record)')
        preSaveFunc: React.PropTypes.func, // function invoked before save after successful validation ('preSave(record, callback)')
        postSaveFunc: React.PropTypes.func, // function invoked after save ('postSave(record)')
        propertyValueChangedFunc: React.PropTypes.func, // function invoked after change of property value ('propertyValueChanged(property, value, record)')
		preWorkflowActionFunc: React.PropTypes.func, // function invoked before workflow action is executed ('preWorkflowAction(record, process, actionExecutionCallback)')

		addButtons: React.PropTypes.arrayOf(React.PropTypes.object), // additional buttons function

        labelKey: React.PropTypes.string, // label key string for header
        labelKeyPrefix: React.PropTypes.string // label key prefix string for header and form properties
	},

	// returns default props
	getDefaultProps: function() {
		return {
			disabled: false
		};
	},

	// returns initial state
	getInitialState: function() {
		return {
			processing: false,

			record: null,
			metadata: null,
			workflowMetadata: null,
			validationFails: []
		};
	},

	// loads data
    componentWillMount: function() {
    	this.load(this.props);
    },

	// loads data
	componentWillReceiveProps: function(nextProps) {
		var restChanged = this.props.rest !== nextProps.rest;
		var recordIdChanged = this.props.recordId !== nextProps.recordId;

		if(restChanged || recordIdChanged) {
			this.load(nextProps);
		}
	},

    // renders section with autocreated grid and control buttons
	render: function() {
		return (
			<Grid collapse>
				<Row>
					<Col sm={12} md={12} lg={12}>
						<ValidationFailsBox validationFails={this.state.validationFails} />

						{this.getWorkflowValidationFailBoxes()}
					</Col>
				</Row>

				<Row>
					<Col sm={12} md={12} lg={12}>
						<Section iconGlyph='icon-fontello-doc-text' labelKey={this.props.labelKey ? this.props.labelKey : (this.props.labelKeyPrefix + '_detail')}>
							<Form>
								{this.createGrid(this.props.children, null)}
							</Form>

							<PanelFooter>
								<Grid>
									<Row>
										<Col sm={2} md={2} lg={2}>
											{this.props.addButtons ?
												<ButtonToolbar>
													{this.props.addButtons}
												</ButtonToolbar>
												: null
											}
										</Col>

										<Col sm={5} md={5} lg={5}>
											<AdvancedFormWorkflow id={this.props.id} record={this.state.record} metadata={this.state.workflowMetadata} loadFunc={this.load} saveFunc={this.save} preWorkflowActionFunc={this.props.preWorkflowActionFunc} />
										</Col>

										<Col sm={5} md={5} lg={5}>
											<ButtonToolbar style={{textAlign: 'right'}}>
												{this.state.processing
													?	<Progress sm striped active value={100} min={0} max={100} style={{marginBottom: '0px'}}/>
													:	<span>
															<Button outlined onlyOnHover bsStyle='red' disabled={this.state.processing} onClick={this.cancel}>
									            				<Entity entity='btn_cancel'/>
									            			</Button>

									            			{(this.state.metadata !== null && !this.state.metadata.canEdit) || this.props.disabled
																?	null
																:	<span>
										            					<Button bsStyle='primary' onClick={this.save.bind(this, false, null)}>
										            						<Entity entity='btn_save'/>
										            					</Button>

										            					<Button bsStyle='primary' onClick={this.save.bind(this, true, null)}>
										            						<Entity entity='btn_save_and_exit'/>
										            					</Button>
									            					</span>
									            			}
														</span>
													}
								            </ButtonToolbar>
								        </Col>
								    </Row>
								</Grid>
							</PanelFooter>
						</Section>
					</Col>
				</Row>
			</Grid>
		);
	},

	// creates grid for body of form
	createGrid: function(children, group) {
		return (
	   		<Grid>
	   			{this.createRows(children, group)}
	   		</Grid>
	   	);
	},

	// creates rows for body grid
	createRows: function(children, group) {
	   	var cells = [[]];
	   	var rowIndex = 0;
	   	var colIndex = 0;

	   	// creates col for each child and auto-adds necessary rows
	   	React.Children.forEach(children, function(child) {
	   		if(child && child.props.visible) {
		   		var size = child.props.size;
		   		// TODO validate size <1; 12>

		   		if(colIndex + size > 12) {
		   			cells.push([]);
		   			rowIndex++;
		   			colIndex = 0;
		   		}

		   		colIndex += size;

		   		cells[rowIndex].push(
		   			<Col sm={size} md={size} lg={size}>
		   				{this.createComponent(child.type.displayName, child.props, group)}
		   			</Col>
		   		);
	   		}
	   	}.bind(this));

	    var rows = [];

	    cells.forEach(function (cellsInRow) {
	    	rows.push(
	    		<Row>
	    			{cellsInRow}
	    		</Row>
	    	);
	    });

	    return rows;
	},

	// creates concrete component using type of child and its properties
	createComponent: function(type, definition, group) {
		var record = this.state.record;

	    if(record) {
	    	var id = this.props.recordId ? this.props.recordId : this.getParams().id;
	    	var ref = (group ? group + '#' : '') + definition.property;
		   	var value = (id && id === 'new' && AdvancedForm.hasProperty(record, definition.property))
		   					? AdvancedForm.setPropertyValue(record, definition.property, definition.predefinedValue)
		   					: AdvancedForm.getPropertyValue(record, definition.property);
		   	var disabled = (this.state.metadata !== null && !this.state.metadata.canEdit) || this.props.disabled || definition.disabled;
		   	switch(type) {
		   		case 'Space':
		   			return (<div/>);
		   		case 'Group':
		   			return this.createGroup(definition);
					case 'Renderer':
						return <span>{definition.renderFunc()}</span>;
		   		case 'Text':
		   			return <AdvancedFormTextProperty
		   						id={id + definition.property}
		   						ref={ref}
		   						value={value}
		   						valueChangedFunc={this.propertyValueChanged.bind(this, definition.property)}
		   						type={definition.type}
		   						labelKey={this.getLabel(definition)}
								label={definition.label}
								tooltip={definition.tooltip}
		   						placeholder={definition.placeholder}
		   	        			disabled={disabled || this.state.processing}
		   						validations={definition.validations}
		   						validationFunc={definition.validationFunc}/>
		   		case 'Number':
		   			return <AdvancedFormNumberProperty
		   						id={id + definition.property}
		   						ref={ref}
		   						value={value}
		   						valueChangedFunc={this.propertyValueChanged.bind(this, definition.property)}
		   						type={definition.type}
		   						labelKey={this.getLabel(definition)}
								label={definition.label}
								tooltip={definition.tooltip}
		   						placeholder={definition.placeholder}
		   	        			disabled={disabled || this.state.processing}
		   						validations={definition.validations}
		   						validationFunc={definition.validationFunc}/>
		   		case 'Boolean':
		   			return <AdvancedFormBooleanProperty
		   						id={id + definition.property}
		   						ref={ref}
								value={value}
								valueChangedFunc={this.propertyValueChanged.bind(this, definition.property)}
								labelKey={this.getLabel(definition)}
								label={definition.label}
								tooltip={definition.tooltip}
		        				disabled={disabled || this.state.processing}
		   						validations={definition.validations}
								validationFunc={definition.validationFunc}/>
		   		case 'Datetime':
					return <AdvancedFormDatetimeProperty
								id={id + definition.property}
								ref={ref}
								value={value}
								valueChangedFunc={this.propertyValueChanged.bind(this, definition.property)}
		   						timeEnabled={definition.timeEnabled}
								labelKey={this.getLabel(definition)}
								label={definition.label}
								tooltip={definition.tooltip}
								showButtons={definition.showButtons}
								placeholder={definition.placeholder}
		       					disabled={disabled || this.state.processing}
								validations={definition.validations}
								validationFunc={definition.validationFunc}/>
		   		case 'Enum':
					return <AdvancedFormEnumProperty
								id={id + definition.property}
								ref={ref}
								value={value ? value : definition.options[0].value}
								valueChangedFunc={this.propertyValueChanged.bind(this, definition.property)}
								options={definition.options}
								labelKey={this.getLabel(definition)}
								label={definition.label}
								tooltip={definition.tooltip}
		   						disabled={disabled || this.state.processing}
								validations={definition.validations}
								validationFunc={definition.validationFunc}/>
		   		case 'Object':
					return <AdvancedFormObjectProperty
								id={id + definition.property}
								ref={ref}
								value={value}
								valueChangedFunc={this.propertyValueChanged.bind(this, definition.property)}
								objectRenderFunc={definition.objectRenderFunc}
								tableRenderFunc={definition.tableRenderFunc}
								labelKey={this.getLabel(definition)}
								label={definition.label}
								tooltip={definition.tooltip}
								disabled={disabled || this.state.processing}
								validations={definition.validations}
								validationFunc={definition.validationFunc}/>
		   		default:
		   			return <div>{type}</div>;
		   	}
	    } else {
	    	return null;
	    }
	},

	// creates group component
	createGroup: function(definition) {
		var mandatoryValid = true;
		var providedValid = true;
		var optionalValid = true;

		for(var ref in this.refs) {
			if(ref.startsWith(definition.name)) {
				var validationResults = this.refs[ref].state.validationResults;

				validationResults.forEach(function (validationResult) {
					if(validationResult.message) {
						switch(validationResult.type) {
							case 'mandatory':
								mandatoryValid = false;
								break;
							case 'provided':
								providedValid = false;
								break;
							case 'optional':
								optionalValid = false;
								break;
							default:
								console.log('Unknown type of validation ' + validationResult.type);
						}
					}
				});
			}
		}

		return (
			<fieldset>
				<legend>
					<Grid>
						<Row>
							<Col sm={6} md={6} lg={6}>
								<Entity entity={(definition.labelKey ? definition.labelKey : definition.name)} />
							</Col>

							<Col sm={6} md={6} lg={6} style={{textAlign: 'right'}}>
								<ButtonGroup>
									{!mandatoryValid
										? 	<Button sm bsStyle='danger' rounded disabled>
												<Icon glyph='icon-fontello-error'/>
										  	</Button>
										: 	null
									}

									{!providedValid
										? 	<Button sm bsStyle='warning' rounded disabled>
												<Icon glyph='icon-fontello-attention'/>
										  	</Button>
										: 	null
									}

									{!optionalValid
										? 	<Button sm bsStyle='info' rounded disabled>
												<Icon glyph='icon-fontello-info-circled'/>
										  	</Button>
										: 	null
									}

									{definition.helpKey
										? 	<Button sm bsStyle='success' rounded
												onClick={ModalDialogManager.create.bind(this,
															this.getHelpModalDialog.bind(this, (definition.labelKey ? definition.labelKey : definition.name), definition.helpKey), this.props.id)}>

												<Icon glyph='icon-fontello-help'/>
										  	</Button>
										: 	null
									}
								</ButtonGroup>
							</Col>
						</Row>
					</Grid>
				</legend>

				{this.createGrid(definition.children, definition.name)}
			</fieldset>
		);
	},

	getHelpModalDialog: function(labelKey, helpKey) {
		return (
			<ModalDialog lg modalId={this.props.id}>
		       	<ModalDialogBody>
			       	<PanelContainer noControls={true} bordered>
		    			<Panel>
		    				<PanelHeader className='bg-darkblue fg-white'>
		    					<Grid>
		    						<Row>
		    							<Col sm={12} md={12} lg={12}>
		    								<h3>
		    									<Icon glyph='icon-fontello-help'/>
		    									{' '}
		    									<Entity entity={labelKey}/>
		    								</h3>
		    							</Col>
		    						</Row>
		    					</Grid>
		    				</PanelHeader>

		    				<PanelBody>
		    					<Grid>
		    						<Row>
		    							<Col sm={12} md={12} lg={12}>
		    								<p>
		    									<Entity entity={helpKey} />
		    								</p>
		    							</Col>
		    						</Row>
		    					</Grid>
		    				</PanelBody>

		    				<PanelFooter>
		    					<Grid>
									<Row>
										<Col sm={12} md={12} lg={12}>
					    					<ButtonToolbar style={{textAlign: 'right'}}>
						    					<Button outlined onlyOnHover bsStyle='red' onClick={ModalDialogManager.remove.bind(this, this.props.id)}>
						    						Close
						    					</Button>
						    				</ButtonToolbar>
						    			</Col>
						    		</Row>
						    	</Grid>
		    				</PanelFooter>
		    			</Panel>
		    		</PanelContainer>
		        </ModalDialogBody>
		    </ModalDialog>
		);
	},

	// returns label for property
	getLabel: function(definition) {
		if(definition.labelKey) {
			return definition.labelKey;
		} else if(this.props.labelKeyPrefix) {
			return (this.props.labelKeyPrefix + '.' + definition.property).replace(/[.]/g, '_');
		} else {
			console.warn('Missing label for property \'' + definition.property + '\'');

			return definition.property;
		}
	},

	getWorkflowValidationFailBoxes: function() {
		var workflowValidationFailBoxes = [];

		if(this.state.workflowMetadata && this.state.workflowMetadata.processes) {
			for(var index = 0; index < this.state.workflowMetadata.processes.length; index++) {
				var processMetadata = this.state.workflowMetadata.processes[index];

				if(processMetadata.processInstance && processMetadata.processInstance.processVariables.result
					&& processMetadata.processInstance.processVariables.result.validationFails && processMetadata.processInstance.processVariables.result.validationFails.length > 0) {

					workflowValidationFailBoxes.push(
						<ValidationFailsBox label={processMetadata.processDefinition.name} validationFails={processMetadata.processInstance.processVariables.result.validationFails} />
					);
				}
			}
		}

		return workflowValidationFailBoxes;
	},

	// listener for cancel button
	cancel: function() {
		if(this.props.modalCloseFunc) {
			this.props.modalCloseFunc();
		} else {
			this.transitionTo(this.props.cancelRoute);
		}
	},

    // loads record into state using id parameter
	load: function(props) {
		if (!props) {
			props = this.props;
		}
		var id = props.recordId ? props.recordId : this.getParams().id;

		if(id && id !== 'new') {
			// FIXME ajax must be in setState callback (currently bug in React - callback not invoked in componentWillMount)
			this.setState({
				processing: true
			});

			$.ajax({
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'GET',
				url: props.rest + '/' + id,
				success: function (response) {
					this.setState({
						processing: false,
						record: response.resource,
						metadata: response.metadata,
						workflowMetadata: response.workflowMetadata
					}, props.postLoadFunc ? props.postLoadFunc.bind(this, response.resource) : null);
				}.bind(this),
				error: function (xhr, status, error) {
					this.processError(xhr, status, error);
				}.bind(this)
			});
		} else {
			this.setState({
				record: props.newRecord ? props.newRecord : {}
			});
		}
	},

	// saves record from state using id parameter
	save: function(exit, successCallback) {
		var id = (this.state.record && this.state.record.id) ? this.state.record.id : (this.props.recordId ? this.props.recordId : this.getParams().id);
		var callback = function(execute) {
			if(execute) {
				$.ajax({
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					method: (id && id !== 'new') ? 'PUT' : 'POST',
					url: this.props.rest,
					data: JSON.stringify(this.state.record),
					success: function (response) {
						this.setState({
							processing: false,
							record: response.resource,
							validationFails: []
						}, this.processSuccess.bind(this, exit, successCallback));
					}.bind(this),
					error: function (xhr, status, error) {
						if(xhr.status === 409) {
							this.processConflictError(xhr, status, error);
						} else {
							this.processError(xhr, status, error);
						}
					}.bind(this)
				});
			} else {
      	this.setState({
        	processing: false
        });
      }
		}.bind(this);

		if(this.validateForm()) {
			this.setState({
				processing: true
			}, function() {
				if(this.props.preSaveFunc) {
					this.props.preSaveFunc(this.state.record, callback);
				} else {
					callback(true);
				}
			});
		} else {
			this.getFlux().actions.flash.add({
                container: 'formErrors',
                messageId: 'formErrors',
                type: 'error',
                message: l20n.ctx.getSync('validation_error')
            });
		}
	},

	// validates form
	validateForm: function() {
		var mandatoryValid = true;

		for(var ref in this.refs) {
			var validationResults = this.refs[ref].validateUsingState(this.refreshAfterValidation);

			validationResults.forEach(function (validationResult) {
				if(validationResult.type === 'mandatory' && validationResult.message) {
					mandatoryValid = false;
				}
			});
		}

		return mandatoryValid;
	},

	refreshAfterValidation: function() {
		this.forceUpdate();
	},

	// processes success of save method
	processSuccess: function(exit, callback) {
		var id = this.props.recordId ? this.props.recordId : this.getParams().id;

		if(this.props.postSaveFunc) {
			this.props.postSaveFunc(this.state.record);
		}

		if(exit) {
			if(this.props.modalCloseFunc) {
				this.props.modalCloseFunc();
			} else {
				this.transitionTo(this.props.cancelRoute);
			}
		} else if(id && id === 'new' && !this.props.modalCloseFunc) {
			var currentRoutes = this.context.getCurrentRoutes();
			this.transitionTo(currentRoutes[currentRoutes.length - 1].name, {id: this.state.record.id})
		}

		if(callback) {
			callback();
		}

		if(exit){
			this.flashSuccessMessage();
		} else {
			this.setState({
				processing: false
			},this.flashSuccessMessage());
		}
	},
	// flux flash success message
	flashSuccessMessage: function(){
		// TODO refactor localization
		this.getFlux().actions.flash.add({
						messageId: 'formErrors',
						type: 'success',
						message: l20n.ctx.getSync('success')
				});
	},

	// processes conflict error (409 Conflict) caused by failed validation
	processConflictError: function(xhr, status, error) {
		this.getFlux().actions.flash.add({
            messageId: 'validation_failed',
            type: 'error',
            message: l20n.ctx.getSync('advancedForm_validation_failed')
        });

		this.setState({
			processing: false,
			validationFails: xhr.responseJSON.causes
		});
	},

	// processes error (shows message to user) of load or save method
    processError: function(xhr, status, error) {
    	//TODO process errors
    	console.log(xhr);
    	console.log(error);

		this.setState({
			processing: false
		});

    	this.getFlux().actions.flash.add({
            messageId: 'formErrors',
            type: 'error',
            message: l20n.ctx.getSync('error')
        });
    },

    // listener for value changes of form components
	propertyValueChanged: function(property, value) {
		var newRecord = this.state.record;
		AdvancedForm.setPropertyValue(newRecord, property, value);

		if(this.props.propertyValueChangedFunc) {
			this.props.propertyValueChangedFunc(property, value, newRecord);
		}

		this.setState({
			record: newRecord
		});
	}
});

/**
 * Empty space component (for visual purposes)
 */
AdvancedForm.Space = React.createClass({

	propTypes: {
		size: React.PropTypes.number.isRequired, // size in grid
		visible: React.PropTypes.bool // visibility field flag
	},

	getDefaultProps: function() {
		return {
			size: 12,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as descriptor
    render: function() {
    	return null;
    }
});

/**
 * Renderer component
 */
AdvancedForm.Renderer = React.createClass({
	propTypes: {
		renderFunc: React.PropTypes.func.isRequired, //function responsible for rendering own contents
		visible: React.PropTypes.bool, // visibility field flag
		size: React.PropTypes.number.isRequired // size in grid
	},

	getDefaultProps: function() {
		return {
			size: 12,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

/**
 * Group component (for grouping property components)
 */
AdvancedForm.Group = React.createClass({

	propTypes: {
		name: React.PropTypes.string.isRequired, // name of this group

		labelKey: React.PropTypes.string, // label string key
		helpKey: React.PropTypes.string, // help string key
		size: React.PropTypes.number.isRequired, // size in grid

		visible: React.PropTypes.bool // visibility field flag
	},

	getDefaultProps: function() {
		return {
			size: 12,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as group descriptor
    render: function() {
    	return null;
    }
});

/**
 * Text component (can use different type of 'output component')
 */
AdvancedForm.Text = React.createClass({

	propTypes: {
		property: React.PropTypes.string.isRequired, // property name
		type: React.PropTypes.oneOf(['text', 'area', 'password', 'hidden', 'html']), // type of text (defines final element - input(text), textarea, input(password) or input(hidden))

		labelKey: React.PropTypes.string, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		size: React.PropTypes.number.isRequired, // size in grid
		placeholder: React.PropTypes.string, // placeholder
        disabled: React.PropTypes.bool, // disabled field flag
        visible: React.PropTypes.bool, // visibility field flag

		validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	getDefaultProps: function() {
		return {
			type: 'text',
			disabled: false,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

/**
 * Number component
 */
AdvancedForm.Number = React.createClass({

	propTypes: {
		property: React.PropTypes.string.isRequired, // property name
		type: React.PropTypes.oneOf(['decimal', 'integer']), // type of number

		labelKey: React.PropTypes.string, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		size: React.PropTypes.number.isRequired, // size in grid
		placeholder: React.PropTypes.string, // placeholder
        disabled: React.PropTypes.bool, // disabled field flag
        visible: React.PropTypes.bool, // visibility field flag

		validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	getDefaultProps: function() {
		return {
			type: 'decimal',
			disabled: false,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

/**
 * Boolean component
 */
AdvancedForm.Boolean = React.createClass({

	propTypes: {
		property: React.PropTypes.string.isRequired, // property name

		labelKey: React.PropTypes.string, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		size: React.PropTypes.number.isRequired, // size in grid
        disabled: React.PropTypes.bool, // disabled field flag
        visible: React.PropTypes.bool, // visibility field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	getDefaultProps: function() {
		return {
			disabled: false,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

/**
 * Datetime component
 */
AdvancedForm.Datetime = React.createClass({

	propTypes: {
		property: React.PropTypes.string.isRequired, // property name
		timeEnabled: React.PropTypes.bool, // time support flag

		labelKey: React.PropTypes.string, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		size: React.PropTypes.number.isRequired, // size in grid
		showButtons:  React.PropTypes.bool, // show buttons (delete, today, prev, next)
		placeholder: React.PropTypes.string, // placeholder
        disabled: React.PropTypes.bool, // disabled field flag
        visible: React.PropTypes.bool, // visibility field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	getDefaultProps: function() {
		return {
			disabled: false,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

/**
 * Enum component
 */
AdvancedForm.Enum = React.createClass({

	propTypes: {
		property: React.PropTypes.string.isRequired, // property name
		options: React.PropTypes.arrayOf(React.PropTypes.string).isRequired, // options (enum values)

		labelKey: React.PropTypes.string, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		size: React.PropTypes.number.isRequired, // size in grid
        disabled: React.PropTypes.bool, // disabled field flag
        visible: React.PropTypes.bool, // visibility field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	getDefaultProps: function() {
		return {
			disabled: false,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

/**
 * Object component
 */
AdvancedForm.Object = React.createClass({

	propTypes: {
		property: React.PropTypes.string.isRequired, // property name
		predefinedValue: React.PropTypes.object, // predefined value for new records
		objectRenderFunc: React.PropTypes.func.isRequired, // function responsible for rendering the object
		tableRenderFunc: React.PropTypes.func.isRequired, // function responsible for rendering the table for selecting ('render(recordChosedCallback)')

		labelKey: React.PropTypes.string, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		size: React.PropTypes.number.isRequired, // size in grid
        disabled: React.PropTypes.bool, // disabled field flag
        visible: React.PropTypes.bool, // visibility field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	getDefaultProps: function() {
		return {
			disabled: false,
			visible: true
		};
	},

	// we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

module.exports = AdvancedForm;
