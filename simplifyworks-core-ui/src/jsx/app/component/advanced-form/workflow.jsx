var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var WorkflowHistory = require('./workflow-history.jsx');

var AdvancedFormWorkflowAjax = require('./workflow-ajax.jsx');

/**
 * Workflow support component for advanced form
 */
var AdvancedFormWorkflow = React.createClass({

	mixins: [FluxMixin],

	propTypes: {
		id: React.PropTypes.string.isRequired, // id

		record: React.PropTypes.object, // record
		metadata: React.PropTypes.object, // workflow meta data

		loadFunc: React.PropTypes.func, // load callback function
		saveFunc: React.PropTypes.func, // save callback function

		preWorkflowActionFunc: React.PropTypes.func // function invoked before workflow action is executed ('preWorkflowAction(record, process, actionExecutionCallback)')
	},

	// sets initial selected process meta data
	getInitialState: function() {
		return {
			selectedProcess: this.props.metadata && this.props.metadata.processes && this.props.metadata.processes.length > 0 ? this.props.metadata.processes[0] : null
		};
	},

	componentWillReceiveProps: function(nextProps) {
		var selectedProcess = null;

		if(nextProps.metadata && nextProps.metadata.processes) {
			if(this.state.selectedProcess) {
				for(var index = 0; index < nextProps.metadata.processes.length; index++) {
					if(nextProps.metadata.processes[index].processDefinition.id === this.state.selectedProcess.processDefinition.id) {
						selectedProcess = nextProps.metadata.processes[index];
						break;
					}
				}
			} else if (nextProps.metadata.processes.length > 0) {
				selectedProcess = nextProps.metadata.processes[0];
			}
		}

		this.setState({
			selectedProcess: selectedProcess
		});
	},

	// renders button tool bar (process switching uses drop down)
	render: function() {
		return (
			<ButtonToolbar id={this.props.id}>
				{this.props.metadata && this.props.metadata.processes && this.props.metadata.processes.length > 1
					?	<ButtonGroup dropup>
							{this.createProcessesDropdown()}
						</ButtonGroup>
					:	null
				}

				{this.state.selectedProcess
					?	this.createProcessToolbar()
					:	null
				}
			</ButtonToolbar>
		);
	},

	// creates processes drop down in case when more processes are available
	createProcessesDropdown: function() {
		return (
			<Dropdown>
				<DropdownButton bsStyle='info' container={this} menu={this.props.id + '-workflow-menu'}>
					<span>
						{this.state.selectedProcess ? this.state.selectedProcess.processDefinition.name : null}
					</span>{' '}
					<Caret/>
				</DropdownButton>

				{this.createProcessesDropdownMenu()}
			</Dropdown>
		);
	},

	// creates process drop down menu (items of menu represent possible processes)
	createProcessesDropdownMenu: function() {
		var processMenuItems = [];

		for(var index = 0; index < this.props.metadata.processes.length; index++) {
			processMenuItems.push(
				<MenuItem key={'workflow-process-' + index} keyaction={index} href='#'>
					{this.props.metadata.processes[index].processDefinition.name}
				</MenuItem>
			);
		}

		return (
			<Menu bsStyle='info' ref={this.props.id + '-workflow-menu'} onItemSelect={this.selectProcess}>
				{processMenuItems}
			</Menu>
		);
	},

	// creates tool bar for single process (the selected one)
	createProcessToolbar: function() {
		return (
			<ButtonToolbar>
				<ButtonGroup>
					{this.createProcessControlButtons()}
				</ButtonGroup>

				<ButtonGroup>
					{this.createProcessActionButtons()}
				</ButtonGroup>

				<ButtonGroup>
					{this.createProcessHelpButton()}
				</ButtonGroup>

				<ButtonGroup>
					{this.createHistoryButton()}
				</ButtonGroup>

				{this.getFlux().store("SecurityStore").hasRole('workflow_admin') ? (
					<ButtonGroup>
						{this.createResetButton()}
					</ButtonGroup>
				)
				:null}
			</ButtonToolbar>
		);
	},

	// creates process control buttons (create, suspend?, etc.)
	createProcessControlButtons: function() {
		var controlButtons = [];

		if(!this.state.selectedProcess.processInstance && !this.state.selectedProcess.historicProcessInstance) {
			controlButtons.push(
				<Button key='create-process' bsStyle='primary' onClick={this.createProcessInstance}>
					{l20n.ctx.getSync('advancedForm_workflow_create_process')}
				</Button>
			);
		}

		return controlButtons;
	},

	// creates process history button
	createHistoryButton: function() {
		var controlButtons = [];

		if(this.state.selectedProcess.processInstance || this.state.selectedProcess.historicProcessInstance) {
			controlButtons.push(
				<Button key='show-history' bsStyle='primary' onClick={this.showHistoryWindow} title={l20n.ctx.getSync('advancedForm_workflow_history')}>
					<Icon glyph='icon-fontello-archive'/>
				</Button>
			);
		}

		return controlButtons;
	},

	// creates process reset button
	createResetButton: function() {
		var controlButtons = [];

		if(this.state.selectedProcess.processInstance || this.state.selectedProcess.historicProcessInstance) {
			controlButtons.push(
				<Button key='reset' bsStyle='danger' onClick={this.resetProcessInstance} title={l20n.ctx.getSync('advancedForm_workflow_reset')}>
					<Icon glyph='icon-fontello-fire-2'/>
				</Button>
			);
		}

		return controlButtons;
	},

	// creates process action buttons
	createProcessActionButtons: function() {
		var actionButtons = [];

		if(this.state.selectedProcess.actions) {
			for(var index = 0; index < this.state.selectedProcess.actions.length; index++) {
				var action = this.state.selectedProcess.actions[index];

				actionButtons.push(
					<Button key={'workflow-action-' + index} bsStyle={action.style ? action.style : 'primary'} onClick={this.executeAction.bind(this, action)} title={action.tooltip}>
						{action.label}
					</Button>
				);
			}
		}

		return actionButtons;
	},

	// creates process help button
	createProcessHelpButton: function() {
		return (
			<Button bsStyle='success' onClick={ModalDialogManager.create.bind(this, this.getProcessHelpModalDialog, this.props.id)}>
				<Icon glyph='icon-fontello-help' />
			</Button>
		);
	},

	// handles another process selection
	selectProcess: function(event) {
		this.setState({
			selectedProcess: this.props.metadata.processes[event.keyaction]
		});
	},

	getModalHistoryContent(processInstanceId) {
		return (
			<ModalDialog lg modalId={this.props.name}>
                <ModalDialogBody>
                    <WorkflowHistory processInstanceId={processInstanceId} flux={this.getFlux()} historicProcessInstance={this.state.selectedProcess.historicProcessInstance}/>
                </ModalDialogBody>
            </ModalDialog>
		);
	},

	showHistoryWindow() {
		var processInstanceId = this.state.selectedProcess.processInstance ? this.state.selectedProcess.processInstance.id : this.state.selectedProcess.historicProcessInstance.id;
		ModalDialogManager.create(this.getModalHistoryContent.bind(this, processInstanceId), 'workflow-history');
	},

	// creates new process instance
	createProcessInstance: function() {
		var entityType = this.props.metadata.entityType;
		var entityId = this.props.metadata.entityId;
		var processDefinitionId = this.state.selectedProcess.processDefinition.id;

		this.props.saveFunc(false, function() {
			AdvancedFormWorkflowAjax.createProcessInstance(entityType, entityId, processDefinitionId, this.props.loadFunc, this.getFlux());
		}.bind(this));
	},

	// resets process instance
	resetProcessInstance: function() {
		var processInstanceId = this.state.selectedProcess.processInstance ? this.state.selectedProcess.processInstance.id : this.state.selectedProcess.historicProcessInstance.id;

		this.props.saveFunc(false, function() {
			AdvancedFormWorkflowAjax.resetProcessInstance(processInstanceId, this.props.loadFunc, this.getFlux());
		}.bind(this));
	},

	// executes process action
	executeAction: function(action) {
		var actionExecutionCallback = this.props.saveFunc.bind(this, false, function() {
			AdvancedFormWorkflowAjax.executeAction(this.state.selectedProcess.processInstance.id, action.id, this.props.loadFunc, this.getFlux());
		}.bind(this));

		if(this.props.preWorkflowActionFunc) {
			this.props.preWorkflowActionFunc(this.props.record, this.state.selectedProcess, action.id, actionExecutionCallback);
		} else {
			actionExecutionCallback();
		}
	},

	//  creates process help modal dialog
	getProcessHelpModalDialog: function() {
		return (
			<ModalDialog lg modalId={this.props.id}>
                <ModalDialogBody>
                    <PanelContainer noControls={true} bordered>
                        <Panel>
                            <PanelHeader className='bg-darkblue fg-white'>
                                <Grid>
                                    <Row>
                                        <Col sm={12} md={12} lg={12}>
                                            <h3>
                                                <Icon glyph='icon-fontello-help'/>
                                                {' ' + this.state.selectedProcess.processDefinition.name}
                                            </h3>
                                        </Col>
                                    </Row>
                                </Grid>
                            </PanelHeader>

                            <PanelBody>
                                <Grid>
                                    <Row>
                                        <Col sm={12} md={12} lg={12}>
                                            <p title={'version: '+this.state.selectedProcess.processDefinition.version+', id: '+this.state.selectedProcess.processDefinition.id}>
                                                {this.state.selectedProcess.processDefinition.description}
                                            </p>
                                        </Col>
                                    </Row>
                                </Grid>
                            </PanelBody>

                            <PanelFooter>
                                <Grid>
                                    <Row>
                                        <Col sm={12} md={12} lg={12}>
                                            <ButtonToolbar style={{textAlign: 'right'}}>
                                                <Button outlined onlyOnHover bsStyle='red' onClick={ModalDialogManager.remove.bind(this, this.props.id)}>
                                                    {l20n.ctx.getSync('btn_close')}
                                                </Button>
                                            </ButtonToolbar>
                                        </Col>
                                    </Row>
                                </Grid>
                            </PanelFooter>
                        </Panel>
                    </PanelContainer>
                </ModalDialogBody>
            </ModalDialog>
		);
	},
});

module.exports = AdvancedFormWorkflow;
