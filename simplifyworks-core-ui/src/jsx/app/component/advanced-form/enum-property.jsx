var AdvancedFormProperty = require('./property.jsx');

/**
 * Class represents input/output enum property component of record.
 */
var AdvancedFormEnumProperty = React.createClass({

	mixins: [AdvancedFormProperty],

	propTypes: {
		id: React.PropTypes.string, // this component id

		value: React.PropTypes.string, // initial value
		valueChangedFunc: React.PropTypes.func.isRequired, // listener for value changes

		options: React.PropTypes.arrayOf(React.PropTypes.object).isRequired, // options (enum values)

		labelKey: React.PropTypes.string.isRequired, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
        disabled: React.PropTypes.bool, // disabled field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	// returns initial state
	getInitialState: function() {
		return {
			index: this.getIndex(this.props.value),
			value: this.props.value,
			validationResults: []
		};
	},

	// changes current value
	componentWillReceiveProps: function(nextProps) {
		this.setState({
			index: this.getIndex(nextProps.value),
			value: nextProps.value
		});
	},

	componentDidMount: function() {
		this.props.valueChangedFunc(this.state.value);
	},

	// renders form group
	render: function() {
		var component = (
			<Select id={this.props.id}
				value={this.state.index}
				onChange={this.onChange}
				disabled={this.props.disabled}
				style={{'width': '100%'}}>

				{this.createOptions()}
			</Select>
		);

		return this.renderProperty(component);
	},

	createOptions: function() {
		var options = [];

		for(var index = 0; index < this.props.options.length; index++) {
			options.push(
				<option value={index}>
					{this.props.options[index].label}
				</option>
			);
		}

		return options;
	},

	// returns index for option value
	getIndex: function(value) {
		for(var index = 0; index < this.props.options.length; index++) {
			if(this.props.options[index].value instanceof Object && value instanceof Object){
				if(this.props.options[index].value.id === value.id) {
					return index;
				}
			}
			else{
				if(this.props.options[index].value === value) {
					return index;
				}
			}

		}
	},

	// changes value and propagates the change to the form
	onChange: function(event) {
		var newValue = this.props.options[event.target.value].value;

		if(!newValue) {
			newValue = null;
		}

		this.setState({
			index: event.target.value,
			value: newValue,
			validationResults: this.validate(newValue)
		}, this.props.valueChangedFunc.bind(this, newValue));
	}
});

module.exports = AdvancedFormEnumProperty;
