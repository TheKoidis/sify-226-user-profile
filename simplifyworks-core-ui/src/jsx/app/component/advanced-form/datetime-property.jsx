var AdvancedFormProperty = require('./property.jsx');
var RenderUtil = require('../../renderer/render-util.jsx');

/**
 * Class represents input/output datetime property component of record.
 */
var AdvancedFormDatetimeProperty = React.createClass({

	mixins: [AdvancedFormProperty],

	propTypes: {
		id: React.PropTypes.string.isRequired, // this component id

		value: React.PropTypes.object, // initial value
		valueChangedFunc: React.PropTypes.func.isRequired, // listener for value changes

		timeEnabled: React.PropTypes.bool, // time support flag

		labelKey: React.PropTypes.string.isRequired, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		showButtons:  React.PropTypes.bool, // show buttons (delete, today, prev, next)
		placeholder: React.PropTypes.string, // placeholder
        disabled: React.PropTypes.bool, // disabled field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	getInitialState: function() {
		return {
			validationResults: []
		};
	},

	// returns default props
	getDefaultProps: function() {
		return {
			timeEnabled: false
		};
	},

	// changes current value
	componentWillReceiveProps: function(nextProps) {
		if(nextProps.value) {
			$('#' + this.props.id).datetimepicker('setDate', new Date(nextProps.value));
		} else if(nextProps.value === null && this.props.value !== null) {
			$('#' + this.props.id).datetimepicker('reset');
		}
	},

	componentDidMount: function () {
        $('#' + this.props.id).datetimepicker({
            weekStart: '1',
            format: (this.props.timeEnabled ? RenderUtil.getDateTimeFormat(true) : RenderUtil.getDateFormat(true)),
            autoclose: true,
            todayBtn: true,
            todayHighlight: true,
            startDate: null,
            minView: (this.props.timeEnabled ? 0 : 2),
            minuteStep: 5,
            useLocalTimezone: true
        }).on('changeDate', function (event) {
        	var newValue = event.date ? new Date((event.date.getTime() + 60 * 1000 * event.date.getTimezoneOffset())) : null;

            this.setState({
                value: newValue,
                validationResults: this.validate(newValue)
            }, this.props.valueChangedFunc.bind(this, newValue));

        }.bind(this));

        if(this.props.value) {
    		var newValue = new Date(this.props.value);

			$('#' + this.props.id).datetimepicker('setDate', newValue);
			this.setState({
				value: newValue,
				validationResults: this.validate(newValue)
			}, this.props.valueChangedFunc.bind(this, newValue));
		}
    },

	// renders form group
	render: function() {
		var component = (
			<InputGroup>
	        	<Input type='text'
	               	id={this.props.id}
	                onChange={this.updateFromInput}
	                placeholder={this.props.placeholder}
	                disabled={this.props.disabled} />
							{this.createButtonGroup()}
	        </InputGroup>
		);

		return this.renderProperty(component);
	},

	// creates buttons for selection and clear
	createButtonGroup: function() {
		return (
			<InputGroupButton>
				<Button outlined bsStyle='primary' disabled={this.props.disabled} onClick={this.showCalendar}>
		   			<Icon bundle='icon-simple-line-icons-calendar' glyph='List'/>
		   		</Button>

					{this.props.showButtons === false ? [] :
						<div>
				   		<Button outlined onlyOnHover bsStyle='danger' disabled={this.props.disabled} onClick={this.clear}>
				    		<Icon bundle='outlined' glyph='delete'/>
				    	</Button>

			        <Button outlined onlyOnHover bsStyle='info' disabled={this.props.disabled} onClick={this.setToday}>
			        	<Icon bundle='icon-simple-line-icons-target' glyph='List'/>
			        </Button>

			        <Button outlined onlyOnHover bsStyle='info' disabled={this.props.disabled} onClick={this.setPreviousDay}>
			        	<Icon bundle='icon-simple-line-icons-arrow-left' glyph='List'/>
			        </Button>

			        <Button outlined onlyOnHover bsStyle='info' disabled={this.props.disabled} onClick={this.setNextDay}>
			        	<Icon bundle='icon-simple-line-icons-arrow-right' glyph='List'/>
			        </Button>
						</div>
					}

			</InputGroupButton>
		);
	},

	updateFromInput: function(event) {
        $('#' + this.props.id).datetimepicker(event.target.value === '' ? 'reset' : 'update');
	},

	showCalendar: function() {
		$('#' + this.props.id).datetimepicker('show');
	},

	clear: function() {
		$('#' + this.props.id).datetimepicker('reset');
	},

	setToday: function() {
		var newValue = this.props.timeEnabled ? new Date() : new Date(new Date().setUTCHours(0, 0, 0, 0));

		$('#' + this.props.id).datetimepicker('setDate', newValue);
		this.setState({
			value: newValue,
			validationResults: this.validate(newValue)
		}, this.props.valueChangedFunc.bind(this, newValue));
	},

	setPreviousDay: function() {
		$('#' + this.props.id).datetimepicker('increment', -1);
	},

	setNextDay: function() {
		$('#' + this.props.id).datetimepicker('increment', 1);
	}
});

module.exports = AdvancedFormDatetimeProperty;
