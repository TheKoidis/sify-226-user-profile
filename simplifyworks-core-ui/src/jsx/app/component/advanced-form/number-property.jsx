var Numbers = require('../../renderer/numbers.jsx');

var AdvancedFormProperty = require('./property.jsx');

/**
 * Class represents input/output number property component of record.
 */
var AdvancedFormNumberProperty = React.createClass({

	mixins: [AdvancedFormProperty],

	propTypes: {
		id: React.PropTypes.string.isRequired, // this component id

		value: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]), // initial value (string for decimals, integer otherwise)
		valueChangedFunc: React.PropTypes.func.isRequired, // listener for value changes

		type: React.PropTypes.oneOf(['decimal', 'integer']).isRequired, // type of number

		labelKey: React.PropTypes.string.isRequired, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		placeholder: React.PropTypes.string, // placeholder
        disabled: React.PropTypes.bool, // disabled field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	// returns initial state
	getInitialState: function() {
		return {
			value: Numbers.format(this.props.value),
			validationResults: []
		};
	},

	// update validations
	componentWillMount: function() {
		this.props.validations = this.getExtendedValidations(this.props);
	},

	// changes current value
	componentWillReceiveProps: function(nextProps) {
		nextProps.validations = this.getExtendedValidations(nextProps);
	},

	// returns extended validations using props
	getExtendedValidations: function (props) {
		switch(props.type) {
			case 'decimal':
				return 'decimal,' + props.validations;
			case 'integer':
				return 'integer,' + props.validations;
			default:
				return validations;
		}
	},

	// renders form group
	render: function() {
		var component = (
			<Input id={this.props.id}
				value={this.state.value}
				onChange={this.onChange}
				type='text'
				placeholder={this.props.placeholder}
				disabled={this.props.disabled}/>
		);

		return this.renderProperty(component);
	},

	// changes value and propagates the change to the form
	onChange: function(event) {
		var newValue = event.target.value;
		var newValidationResults = this.validate(newValue);

		this.setState({
			value: newValue,
			validationResults: newValidationResults
		}, this.notifyAboutChange(newValidationResults, (Numbers.parse(newValue))));
	},
});

module.exports = AdvancedFormNumberProperty;
