/**
 * Utility class for workflow AJAX support
 */
var WorkflowAjax = {

	// creates process instance, then invokes callback
	createProcessInstance: function(entityType, entityId, processDefinitionId, callback, flux) {
	    $.ajax({
			headers: {
				'Accept': 'application/json',
	 	        'Content-Type': 'application/json'
	 	    },
	 	    method: 'POST',
	 	    url: '/api/workflow/process-instances',
			data: JSON.stringify({entityType: entityType, entityId: entityId, processDefinitionId: processDefinitionId}),
	 	    success: function (response) {
	 	    	callback();
	 	    }.bind(this),
	 	    error: function (xhr, status, error) {
	 	       	this.processError(xhr, status, error, flux);
	 	    }.bind(this)
	    });
	},

	// resets process instance, then invokes callback
	resetProcessInstance: function(processInstanceId, callback, flux) {
		//TODO skip when process is not running
		$.ajax({
			headers: {
				'Accept': 'text/plain',
				'Content-Type': 'text/plain'
			},
			method: 'DELETE',
			url: '/api/workflow/process-instances/' + processInstanceId,
			success: function (response) {
				this.resetProcessHistory(processInstanceId, callback, flux);
			}.bind(this),
			error: function (xhr, status, error) {
				//skip when process is not running
				this.resetProcessHistory(processInstanceId, callback, flux);
			}.bind(this)
		});
	},

	// resets process instance, then invokes callback
	resetProcessHistory: function(processInstanceId, callback, flux) {
		$.ajax({
			headers: {
				'Accept': 'text/plain',
				'Content-Type': 'text/plain'
			},
			method: 'DELETE',
			url: '/api/workflow/history/' + processInstanceId,
			success: function (response) {
				callback();
			}.bind(this),
			error: function (xhr, status, error) {
				//TODO
				callback();
			}.bind(this)
		});
	},

	// executes action suing process instance id and action id, then invokes callback
	executeAction: function(processInstanceId, actionId, callback, flux) {
		$.ajax({
			headers: {
				'Accept': 'application/json',
	 	        'Content-Type': 'application/json'
	 	    },
	 	    method: 'PUT',
	 	    url: '/api/workflow/process-instances/' + processInstanceId + '/' + actionId,
	 	    success: function (response) {
	 	    	callback();
	 	    }.bind(this),
	 	    error: function (xhr, status, error) {
	 	       	this.processError(xhr, status, error, flux);
	 	    }.bind(this)
	    });
	},

	// get history for process instance id, then invokes callback
	history: function(url, callback, flux) {
		$.ajax({
			headers: {
				'Accept': 'application/json',
	 	        'Content-Type': 'application/json'
	 	    },
	 	    method: 'GET',
	 	    url: url,
	 	    success: function (response) {
	 	    	callback(response);
	 	    }.bind(this),
	 	    error: function (xhr, status, error) {
	 	       	this.processError(xhr, status, error, flux);
	 	    }.bind(this)
	    });
	},

	// processes error (shows message to user) of AJAX requests
    processError: function(xhr, status, error, flux) {
    	//TODO process errors
    	console.log(xhr);
    	console.log(error);

    	flux.actions.flash.add({
            messageId: 'formErrors',
            type: 'error',
            message: l20n.ctx.getSync('error')
        });
    }
}

module.exports = WorkflowAjax;
