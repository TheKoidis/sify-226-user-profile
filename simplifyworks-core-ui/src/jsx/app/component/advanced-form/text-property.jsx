var AdvancedFormProperty = require('./property.jsx');
//var ReactQuill = require('react-quill');

/**
 * Class represents input/output text property component of record.
 */
var AdvancedFormTextProperty = React.createClass({

	mixins: [AdvancedFormProperty],

	statics: {
		defaultTextareaRows: 3
	},

	propTypes: {
		id: React.PropTypes.string.isRequired, // this component id

		value: React.PropTypes.string, // initial value
		valueChangedFunc: React.PropTypes.func.isRequired, // listener for value changes

		type: React.PropTypes.oneOf(['text', 'area', 'password', 'hidden', 'html']).isRequired, // type of text (defines final element - input(text), textarea, input(password) or input(hidden))

		labelKey: React.PropTypes.string.isRequired, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		placeholder: React.PropTypes.string, // placeholder
		required: React.PropTypes.bool, // required field flag
        disabled: React.PropTypes.bool, // disabled field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	// returns initial state
	getInitialState: function() {
		return {
			value: this.props.value,
			validationResults: []
		};
	},

	// changes current value
	componentWillReceiveProps: function(nextProps) {
		this.setState({
			value: nextProps.value
		});
	},

	// renders form group
	render: function() {
		var component;

		if(this.props.type === 'area') {
			component = (
				<Textarea id={this.props.id}
					value={this.state.value}
					onChange={this.onChange}
		 			placeholder={this.props.placeholder}
					disabled={this.props.disabled}
					rows={AdvancedFormTextProperty.defaultTextareaRows} />
			);
		} else if(this.props.type === 'html') {
			component = (
				<Textarea id={this.props.id}
					value={this.state.value}
					onChange={this.onChange}
		 			placeholder={this.props.placeholder}
					disabled={this.props.disabled}
					rows={AdvancedFormTextProperty.defaultTextareaRows} />
			);
		} else {
			component = (
				<Input id={this.props.id}
					value={this.state.value}
					onChange={this.onChange}
					type={this.props.type}
					placeholder={this.props.placeholder}
					disabled={this.props.disabled}/>
			);
		}

		return this.renderProperty(component);
	},

	// changes value and propagates the change to the form
	onChange: function(event) {
		var newValue = event.target.value;

		this.setState({
			value: newValue,
			validationResults: this.validate(newValue)
		}, this.props.valueChangedFunc.bind(this, newValue));
	}
});

module.exports = AdvancedFormTextProperty;
