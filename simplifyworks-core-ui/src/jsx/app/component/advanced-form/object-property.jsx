var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var AdvancedFormProperty = require('./property.jsx');

/**
 * Class represents input/output object property component of record.
 */
var AdvancedFormObjectProperty = React.createClass({

	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin, AdvancedFormProperty],

	propTypes: {
		id: React.PropTypes.string.isRequired, // this component id

		value: React.PropTypes.object, // initial value
		valueChangedFunc: React.PropTypes.func.isRequired, // listener for value changes

		objectRenderFunc: React.PropTypes.func.isRequired, // function responsible for rendering the object ('render()'
		tableRenderFunc: React.PropTypes.func.isRequired, // function responsible for rendering the table for selecting ('render(recordSelectedCallback)')

		labelKey: React.PropTypes.string.isRequired, // label string key
		label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string
		placeholder: React.PropTypes.string, // placeholder
        disabled: React.PropTypes.bool, // disabled field flag

        validations: React.PropTypes.string, // validations string (format: [validatorName](.[validationType])?(:[validationArgument])*)
		validationFunc: React.PropTypes.func // function responsible for validation (can be combined with validations; 'validationFunc(value)')
	},

	// returns initial state
	getInitialState: function() {
		return {
			value: this.props.value,
			validationResults: []
		};
	},

	// changes current value
	componentWillReceiveProps: function(nextProps) {
		this.setState({
			value: nextProps.value
		});
	},

	// renders form group
	render: function() {
		var component = (
			<InputGroup>
				<Input id={this.props.id}
					type='text'
					value={this.props.objectRenderFunc(this.state.value)}
					disabled={true}/>

				<InputGroupButton>
					{this.createButtons()}
				</InputGroupButton>
			</InputGroup>
		);

		return this.renderProperty(component);
	},

	// creates buttons for selection and clear
	createButtons: function() {
		var buttons = [];

		buttons.push(
			<Button outlined bsStyle='primary' key='select' disabled={this.props.disabled} onClick={ModalDialogManager.create.bind(this, this.getModalDialog, this.props.id)}>
				<Icon glyph='icon-fontello-menu' />
			</Button>
		);

		if(this.state.value) {
			buttons.push(
				<Button outlined onlyOnHover bsStyle='red' key='clear' disabled={this.props.disabled} onClick={this.clear}>
					<Icon glyph='icon-fontello-cancel' />
				</Button>
			);
		}

		return buttons;
	},

	// returns modal dialog with table for choosing the record
	getModalDialog: function () {
		return (
			<ModalDialog lg modalId={this.props.name}>
	        	<ModalDialogBody>
	        		<Section iconGlyph='icon-fontello-list' labelKey={this.props.labelKey}>
	        			{this.props.tableRenderFunc(this.context.flux, this.choose)}

	        			<PanelFooter style={{'textAlign': 'right'}}>
	        				<Button outlined onlyOnHover bsStyle='red' onClick={ModalDialogManager.remove.bind(this, this.props.id)}>
	            				<Entity entity='btn_close' />
	            			</Button>
	        			</PanelFooter>
	        		</Section>
	            </ModalDialogBody>
	        </ModalDialog>
	    );
	},

	// listener for choosing the object
	choose: function(object) {
		ModalDialogManager.remove(this.props.id);

		this.setState({
			value: object,
			validationResults: this.validate(object)
		}, this.props.valueChangedFunc.bind(this, object));
	},

	// listener for clearing
	clear: function() {
		this.setState({
			value: null,
			validationResults: this.validate(null)
		}, this.props.valueChangedFunc.bind(this, null));
	},
});

module.exports = AdvancedFormObjectProperty;
