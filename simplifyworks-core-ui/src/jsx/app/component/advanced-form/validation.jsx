var Numbers = require('../../renderer/numbers.jsx');

/**
 * Common utility class for validation support
 */
var Validation = {

	required: function(value, type, args) {
		if(!value) {
			return l20n.ctx.getSync('validation', {item: 'required'});
		} else {
			return null;
		}
	},

	length: function(value, type, args) {
		if(value !== undefined && value !== null && (value.length < args[0] || value.length > args[1])) {
			return l20n.ctx.getSync('validation', {item: 'isLength', arg1: args[0], arg2: args[1]});
		} else {
			return null;
		}
	},

	lowercase: function(value, type, args) {
		if(value !== undefined && value !== null && value !== ''  && (value.toLowerCase() !== value)) {
			return l20n.ctx.getSync('validation', {item: 'isLowercase'});
		} else {
			return null;
		}
	},

	email: function(value, type, args) {
		if(value !== undefined && value !== null && value !== '' && (value.indexOf('@') === -1)) {
			return l20n.ctx.getSync('validation', {item: 'isEmail'});
		} else {
			return null;
		}
	},

	integer: function(value, type) {
		if(value !== undefined && value !== null && value !== '' && (Numbers.parse(value) === null || Numbers.parse(value).indexOf('.') !== -1)) {
			return l20n.ctx.getSync('validation', {item: 'isInteger'});
		} else {
			return null;
		}
	},

	decimal: function(value, type) {
		if(value !== undefined && value !== null && value !== '' && Numbers.parse(value) === null) {
			return l20n.ctx.getSync('validation', {item: 'isDecimal'});
		} else {
			return null;
		}
	},

	maxScale: function(value, type, args) {
		var number;

		if(value !== undefined && value !== null && value !== '' && (number = Numbers.parse(value)) !== null) {
			if(number.indexOf('.') !== -1 && (number.length - number.indexOf('.') - 1) > args[0]) {
				return l20n.ctx.getSync('validation', {item: 'maxScale', arg1: args[0]});
			}
		}

		return null;
	},

	maxValue: function(value, type, args) {
		if(value !== undefined && value !== null && value !== '' && Numbers.parse(value) > args[0]*1) {
			return l20n.ctx.getSync('validation', {item: 'maxValue', arg1: args[0]});
		} else {
			return null;
		}
	},
}

module.exports = Validation;
