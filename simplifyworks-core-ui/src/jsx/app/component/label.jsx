var AbstractFormComponent = require('./abstract-form-component.jsx');

var Label = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        name: React.PropTypes.string.isRequired,
        labelKey: React.PropTypes.string,
    },

    render: function () {

        return this.formGroup('', '');
    }
});

module.exports = Label;
