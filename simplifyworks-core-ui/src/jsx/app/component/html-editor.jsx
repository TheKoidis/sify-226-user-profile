var AbstractFormComponent = require('./abstract-form-component.jsx');

var HtmlEditor = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        //Input Name
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.string,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        placeholder: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string,
        password: React.PropTypes.bool,
        hidden: React.PropTypes.bool,
        changeValueFunc: React.PropTypes.func
    },

    // Whenever the input changes we update the value state
    // of this component
    setValue: function (event) {
        //TODO set value to state (without rerendering wysiwyg - and lost cursor position)
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true
        })

        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }

    },

    componentWillUnmount: function () {
        console.log('componentWillUnmount: ' + this.props.name);
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    componentDidMount: function() {
        var that = this;
        $('#editor_'+this.props.name).trumbowyg({
            mobile: false,
            tablet: false,
            autogrow: true,
            dir: $('html').attr('dir')
        }).on('tbwchange', function(){ that.setValue(); }).trumbowyg('html', this.state.value);

        $('#editor_'+this.props.name).attr('contenteditable', !this.props.disabled);
    },

    render: function () {
        var value = this.state.value;

        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }

        //TODO hack with test attribute (to force react rerender when value changed)
        var component = <div id={'editor_'+this.props.name} test={this.state.value}></div>

        return this.props.hidden ? component : this.formGroup(component, className);
    }
});

module.exports = HtmlEditor;
