var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

/******************************************************************************************************************************************************
 *
 * Detail is complex React component for quick and easy object detail building.
 *
 ******************************************************************************************************************************************************/
var Detail = React.createClass({
    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    propTypes: {
        id: React.PropTypes.string.isRequired, // id

        rest: React.PropTypes.string.isRequired, // REST URL for loading global data (for head)
        recordId: React.PropTypes.any, // id of record to load/save

        renderHeadFunc: React.PropTypes.func.isRequired // function responsible for rendering detail head ('headRenderFunc(record)'); when invoked with not null argument, it is the loaded one; otherwise the record is new one
    },

    getInitialState: function() {
        return {
            selectedSectionIndex: 0,
            record: null
        };
    },

    // loads data
    componentWillMount: function() {
    	this.load();
    },

    // loads data
    componentWillReceiveProps: function() {
    	this.load();
    },

    render: function() {
        return (
            <Grid>
                <Row>
                    <Col sm={12} md={12} lg={12}>
                    	<Panel style={{marginBottom: '10px'}}>
                    		<PanelHeader className='bg-darkblue fg-white' style={{marginBottom: '0px', borderBottomLeftRadius: '3px', borderBottomRightRadius: '3px'}}>
                    			<Grid>
                    				<Row>
                    				    <Col sm={12} md={12} lg={12}>
                    					    <h3>
                    						    {this.props.renderHeadFunc(this.state.record)}
                    						</h3>
                    					</Col>
                    				</Row>
                    			</Grid>
                    		</PanelHeader>
                    	</Panel>
                    </Col>
                </Row>

                {!this.isNew() && React.Children.count(this.props.children) > 1
                    ?   <Row>
                            <Col sm={12} md={12} lg={12}>
                                <ButtonGroup style={{marginBottom: '10px'}}>
                                    {this.createSectionButtons()}
                                </ButtonGroup>
                            </Col>
                        </Row>
                    :   null
                }

                <Row>
                    <Col sm={12} md={12} lg={12}>
                        {this.getSection()}
                    </Col>
                </Row>
            </Grid>
        );
    },

    isNew: function() {
        var id = this.props.recordId ? this.props.recordId : this.getParams().id;

        return id === 'new';
    },

    createSectionButtons: function() {
        var sectionButtons = [];

        React.Children.forEach(this.props.children, function(child) {
            if (child != null) {
                var label = this.getLocalized(child.props.label, child.props.labelKey);
                var tooltip = this.getLocalized(child.props.tooltip, child.props.tooltipKey);

                sectionButtons.push(
                    <Button key={'section-' + sectionButtons.length} bsStyle={child.props.className} active={this.state.selectedSectionIndex === sectionButtons.length} title={tooltip} onClick={this.changeSection.bind(this, sectionButtons.length)}>
                        {label}
                    </Button>
                );
            }
        }.bind(this));

        return sectionButtons;
    },

    getLocalized: function(value, valueKey) {
        if(value) {
            return value;
        } else if(valueKey) {
            return l20n.ctx.getSync(valueKey);
        } else {
            return null;
        }
    },

    getSection: function() {
        var index = 0;
        var section = null;

        React.Children.forEach(this.props.children, function(child) {
            if(this.state.selectedSectionIndex === index) {
                section = child.props.children;
            }

            index++;
        }.bind(this));

        return section;
    },

    changeSection: function(selectedSectionIndex) {
        this.setState({
            selectedSectionIndex: selectedSectionIndex
        });
    },

    // loads record into state using id parameter
	load: function() {
		var id = this.props.recordId ? this.props.recordId : this.getParams().id;

		if(id && id !== 'new') {
			$.ajax({
				headers: {
	 	    		'Accept': 'application/json',
	 	            'Content-Type': 'application/json'
	 	        },
	 	        method: 'GET',
	 	        url: this.props.rest + '/' + id,
	 	        success: function (response) {
	 	        	this.setState({
	 	        		record: response.resource
	 	        	});
	 	        }.bind(this),
	 	        error: function (xhr, status, error) {
	 	        	this.processError(xhr, status, error);
	 	        }.bind(this)
	        });
		}
	},

    // processes error (shows message to user) of load or save method
    processError: function(xhr, status, error) {
    	//TODO process errors
    	console.log(xhr);
    	console.log(error);

    	this.getFlux().actions.flash.add({
            messageId: 'formErrors',
            type: 'error',
            message: l20n.ctx.getSync('error')
        });
    },
});

Detail.Section = React.createClass({

    propTypes: {
        labelKey: React.PropTypes.string, // label string key
    	label: React.PropTypes.string, // label string

        tooltipKey: React.PropTypes.string, // tool tip string key
    	tooltip: React.PropTypes.string, // tool tip string

        className: React.PropTypes.string // selection button class name
    },

    // we do not use this render function because we use this component only as section descriptor
    render: function() {
    	return null;
    }
});

module.exports = Detail;
