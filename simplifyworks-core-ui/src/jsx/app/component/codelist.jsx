var AbstractFormComponent = require('./abstract-form-component.jsx');

var CodeList = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.object, //Full rest object
        restDetail: React.PropTypes.string,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string.isRequired,
        niceLabelFunc: React.PropTypes.func.isRequired, //Function for create niceLabel from object
        filter: React.PropTypes.string, //Parameters for filter records
        autoload: React.PropTypes.bool, //Tries to get all possible values and if there is only one result then fills it as value
								changeListener: React.PropTypes.func
    },

    // Whenever the input changes we update the value state
    // of this component
    setNiceLabel: function (event) {
        this.setState({
            niceLabel: event.currentTarget.value
            // When the value changes, wait for it to propagate and
            // then validate the input
        }, function () {
            this.props.validate(this);
        }.bind(this));
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true

        })

        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }
    },

    selectItem: function (id) {
        console.log('selectItemCodeList: ' + id);
        this.loadEntity(id, true);
    },

    componentWillUnmount: function () {
        console.log('componentWillUnmount: ' + this.props.name);
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    getDefaultProps: function () {
        return {
            width: '150px'
        };
    },


    loadEntity: function (id, closeModal) {
        $.ajax({
            url: (this.props.restDetail ? this.props.restDetail : this.props.rest) + '/' + id,
            dataType: 'json',
            success: function (data) {
                this.setState({
                    value: data.resource,
                    idItem: id,
                    niceLabel: this.props.niceLabelFunc(data.resource)
                }, function () {
                    if (this.props.validate) {
                        this.props.validate(this);
                    }
                    if (closeModal) {
                        ModalDialogManager.remove(this.props.name);
                    }
                    //if we have change callback function than we call it
                    if (this.props.changeListener) {
                        this.props.changeListener(this);
                    }
                }.bind(this));
            }.bind(this)
        });

    },

    autoloadIfOne: function() {
        $.ajax({
            url: this.props.filter ? this.props.rest+'?filter='+this.props.filter:this.props.rest,
            dataType: 'json',
            success: function (data) {
                console.log("autoload ",data );
                if (data.recordsFiltered == 1){
                    var resource = data.resources[0].resource;
                    this.setState({
                        value: resource,
                        idItem: resource.id,
                        niceLabel: this.props.niceLabelFunc(resource)
                    }, function () {
                        this.props.validate(this);
                        //if we have change callback function than we call it
                        if (this.props.changeListener) {
                            this.props.changeListener(this)
                        }
                    }.bind(this));
                }
            }.bind(this)
        });
    },

    getInitialState: function () {

        if (!this.props.value && this.props.autoload) {
            this.autoloadIfOne();
        }

        if (this.props.value && !this.props.value.id) {
            this.loadEntity(this.props.value, false);
        }

        return {
            niceLabel: this.props.niceLabelFunc(this.props.value)
        };
    },

    componentWillReceiveProps: function(nextProps) {
    	this.setState({
    		niceLabel: this.props.niceLabelFunc(this.props.value)
    	});
    },

    clear: function () {
        this.showValidations();
        this.setState({
            value: null,
            idItem: null,
            niceLabel: null
        }, function () {
            this.props.validate(this);
            //if we have change callback function than we call it
            if (this.props.changeListener) {
                this.props.changeListener(this)
            }
        }.bind(this));

    },

    getModalDialog: function () {
        return (
            <ModalDialog lg modalId={this.props.name}>
                <ModalDialogBody>
                    <BasicTable id={this.props.name + 'table'}
                                rest={this.props.filter ? this.props.rest+'?filter='+this.props.filter:this.props.rest}
                                paging={true}
                                searching={true}
                                selectItem={this.selectItem}
                                disabled={false}
                                detail={this.props.detail}
                                showNewButton={false}
                                flux={this.props.flux}>
                        {this.props.children}
                    </BasicTable>

                </ModalDialogBody>
                <ModalDialogFooter>
                    <Button outlined bsStyle='danger' onClick={ModalDialogManager.remove.bind(this, this.props.name)}
                            onTouchEnd={ModalDialogManager.remove.bind(this, this.props.name)}>Close</Button>
                </ModalDialogFooter>
            </ModalDialog>
        );
    },

    render: function () {
        var value = this.state.value;

        var style = {
            display: 'inline',
            width: this.props.width
        }

        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }

        var component = <div>

            <InputGroup style={style} className='codelist-control'>
                <Input type='text' id={this.props.name}
                       onChange={this.setNiceLabel}
                       style={style}
                       sm={this.props.sm}
                       lg={this.props.lg}
                       xs={this.props.xs}
                       placeholder={this.props.placeholder}
                       className={this.props.className}
                    //We not have whisperer yet ... input is permanent disabled
                       disabled={true}
                       ref={this.props.name}
                       value={this.state.niceLabel}/>


                <InputGroupButton style={style}>
                    <Button outlined
                            sm={this.props.sm}
                            lg={this.props.lg}
                            xs={this.props.xs}
                            bsStyle='primary'
                            onClick={ModalDialogManager.create.bind(this, this.getModalDialog, this.props.name)}
                            disabled={this.state.disabled}
                            onTouchEnd={ModalDialogManager.create.bind(this, this.getModalDialog, this.props.name)}>
                        <Icon bundle='stroke-gap-icons' glyph='List'/>
                    </Button>
                    {this.state.value ?
                        <Button outlined
                                sm={this.props.sm}
                                lg={this.props.lg}
                                xs={this.props.xs}
                                bsStyle='danger'
                                onClick={this.clear}
                                disabled={this.state.disabled}>
                            <Icon bundle='outlined' glyph='delete'/>
                        </Button> : null}


                </InputGroupButton>
            </InputGroup>
        </div>

        return this.formGroup(component, className);
    }
});

module.exports = CodeList;
