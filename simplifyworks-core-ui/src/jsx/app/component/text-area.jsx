var AbstractFormComponent = require('./abstract-form-component.jsx');

var TextArea = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        //Input Name
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.string,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string.isRequired
    },


    // Whenever the input changes we update the value state
    // of this component
    setValue: function (event) {
        this.showValidations();
        this.setState({
            value: event.currentTarget.value
            // When the value changes, wait for it to propagate and
            // then validate the input
        }, function () {          
            if (this.props.validate) {
                this.props.validate(this);
            }
        }.bind(this));
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true
        })

        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }
    },

    componentWillUnmount: function () {
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    render: function () {

        var value = this.state.value;
        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }

        var component = <Textarea id={this.props.name}
                                  onChange={this.setValue}
                                  placeholder={this.state.disabled?'':this.props.placeholder}
                                  className={this.props.className}
                                  ref={this.props.name}
                                  disabled={this.state.disabled}
                                  rows={this.props.rows ? this.props.rows : '3'}
                                  value={this.state.value ? this.state.value :"" }/> //if put to Textarea null than value is not change. I have to put empty string!

        return this.formGroup(component, className);
    }
});
module.exports = TextArea;
