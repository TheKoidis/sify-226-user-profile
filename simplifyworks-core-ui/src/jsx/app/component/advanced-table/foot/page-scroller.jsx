/******************************************************************************************************************************************************
 *
 * Represents component which is responsible for changing pages.
 *
 ******************************************************************************************************************************************************/
PageScroller = React.createClass({

	propTypes: {
		recordsFiltered: React.PropTypes.number.isRequired, // number of filtered records in last response

		pageNumber: React.PropTypes.number.isRequired, // page number (starts with 1)
		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)

		changePageNumberFunc: React.PropTypes.func.isRequired // function for changing page number
	},

	// returns initial state for input
	getInitialState: function() {
		return {
			pageNumber: this.props.pageNumber
		}
	},

	// renders change page buttons and input for direct page selection
	render: function() {
		var pageCount = Math.floor(this.props.recordsFiltered / this.props.pageSize) + (this.props.recordsFiltered % this.props.pageSize > 0 ? 1 : 0);

		return (
			<div>
				{pageCount > 1
					?	<Grid>
							<Row>
								<Col xs={12} md={8}>
									<ButtonGroup sm>
										{this.createChangePageButtons()}
									</ButtonGroup>
								</Col>

								<Col md={4} hidden='xs, sm' visible='sm, md, lg'>
									<Grid collapse>
										<Row>
											<Col xs={3}>
												<div style={{'marginRight' : '12.5px', 'marginTop': '2px'}}>
													{l20n.ctx.getSync('advancedTable_page_prefix')}
												</div>
											</Col>
											<Col xs={5}>
												<Input sm type='text' value={this.props.pageNumber} onChange={this.onChange} style={{'width': '100%'}}/>
											</Col>
											<Col xs={4}>
												<div style={{'marginLeft' : '12.5px', 'marginTop': '2px'}}>
													{l20n.ctx.getSync('advancedTable_page_suffix', {count: pageCount})}
												</div>
											</Col>
										</Row>
									</Grid>
								</Col>
							</Row>
						</Grid>
					:	null
				}
			</div>
		);
	},

	// creates change page buttons using properties
	createChangePageButtons: function() {
		var changePageButtons = [];
		var pageCount = Math.floor(this.props.recordsFiltered / this.props.pageSize) + (this.props.recordsFiltered % this.props.pageSize > 0 ? 1 : 0);

		changePageButtons.push(this.createChangePageButton('<<', 1, false, this.props.pageNumber > 1));
		changePageButtons.push(this.createChangePageButton('<', this.props.pageNumber - 1, false, this.props.pageNumber > 1));

		var minPageNumber = Math.max(Math.min(Math.max(this.props.pageNumber - 4, 1), pageCount - 9), 1);
		var maxPageNumber = Math.min(Math.max(Math.min(this.props.pageNumber + 5, pageCount), 10), pageCount);

		for(var pageNumber = minPageNumber; pageNumber <= maxPageNumber; pageNumber++) {
			changePageButtons.push(this.createChangePageButton(pageNumber, pageNumber, pageNumber === this.props.pageNumber, true));
		}

		changePageButtons.push(this.createChangePageButton('>', this.props.pageNumber + 1, false, this.props.pageNumber < pageCount));
		changePageButtons.push(this.createChangePageButton('>>', pageCount, false, this.props.pageNumber < pageCount));

		return changePageButtons;
	},

	// creates single change page button
	createChangePageButton: function(key, pageNumber, active, enabled) {
		return (
			<Button xs key={key} onClick={this.props.changePageNumberFunc.bind(null, pageNumber)} bsStyle={active ? 'darkblue' : 'grey'} disabled={!enabled}>
				{key}
			</Button>
		);
	},

	// listener for input
	onChange: function(event) {
		this.setState({
			pageNumber: event.target.value
		}, this.props.changePageNumberFunc.bind(null, event.target.value));
	}
});

module.exports = PageScroller;
