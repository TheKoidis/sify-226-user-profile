/******************************************************************************************************************************************************
 * 
 * Represents component which is responsible for displaying page statistics.
 * 
 ******************************************************************************************************************************************************/
AdvancedTablePageStatistics = React.createClass({
	
	propTypes: {
		recordsFiltered: React.PropTypes.number.isRequired, // count of filtered entries
		recordsReturned: React.PropTypes.number.isRequired, // count of returned entries
		recordsTotal: React.PropTypes.number.isRequired, // total count of entries
		
		pageNumber: React.PropTypes.number.isRequired, // page number (starts with 1)
		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)
	},

	// renders statistics as plain text
	render: function() {
		var first = this.getFirstVisibleRecordOrder();
		var last = this.getLastVisibleRecordOrder();
		var total = this.props.recordsTotal;
		var filtered = this.props.recordsFiltered;
		
		var statistics;
		
		if(filtered < total) {
			statistics = l20n.ctx.getSync('advancedTable_page_statistics_with_filtered', {first: first, last: last, total: total, filtered: filtered});
		} else {
			statistics = l20n.ctx.getSync('advancedTable_page_statistics_without_filtered', {first: first, last: last, total: total});
		}
		
		return (
			<div style={{'marginTop': '2px'}}>
				{statistics}							
			</div>
		);		
	},
	
	// returns number of first visible record
	getFirstVisibleRecordOrder: function() {
		return this.props.recordsFiltered > 0 ? ((this.props.pageNumber - 1) * this.props.pageSize + 1) : 0;
	},
	
	// returns number of last visible record
	getLastVisibleRecordOrder: function() {
		return this.props.recordsFiltered > 0 ? (this.getFirstVisibleRecordOrder() + this.props.recordsReturned - 1) : 0;
	}	
});

module.exports = AdvancedTablePageStatistics;