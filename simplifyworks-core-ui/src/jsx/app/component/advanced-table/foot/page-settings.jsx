/******************************************************************************************************************************************************
 *
 * Represents component which is responsible for page settings.
 *
 ******************************************************************************************************************************************************/
AdvancedTablePageSettings = React.createClass({

	statics: {
		pageSizes: [10, 25, 50, 100]
	},

	propTypes: {
		recordsFiltered: React.PropTypes.number.isRequired, // number of filtered records in last response

		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)
		changePageSizeFunc: React.PropTypes.func.isRequired // function for changing page size
	},

	// renders label and select for page size
	render: function() {
		return (
			<div style={{float: 'right'}}>
				{this.props.recordsFiltered > AdvancedTablePageSettings.pageSizes[0]
					?	<ButtonGroup dropup>
							<Dropdown>
								<DropdownButton sm container={this} menu='optionsMenu'>
									{l20n.ctx.getSync('advancedTable_entries_per_page', {pageSize: this.props.pageSize})}
								</DropdownButton>

								<Menu ref='optionsMenu' onItemSelect={this.onItemSelect}>
									{this.createOptions()}
								</Menu>
							</Dropdown>
						</ButtonGroup>
					:	null
				}
			</div>
		);
	},

	// creates options for page size
	createOptions: function() {
		var options = [];

		for(var index = 0; index < AdvancedTablePageSettings.pageSizes.length; index++) {
			options.push(
				<MenuItem
					key={'page-size' + index}
					keyaction={AdvancedTablePageSettings.pageSizes[index]}
					href='#'
					active={this.props.pageSize === AdvancedTablePageSettings.pageSizes[index]}
					title={AdvancedTablePageSettings.pageSizes[index]}>

					{AdvancedTablePageSettings.pageSizes[index]}
				</MenuItem>
			);
		}

		return options;
	},

	// listener for select
	onItemSelect: function(event) {
		this.props.changePageSizeFunc(event.keyaction);
	}
});

module.exports = AdvancedTablePageSettings;
