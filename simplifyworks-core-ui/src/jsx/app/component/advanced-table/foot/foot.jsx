var AdvancedTableRecordsSelectionSummary = require('./records-selection-summary.jsx');
var AdvancedTableRecordsActions = require('./records-actions.jsx');

var AdvancedTablePageStatistics = require('./page-statistics.jsx');
var AdvancedTablePageScroller = require('./page-scroller.jsx');
var AdvancedTablePageSettings = require('./page-settings.jsx');

/******************************************************************************************************************************************************
 *
 * Represents foot of the table (shows PageScroller, PageStatistics and PageSettings).
 *
 ******************************************************************************************************************************************************/
AdvancedTableFoot = React.createClass({

	propTypes: {
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		searchFunc: React.PropTypes.func, // function responsible for searching

		selectedRecords: React.PropTypes.arrayOf(React.PropTypes.object).isRequired, // selected records
		records: React.PropTypes.arrayOf(React.PropTypes.object), // array of records
		actions: React.PropTypes.arrayOf(React.PropTypes.object), // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')

		columns: React.PropTypes.array, // columns definition
		columnsCount: React.PropTypes.number.isRequired, // count of columns

		recordsReturned: React.PropTypes.number.isRequired, // number of records returned in last response
		recordsTotal: React.PropTypes.number.isRequired, // total number of records
		recordsFiltered: React.PropTypes.number.isRequired, // number of filtered records in last response

		disabledPaging: React.PropTypes.bool.isRequired, // disabled paging flag
		disabledFooter: React.PropTypes.bool.isRequired, //disabled table footer

		pageNumber: React.PropTypes.number.isRequired, // page number
		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)

		changePageNumberFunc: React.PropTypes.func.isRequired, // function for changing page number
		changePageSizeFunc: React.PropTypes.func.isRequired // function for changing page size
	},

	// renders foot with paging components
	render: function() {
		var existActions = this.props.actions !== undefined;
		var existColumnSummaries = this.existColumnSummaries();

		return (
			<tfoot>
				{((existActions && this.props.selectedRecords.length > 0) || existColumnSummaries)
					? <tr>
						{(existActions && this.props.selectedRecords.length > 0)
							? <td>
								<AdvancedTableRecordsSelectionSummary
									selectedRecordsCount={this.props.selectedRecords.length} />
							  </td>
							: null
						}

						{this.createColumnSummaries()}

						{(existActions && this.props.selectedRecords.length > 0)
							? <td>
								<AdvancedTableRecordsActions
									rest={this.props.rest}
									searchFunc={this.props.searchFunc}
									selectedRecords={this.props.selectedRecords}
									actions={this.props.actions} />
							  </td>
							: null
						}
					  </tr>
					  : null
				}
				{!this.props.disabledFooter ?
				<tr>
					<td colSpan={this.props.columnsCount}>
						<Grid>
							<Row>
								<Col xs={6} md={3}>
									<AdvancedTablePageStatistics
										recordsReturned={this.props.recordsReturned}
										recordsTotal={this.props.recordsTotal}
										recordsFiltered={this.props.recordsFiltered}
										pageNumber={this.props.pageNumber}
										pageSize={this.props.pageSize} />
								</Col>

								{this.props.disabledPaging === false
									?	<Col xs={6} md={7}>
											<AdvancedTablePageScroller
												recordsFiltered={this.props.recordsFiltered}
												pageNumber={this.props.pageNumber}
												pageSize={this.props.pageSize}
												changePageNumberFunc={this.props.changePageNumberFunc} />
										</Col>
									:	null
								}

								{this.props.disabledPaging === false
									?	<Col md={2} hidden='xs, sm' visible='md, lg'>
											<AdvancedTablePageSettings
												recordsFiltered={this.props.recordsFiltered}
												pageSize={this.props.pageSize}
												changePageSizeFunc={this.props.changePageSizeFunc} />
										</Col>
								:	null
								}
							</Row>
						</Grid>
					</td>
				</tr>
				: null}
			</tfoot>
		);
	},

	existColumnSummaries: function() {
		for(var index = 0; index < this.props.columns.length; index++) {
			if(this.props.columns[index].summaryRenderFunc) {
				return true;
			}
		}

		return false;
	},

	createColumnSummaries: function() {
		if(this.existColumnSummaries()) {
			var columnSummaries = [];

			for(var index = 0; index < this.props.columns.length; index++) {
				if(this.props.columns[index].summaryRenderFunc) {
					columnSummaries.push(
							<td key={'column-summary-' + index}>
							{this.props.columns[index].summaryRenderFunc(this.props.selectedRecords, this.props.records)}
							</td>
					);
				} else {
					columnSummaries.push(
							<td key={'column-summary-' + index}/>
					);
				}
			}

			return columnSummaries;
		} else {
			return (
				<td colSpan={this.props.columns.length}/>
			);
		}
	}
});

module.exports = AdvancedTableFoot;
