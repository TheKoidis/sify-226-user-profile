/******************************************************************************************************************************************************
 *
 * Component which renders simple text containing number of selected records.
 *
 ******************************************************************************************************************************************************/
AdvancedTableRecordsSelectionSummary = React.createClass({

	propTypes: {
		selectedRecordsCount: React.PropTypes.number.isRequired, // selected records count
	},

	// renders text
	render: function() {
		return this.props.selectedRecordsCount !== 0
			? 	<span>
					{l20n.ctx.getSync('advancedTable_records_selection_summary', {count: this.props.selectedRecordsCount})}
				</span>
			: 	null;
	}
});

module.exports = AdvancedTableRecordsSelectionSummary;
