/******************************************************************************************************************************************************
 *
 * Component which renders action buttons for multiple selected records.
 *
 ******************************************************************************************************************************************************/
AdvancedTableRecordsActions = React.createClass({

	propTypes: {
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		searchFunc: React.PropTypes.func.isRequired, // function responsible for searching
		selectedRecords: React.PropTypes.arrayOf(React.PropTypes.object).isRequired, // selected records
		actions: React.PropTypes.arrayOf(React.PropTypes.object) // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')
	},

	// renders action buttons or some text when no actions are available for current record selection
	render: function() {
		var actionButtons = this.createActionButtons();

		return (
			<span>
				{actionButtons.length === 0
					?	null
					:	actionButtons
				}
			</span>
		);
	},

	// creates action buttons
	createActionButtons: function() {
		var buttons = [];
		var longLabel = true; //true - render icon and label, false - render only icon
		if(this.props.actions) {
			for(var index = 0; index < this.props.actions.length; index++) {
				var action = this.props.actions[index];
				if(action.isEnabled(this.props.rest, this.props.selectedRecords, 'multiple')) {
					buttons.push(
						<Button sm key={index}
										bsStyle={action.getStyle?action.getStyle():'default'}
										title={l20n.ctx.getSync(action.title)}
										onClick={action.execute.bind(action, this.props.rest, this.props.selectedRecords, this.props.searchFunc)}>
							{action.render(longLabel)}
						</Button>
					);
				}
			}
		}

		return buttons;
	}
});

module.exports = AdvancedTableRecordsActions;
