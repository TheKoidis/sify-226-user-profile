var AdvancedTableOptions = require('./options.jsx');
var AdvancedTableColumnHead = require('./column/column-head.jsx');
var AdvancedTableRecordSelectionOptions = require('./record-selection-options.jsx');

/******************************************************************************************************************************************************
 *
 * Component which represents head of table.
 *
 ******************************************************************************************************************************************************/
AdvancedTableHead = React.createClass({

	propTypes: {
		tableId: React.PropTypes.string, // table id

		searchFunc: React.PropTypes.func, // function responsible for searching
		searchWithCustomFilterFunc: React.PropTypes.func, // function responsible for searching with custom filter
		columnsCount: React.PropTypes.number, // total count of columns (may be different from columns.length)

		rest: React.PropTypes.string, // REST URL for getting data

	    createNewDetail: React.PropTypes.string, // URL of detail of new object (primarily used if defined instead of following)
	    createNewDetailModalFunc: React.PropTypes.func, // function responsible for creating modal detail of new object

	    customFilterFunc: React.PropTypes.func, // function responsible for rendering of custom filter
	    customNamedFilters: React.PropTypes.arrayOf(React.PropTypes.object), // custom named filters (object with attributes name and filter)
			defaultCustomNamedFilter: React.PropTypes.number, // index of default active filter
		columns: React.PropTypes.array, // columns definition

		columnFilterGroupConjunction: React.PropTypes.string, // conjunction in column filter group
	    changeColumnFilterGroupConjunctionFunc: React.PropTypes.func, // function responsible for changing conjunction
		disabledColumnFilterGroupConjunction: React.PropTypes.bool, // show/hide columns filter conjunction button

		ordering: React.PropTypes.array, // ordering state
		changeOrderingFunc: React.PropTypes.func, // function for changing ordering
		disabledOrdering: React.PropTypes.bool, // disabled ordering flag
    disabledSearching: React.PropTypes.bool, // disabled searching flag
		disabledSearchingOptions: React.PropTypes.bool, // disabled button with filter options flag
		disabledHeader: React.PropTypes.bool, //disabled table header

		globalActions: React.PropTypes.arrayOf(React.PropTypes.object), // global actions (object with functions 'render()' and 'execute(rest, searchCallback)')

		updateFilteringFunc: React.PropTypes.func, // function responsible for update of filter
		showActionsHeaders: React.PropTypes.bool, // show actions related header (action column)
		showSelectionHeader: React.PropTypes.bool, //show selection header

		selectPageFunc: React.PropTypes.func, // function responsible for adding whole page of records to current selection
        deselectAllFunc: React.PropTypes.func, // function responsible for removing all records from current selection

        recordChosedFunc: React.PropTypes.func, // function responsible for choosing record

        labelKeyPrefix: React.PropTypes.string // prefix of label key string (it is used together with property when labelKey attribute is not present for column)
	},

	// returns initial state
	getInitialState: function() {
		var columnFilterTypes = [];

		for(var index = 0; index < this.props.columns.length; index++) {
			columnFilterTypes.push({
				property: this.props.columns[index].property,
				filterType: this.getInitialFilterType(this.props.columns[index])
			});
		}

		return {
			columnFilterTypes: columnFilterTypes
		};
	},

	// sets initial state
	componentWillReceiveProps: function(nextProps) {
		var columnFilterTypes = [];
		var change = false;

		for(var index = 0; index < nextProps.columns.length; index++) {
			if(this.props.columns.length !== nextProps.columns.length || this.props.columns[index].property !== nextProps.columns[index].property) {
				change = true;
				break;
			}
		}

		if(change) {
			for(var index = 0; index < nextProps.columns.length; index++) {
				columnFilterTypes.push({
					property: nextProps.columns[index].property,
					filterType: this.getInitialFilterType(nextProps.columns[index])
				});
			}

			this.setState({
				columnFilterTypes: columnFilterTypes
			});
		}
	},

	// returns initial filter type
	getInitialFilterType: function(column) {
		if(this.props.disabledSearching === false && column.searchable) {
			var filterType = column.filterType;

			if(filterType == null) {
				return this.getDefaultFilterType(column.face);
			} else if(filterType === 'none') {
				return null;
			} else {
				return column.filterType;
			}
		} else {
			return null;
		}
	},

	// returns default filter type if not defined by column
    getDefaultFilterType: function(face) {
    	switch(face) {
    		case 'date':
    		case 'datetime':
    		case 'time':
    		case 'decimal':
    			return 'range';
    		case 'enum':
    		case 'integer':
    			return 'eq';
    		case 'string':
    			return 'like';
    		default:
    			return null;
    	}
    },

	// renders table options and column heads&filters
	render: function() {
		var columnHeads = this.createColumnHeads();
		var columnFilters = this.createColumnFilters();
    	return (
    		<thead>
					{!this.props.disabledHeader ?
    			<tr>
    				<th colSpan={this.props.columnsCount}>
	    				<AdvancedTableOptions
	    					ref='options'
	    				 	tableId={this.props.tableId}
	    				 	searchFunc={this.props.searchFunc}
	    					searchWithCustomFilterFunc={this.props.searchWithCustomFilterFunc}
	    				 	rest={this.props.rest}
								labelKeyPrefix={this.props.labelKeyPrefix}
	    			    createNewDetail={this.props.createNewDetail}
	    			    createNewDetailModalFunc={this.props.createNewDetailModalFunc}
	    					customFilterFunc={this.props.customFilterFunc}
	    					customNamedFilters={this.props.customNamedFilters}
								defaultCustomNamedFilter={this.props.defaultCustomNamedFilter}
	    					globalActions={this.props.globalActions}
								columnFilterTypes={this.state.columnFilterTypes}
								disabledColumnFilterGroupConjunction={this.props.disabledColumnFilterGroupConjunction}
	    					columnFilterGroupConjunction={this.props.columnFilterGroupConjunction}
	    			    changeColumnFilterGroupConjunctionFunc={this.props.changeColumnFilterGroupConjunctionFunc} />
    				</th>
    			</tr>
					: null}

    			<tr>
    				{columnHeads}
    			</tr>

    			{columnFilters
    				?	<tr>
    						{columnFilters}
    					</tr>
    				:	null
    			}
    		</thead>
    	);
    },

    // creates heads for all visible columns
    createColumnHeads: function() {
    	var columnHeads = [];
    	if(this.props.recordChosedFunc) {
    		columnHeads.push(<th key='empty-choosing-head' style={{width: '2.5%'}} />);
    	}

    	if(this.props.showSelectionHeader) {
			columnHeads.push(
				<AdvancedTableRecordSelectionOptions
					key='record-selection-options'
					selectPageFunc={this.props.selectPageFunc}
					deselectAllFunc={this.props.deselectAllFunc} />
			);
    	}

    	for(var index = 0; index < this.props.columns.length; index++) {
    		var ordering = this.getOrdering(index);

    		columnHeads.push(
    			<AdvancedTableColumnHead
    				key={'head-' + index}
    				column={this.props.columns[index]}
    				labelKeyPrefix={this.props.labelKeyPrefix}
    				order={ordering ? ordering.order : null}
    				nullsOrder={ordering ? ordering.nullsOrder : null}
    				changeOrderingFunc={this.props.changeOrderingFunc}
    				changeFilterTypeFunc={this.changeFilterType}
    				updateFilteringFunc={this.props.updateFilteringFunc}
						disabledSearching={this.props.disabledSearching}
            disabledOrdering={this.props.disabledOrdering}
						disabledSearchingOptions={this.props.disabledSearchingOptions}/>
    		);
    	}

    	if(this.props.showActionsHeaders) {
    		columnHeads.push(<th key='empty-actions-head' style={{width: '40px'}} />);
    	}

    	return columnHeads;
    },

    // creates filters for all searchable columns
    createColumnFilters: function() {
    	var columnFilters = [];
    	var empty = true;

    	if(this.props.recordChosedFunc) {
    		columnFilters.push(<th key='empty-choosing-filter'/>);
    	}

    	if(this.props.showSelectionHeader) {
    		columnFilters.push(<th key='empty-selection-head'/>);
    	}

    	for(var index = 0; index < this.props.columns.length; index++) {
    		if(this.props.columns[index].searchable) {
    			columnFilters.push(
	    			<AdvancedTableColumnFilter
	    				key={'filter-' + index}
						column={this.props.columns[index]}
						filterType={this.state.columnFilterTypes[index].filterType}
						updateFilteringFunc={this.props.updateFilteringFunc} />
    			)

    			if(this.state.columnFilterTypes[index].filterType) {
    				empty = false;
    			}
    		} else {
    			columnFilters.push(
    				<th key={'filter-' + index} />
    			);
    		}
    	}

    	if(this.props.showActionsHeaders) {
    		columnFilters.push(<th key='empty-actions-filter'/>);
    	}

    	return empty ? null : columnFilters;
    },

    // returns ordering for specified column
    getOrdering: function(column) {
    	for(var index = 0; index < this.props.ordering.length; index++) {
    		if(this.props.ordering[index].property === this.props.columns[column].property) {
    			return this.props.ordering[index];
    		}
    	}

    	return null;
    },

    // changes type of filter for specified property
    changeFilterType: function(property, filterType) {
    	var newColumnFilterTypes = [];

    	for(var index = 0; index < this.state.columnFilterTypes.length; index++) {
    		if(this.state.columnFilterTypes[index].property === property) {
    			newColumnFilterTypes.push({
    				property: property,
    				filterType: filterType
    			});
    		} else {
    			newColumnFilterTypes.push({
    				property: this.state.columnFilterTypes[index].property,
    				filterType: this.state.columnFilterTypes[index].filterType
    			});
    		}
    	}

    	this.setState({
    		columnFilterTypes: newColumnFilterTypes
    	}, this.props.updateFilteringFunc.bind(this, property, filterType, []));
    }
});

module.exports = AdvancedTableHead;
