var AdvancedTableColumnFilterEmptyValueComponent = require('./column-filter-empty-value-component.jsx');
var AdvancedTableColumnFilterOneValueComponent = require('./column-filter-one-value-component.jsx');
var AdvancedTableColumnFilterTwoValuesComponent = require('./column-filter-two-values-component.jsx');

/******************************************************************************************************************************************************
 *
 * Represents single filter cell which is responsible for filtering per column.
 *
 ******************************************************************************************************************************************************/
AdvancedTableColumnFilter = React.createClass({

	statics: {
		// common options for column filters
		options: [
		 {filterType: null, faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: null},
		 {filterType: 'like', faces: ['string'], filterOperator: 'LIKE'},
		 {filterType: 'mask', faces: ['string'], filterOperator: 'LIKE_MASK'},

		 {filterType: 'eq', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'EQUALS'},
		 {filterType: 'ne', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'NOT_EQUALS'},

		 {filterType: 'lt', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'LESS'},
		 {filterType: 'le', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'LESS_EQUALS'},

		 {filterType: 'gt', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'GREATER'},
		 {filterType: 'ge', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'GREATER_EQUALS'},

		 {filterType: 'range', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'INTERVAL'},

		 {filterType: 'null', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'IS_NULL'},
		 {filterType: 'notnull', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'IS_NOT_NULL'},
		],

		// returns description for specified filterType
		getDescription: function(filterType) {
			return l20n.ctx.getSync('advancedTable_filter_type_' + (filterType ? filterType : 'no_filter'));
		},

		// returns all options which are available for specified column face
		getOptions: function(face) {
			var options = [];

			for(var index = 0; index < AdvancedTableColumnFilter.options.length; index++) {
				if(AdvancedTableColumnFilter.options[index].faces.indexOf(face) != -1) {
					options.push(AdvancedTableColumnFilter.options[index]);
				}
			}

			return options;
		},

		// returns operator type for specified filter type (translation to FilterGroup)
		getFilterOperatorType: function(filterType) {
	    	for(var index = 0; index < AdvancedTableColumnFilter.options.length; index++) {
	    		if(AdvancedTableColumnFilter.options[index].filterType === filterType) {
	    			return AdvancedTableColumnFilter.options[index].filterOperator;
	    		}
	    	}

	    	return null;
	    }
	},

	propTypes: {
		column: React.PropTypes.object, // column definition

		filterType: React.PropTypes.string, // current column filter type
		updateFilteringFunc: React.PropTypes.func // function responsible for update of filter
	},

	// renders filter component according to the filter type
	render: function() {
		switch(this.props.filterType) {
			case 'null':
			case 'notnull':
				return (
					<th style={{verticalAlign: 'top'}}>
						<AdvancedTableColumnFilterEmptyValueComponent
							column={this.props.column}
							filterType={this.props.filterType}
							updateFilteringFunc={this.props.updateFilteringFunc} />
					</th>
				);
			case 'like':
			case 'mask':
			case 'eq':
			case 'ne':
			case 'lt':
			case 'le':
			case 'gt':
			case 'ge':
				return (
					<th style={{verticalAlign: 'top'}}>
						<AdvancedTableColumnFilterOneValueComponent
							column={this.props.column}
							filterType={this.props.filterType}
							updateFilteringFunc={this.props.updateFilteringFunc} />
					</th>
				);
			case 'range':
				return (
					<th style={{verticalAlign: 'top'}}>
						<AdvancedTableColumnFilterTwoValuesComponent
							column={this.props.column}
							filterType={this.props.filterType}
							updateFilteringFunc={this.props.updateFilteringFunc} />
					</th>
				);
			default:
				return (
					<th/>
				);
		}
	}
});

module.exports = AdvancedTableColumnFilter;
