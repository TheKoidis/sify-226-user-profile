var AdvancedTableColumnHead = require('./column-head.jsx');

/******************************************************************************************************************************************************
 * 
 * Represents filter component for filters which do not need any values (like IS_NULL and IS_NOT_NULL).
 * 
 ******************************************************************************************************************************************************/
AdvancedTableColumnFilterEmptyValueComponent = React.createClass({
	
	propTypes: {
		column: React.PropTypes.object, // column definition
		
		filterType: React.PropTypes.string, // current column filter type
		updateFilteringFunc: React.PropTypes.func // function responsible for update of filter
	},
	
	componentWillMount: function() {
		this.props.updateFilteringFunc(this.props.column.property, this.props.filterType, []);
	},
	
	render: function() {
		switch(this.props.column.face) {
			case 'integer':
			case 'decimal':
			case 'string':
			case 'boolean':
			case 'enum':
			case 'date':
			case 'datetime':
			case 'time':
				return (
					<Grid>
						<Row>
							<Col Col sm={12} md={12} lg={12}>
								{AdvancedTableColumnFilter.getDescription(this.props.filterType)}
							</Col>
						</Row>
					</Grid>	
				);
			default:
				return null;
		}
	},
});

module.exports = AdvancedTableColumnFilterEmptyValueComponent;