/******************************************************************************************************************************************************
 *
 * Represents column head cell which is responsible for displaying column name, ordering and conditionally for filter type selection.
 *
 ******************************************************************************************************************************************************/
AdvancedTableColumnHead = React.createClass({

	statics: {
		commonProperties: ['id', 'creator', 'created', 'modifier', 'modified', 'version']
	},

	propTypes: {
		column: React.PropTypes.object, // column definition
		labelKeyPrefix: React.PropTypes.string, // prefix of label key string (it is used together with property when labelKey attribute is not present for column)

		order: React.PropTypes.string, // order
		nullsOrder: React.PropTypes.string, // nulls order (currently not used - not supported by server)
		changeOrderingFunc: React.PropTypes.func, // function for changing ordering
		changeFilterTypeFunc: React.PropTypes.func, // function responsible for changing type of filter

		disabledOrdering: React.PropTypes.bool, // disabled ordering flag
    disabledSearching: React.PropTypes.bool, // disabled searching flag
		disabledSearchingOptions: React.PropTypes.bool // disabled button with filter options flag
	},

	// renders button with column name and ordering support and conditionally also filter type dropdown
	render: function() {
		if(this.props.column.face === 'detail') {
			return (
				<th style={{width: this.props.column.width}} />
			);
		} else {
			return (
				<th style={{width: this.props.column.width, verticalAlign: 'top'}}>
					<div>
						<Grid collapse>
							<Row>
								<Col xs={10}>
									{this.props.disabledOrdering === false && this.props.column.orderable === true
										?	<Label title={this.props.column.tooltip} style={{'cursor': 'pointer', 'verticalAlign': 'text-bottom'}}
												onClick={this.props.changeOrderingFunc.bind(null, this.props.column.property, this.getNextOrder(), this.getNextNullsOrder())}>

												<Icon glyph={this.getGlyph()} />
												{' '}{this.getLabel()}
											</Label>
										:	<Label title={this.props.column.tooltip} style={{'verticalAlign': 'text-bottom'}}>
												{this.getLabel()}
											</Label>
									}
								</Col>

								<Col xs={2}>

									{this.props.disabledSearchingOptions === false && this.props.disabledSearching === false && this.props.column.searchable === true && this.props.column.face !== 'actions'
										?	<div style={{float: 'right'}}>
												<ButtonGroup>
													<Dropdown>
														<DropdownButton xs container={this} menu='optionsMenu'>
															<Icon glyph='icon-fontello-menu' />
														</DropdownButton>

														<Menu ref='optionsMenu' onItemSelect={this.onItemSelect}>
															{this.createFilterTypeMenuItems()}
														</Menu>
													</Dropdown>
												</ButtonGroup>
											</div>
										: 	null
									}
								</Col>
							</Row>
						</Grid>
					</div>
				</th>
			);
		}
	},

	// returns glyph according to the current order and nulls order
	getGlyph: function() {
		switch(this.props.order ? this.props.order.toUpperCase() : null) {
			case 'ASC':
				return 'icon-fontello-sort-up';
			case 'DESC':
				return 'icon-fontello-sort-down';
			default:
				return 'icon-fontello-sort';
		}
	},

	// returns label for column
	getLabel: function() {
		if(this.props.column.label) {
			return this.props.column.label;
		} else if(this.props.column.labelKey) {
			return l20n.ctx.getSync(this.props.column.labelKey);
		} else if(this.props.labelKeyPrefix) {
			if(AdvancedTableColumnHead.commonProperties.indexOf(this.props.column.property) !== -1) {
				return l20n.ctx.getSync(this.props.column.property);
			} else {
				return l20n.ctx.getSync((this.props.labelKeyPrefix + '.' + this.props.column.property).replace(/[.]/g, '_'));
			}
		} else {
			console.warn('Missing label for column \'' + this.props.column.property + '\'');
			return l20n.ctx.getSync(this.props.column.property);
		}
	},

	// returns filter type menu items according to column face
	createFilterTypeMenuItems: function() {
		var filterTypeMenuItems = [];
		var options = AdvancedTableColumnFilter.getOptions(this.props.column.face);

		for(var index = 0; index < options.length; index++) {
			var filterType = options[index].filterType;

			filterTypeMenuItems.push(
				<MenuItem
					key={'filter-type' + filterType}
					keyaction={filterType}
					href='#'
					active={filterType === this.props.filterType}>

					<Entity entity={'advancedTable_filter_type_' + (filterType ? filterType : 'no_filter')} />
				</MenuItem>
			);
		}

		return filterTypeMenuItems;
	},

	// returns next order according to the current order
	getNextOrder: function() {
		switch(this.props.order ? this.props.order.toUpperCase() : null) {
			case 'ASC':
				return 'DESC';
			case 'DESC':
				return null;
			default:
				return 'ASC';
		}
	},

	// returns next null order according to the current nulls order
	getNextNullsOrder: function() {
		return null;
	},

	// listener for filter type selection
	onItemSelect: function(event) {
		this.props.changeFilterTypeFunc(this.props.column.property, event.keyaction);
	}
});

module.exports = AdvancedTableColumnHead;
