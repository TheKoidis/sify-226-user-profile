/******************************************************************************************************************************************************
 *
 * Represents filter component with one value in state.
 *
 ******************************************************************************************************************************************************/
AdvancedTableColumnFilterOneValueComponent = React.createClass({

	propTypes: {
		column: React.PropTypes.object, // column definition

		filterType: React.PropTypes.string, // current column filter type
		updateFilteringFunc: React.PropTypes.func // function responsible for update of filter
	},

	// returns initial state
	getInitialState: function() {
		return {
			value: null
		}
	},

	// updates filtering with null value when filter type changed
	componentWillReceiveProps: function(newProps) {
		if(this.props.filterType != newProps.filterType) {
			this.setState({
				value: null
			}, this.props.updateFilteringFunc(this.props.column.property, newProps.filterType, null));
		}
	},

	// renders component using column face
	render: function() {
		switch(this.props.column.face) {
			case 'integer':
			case 'decimal':
			case 'string':
				return (
					<Input sm
						placeholder={AdvancedTableColumnFilter.getDescription(this.props.filterType)}
						value={this.state.value}
						onChange={this.onInputChange} />
				);
			case 'boolean':
				return (
					<Select sm
						value={this.state.value}
						onChange={this.onSelectChange}>

						<option value='true'>{'\u2611'}</option>
						<option value='false'>{'\u2610'}</option>
					</Select>
				);
			case 'enum':
				// TODO column filter one value component for enum
				return null;
			case 'date':
			case 'datetime':
			case 'time':
				return (
					<Datepicker sm
						labelKey=''
						name={'dateFilter-' + this.props.column.property + '-' + this.props.filterType}
						placeholder={AdvancedTableColumnFilter.getDescription(this.props.filterType)}
						showButtons={false}
						timeEnabled={this.props.column.face != 'date'}
						onChange={this.onDatepickerChange} />
				);
			default:
				return null;
		}
	},

	clearAllTimeOuts: function(){
		var highestTimeoutId = setTimeout(";");
		for (var i = 0 ; i < highestTimeoutId ; i++) {
		    clearTimeout(i);
		}
	},

	changeValueWithDelay: function(callback){
	 this.clearAllTimeOuts();
	 setTimeout(callback,500);
  },

	// listener for input
	onInputChange: function(event) {
		var newValue = event.target.value;
		this.setState({
			value : newValue
		},this.changeValueWithDelay.bind(this,this.props.updateFilteringFunc.bind(this,this.props.column.property, this.props.filterType, newValue == null || newValue == '' ? null : [newValue])));

	},

	// listener for select
	onSelectChange: function(event) {
		this.setState({
			value : event.currentTarget.value
		}, this.props.updateFilteringFunc.bind(this, this.props.column.property, this.props.filterType, event.currentTarget.value == null ? null : [event.currentTarget.value]));
	},

	// listener for datepicker
	onDatepickerChange: function(newValue) {
		this.setState({
			value : newValue
		}, this.props.updateFilteringFunc.bind(this, this.props.column.property, this.props.filterType, newValue == null ? null : [newValue]));
	}
});

module.exports = AdvancedTableColumnFilterOneValueComponent;
