/******************************************************************************************************************************************************
 *
 * Represents selection column head cell.
 *
 ******************************************************************************************************************************************************/
AdvancedTableRecordSelectionOptions = React.createClass({

	propTypes: {
		selectPageFunc: React.PropTypes.func.isRequired, // function responsible for adding whole page of records to current selection
        deselectAllFunc: React.PropTypes.func.isRequired // function responsible for removing all records from current selection
	},

	// renders buttons for selectPage and deselectAll
	render: function() {
		var buttons = [];

		buttons.push(
			<Button sm key='select-page' title={l20n.ctx.getSync('advancedTable_select_page')} onClick={this.props.selectPageFunc}>
				<Icon glyph='icon-fontello-ok-squared' />
			</Button>
		);

		buttons.push(
			<Button sm key='deselect-all' title={l20n.ctx.getSync('advancedTable_deselect_all')} onClick={this.props.deselectAllFunc}>
				<Icon glyph='icon-fontello-minus-squared' />
			</Button>
		);

		return (
			<th style={{verticalAlign: 'top', width: '2.5%'}}>
				<ButtonGroup>
					<Dropdown>
						<DropdownButton xs container={this} menu='optionsMenu'>
							<Icon glyph='icon-fontello-menu' />
						</DropdownButton>

						<Menu ref='optionsMenu' onItemSelect={this.onItemSelect}>
							<MenuItem
								key='select-page'
								keyaction='select-page'
								href='#'
								active={false}
								title={l20n.ctx.getSync('advancedTable_select_page')}>

								<Icon glyph='icon-fontello-ok-squared' />{' ' + l20n.ctx.getSync('advancedTable_select_page')}
							</MenuItem>

							<MenuItem
								key='deselect-all'
								keyaction='deselect-all'
								href='#'
								active={false}
								title={l20n.ctx.getSync('advancedTable_deselect_all')}>

								<Icon glyph='icon-fontello-minus-squared' />{' ' + l20n.ctx.getSync('advancedTable_deselect_all')}
							</MenuItem>
						</Menu>
					</Dropdown>
				</ButtonGroup>
			</th>
		);
	},

	// listener for selection
	onItemSelect: function(event) {
		switch(event.keyaction) {
			case 'select-page':
				this.props.selectPageFunc();
				return;
			case 'deselect-all':
				this.props.deselectAllFunc();
				return;
			default:
				return;
		}
	}
});

module.exports = AdvancedTableRecordSelectionOptions;
