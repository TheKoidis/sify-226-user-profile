var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

/******************************************************************************************************************************************************
 *
 * Component responsible for rendering common table buttons (create new, toggle on/off custom filter and custom filters).
 *
 ******************************************************************************************************************************************************/
AdvancedTableOptions = React.createClass({
	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	statics: {
		conjunctions: ['AND', 'OR']
	},

	propTypes: {
		 tableId: React.PropTypes.string, // table id

		 searchFunc: React.PropTypes.func, // function responsible for searching
		 searchWithCustomFilterFunc: React.PropTypes.func, // function responsible for searching with custom filter

		 rest: React.PropTypes.string, // REST URL for getting data

	     createNewDetail: React.PropTypes.string, // URL of detail of new object (primarily used if defined instead of following)
	     createNewDetailModalFunc: React.PropTypes.func, // function responsible for creating modal detail of new object

	     customFilterFunc: React.PropTypes.func, // function responsible for rendering of custom filter
	     customNamedFilters: React.PropTypes.arrayOf(React.PropTypes.object), // custom named filters (object with attributes name and filter)
			 defaultCustomNamedFilter: React.PropTypes.number, // index of default active filter
			 labelKeyPrefix: React.PropTypes.string, // prefix of label key string (used for create button)

	     globalActions: React.PropTypes.arrayOf(React.PropTypes.object), // global actions (object with functions 'render()','execute(rest, searchCallback)' and optional attribute dropdownKey)

		 columnFilterTypes: React.PropTypes.arrayOf(React.PropTypes.object), // types of filters per column
	     columnFilterGroupConjunction: React.PropTypes.string, // conjunction in column filter group
	     changeColumnFilterGroupConjunctionFunc: React.PropTypes.func, // function responsible for changing conjunction
			 disabledColumnFilterGroupConjunction: React.PropTypes.bool, // show/hide columns filter conjunction button
	},

	// returns initial state
	getInitialState: function() {
		return {
			customFilterHidden: this.props.customFilterFunc ? false : null, // custom filter visibility flag
			activeCustomNamedFilterIndex: null // currently active custom named filter index
		};
	},

	componentDidMount: function () {
			//default filter
			var index = this.props.defaultCustomNamedFilter;
			if (index) {
				 var customNamedFilter = this.props.customNamedFilters[index];
				 this.setState({
					 activeCustomNamedFilterIndex: index
				 }, this.props.searchWithCustomFilterFunc(customNamedFilter ? customNamedFilter.createFilter() : ''));
			}
	},

	// renders buttons and conditionally custom filter
	render: function() {
		var primaryOptions = this.createPrimaryOptions();
		var secondaryOptions = this.createSecondaryOptions();

		return (
			<Grid collapse>
				<Row>
					<Col xs={10}>
						<ButtonToolbar>
							{primaryOptions}
						</ButtonToolbar>
					</Col>

					<Col xs={2}>
						<ButtonToolbar style={{float: 'right'}}>
							{secondaryOptions}
						</ButtonToolbar>
					</Col>
				</Row>

				{this.state.customFilterHidden
					?	<Row>
							{this.props.customFilterFunc()}
						</Row>
					: 	null
				}
			</Grid>
		);
	},

	getDropdownAction: function (item) {
		item.action();
	},

	createPrimaryOptions: function() {
		var primaryOptions = [];

		if(this.props.createNewDetail || this.props.createNewDetailModalFunc) {
			primaryOptions.push(
				<ButtonGroup key='create-new-button-group'>
					<Button sm key='create-new' bsStyle='primary' onClick={this.props.createNewDetail ? this.goToCreateNewDetail : this.showCreateNewDetailModal}>
						<Icon glyph='icon-simple-line-icons-plus'/>{' '}
						{this.props.labelKeyPrefix ? l20n.ctx.getSync(this.props.labelKeyPrefix + '_btn_create') : l20n.ctx.getSync('advancedTable_create_new')}
					</Button>
				</ButtonGroup>
			);
		}

		if(this.props.globalActions) {
			var actionButtons = [];
			var dropdownButtons = [];
			var dropdownDefinitions = []; //array of objects {dropdownKey: 'key', items: [actions]}

			for (var index = 0; index < this.props.globalActions.length; index++) {
				var action = this.props.globalActions[index];

				if (action.dropdownKey) { //action belongs to dropdown menu
					var result = $.grep(dropdownDefinitions, function (d) {
						return d.dropdownKey === action.dropdownKey;
					});
					if (result.length === 0) { //create definition for new dropdown menu
						dropdownDefinitions.push({dropdownKey: action.dropdownKey, items: [action]});
					} else {
						result[0].items.push(action); //add action to selected dropdown definition
					}

				} else { //action is rendered as button
					actionButtons.push(
						<Button sm bsStyle='info' key={'global-action-' + index} onClick={action.execute.bind(this, this.props.rest, this.props.searchFunc)}>
							{action.render()}
						</Button>
					);
				}
			}

			//render dropdown buttons
			for (var i = 0; i < dropdownDefinitions.length; i++) {
				var dropdown = dropdownDefinitions[i];

				//create menu items
				var items = [];
				for (var index = 0; index < dropdown.items.length; index++) {
					var action = dropdown.items[index];
					items.push(
						<MenuItem index={index}
											title={l20n.ctx.getSync(action.title)}
											href='#'
											action={action.execute}
											disabled={!action.isEnabled && action.isEnabled !== undefined}>
							{action.render()}
						</MenuItem>
					);
				}

				//create dropdown button
				var key = dropdown.dropdownKey;
				dropdownButtons.push(
					<ButtonGroup>
						<DropdownButton sm bsStyle='info'
														menu={key}
														container={this}>
							<Caret/>{' '+l20n.ctx.getSync(key)}
						</DropdownButton>
						<Menu bsStyle='info'
									onItemSelect={this.getDropdownAction}
									ref={key}>
							{items}
						</Menu>
					</ButtonGroup>
				);
			}

			primaryOptions.push(
				<ButtonGroup key='global-actions'>
					{dropdownButtons}
					{actionButtons}
				</ButtonGroup>
			);
		}

		// toggle on/off custom filter button is visible conditionally
		if(this.state.customFilterHidden != null) {
			var customFilterButtons = [];

			customFilterButtons.push(
				<Button sm key ='toggle-custom-filter' onClick={this.toggleCustomFilter} title={l20n.ctx.getSync('advancedTable_toggle_filter')}>
					<Icon glyph={this.state.customFilterHidden ? 'icon-fontello-down-open-3' : 'icon-fontello-up-open-3'}/>{' '}
					<Entity entity='advancedTable_filter' />
				</Button>
			);

			if(this.props.customNamedFilters) {
				for(var index = 0; index < this.props.customNamedFilters.length; index++) {
					var customNamedFilter = this.props.customNamedFilters[index];
					var active = this.state.activeCustomNamedFilterIndex === index;

					customFilterButtons.push(
						<Button sm key={'custom-named-filter-' + index} active={active} onClick={this.selectCustomNamedFilter.bind(null, customNamedFilter, index)}>
							{customNamedFilter.render()}
						</Button>
					);
				}

				if(this.state.activeCustomNamedFilterIndex !== null) {
					customFilterButtons.push(
						<Button sm key={'clear-custom-named-filter'} title={l20n.ctx.getSync('advancedTable_clear_filter_selection')} onClick={this.selectCustomNamedFilter.bind(null, null, null)}>
							<Icon glyph='icon-fontello-cancel' />
						</Button>
					);
				}
			}

			primaryOptions.push(
				<ButtonGroup key='custom-filter-button-group'>
					{customFilterButtons}
				</ButtonGroup>
			);
		}

		return primaryOptions;
	},

	createSecondaryOptions: function() {
		var secondaryOptions = [];

		if(this.isConjunctionDropdownVisible()) {
			secondaryOptions.push(
				<Dropdown key='conjunction-dropdown'>
					<DropdownButton sm container={this} menu='conjunctionsMenu'>
						<span>
							<Icon glyph='icon-fontello-cog-alt' />{' '}
							<Entity entity='advancedTable_conjunction' />
						</span>
					</DropdownButton>

					<Menu ref='conjunctionsMenu' onItemSelect={this.onItemSelect} alignRight>
						{this.createConjunctionMenuItems()}
					</Menu>
				</Dropdown>
			);
		}

		return secondaryOptions;
	},

	// returns conjunction dropdown visibility flag
	isConjunctionDropdownVisible: function() {
		if(!this.props.disabledColumnFilterGroupConjunction){
			for(var index = 0; index < this.props.columnFilterTypes.length; index++) {
				if(this.props.columnFilterTypes[index].filterType) {
					return true;
				}
			}
		}
		return false;
	},

	// goes to create new detail
    goToCreateNewDetail: function() {
    	this.transitionTo(this.props.createNewDetail, {id: 'new'})
    },

    // shows create new detail modal
    showCreateNewDetailModal: function() {
    	ModalDialogManager.create(this.createNewDetailModal, this.props.tableId);
    },

    // returns modal dialog component with content specified by createNewDetailModalFunc
    createNewDetailModal: function() {
    	return (
    		<ModalDialog lg modalId={this.props.tableId}>
    	    	<ModalDialogBody>
    	        	{this.props.createNewDetailModalFunc(this.props.rest + '/new', true, 'new', this.closeCreateNewDetailModal)}
    	        </ModalDialogBody>
    	    </ModalDialog>
    	)
    },

    // closes modal dialog and runs search to refresh data
    closeCreateNewDetailModal: function() {
    	ModalDialogManager.remove(this.props.tableId);

    	this.props.searchFunc(false);
    },

    // toggles custom filter
	toggleCustomFilter: function(flag) {
		this.setState({
			customFilterHidden: !this.state.customFilterHidden
		});
	},

	// selects custom named filter (or unselects if argument is null)
	selectCustomNamedFilter: function(customNamedFilter, index) {
		this.setState({
			activeCustomNamedFilterIndex: index
		}, this.props.searchWithCustomFilterFunc(customNamedFilter ? customNamedFilter.createFilter() : ''));
	},

	// returns conjunction menu items
	createConjunctionMenuItems: function() {
		var conjunctionMenuItems = [];

		for(var index = 0; index < AdvancedTableOptions.conjunctions.length; index++) {
			var conjunction = AdvancedTableOptions.conjunctions[index];

			conjunctionMenuItems.push(
					<MenuItem
						key={conjunction}
						keyaction={conjunction}
						href='#'
						active={conjunction === this.props.columnFilterGroupConjunction}>

						<Entity entity={'advancedTable_conjunction_' + conjunction} />
					</MenuItem>
			);
		}

		return conjunctionMenuItems;
	},

	// listener for conjunction selection
	onItemSelect: function(event) {
		this.props.changeColumnFilterGroupConjunctionFunc(event.keyaction);
	}
});

module.exports = AdvancedTableOptions;
