var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var AdvancedTableHead = require('./head/head.jsx');
var Body = require('./body/body.jsx');
var Foot = require('./foot/foot.jsx')

var PageSettings = require('./foot/page-settings.jsx');
var ColumnFilter = require('./head/column/column-filter.jsx');

var RestUtil = require('../../domain/rest-util.jsx');

/******************************************************************************************************************************************************
 *
 * AdvancedTable is complex React component for quick and easy table building.
 *
 ******************************************************************************************************************************************************/
var AdvancedTable = React.createClass({
    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    statics: {
    	// returns property value for specified object (supports 'dot convention')
    	getPropertyValue: function(object, property) {
    		if(property) {
    			var value = object;
    			var propertyParts = property.split('.');

    			// iterate through property name parts and updates value
    			for(var index = 0; index < propertyParts.length; index++) {
    				if(value) {
    					value = value[propertyParts[index]];
    				}
    			}

    			return value;
    		} else {
    			return null;
    		}
    	},

    	// default action for common deletion
        defaultDeleteAction: {
            title: 'btn_delete',

            render: function (longLabel) {
                return (
                    <span>
                        <Icon glyph={'icon-fontello-trash'}/>
                        {longLabel ? (' ' + l20n.ctx.getSync('btn_delete')) : ''}
                    </span>
                );
            },

            execute: function(rest, records, searchCallback) {
                vex.dialog.confirm({
                    //message: 'Do you really want to delete ' + records.length + ' record(s)?',
                    message: l20n.ctx.getSync('delete_confirm'), //TODO
                    callback: function(confirm) {
                        if(confirm) {
                            var counter = 0;
                            for(var index = 0; index < records.length; index++) {
                                $.ajax({
                                    headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    },
                                    method: 'DELETE',
                                    url: rest + '/' + records[index].id,
                                    dataType: 'text',
                                    success: function (response) {
                                        counter++;

                                        if(counter === records.length) {
                                            searchCallback(true);
                                        }
                                    }.bind(this),
                                    error: function (xhr, status, error) {
                                        console.log(xhr + ' ' + status + ' ' + error);
                                    }.bind(this)
                                });
                            }
                        }
                    }
                });
            },

            isEnabled: function(rest, records) {
                return records && records.length >= 1;
            }
        },

        // returns generated workflow actions using their names; callback has signature callback(processedRecords, failedRecords)
        getWorkflowActions(actionsSettings, callback) {
            var actions = [];

            if(actionsSettings) {
                for(var index = 0; index < actionsSettings.length; index++) {
                    actions.push({
                        name: actionsSettings[index].name,
                        callback: callback,
                        style: actionsSettings[index].style,

                        render: function(longLabel) {
                            return (
                                <span title={l20n.ctx.getSync('workflowAction_' + this.name)}>
                                    <Icon glyph={'icon-fontello-arrows-cw'}/>
                                    {longLabel ? (' ' + l20n.ctx.getSync('workflowAction_' + this.name)) : ''}
                                </span>
                            )
                        },

                        execute: function(rest, records, searchCallback) {
                            if(records) {
                                var counter = 0;
                                var processedRecords = [];
                                var failedRecords = [];

                                for(var index = 0; index < records.length; index++) {
                                    var record = records[index];

                                    $.ajax({
                            			headers: {
                            				'Accept': 'application/json',
                            				'Content-Type': 'application/json'
                            			},
                            			method: 'GET',
                            			url: rest + '/' + record.id,
                                        success: function (response) {
                                            var record = response.resource;
                                            var workflowMetadata = response.workflowMetadata;

                                            $.ajax({
                                    			headers: {
                                    				'Accept': 'application/json',
                                    	 	        'Content-Type': 'application/json'
                                    	 	    },
                                    	 	    method: 'PUT',
                                    	 	    url: '/api/workflow/process-instances/' + this.getProcessInstanceId(workflowMetadata) + '/' + this.name,
                                    	 	    success: function () {
                                                    processedRecords.push(record);
                                    	 	    	counter++;

                                                    if(counter == records.length) {
                                                        if(this.callback) {
                                                            this.callback(processedRecords, failedRecords);
                                                        }

                                                        searchCallback();
                                                    }
                                    	 	    }.bind(this),
                                    	 	    error: function (xhr, status, error) {
                                                    failedRecords.push(record);
                                                    counter++;

                                                    if(counter == records.length) {
                                                        if(this.callback) {
                                                            this.callback(processedRecords, failedRecords);
                                                        }

                                                        searchCallback();
                                                    }
                                    	 	    }.bind(this)
                                    	    });
                            			}.bind(this),
                            			error: function (xhr, status, error) {
                            				this.processError(xhr, status, error);
                            			}.bind(this)
                                    });
                                }
                            }
                        },

                        getProcessInstanceId: function(workflowMetadata) {
                            if(workflowMetadata && workflowMetadata.processes) {
                                for(var processIndex = 0; processIndex < workflowMetadata.processes.length; processIndex++) {
                                    var process = workflowMetadata.processes[processIndex];

                                    if(process.actions) {
                                        for(var actionIndex = 0; actionIndex < process.actions.length; actionIndex++) {
                                            if(this.name === process.actions[actionIndex].id) {
                                                return process.processInstance.id;
                                            }
                                        }
                                    }
                                }
                            }

                            return null;
                        },

                        isEnabled: function(rest, records, selectionType) {
                            var enabled = false;

                            if(selectionType === 'multiple' && records && records.length >= 1) {
                                var ids = JSON.stringify(records.map(function(value, index, array) { return value.id; }));

                                $.ajax({
                        			headers: {
                        				'Accept': 'application/json',
                        				'Content-Type': 'application/json'
                        			},
                        			method: 'POST',
                        			url: rest + '/action-permit/' + this.name,
                                    data: ids,
                                    async: false,
                                    success: function (response) {
                                        enabled = response;
                        			}.bind(this),
                        			error: function (xhr, status, error) {
                        				this.processError(xhr, status, error);
                        			}.bind(this)
                                });
                            }

                            return enabled;
                        },

                        processError: function(xhr, status, error) {
                        	//TODO process errors
                        	console.log(xhr);
                        	console.log(error);

                        	this.getFlux().actions.flash.add({
                                messageId: 'errors',
                                type: 'error',
                                message: error
                            });

                        	this.setState({
                        		processing: false
                        	});
                        },

                        getStyle: function() {
                            return this.style;
                        }
                    });
                }
            }

            return actions;
        },

        // default action for adding to basket
        getAddToBasketAction(flux, type, isEnabledFunc, parent) {
            return {
                title: 'btn_basket_add',

                render: function (longLabel) {
                    longLabel = true;
                    return (
                        <span>
                            <Icon glyph={'icon-fontello-basket'}/>
                            {longLabel ? (' ' + l20n.ctx.getSync('btn_basket_add')) : ''}
                        </span>
                    );
                },

                execute: function(rest, records, searchCallback) {
                    for(var index = 0; index < records.length; index++) {
                        flux.store("BasketStore").add(records[index].id, type);
                    }
                    parent.setState({basketItems: parent.state.basketItems + records.length}); //set count items in basket
                    searchCallback(true);
                    flux.actions.flash.add({
                              messageId: 'formErrors',
                              type: 'success',
                              message: l20n.ctx.getSync('basket_add_success')
                          });
                },

                isEnabled: function(rest, records) {
                    if (!records || records.length == 0) {
                        return false;
                    }
                    for(var index = 0; index < records.length; index++) {
                        if ((isEnabledFunc && !isEnabledFunc(records[index])) || flux.store("BasketStore").contains(records[index].id, type)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        },

        // default action for remove from to basket
        getRemoveFromBasketAction(flux, type, parent) {
            return {
                title: 'btn_basket_remove',

                render: function (longLabel) {
                    longLabel = true;
                    return (
                        <span>
                            <Icon glyph={'icon-fontello-cancel'}/>
                            {longLabel ? (' ' + l20n.ctx.getSync('btn_basket_remove')) : ''}
                        </span>
                    );
                },

                execute: function(rest, records, searchCallback) {
                    for(var index = 0; index < records.length; index++) {
                        flux.store("BasketStore").remove(records[index].id, type);
                    }
                    parent.setState({basketItems: parent.state.basketItems - records.length}); //set count items in basket
                    searchCallback(true);
                    flux.actions.flash.add({
                              messageId: 'formErrors',
                              type: 'success',
                              message: l20n.ctx.getSync('basket_remove_success')
                          });
                },

                isEnabled: function(rest, records) {
                    for(var index = 0; index < records.length; index++) {
                        if (flux.store("BasketStore").contains(records[index].id, type)) return true;
                    }
                    return false;
                }
            }
        }
    },

    propTypes: {
        id: React.PropTypes.string.isRequired, // id

        rest: React.PropTypes.string.isRequired, // REST URL for getting data
        filter: React.PropTypes.string, // default filter for data
        customFilterFunc: React.PropTypes.func, // function responsible for rendering of custom filter
        customNamedFilters: React.PropTypes.arrayOf(React.PropTypes.object), // custom named filters (object with attributes name and filter)
        defaultCustomNamedFilter: React.PropTypes.number, // index of default active filter

        createNewDetail: React.PropTypes.string, // URL of detail of new object (primarily used if defined instead of following)
        createNewDetailModalFunc: React.PropTypes.func, // function responsible for creating modal detail of new object

        ordering: React.PropTypes.arrayOf(React.PropTypes.object), // default ordering (array of objects: {property: 'id', order: 'asc'})
        disabledPaging: React.PropTypes.bool.isRequired, // disabled paging flag
        disabledOrdering: React.PropTypes.bool.isRequired, // disabled ordering flag
        disabledSearching: React.PropTypes.bool.isRequired, // disabled searching flag
        disabledSearchingOptions: React.PropTypes.bool, // disabled button with filter options flag
        disabledFilterConjunction: React.PropTypes.bool, // show/hide columns filter conjunction button

        selectedRecords: React.PropTypes.arrayOf(React.PropTypes.object), // initially selected records
        onRecordSelection: React.PropTypes.func, // function which is called on record selection/unselection (onRecordSelection(records, checked))

        actionUniqueProperty: React.PropTypes.string, // unique property for actions purposes (it is used for selection state evaluation)
        actions: React.PropTypes.arrayOf(React.PropTypes.object), // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, records)')
        globalActions: React.PropTypes.arrayOf(React.PropTypes.object), // global actions (object with functions 'render()' and 'execute(rest, searchCallback)')

        disabledSelection: React.PropTypes.bool, //disabled records selection
        disabledFooter: React.PropTypes.bool, //disabled table footer
        disabledHeader: React.PropTypes.bool, //disabled table header

        reportRest: React.PropTypes.string, // report REST URL (required for report support)
        reportObjectType: React.PropTypes.string, // report object type (required for report support)

        afterSearchFunc: React.PropTypes.func, // function which is called after successful search ('afterSearch()')
        recordChosedFunc: React.PropTypes.func, // function responsible for choosing record
        recordChosedProperty: React.PropTypes.string, // property used for choosing record (if empty, whole record is used)

        labelKeyPrefix: React.PropTypes.string // prefix of label key string (it is used together with property when labelKey attribute is not present for column)
    },

    // returns default props
    getDefaultProps: function() {
    	return {
        disabledFooter: false,
        disabledHeader: false,
    		disabledPaging: false,
        disabledOrdering: false,
        disabledSearching: false,
        disabledSelection: false,
        disabledFilterConjunction: false,
        disabledSearchingOptions: false,
    		actionUniqueProperty: 'id'
    	};
    },

    // returns initial state for table
    getInitialState: function() {
        return {
        	// component property
        	processing: true, // we start in processing state

        	// request properties
        	pageNumber: 1,
        	pageSize: this.props.disabledPaging || this.props.disabledFooter ? 2147483647 : PageSettings.pageSizes[0], // use max integer value (32b) or default value
        	ordering: this.props.ordering ? this.props.ordering : [],
        	defaultFilterGroup: {},
        	columnFilterGroup: {
        		name: 'column_group',
        		groupOperator: 'AND',
        		filters: []
        	},
        	customFilterGroup: {},
        	customFilterHidden: false,

        	// selected records (can be from different pages)
        	selectedRecords: this.props.selectedRecords ? this.props.selectedRecords : [],

        	// actions
        	actions: this.props.actions,

        	// response properties
        	recordsFiltered: 0,
    		  recordsReturned: 0,
    		  recordsTotal: 0,
    		  records: []
        };
    },

    // builds default filter and runs search
    componentWillMount: function() {
    	this.init(this.props);
    },

    // builds default filter and runs search
    componentWillReceiveProps: function(nextProps) {
        this.setState({
            // component property
        	processing: true, // we start in processing state

        	// request properties
        	pageNumber: 1,
        	pageSize: nextProps.disabledPaging || nextProps.disabledFooter ? 2147483647 : PageSettings.pageSizes[0], // use max integer value (32b) or default value
        	ordering: nextProps.ordering ? nextProps.ordering : [],
        	defaultFilterGroup: {},
        	columnFilterGroup: {
        		name: 'column_group',
        		groupOperator: 'AND',
        		filters: []
        	},
        	customFilterGroup: {},
        	customFilterHidden: false,

        	// selected records (can be from different pages)
        	selectedRecords: nextProps.selectedRecords ? nextProps.selectedRecords : [],

        	// actions
        	actions: nextProps.actions,

        	// response properties
        	recordsFiltered: 0,
    		recordsReturned: 0,
    		recordsTotal: 0,
    		records: []
        }, this.init.bind(this, nextProps));
    },

    // renders whole component
    render: function () {
    	return (
    		<Table id={this.props.id} hover responsive className='table table-striped table-bordered table-condensed' cellSpacing='0' width='100%'>
    			<AdvancedTableHead
    				key='head'
    				ref='head'
    				tableId={this.props.id}
    				searchFunc={this.search}
    				searchWithCustomFilterFunc={this.searchWithCustomFilter}
    				columnsCount={this.getColumnsCount()}
    				rest={this.props.rest}
    				createNewDetail={this.props.createNewDetail}
    				createNewDetailModalFunc={this.props.createNewDetailModalFunc}
    				customFilterFunc={this.props.customFilterFunc}
    				customNamedFilters={this.props.customNamedFilters}
            defaultCustomNamedFilter={this.props.defaultCustomNamedFilter}
    				columns={this.getColumns()}
            disabledColumnFilterGroupConjunction={this.props.disabledFilterConjunction}
    				columnFilterGroupConjunction={this.state.columnFilterGroup.groupOperator}
		     		changeColumnFilterGroupConjunctionFunc={this.changeColumnFilterGroupConjunction}
    				ordering={this.state.ordering}
    				changeOrderingFunc={this.changeOrdering}
    				globalActions={this.props.globalActions}
    				updateFilteringFunc={this.updateFiltering}
    				showActionsHeaders={this.visibleActionHeaders()}
            showSelectionHeader={this.visibleSectionHeader()}
    				selectPageFunc={this.selectPage}
    				deselectAllFunc={this.deselectAll}
    				recordChosedFunc={this.props.recordChosedFunc}
    				labelKeyPrefix={this.props.labelKeyPrefix}
            disabledSearching={this.props.disabledSearching}
            disabledOrdering={this.props.disabledOrdering}
            disabledSearchingOptions={this.props.disabledSearchingOptions}
            disabledHeader={this.props.disabledHeader}/>

    			<AdvancedTableBody
    				key='body'
    				tableId={this.props.id}
    				rest={this.props.rest}
    				columns={this.getColumns()}
    				records={this.state.records}
            processing={this.state.processing}
    				searchFunc={this.search}
    	    	actions={this.state.actions}
            disabledSelection={this.props.disabledSelection}
    				onRecordSelection={this.props.onRecordSelection}
    				isRecordSelectedFunc={this.isRecordSelected}
    				selectRecordFunc={this.selectRecord}
    				deselectRecordFunc={this.deselectRecord}
    				recordChosedFunc={this.props.recordChosedFunc}
            recordChosedProperty={this.props.recordChosedProperty}/>

          {this.state.recordsTotal > 0
    				?	<AdvancedTableFoot
		    				key='foot'
		    				rest={this.props.rest}
		    				columnsCount={this.getColumnsCount()}
		    				columns={this.getColumns()}
		    				recordsReturned={this.state.recordsReturned}
		    				recordsTotal={this.state.recordsTotal}
		    				recordsFiltered={this.state.recordsFiltered}
    						disabledPaging={this.props.disabledPaging}
                disabledFooter={this.props.disabledFooter}
		    				pageNumber={this.state.pageNumber}
		    				pageSize={this.state.pageSize}
		    				changePageNumberFunc={this.changePageNumber}
		    				changePageSizeFunc={this.changePageSize}
		    				searchFunc={this.search}
		    				selectedRecords={this.state.selectedRecords}
    						records={this.state.records}
		    				actions={this.state.actions} />
	    			:	null
    			}
    		</Table>
        );
    },

    visibleSectionHeader: function(){
      return this.visibleActionHeaders() && !this.props.disabledSelection;
    },

    visibleActionHeaders: function() {
    	return this.state.actions != undefined && this.state.recordsReturned > 0;
    },

    getColumnsCount: function() {
    	var columnsCount = 0;

    	if(this.props.recordChosedFunc) {
    		columnsCount += 1;
    	}

    	if(this.visibleActionHeaders()) {
    		columnsCount += 2;
    	}

    	React.Children.forEach(this.props.children, function(child, column) {
    		if(child && child.props.visible) {
    			columnsCount++;
    		}
    	});

    	return columnsCount;
    },

    // returns columns for further processing (to render column headers etc.)
    getColumns: function() {
    	var columns = [];

    	React.Children.forEach(this.props.children, function(child, column) {
    		if(child && child.props.visible) {
    			columns.push(child.props);
    		}
    	});

    	return columns;
    },

    // changes page number and invokes search function
    changePageNumber: function(newPageNumber) {
    	var pageCount = Math.floor(this.state.recordsTotal / this.state.pageSize) + (this.state.recordsTotal % this.state.pageSize > 0 ? 1 : 0);

    	if(!isNaN(newPageNumber) && newPageNumber >= 1 && newPageNumber <= pageCount) {
		    this.setState({
		    	processing: true,
		    	pageNumber: parseInt(newPageNumber)
		    }, this.search);
    	} else {
    		// TODO number validation failed
    		console.log(newPageNumber);
    	}
    },

    // changes page size and invokes search function
    changePageSize: function(newPageSize) {
    	if(!isNaN(newPageSize) && newPageSize > 0) {
	    	this.setState({
	    		processing: true,
	    		pageSize: parseInt(newPageSize),
	    		pageNumber: 1
	    	}, this.search);
    	} else {
    		// TODO number validation failed
    		console.log(newPageNumber);
    	}
    },

    // changes ordering and invokes search function
    changeOrdering: function(property, order, nullsOrder) {
    	// TODO validate property
    	var newOrdering = [];

    	// conditionally add property for ordering
    	if(order) {
    		newOrdering.push({
	    		property: property,
	    		order: order,
	    		nullsOrder: nullsOrder
	    	});
    	}

    	// add previous ordering except current ordering (property)
    	for(var index = 0; index < this.state.ordering.length; index++) {
    		if(this.state.ordering[index].property !== property) {
    			newOrdering.push({
    				property: this.state.ordering[index].property,
    				order: this.state.ordering[index].order,
    				nullsOrder: this.state.ordering[index].nullsOrder
    			});
    		}
    	}

    	this.setState({
    		processing: true,
    		ordering: newOrdering
    	}, this.search);
    },

    updateFiltering: function(property, filterType, filterValues) {
    	var filterGroup = this.getFilterGroup(property);

    	if(filterType === 'null' || filterType === 'notnull' || !this.isFilterValuesArrayEmpty(filterValues)) {
	    	filterGroup.filters.push({
	    		propertyName: property,
	    		operator: {'@type': AdvancedTableColumnFilter.getFilterOperatorType(filterType)},
	    		values: filterValues
	    	});
    	}

    	this.setState({
    		processing: true,
    		columnFilterGroup: filterGroup,
    		pageNumber: 1
    	}, this.search);
    },

    isFilterValuesArrayEmpty: function(filterValues) {
    	if(filterValues != null) {
	    	for(var index = 0; index < filterValues.length; index++) {
	    		if(filterValues[index] != undefined && filterValues[index] != null && filterValues[index] != '') {
	    			return false;
	    		}
	    	}
    	}

    	return true;
    },

    getFilterGroup: function(ignoredProperty) {
    	var filterGroup = {
    		name: 'column_group',
    		groupOperator: this.state.columnFilterGroup.groupOperator,
    		filters: this.getFilters(this.state.columnFilterGroup.filters, ignoredProperty)
    	}

    	return filterGroup;
    },

    getFilters: function(filters, ignoredProperty) {
    	var newFilters = [];

    	for(var index = 0; index < filters.length; index++) {
    		if(filters[index].propertyName !== ignoredProperty) {
    			newFilters.push(filters[index]);
    		}
    	}

    	return newFilters;
    },

    changeColumnFilterGroupConjunction: function(conjunction) {
    	var columnFilterGroup = this.getFilterGroup(null, null);

    	columnFilterGroup.groupOperator = conjunction;

    	this.setState({
    		columnFilterGroup: columnFilterGroup
    	}, this.search);
    },

    // returns record selection flag (uses actionUniqueProperty)
    isRecordSelected: function(record) {
    	return this.state.selectedRecords.map(this.getActionUniquePropertyValue).indexOf(this.getActionUniquePropertyValue(record)) != -1;
    },

    // returns action unique property value for specified record
    getActionUniquePropertyValue: function(record) {
    	return AdvancedTable.getPropertyValue(record, this.props.actionUniqueProperty);
    },

    // adds record to current selection
    selectRecord: function(record) {
    	var newSelectedRecords = [];

    	newSelectedRecords.push(record);

    	for(var index = 0; index < this.state.selectedRecords.length; index++) {
    		newSelectedRecords.push(this.state.selectedRecords[index]);
    	}

    	this.setState({
    		selectedRecords: newSelectedRecords
    	});
    },

    // removes record from current selection
    deselectRecord: function(record) {
    	var newSelectedRecords = [];

    	for(var index = 0; index < this.state.selectedRecords.length; index++) {
    		if(this.getActionUniquePropertyValue(this.state.selectedRecords[index]) !== this.getActionUniquePropertyValue(record)) {
    			newSelectedRecords.push(this.state.selectedRecords[index]);
    		}
    	}

    	this.setState({
    		selectedRecords: newSelectedRecords
    	});
    },

    // adds all records from current page to selection
    selectPage: function() {
    	var newSelectedRecords = [];
    	var addedSelectedRecords = [];

    	for(var index = 0; index < this.state.selectedRecords.length; index++) {
    		newSelectedRecords.push(this.state.selectedRecords[index]);
    	}

    	for(var index = 0; index < this.state.records.length; index++) {
    		var record = this.state.records[index];

    		if(this.state.selectedRecords.map(function(selectedRecord) {return this.getActionUniquePropertyValue(selectedRecord)}.bind(this)).indexOf(this.getActionUniquePropertyValue(record)) === -1) {
    			newSelectedRecords.push(record);
    			addedSelectedRecords.push(record);
    		}
    	}

    	this.setState({
    		selectedRecords: newSelectedRecords
    	}, this.props.onRecordSelection ? this.props.onRecordSelection.bind(this, addedSelectedRecords, true) : null);
    },

    // removes all records from current selection
    deselectAll: function() {
    	var removedSelectedRecords = this.state.selectedRecords;

    	this.setState({
    		selectedRecords: []
    	}, this.props.onRecordSelection ? this.props.onRecordSelection.bind(this, removedSelectedRecords, false) : null);
    },

    // initializes table
    init: function(props) {
	    if(props.filter) {
            var searchParameters = RestUtil.toSearchParameters(props.filter);

            //react bug when setState is called form componentWillMount - callback is not fired
            this.state.defaultFilterGroup = searchParameters;
            this.search();
		} else {
			this.search(false);
		}

	    if(props.reportRest && props.reportObjectType) {
		    $.ajax({
		    	headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'GET',
				url: '/api/core/print-modules?filter=objectType:' + props.reportObjectType,
				success: function (response) {
					var newActions = [].concat(props.actions);

					if(response.resources) {
			    		var resources = response.resources;

			    		for(var index = 0; index < resources.length; index++) {
			    			newActions.push({
			    				reportRest: props.reportRest,
			    				name: resources[index].resource.name,
			    				template: resources[index].resource.template,
			    				type: resources[index].resource.type,
			    				actionUniqueProperty: this.props.actionUniqueProperty,

			    				render: function (longLabel) {
			    					return this.name;
			    				},

			    				execute: function(rest, records, searchCallback) {
			    					var ids = records.map(function(record) {
			    						return AdvancedTable.getPropertyValue(record, this.actionUniqueProperty);
			    					}.bind(this));

			    					var href = this.reportRest + '/download';
			    					href += ('&name=' + this.name + '&template=' + this.template + '&type=' + this.type);

			    					if(ids && ids.length >= 1) {
			    						href += ('&ids=' + ids.join());
			    					}

			    					window.location.href = href;
			    				},

			    				isEnabled: function(rest, records, selectionType) {
			    					return selectionType === 'multiple';
			    				}
			    			});
			    		}
			    	}

					this.setState({
				    	actions: newActions,
				    	processing: false
				    });
				}.bind(this),
	 	        error: function (xhr, status, error) {
	 	        	this.processError(xhr, status, error);
	 	        }.bind(this)
		    });
	    }
    },

    // searches for data
    search: function(clearSelectedRecords) {
	    $.ajax({
	    	headers: {
	    		'Accept': 'application/json',
	            'Content-Type': 'application/json'
	        },
	        method: 'POST',
	        url: this.props.rest + '/filter',
	        data: JSON.stringify(this.createSearchParameters()),
	        success: function (response) {
	        	this.processResponse(response, clearSelectedRecords);
	        }.bind(this),
	        error: function (xhr, status, error) {
	        	this.processError(xhr, status, error);
	        }.bind(this)
	    });
    },

    searchWithCustomFilter: function(filter) {
        var searchParameters = RestUtil.toSearchParameters(filter);

        this.setState({
            customFilterGroup: searchParameters
                                ? {
                                    name: 'custom_group',
                                    groupOperator: searchParameters.groupOperator,
                                    filters: searchParameters.filters
                                }
                                : null
        }, this.search);
    },

    // creates search parameters using component state
    createSearchParameters: function() {
    	return {
    		'range': this.createSearchParametersRange(),
    		'sorts': this.createSearchParametersSorts(),
    		'filterGroups' : this.createSearchParametersFilterGroups()
    	};
    },

    // creates range attribute for search parameters using component state
    createSearchParametersRange: function() {
    	var firstRow = (this.state.pageNumber - 1) * this.state.pageSize;
    	var rows = this.state.pageSize;

    	return {
    		'firstRow': firstRow,
			'rows': rows
    	};
    },

    // creates sorts attribute for search parameters using component state
    createSearchParametersSorts: function() {
    	var sorts = [];

    	for(var index = 0; index < this.state.ordering.length; index++) {
    		sorts.push({
        		propertyName: this.state.ordering[index].property,
    	    	sortOrder: this.state.ordering[index].order.toUpperCase()
        	});
    	}

    	return sorts;
    },

    // creates filter groups attribute for search parameters using component properties and state
    createSearchParametersFilterGroups: function() {
    	var filterGroups = [];

    	if(this.state.defaultFilterGroup) {
    		filterGroups.push(this.state.defaultFilterGroup);
    	}

    	if(this.state.columnFilterGroup) {
    		filterGroups.push(this.state.columnFilterGroup);
    	}

    	if(this.state.customFilterGroup) {
    		filterGroups.push(this.state.customFilterGroup);
    	}

    	return filterGroups;
    },

    // processes response (refreshes state of component)
    processResponse: function(response, clearSelectedRecords) {
    	var records = [];

    	if(response.resources) {
    		var resources = response.resources;

    		for(var index = 0; index < resources.length; index++) {
    			records.push(resources[index].resource);
    		}
    	}

    	this.setState({
    		processing: false,

    		selectedRecords: clearSelectedRecords === true ? [] : this.state.selectedRecords,

    		recordsReturned: response.recordsReturned,
    		recordsTotal: response.recordsTotal,
    		recordsFiltered: response.recordsFiltered,
    		records: records
    	}, this.props.afterSearchFunc);
    },

    // processes error (shows message to user)
    processError: function(xhr, status, error) {
    	//TODO process errors
    	console.log(xhr);
    	console.log(error);

    	this.getFlux().actions.flash.add({
            messageId: 'errors',
            type: 'error',
            message: error
        });

    	this.setState({
    		processing: false
    	});
    }
});

/******************************************************************************************************************************************************
 *
 * Represents column definition for advanced table.
 *
 ******************************************************************************************************************************************************/
AdvancedTable.Column = React.createClass({

    propTypes: {
    	id: React.PropTypes.string, // this component id

    	property: React.PropTypes.string, // property name
    	labelKey: React.PropTypes.string, // label string key
    	label: React.PropTypes.string, // label string
    	tooltip: React.PropTypes.string, // tool tip string

    	width: React.PropTypes.any, // width of column (for example using percents)

    	visible: React.PropTypes.bool, // visibility flag
    	orderable: React.PropTypes.bool, // ordering flag

    	searchable: React.PropTypes.bool, // filtering flag
    	filterType: React.PropTypes.string, // default filter type (leave empty for default filter type based on face; set to 'none' for blank filter)

    	face: React.PropTypes.oneOf(['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string']), // face of this component
    	renderFunc: React.PropTypes.func, // own cell render function which is used instead of face ('renderFunc(value, record)')
    	summaryRenderFunc: React.PropTypes.func, // summary cell render function ('summaryRenderFunc(selectedRecords, pageRecords)')

    	detail: React.PropTypes.string, // detail URL
    	detailProperty: React.PropTypes.string, // property used for detail URL creation

    	detailRest: React.PropTypes.string, // REST URL in case of modal usage
    	detailModalFunc: React.PropTypes.func // function responsible for creating modal detail of selected object
    },

    getDefaultProps() {
        return {
        	visible: true,
            orderable: true,
            searchable: true,
            filterType: null,
            face: 'string'
        };
    },

    // we do not use this render function because we use this component only as column descriptor
    render: function() {
    	return null;
    }
});

module.exports = AdvancedTable;
