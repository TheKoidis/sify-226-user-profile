/******************************************************************************************************************************************************
 *
 * Component which renders action buttons for single record.
 *
 ******************************************************************************************************************************************************/
AdvancedTableRecordActions = React.createClass({

	mixins: [ReactRouter.Navigation],

	propTypes: {
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		record: React.PropTypes.object.isRequired, // record
		searchFunc: React.PropTypes.func.isRequired, // function responsible for searching
		actions: React.PropTypes.arrayOf(React.PropTypes.object) // actions (object with functions 'render(longLabel)', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, records)')
	},

	statics: {
		ACTIONS_LIMIT: 2 //minimum number of actions to create dropdown button instead of buttons
	},

	getEnabledActions: function () {
		var enabledActions = [];
		var actions = this.props.actions;
		for (var i = 0; i < actions.length; i++) {
			var action = actions[i];
			if (action.isEnabled(this.props.rest, [this.props.record])) enabledActions.push(action);
		}
		return enabledActions;
	},

	getButtons: function (actions) {
		var longLabel = false; //true - render icon and label, false - render only icon
		var buttons = [];
		for (var i = 0; i < actions.length; i++) {
			var action = actions[i];
			buttons.push(
				<Button sm key={i}
								bsStyle={action.getStyle ? action.getStyle() : 'default'}
								title={l20n.ctx.getSync(action.title)}
								onClick={action.execute.bind(action, this.props.rest, [this.props.record], this.props.searchFunc)}>
					{action.render(longLabel)}
				</Button>
			);
		}
		return buttons;
	},

	getDropdownButton: function () {
		var longLabel = true; //true - render icon and label, false - render only icon
		var items = [];
		var actions = this.props.actions;
		for (var i = 0; i < actions.length; i++) {
			var action = actions[i];
			if (action.isEnabled(this.props.rest, [this.props.record])) {
				items.push(
					<MenuItem index={i}
										title={l20n.ctx.getSync(action.title)}
										href='#'>
						{action.render(longLabel)}
					</MenuItem>
				);
			}
		}
		var menu = [];
		if (items.length !== 0) {
			menu = <ButtonGroup>
							<DropdownButton bsStyle='blue'
															menu='menu_actions'
														  container={this}>
								<Icon glyph='icon-fontello-th-list'/> <Caret/>
							</DropdownButton>
							<Menu bsStyle='blue'
										ref='menu_actions'
										onItemSelect={this.getDropdownAction}
										alignRight>
								{items}
							</Menu>
						</ButtonGroup>;
		}
		return menu;
	},

	getDropdownAction: function (item) {
		var action = this.props.actions[item.index];
		action.execute(this.props.rest, [this.props.record], this.props.searchFunc, this);
	},

	render: function() {
		var actions = this.getEnabledActions();
		return (
			<td>
				{actions.length < this.constructor.ACTIONS_LIMIT ? this.getButtons(actions) : this.getDropdownButton()}
			</td>
		);
	}
});

module.exports = AdvancedTableRecordActions;
