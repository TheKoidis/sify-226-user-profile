var Numbers = require('../../../../renderer/numbers.jsx');

/******************************************************************************************************************************************************
 * 
 * Component which renders integer property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableIntegerProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.number // value for rendering (integer)
	},
	
	// renders integer using format
	render: function() {
		return (
			<div style={{textAlign: 'right'}}>
				{Numbers.format(this.props.value)}
			</div>
		)
	}
});

module.exports = AdvancedTableIntegerProperty;