var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var AdvancedTableDetailProperty = require('./detail-property.jsx');
var AdvancedTableBooleanProperty = require('./boolean-property.jsx');
var AdvancedTableDateProperty = require('./date-property.jsx');
var AdvancedTableDateTimeProperty = require('./date-time-property.jsx');
var AdvancedTableDecimalProperty = require('./decimal-property.jsx');
var AdvancedTableDefaultProperty = require('./default-property.jsx');
var AdvancedTableEnumProperty = require('./enum-property.jsx');
var AdvancedTableIntegerProperty = require('./integer-property.jsx');
var AdvancedTableTimeProperty = require('./time-property.jsx');

/******************************************************************************************************************************************************
 *
 * Component which renders single record property (using single cell in table).
 *
 ******************************************************************************************************************************************************/
AdvancedTableRecordProperty = React.createClass({
	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	propTypes: {
		tableId: React.PropTypes.string, // id of table

		column: React.PropTypes.object, // column definition
		value: React.PropTypes.any, // cell value (i.e. single attribute)
		record: React.PropTypes.object, // complete record with all attributes
		detailValue: React.PropTypes.any, // value for detail (i.e. the one appended to detail URL)

		searchFunc: React.PropTypes.func // function responsible for searching
	},

	// renders inner cell component ( wrapped in link if specified by column definition)
	render: function() {		
		var component = this.createComponent();

		if(this.props.column.detailRest && this.props.column.detailModalFunc) {
			component = this.addLinkToDetailModal(component);
		} else if(this.props.column.detail && this.props.detailValue) {
			component = this.addLinkToDetail(component);
		}

		return (
			<td>
				{component}
			</td>
		)
	},

	// creates component for this cell (uses specified render function if available, uses face otherwise)
	createComponent: function() {
		return this.props.column.renderFunc ? this.props.column.renderFunc(this.props.value, this.props.record) : this.createCellComponent();
	},

	// returns wrapped component linked to detail
	addLinkToDetail: function(component) {
		return (
			<a onClick={this.goToDetail} style={{'cursor': 'pointer'}} title={l20n.ctx.getSync('advancedTable_go_to_detail')}>
				{component}
			</a>
		);
	},

	// goes to create new detail
	goToDetail: function() {
		this.transitionTo(this.props.column.detail, {id: this.props.detailValue})
	},

	// returns wrapped component linked to detail modal dialog
	addLinkToDetailModal: function(component) {
		return (
			<a onClick={this.showDetailModal} style={{'cursor': 'pointer'}} title={l20n.ctx.getSync('advancedTable_show_detail')}>
				{component}
			</a>
		);
	},

	 // shows detail modal
    showDetailModal: function() {
    	ModalDialogManager.create(this.createDetailModal, this.props.tableId);
    },

    // returns modal dialog component with content specified by detailModalFunc
    createDetailModal: function() {
    	return (
    		<ModalDialog lg modalId={this.props.tableId}>
    	    	<ModalDialogBody>
    	        	{this.props.column.detailModalFunc(this.props.column.detailRest + '/' + this.props.detailValue, false, this.props.detailValue, this.closeDetailModal)}
    	        </ModalDialogBody>
    	    </ModalDialog>
    	)
    },

    // closes modal dialog and runs search to refresh data
    closeDetailModal: function() {
    	ModalDialogManager.remove(this.props.tableId);

    	this.props.searchFunc(false);
    },

	// creates cell component using face from column definition
	createCellComponent: function() {
		switch(this.props.column.face) {
			case 'detail':
				return <AdvancedTableDetailProperty />
			case 'date':
				return <AdvancedTableDateProperty value={this.props.value} />
			case 'datetime':
				return <AdvancedTableDateTimeProperty value={this.props.value} />
			case 'time':
				return <AdvancedTableTimeProperty value={this.props.value} />
			case 'enum':
				return <AdvancedTableEnumProperty value={this.props.value} />
			case 'integer':
				return <AdvancedTableIntegerProperty value={this.props.value} />
			case 'decimal':
				return <AdvancedTableDecimalProperty value={this.props.value} />
			case 'boolean':
				return <AdvancedTableBooleanProperty value={this.props.value} />
			default:
				return <AdvancedTableDefaultProperty value={this.props.value} />
		}
	}
});

module.exports = AdvancedTableRecordProperty;
