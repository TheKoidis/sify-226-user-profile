/******************************************************************************************************************************************************
 * 
 * Component which renders simple detail icon.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableDetailProperty = React.createClass({
		
	// renders icon
	render: function() {		
		return (
			<Icon glyph='icon-fontello-search' />
		);
	}
});

module.exports = AdvancedTableDetailProperty;