/******************************************************************************************************************************************************
 *
 * Component which renders date property of some record.
 *
 ******************************************************************************************************************************************************/
var RenderUtil = require('../../../../renderer/render-util.jsx');

AdvancedTableDateProperty = React.createClass({

	propTypes: {
		value: React.PropTypes.string // value for rendering (date)
	},

	render: function() {
		return (
			<div>
				{this.props.value ? new Date(this.props.value).format(RenderUtil.getDateFormat()) : ''}
			</div>
		)
	}
});

module.exports = AdvancedTableDateProperty;
