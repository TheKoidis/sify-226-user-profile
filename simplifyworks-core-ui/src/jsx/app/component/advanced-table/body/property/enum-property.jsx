/******************************************************************************************************************************************************
 * 
 * Component which renders enum property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableEnumProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.string // value for rendering (enum)
	},
	
	// renders enum using localization as plain string
	// TODO localization of enum properties
	render: function() {
		return (
			<div>
				{this.props.value}
			</div>
		)
	}
});

module.exports = AdvancedTableEnumProperty;