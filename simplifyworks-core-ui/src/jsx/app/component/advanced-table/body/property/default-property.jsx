/******************************************************************************************************************************************************
 * 
 * Component which renders any property of some record as plain string.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableDefaultProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.node // value for rendering (for columns with unspecified face)
	},
	
	// renders value with no decoration (as plain string)
	render: function() {
		return (
			<div>
				{this.props.value}
			</div>
		)
	}
});

module.exports = AdvancedTableDefaultProperty;