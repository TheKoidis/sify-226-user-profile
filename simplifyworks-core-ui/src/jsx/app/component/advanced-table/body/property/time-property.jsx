/******************************************************************************************************************************************************
 *
 * Component which renders time property of some record.
 *
 ******************************************************************************************************************************************************/
var RenderUtil = require('../../../../renderer/render-util.jsx');

AdvancedTableTimeProperty = React.createClass({

	propTypes: {
		value: React.PropTypes.string // value for rendering (time)
	},

	render: function() {
		return (
			<div>
				{this.props.value ? new Date(this.props.value).format(RenderUtil.getTimeFormat()) : ''}
			</div>
		)
	}
});

module.exports = AdvancedTableTimeProperty;
