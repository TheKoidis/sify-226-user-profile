var Numbers = require('../../../../renderer/numbers.jsx');

/******************************************************************************************************************************************************
 * 
 * Component which renders decimal property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableDecimalProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]) // value for rendering (decimal)
	},
	
	// renders decimal using format
	render: function() {
		return (
			<div style={{textAlign: 'right'}}>
				{Numbers.format(this.props.value)}
			</div>
		)
	}
});

module.exports = AdvancedTableDecimalProperty;