/******************************************************************************************************************************************************
 *
 * Component which renders datetime property of some record.
 *
 ******************************************************************************************************************************************************/
var RenderUtil = require('../../../../renderer/render-util.jsx');

AdvancedTableDateTimeProperty = React.createClass({

	propTypes: {
		value: React.PropTypes.string // value for rendering (datetime)
	},

	render: function() {
		return (
			<div>
				{this.props.value ? new Date(this.props.value).format(RenderUtil.getDateTimeFormat()) : ''}
			</div>
		)
	}
});

module.exports = AdvancedTableDateTimeProperty;
