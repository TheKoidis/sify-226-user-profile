var AdvancedTableRecord = require('./record.jsx');

/******************************************************************************************************************************************************
 *
 * Represents body of table.
 *
 ******************************************************************************************************************************************************/
AdvancedTableBody = React.createClass({

	propTypes: {
		tableId: React.PropTypes.string, // id of table
		rest: React.PropTypes.string.isRequired, // REST URL for getting data

		columns: React.PropTypes.array, // columns definition
		records: React.PropTypes.arrayOf(React.PropTypes.object), // array of records
		processing: React.PropTypes.bool, // processing flag

		searchFunc: React.PropTypes.func, // function responsible for searching

		actions: React.PropTypes.arrayOf(React.PropTypes.object), // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')
		disabledSelection: React.PropTypes.bool, //disable records selection

		onRecordSelection: React.PropTypes.func, // function which is called on record selection/unselection
        isRecordSelectedFunc: React.PropTypes.func, // record selection status function
        selectRecordFunc: React.PropTypes.func, // function responsible for adding of record to current selection
        deselectRecordFunc: React.PropTypes.func, // function responsible for removing of record from current selection

        recordChosedFunc: React.PropTypes.func, // function responsible for choosing record
		recordChosedProperty: React.PropTypes.string // property used for choosing record (if empty, whole record is used)
	},

	// renders table body using set of record components (alternatively renders plain string for no records)
	render: function() {
		var columnsCount = this.props.columns.length + (this.props.actions ? 2 : 0) + (this.props.recordChosedFunc ? 1 : 0);
		var records = [];

		// create rows using properties
    	for(var index = 0; index < this.props.records.length; index++) {
    		records.push(
    			<AdvancedTableRecord
    				key={index}
    				tableId={this.props.tableId}
    				rest={this.props.rest}
    				columns={this.props.columns}
    				record={this.props.records[index]}
    				searchFunc={this.props.searchFunc}
    				actions={this.props.actions}
						disabledSelection={this.props.disabledSelection}
    				onRecordSelection={this.props.onRecordSelection}
    				isRecordSelectedFunc={this.props.isRecordSelectedFunc}
    				selectRecordFunc={this.props.selectRecordFunc}
					deselectRecordFunc={this.props.deselectRecordFunc}
    				recordChosedFunc={this.props.recordChosedFunc}
					recordChosedProperty={this.props.recordChosedProperty} />
    		);
    	}

		return (
			<tbody>
				{this.props.processing ||	records.length === 0
					?	<tr>
							<td colSpan={columnsCount} style={{'textAlign': 'center'}}>
								<br/>
									<div>
										{this.props.processing ?
											<Progress sm striped active value={100} min={0} max={100} style={{marginBottom: '0px', width: '33%', marginLeft: 'auto', marginRight: 'auto'}}/>
										: l20n.ctx.getSync('advancedTable_no_data_found')
										}
									</div>
								<br/>
							</td>
						</tr>
					:	records
				}
			</tbody>
		)
	}
});

module.exports = AdvancedTableBody;
