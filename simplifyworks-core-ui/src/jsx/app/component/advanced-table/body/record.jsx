var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var AdvancedTableRecordSelection = require('./record-selection.jsx');
var AdvancedTableRecordActions = require('./record-actions.jsx');
var AdvancedTableRecordProperty = require('./property/record-property.jsx');

/******************************************************************************************************************************************************
 *
 * Component which renders single record and conditionally selection and actions.
 *
 ******************************************************************************************************************************************************/
AdvancedTableRecord = React.createClass({
	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

	propTypes: {
		tableId: React.PropTypes.string.isRequired, // id of table
		rest: React.PropTypes.string.isRequired, // REST URL for getting data

		columns: React.PropTypes.array.isRequired, // columns definition
		record: React.PropTypes.object.isRequired, // row value (i.e. values for all columns)

		searchFunc: React.PropTypes.func.isRequired, // function responsible for searching

        actions: React.PropTypes.arrayOf(React.PropTypes.object), // actions
				disabledSelection: React.PropTypes.bool, //disable records selection

        onRecordSelection: React.PropTypes.func, // function which is called on record selection/unselection
        isRecordSelectedFunc: React.PropTypes.func, // record selection status function
        selectRecordFunc: React.PropTypes.func, // function responsible for adding of record to current selection
        deselectRecordFunc: React.PropTypes.func, // function responsible for removing of record from current selection

        recordChosedFunc: React.PropTypes.func, // function responsible for choosing record
		recordChosedProperty: React.PropTypes.string // property used for choosing record (if empty, whole record is used)
	},

	// renders single row
	render: function() {
		var record = this.props.record;
		var cells = [];

		if(this.props.recordChosedFunc) {
			cells.push(this.createChooseButton(record));
		}

		if(this.props.actions && !this.props.disabledSelection) {
			cells.push(
				<AdvancedTableRecordSelection
					key={'record-selection'}
					record={record}
					onRecordSelection={this.props.onRecordSelection}
					recordSelected={this.props.isRecordSelectedFunc(record)}
					selectRecordFunc={this.props.selectRecordFunc}
					deselectRecordFunc={this.props.deselectRecordFunc} />
			);
		}

		// creates cells using properties
		for(var index = 0; index < this.props.columns.length; index++) {
			var column = this.props.columns[index];

			var value = AdvancedTable.getPropertyValue(record, column.property);
			var detailValue = AdvancedTable.getPropertyValue(record, column.detailProperty);

			cells.push(
				<AdvancedTableRecordProperty
					key={'record-property-' + index}
					tableId={this.props.tableId}
					column={column}
					value={value}
					record={record}
					detailValue={detailValue ? detailValue : value}
					searchFunc={this.props.searchFunc}/>
			);
		}

		if(this.props.actions) {
			cells.push(
				<AdvancedTableRecordActions
					key={'record-actions'}
					rest={this.props.rest}
					record={record}
					searchFunc={this.props.searchFunc}
					actions={this.props.actions} />
			);
		}

		return (
			<tr>
				{cells}
			</tr>
		);
	},

	createChooseButton: function(record) {
		var value = this.props.recordChosedProperty ? AdvancedTable.getPropertyValue(record, this.props.recordChosedProperty) : record;
		return (
			<td>
				<Button xs bsStyle='primary' onClick={this.props.recordChosedFunc.bind(this, value)}>
					<Icon glyph='icon-fontello-check' />
				</Button>
			</td>
		);
	}
});

module.exports = AdvancedTableRecord;
