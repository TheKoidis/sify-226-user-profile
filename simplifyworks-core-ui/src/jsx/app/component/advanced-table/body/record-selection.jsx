/******************************************************************************************************************************************************
 * 
 * Component which renders checkbox for record selection (selected records can be processed via actions).
 * 
 ******************************************************************************************************************************************************/
AdvancedTableRecordSelection = React.createClass({
	
	propTypes: {
		record: React.PropTypes.object.isRequired, // record
		onRecordSelection: React.PropTypes.func, // function which is called on record selection/unselection
		recordSelected: React.PropTypes.bool.isRequired, // record selection status
		selectRecordFunc: React.PropTypes.func.isRequired, // function responsible for adding of record to current selection
        deselectRecordFunc: React.PropTypes.func.isRequired // function responsible for removing of record from current selection
	},
	
	// renders checkbox for record selection
	render: function() {
		return (
			<td>
				<input type='checkbox' checked={this.props.recordSelected} onChange={this.onChange} />
			</td>
		);
	},
	
	// listener for checkbox
	onChange: function(event) {
		if(event.currentTarget.checked) {
			this.props.selectRecordFunc(this.props.record);
		} else {
			this.props.deselectRecordFunc(this.props.record);
		}
		
		if(this.props.onRecordSelection) {
			this.props.onRecordSelection(this.props.record, event.currentTarget.checked);
		}
	}
});

module.exports = AdvancedTableRecordSelection;