var AbstractFormComponent = require('./abstract-form-component.jsx');

var Enumbox = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        //Input Name
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.string,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string.isRequired,
        options: React.PropTypes.array
    },

    statics: {
        enumOptions: function (keys, prefix, emptyVal) {
            var options = [];

            if (emptyVal) {
                options[0] = {value: null, label: ''};
            }
            for (var i = 0; i < keys.length; i++) {
                options[i + (emptyVal?1:0)] = {value: keys[i], label: l20n.ctx.getSync(prefix + keys[i])};
            }

            return options;
        }
    },

    // Whenever the input changes we update the value state
    // of this component
    setValue: function (event) {
        this.showValidations();
        this.setState({
            value: event.currentTarget.value
            // When the value changes, wait for it to propagate and
            // then validate the input
        }, function () {
            if (this.props.validate) {
                this.props.validate(this);
            }
            if (this.props.onChange) {
                this.props.onChange(this.state.value);
            }
        }.bind(this));
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            value: nextProps.value!==undefined && nextProps.value!=null ? nextProps.value : nextProps.options[0].value
        });
    },

    componentDidMount: function() {
        if (!this.state.value)
        this.setState({
            value: this.props.options[0].value
        });
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true
        })

        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }

    },

    componentWillUnmount: function () {
        console.log('componentWillUnmount: ' + this.props.name);
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    render: function () {
        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }

        var component = <Select id={this.props.name}
                                sm={this.props.sm}
                                lg={this.props.lg}
                                xs={this.props.xs}
                                onChange={this.setValue}
                                onKeyPress={this.props.keyPressListener}
                                placeholder={this.state.disabled?'':this.props.placeholder}
                                className={this.props.className}
                                disabled={this.state.disabled}
                                ref={this.props.name}
                                value={this.state.value}>
            {this.props.options.map(function (option) {
                return <option value={option.value}>{option.label}</option>;
            })}
        </Select>

        return this.formGroup(component, className);
    }
});

module.exports = Enumbox;
