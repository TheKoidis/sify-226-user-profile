/*
- UI messages
- Required l20n context to be readz before using this component

- TODO: static method parameters as array will be better?
- TODO: React.RenderToString doesnt work in Messenger
 */

var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var FlashMessages = React.createClass({

    mixins: [FluxMixin, Fluxxor.StoreWatchMixin("FlashStore")],

    propTypes: {

    },

    getInitialState() {
        return {
        };
    },

    getStateFromFlux() {
        return this.getFlux().store("FlashStore").getState();
    },

    getDefaultProps() {
        return {
        };
    },

    componentWillMount() {
    },

    componentDidMount() {
    },

    componentWillUnmount() {
    },

    render() {
        var flashMessages = [];
        if(this.props.id !== undefined) {
            this.state.flashMessages.forEach(function(options) {
                if(options.container == this.props.id || options.container == this) {
                    flashMessages.push(
                        <FlashMessages.FlashMessage container={options.container} messageId={options.messageId} type={options.type} message={options.message} />
                    );
                }
            }.bind(this));
        } else {
            if (console && console.log) {
                console.log('[FlashMessages] - please provide id');
            }
        }
        return (
            <div>
                {flashMessages}
            </div>
        )

    }

});

FlashMessages.FlashMessage = React.createClass({

    mixins: [FluxMixin],

    propTypes: {
        container: React.PropTypes.object,
        messageId: React.PropTypes.string,
        type: React.PropTypes.oneOf(['success', 'error', 'warning', 'info']),
        message: React.PropTypes.object
    },

    getInitialState() {
        return {
            hidden: false
        };
    },

    getDefaultProps() {
        return {
            container: null,
            messageId: null,
            type: 'success',
            message: 'Ok'
        };
    },

    componentWillMount() {
    },

    componentDidMount() {
    },

    componentWillUnmount() {
    },

    handleClose() {
        this.setState({hidden: true}, function() {
            this.getFlux().store("FlashStore").remove(this.props.container, this.props.messageId);
        }.bind(this));

    },

    render() {
        var classes = React.addons.classSet({
            'alert': true,
            'hidden': this.state.hidden,
            'alert-info': this.props.type == 'info',
            'alert-danger': this.props.type == 'error',
            'alert-success': this.props.type == 'success',
            'alert-warning': this.props.type == 'warning',
            'alert-dismissible': true
        });

        var message = this.props.message;

        // TODO: configurable
        if(1==1) {
            message = (
                <div>
                    <Button close onClick={this.handleClose}>
                        <span aria-hidden='true'>&times;</span>
                        <span className='sr-only'>Close</span>
                    </Button>
                    {this.props.message}
                </div>
            );
        }

        var props = React.mergeProps({
            role: 'alert',
            className: classes.trim(),
            style: {
                marginBottom: this.props.collapseBottom ? 0 : null
            }
        }, this.props);
        return (
            <div {...props}>
                {message}
            </div>
        )

    }

});

module.exports = FlashMessages;