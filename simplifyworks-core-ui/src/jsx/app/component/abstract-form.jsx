/**
 * Created by Svanda on 29.4.2015.
 */
var validator = require('validator');
var AbstractForm = {

    getStateFromFlux: function () {
        return this.getFlux().store("FlashStore").getState();
    },

    getInitialState: function () {
        // We add state isValid, which will be true initially.
        // When inputs are attached they will be validated, in turn
        // changing this value to false if any inputs are invalid
        return {
            isValid: true,
            isSubmitting: false,
            isNew: this.props.isNew === true ? true : false
        };
    },

    getDefaultProps: function () {
        return {
            isFormChild: false,
            isNew: false,
            visibleButtons: true,
            searchForm: false,
            disabled: false
        };
    },

    //This method is call after reditrect with Route.
    componentWillReceiveProps: function (next) {
        if (this.isMounted()) {
            this.refreshComponents(next);
            this.setState({isNew: next.isNew});
        }
    },

    componentWillMount: function () {
        this.inputs = {}; // We create a map of traversed inputs
        this.tabs = {};   // We create a map of tabs
        this.registerComponents(this.props.children,null); // We register inputs from the children
        this.model = {}; // We add a model to use when submitting the form
    },

    refreshComponents: function (props) {
        var inputs = this.inputs;
        var tabs = this.tabs;
        Object.keys(inputs).forEach(function (name) {
            inputs[name].setState({
                disabled: props.disabled || inputs[name].props.disabled,
                firstValidation: true
            });
        });
    },

    showValidations: function () {
        var inputs = this.inputs;
        Object.keys(inputs).forEach(function (name) {
            inputs[name].showValidations();
        });
    },

    // When the form loads we validate it
    componentDidMount: function () {
        this.refreshComponents(this.props);
    },

    componentWillUnmount: function () {
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    registerComponents: function (children,tab) {
        // A React helper for traversing children
        React.Children.forEach(children, function (child) {

            //If child haven't props than is not component for basic form
            if (!child.props) {
                return;
            }
            // We do a simple check for "name" on the child, which indicates it is an input.
            // You might consider doing a better check though

            if (child.props.name && child.type === BasicForm.type) {
                //console.log('register children form ' + child.props.name)
            }

            //register tab
            if ( child.props.pane) {
                 child.props.attachTabToForm = this.attachTabToForm(child);
            }

            if (child.props.name) {
               // console.log('register children ' + child.props.name)

                // We attach a method for the input to register itself to the form
                child.props.attachToForm = this.attachToForm;

                // We attach a method for the input to detach itself from the form
                child.props.detachFromForm = this.detachFromForm;

                // We also attach a validate method to the props of the input so
                // whenever the value is upated, the input will run this validate method
                child.props.validate = this.validate;

                //We attach a tab to which it belongs to the props of the input
                child.props.attachToTab = this.attachToTab(child,tab);

                ///if (this.props.searchForm) {
                child.props.keyPressListener = this.fireSubmit;
                //}
            }

            // If the child has its own children, traverse through them also...
            // in the search for inputs
            if (child.props.children && child.type !== BasicForm.type) {
                //If the child is Tab then attach the name of tab to component
                if(child.type === TabPane.type){
                   // tab = child.props.tab;
                    tab = child;
                }
                this.registerComponents(child.props.children,tab);
            }
        }.bind(this));
    },

    attachToTab: function (component,tabComponent){
        if (tabComponent){
            component.props.tab = tabComponent;                         // register tab to component
            this.addComponentToTab(tabComponent.props.tab,component);   // register component to tab
        }
    },

    addComponentToTab: function (key, component) {
        this.tabs[key] = this.tabs[key];
        this.tabs[key].push(component); // index 1..n is the components with props name
    },

    attachTabToForm: function (component){
        this.tabs[component.props.pane] = new Array();
        this.tabs[component.props.pane].push(component);    // index 0 in Map is tab
    },

    // All methods defined are bound to the component by React JS, so it is safe to use "this"
    // even though we did not bind it. We add the input component to our inputs map
    attachToForm: function (component) {
       // console.log('attach children ' + component.props.name + ' to ' + this.props.name)
        this.inputs[component.props.name] = component;

        // We add the value from the component to our model, using the
        // name of the component as the key. This ensures that we
        // grab the initial value of the input
        this.model[component.props.name] = component.state.value;
        // We have to validate the input when it is attached to put the
        // form in its correct state
        this.attachToTab(component);
        this.validate(component);
    },

    fireSubmit: function (event) {
        var code = (event.keyCode ? event.keyCode : event.which);
        if (code == 13) {
            this.handleSubmit(null,null, event);
        }
    },

    // We want to remove the input component from the inputs map
    detachFromForm: function (component) {
        //console.log('detach children ' + component.props.name + ' from ' + this.props.name)
        delete this.inputs[component.props.name];
        // We of course have to delete the model property
        // if the component is removed
        delete this.model[component.props.name];
    },

    // We need a method to update the model when submitting the form.
    // We go through the inputs and update the model
    updateModel: function () {
        Object.keys(this.inputs).forEach(function (name) {
            this.model[name] = this.inputs[name].state.value;
        }.bind(this));
    },

    updateFromModel: function (component) {
        Object.keys(this.inputs).forEach(function (name) {
            this.inputs[name].setState({
                value: this.model[name]
            });
        }.bind(this));
    },

    handleChange: function (event) {
        this.setState({value: event.target.value});
    },

    handleSubmit: function (button, processDefinition, e) {
        // We change the state of the form before submitting
        this.setState({
            isSubmitting: true
        });

        this.updateModel();
        //show validations warnings
        this.showValidations();
        // We validate form. When form is valid we call submit function
        //console.log(this.model);
        if (this.validateForm()) {
            this.getFlux().actions.flash.clear('formErrors');
            this.props.onSubmit(this, button, processDefinition);
        } else {
            this.getFlux().actions.flash.toast({
                messageId: 'formErrors',
                type: 'error',
                message: l20n.ctx.getSync('validation_error')
            });
            this.getFlux().actions.flash.add({
                container: 'formErrors',
                messageId: 'formErrors',
                type: 'error',
                message: l20n.ctx.getSync('validation_error')
            });
            this.setState({
                isSubmitting: false
            });
        }

    },

    normalizeNumber: function(str) {
        //TODO MS: convert from locale specific format to english format
        return str.replace(',','.');
    },

    //TODO MS: move to separate validator class
    maxValue: function(args) {
        return args[0]*1 <= args[1];
    },

    maxScale: function(args) {
        return (args[0]+'').match(new RegExp('^[^.]+(\\.\\d{0,'+args[1]+'})?$'));
    },

    // The validate method grabs what it needs from the component,
    // validates the component and then validates the form
    validate: function (component) {
        // If no validations property, do not validate
        if (!component.props.validations && component.props.required == false) {
            return;
        }

        if (!component.props.visible) {
            return;
        }

        // We initially set isValid to true and then flip it if we
        // run a validator that invalidates the input
        var isValid = true;
        // We only validate if component is enabled and if it is has value or if it is required
        if (component.props.disabled !== true /*&& (component.state.value || component.props.required)*/) {
            var invalidValidators = {};

            // We split on comma to iterate the list of validation rules
            component.props.validations.split(',').forEach(function (validation) {

                // By splitting on ":"" we get an array of arguments that we pass
                // to the validator. ex.: isLength:5 -> ['isLength', '5']
                var args = validation.split(':');

                // We remove the top item and are left with the method to
                // call the validator with ['isLength', '5'] -> 'isLength'
                var validateMethod = args.shift();

                // We use JSON.parse to convert the string values passed to the
                // correct type. Ex. 'isLength:1' will make '1' actually a number
                args = args.map(function (arg) {
                    return JSON.parse(arg);
                });

                // We then merge two arrays, ending up with the value
                // to pass first, then options, if any. ['valueFromInput', 5]
                argsValidator = args;
                args = [component.state.value].concat(args);

                if (validateMethod) {
                    if (!args[0] && (validateMethod == 'isEmail' ||validateMethod == 'isDecimal' || validateMethod == 'isNumeric' || validateMethod == 'maxValue' || validateMethod == 'maxScale')) {
                        //skip, null value is valid
                    } else {
                        if (validateMethod == 'isDecimal' || validateMethod == 'maxValue' || validateMethod == 'maxScale') {
                            args[0] = this.normalizeNumber(args[0]+'');
                        }

                        if (validator[validateMethod]) {
                            if (!validator[validateMethod].apply(validator, args)) {
                                isValid = false;
                                if (validateMethod == 'isLength' && argsValidator.length == 1) {
                                    invalidValidators[validateMethod + 'Min'] = argsValidator;
                                } else {
                                    invalidValidators[validateMethod] = argsValidator;
                                }
                            }
                        } else if (this[validateMethod]) {
                            //simplifyworks custom validators
                            if (!this[validateMethod](args)) {
                                isValid = false;
                                invalidValidators[validateMethod] = argsValidator;
                            }
                        } else {
                            isValid = false;
                            invalidValidators['validatorNotExist'] = validateMethod;
                        }
                    }
                }
            }.bind(this));

            if (component.props.required === true) {
                if (validator.isNull(component.state.value) || !validator.isLength(component.state.value, 1)) {
                    isValid = false;
                    invalidValidators['required'] = '';
                   // console.log('Validation false: ' + component.props.name + ' value: ' + component.state.value);
                }
            }
        }

        // We set the state of the input based on the validation
        component.setState({
            isValid: isValid,
            invalidValidators: invalidValidators
            // We use the callback of setState to wait for the state
            // change being propagated, then we validate the form itself
        },function() {
           if(component.props.tab)
             this.validateTab(component.props.tab.props.tab);
        }.bind(this));
    },

    validateTab: function(tabKey){
            var tabs = this.tabs,
                inputs = this.inputs,
                tabIsValid = true;
            var controlledTab;
            Object.keys(tabs).forEach(function (key, i) {
                if(key === tabKey)
                    controlledTab = $($('li.b-tab').get(i));
            });
            Object.keys(tabs[tabKey]).forEach(function (i) {
                var component = tabs[tabKey][i];
                if(component.props.name && inputs[component.props.name]){
                    if (!inputs[component.props.name].state.isValid)
                        tabIsValid = false;
                }
            });
            tabIsValid ? controlledTab.removeAttr('valid') : controlledTab.attr('valid','false');
    },

    validateForm: function () {
        // We set allIsValid to true and flip it if we find any
        // invalid input components
    	var mandatoryValidationSuccessful = true;
    	var providedValidationSuccessful = true;
    	var optionalValidationSuccessful = true;

        // if we find an invalid input component
        var inputs = this.inputs;
        Object.keys(inputs).forEach(function (name) {

            //We will validateForm when is component BasicForm
            if (inputs[name].props.isFormChild === true) {
                if (inputs[name].validateForm() === false) {
                	mandatoryValidationSuccessful = false;
                }

            } else if (!inputs[name].state.isValid) {
                if (!inputs[name].state.firstValidation) {
                    inputs[name].setState(inputs[name].state);
                }

                switch(inputs[name].props.validationType) {
                	case 'mandatory':
                		mandatoryValidationSuccessful = false;
                		break;
                	case 'provided':
                		providedValidationSuccessful = false;
                		break;
                	case 'optional':
                		optionalValidationSuccessful = false;
                		break;
                	default:
                		console.log("Unknown validation typ " + inputs[name].props.validationType);
                		break;
                }
            }
        });
        // if we find an invalid tab component
        var tabs = this.tabs;
        Object.keys(tabs).forEach(function(tabKey){
            this.validateTab(tabKey);
        }.bind(this));

        // We set the valid state of the
        // form itself
        this.setState({
        	mandatoryValidationSuccessful: mandatoryValidationSuccessful,
        	providedValidationSuccessful: providedValidationSuccessful,
        	optionalValidationSuccessful: optionalValidationSuccessful
        });

        return mandatoryValidationSuccessful;
    },

    cancelForm: function (event) {
        //console.log("cancelModalForm", this.props.modalClose);

        if (this.props.modalClose) {
            //in case of modal window, only close them
            this.props.modalClose();
        } else {
            this.back();
            //this.transitionTo(this.props.routeOnCancel);
        }
    },

    resetForm: function (event) {
        this.props.resetForm();
    }
};


module.exports = AbstractForm;
