//form components
var InputText = require("./src/jsx/app/component/input-text.jsx");
var Label = require('./src/jsx/app/component/label.jsx');
var BasicForm = require('./src/jsx/app/component/basic-form.jsx');
var Attachment = require('./src/jsx/app/component/attachment.jsx');
var Dropzone = require('./src/jsx/app/component/dropzone.jsx');
var FilterForm = require('./src/jsx/app/component/filter-form.jsx');
var TextArea = require('./src/jsx/app/component/text-area.jsx');
var CodeList = require('./src/jsx/app/component/codelist.jsx');
var FormTable = require('./src/jsx/app/component/form-table.jsx');
var MultiSelect = require('./src/jsx/app/component/multi-select.jsx');
var Checkbox = require('./src/jsx/app/component/checkbox.jsx');
var HtmlEditor = require('./src/jsx/app/component/html-editor.jsx');
var Datepicker = require('./src/jsx/app/component/datepicker.jsx');
var Enumbox = require('./src/jsx/app/component/enumbox.jsx');
var FlashMessages = require('./src/jsx/app/component/flash-messages.jsx');
var BasicTable = require('./src/jsx/app/component/basic-table.jsx');
var Breadcrumbs = require('./src/jsx/app/component/breadcrumbs.jsx');
var ModalDialog = require('./src/jsx/app/component/modal-dialog.jsx');
var WorkflowForm = require('./src/jsx/app/component/workflow-form.jsx');
var AdvancedTable = require('./src/jsx/app/component/advanced-table/advanced-table.jsx');
var AdvancedForm = require('./src/jsx/app/component/advanced-form/advanced-form.jsx');
var AdvancedDetail = require('./src/jsx/app/component/advanced-detail/advanced-detail.jsx');
var Section = require('./src/jsx/app/component/section.jsx');
var ValidationFailsBox = require('./src/jsx/app/component/advanced-form/validation-fails-box.jsx');

//stores
var FlashStore = require('./src/jsx/app/stores/flash-store.jsx');
var SecurityStore = require('./src/jsx/app/stores/security-store.jsx');
var BasketStore = require('./src/jsx/app/stores/basket-store.jsx');

//actions
var FlashActions = require('./src/jsx/app/actions/flash-actions.jsx');
var SecurityActions = require('./src/jsx/app/actions/security-actions.jsx');
var BasketActions = require('./src/jsx/app/actions/basket-actions.jsx');

//routes
var systemRoutes = require('./src/jsx/app/routes.jsx');
var system = require('./src/jsx/app/routes/app/system/system.jsx');

var systemUsers = require('./src/jsx/app/routes/app/system/user/users.jsx');
var systemUserList = require('./src/jsx/app/routes/app/system/user/user-list.jsx');
var systemUserDetail = require('./src/jsx/app/routes/app/system/user/user-detail.jsx');

var systemUserRoleForm = require('./src/jsx/app/routes/app/system/user/user-role-form.jsx');

var systemPersons = require('./src/jsx/app/routes/app/system/person/persons.jsx');
var systemPersonList = require('./src/jsx/app/routes/app/system/person/person-list.jsx');
var systemPersonDetail = require('./src/jsx/app/routes/app/system/person/person-detail.jsx');

var systemRoles = require('./src/jsx/app/routes/app/system/role/roles.jsx');
var systemRoleList = require('./src/jsx/app/routes/app/system/role/role-list.jsx');
var systemRoleDetail = require('./src/jsx/app/routes/app/system/role/role-detail.jsx');

var systemOrganizations = require('./src/jsx/app/routes/app/system/organization/organizations.jsx');
var systemOrganizationList = require('./src/jsx/app/routes/app/system/organization/organization-list.jsx');
var systemOrganizationDetail = require('./src/jsx/app/routes/app/system/organization/organization-detail.jsx');

var systemPersonOrganizationForm = require('./src/jsx/app/routes/app/system/person/person-organization-form.jsx');

var systemSidebar = require('./src/jsx/app/common/sidebar.jsx');

//other classes
var RestUtil = require('./src/jsx/app/domain/rest-util.jsx');
var RenderUtil = require('./src/jsx/app/renderer/render-util.jsx');
var CoreSexEnum = require('./src/jsx/app/domain/core-sex-enum.jsx');


var SimplifyworksCore = {
    //form components
    InputText: InputText,
    TextArea: TextArea,
    MultiSelect: MultiSelect,
    ModalDialog: ModalDialog,
    FormTable: FormTable,
    FlashMessages: FlashMessages,
    FilterForm: FilterForm,
    Enumbox: Enumbox,
    Datepicker: Datepicker,
    CodeList: CodeList,
    Checkbox: Checkbox,
    HtmlEditor: HtmlEditor,
    Breadcrumbs: Breadcrumbs,
    BasicTable: BasicTable,
    BasicForm: BasicForm,
    Attachment: Attachment,
    Dropzone: Dropzone,
    WorkflowForm: WorkflowForm,
    AdvancedTable: AdvancedTable,
    AdvancedForm: AdvancedForm,
    AdvancedDetail: AdvancedDetail,
    Label: Label,
    Section: Section,
    ValidationFailsBox: ValidationFailsBox,

    //stores
    CoreStores: {
        FlashStore: FlashStore,
        SecurityStore: SecurityStore,
        BasketStore: BasketStore
    },

    //actions
    CoreActions: {
        FlashActions: FlashActions,
        SecurityActions: SecurityActions,
        BasketActions: BasketActions
    },

    //routes
    systemRoutes: systemRoutes,

    system: system,

    systemUsers: systemUsers,
    systemUserList: systemUserList,
    systemUserDetail: systemUserDetail,

    systemUserRoleForm: systemUserRoleForm,

    systemPersons: systemPersons,
    systemPersonList: systemPersonList,
    systemPersonDetail: systemPersonDetail,

    systemRoles: systemRoles,
    systemRoleList: systemRoleList,
    systemRoleDetail: systemRoleDetail,

    systemOrganizations: systemOrganizations,
    systemOrganizationList: systemOrganizationList,
    systemOrganizationDetail: systemOrganizationDetail,

    systemPersonOrganizationForm: systemPersonOrganizationForm,

    systemSidebar: systemSidebar,

    //other classes
    RestUtil: RestUtil,
    RenderUtil: RenderUtil,
    CoreSexEnum: CoreSexEnum,

    version: require("./version")
};

module.exports = SimplifyworksCore;
