package org.simplifyworks.client.config.flyway;

import javax.annotation.PostConstruct;

import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

/**
 * SIFY-132 multi-module db migrations
 * - based on org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration
 *
 * @author hanak@ders.cz
 */
@Configuration
@ConditionalOnClass(Flyway.class)
@ConditionalOnProperty(prefix = "flyway", name = "enabled", matchIfMissing = false)
//bug ? nefacha s jre1.8.0_45 + Apache Tomcat/8.0.23
//@ConditionalOnBean(DataSource.class)
@AutoConfigureBefore(HibernateJpaAutoConfiguration.class)
@AutoConfigureAfter(DataSourceAutoConfiguration.class)
public class FlywayConfig extends FlywayAutoConfiguration {

	@Configuration
	@EnableConfigurationProperties(FlywayProperties.class)
	@Import(FlywayJpaDependencyConfiguration.class)
	public static class FlywayConfigurationExt extends FlywayAutoConfiguration.FlywayConfiguration {

		@PostConstruct
		@Override
		public void checkLocationExists() {
			// cannot be used - specific property prefix for modules "locations" property
		}

		@Override
		public Flyway flyway() {
			return null;
		}

		@ConditionalOnMissingBean(name="flywayCore")
		@ConditionalOnExpression("${flyway.enabled:true} && '${flyway.core.locations}'!=''")
		@ConfigurationProperties(prefix = "flyway.core")
		@Bean(initMethod = "migrate")
		public Flyway flywayCore() {
			Flyway flyway = super.flyway();
			return flyway;
		}

		@DependsOn("flywayCore")
		@ConditionalOnMissingBean(name="flywayClient")
		@ConditionalOnExpression("${flyway.enabled:true} && '${flyway.client.locations}'!=''")
		@ConfigurationProperties(prefix = "flyway.client")
		@Bean(initMethod = "migrate")
		public Flyway flywayClient() {
			Flyway flyway = super.flyway();
			return flyway;
		}

	}

}
