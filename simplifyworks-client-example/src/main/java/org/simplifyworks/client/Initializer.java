package org.simplifyworks.client;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.simplifyworks.core.service.ElasticsearchAdminService;
import org.simplifyworks.core.service.EntityDependencyFactory;
import org.simplifyworks.email.service.Emailer;
import org.simplifyworks.scheduler.service.SchedulerService;
import org.simplifyworks.uam.service.CoreRoleHierarchy;
import org.simplifyworks.uam.service.OrganizationStructureService;
import org.simplifyworks.uam.service.RoleStructureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Initializes application
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Component
public class Initializer implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger LOG = LoggerFactory.getLogger(Initializer.class);

	@Autowired
	private ElasticsearchAdminService elasticSearchAdminService;
	@Autowired
	private RoleStructureService roleStructureService;
	@Autowired
	private OrganizationStructureService organizationStructureService;
	@Autowired
	private EntityDependencyFactory entityDependencyFactory;
	@Autowired
	private Emailer emailer;
	@Autowired
	private CoreRoleHierarchy coreRoleHierarchy;
	@Autowired
	private Scheduler scheduler;
	@Autowired
	private SchedulerService schedulerService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		coreRoleHierarchy.init();

		elasticSearchAdminService.initIndex();

		roleStructureService.rebuild();
		organizationStructureService.rebuild();

		elasticSearchAdminService.refreshAll();

		entityDependencyFactory.init();

		emailer.init();

		//quartz
		if (scheduler != null && schedulerService != null) {
			try {
				scheduler.start();
				schedulerService.init();
			} catch (SchedulerException ex) {
				LOG.error(null, ex);
			}
		}
	}
}
