/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Controller - just for docu purpose
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public abstract class BaseController {
	
	@Autowired  
	private MessageSource messageSource;

	@ModelAttribute("currentUser")
	public WebAuthenticationDetails currentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication == null || !authentication.isAuthenticated() || authentication instanceof AnonymousAuthenticationToken) {
			return null;
		}
		return (WebAuthenticationDetails)authentication.getDetails();
	}
}
