/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Index documentation page
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@Controller
public class IndexController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(IndexController.class);

    @ModelAttribute("page")
    public String module() {
        return "index";
    }

    @RequestMapping("/")
    public String index(Model model) {
        return module();
    }
}
