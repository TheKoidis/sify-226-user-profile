/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.RecordRange;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.security.domain.UserAuthentication;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.service.CorePersonManager;
import org.simplifyworks.uam.service.CoreUserWithPersonManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.Lists;

/**
 * http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html
 * http://www.jayway.com/2014/07/04/integration-testing-a-spring-boot-application/
 * http://g00glen00b.be/spring-boot-rest-assured/
 * http://www.vogella.com/tutorials/Mockito/article.html
 *
 * TODO: Zatim navazano na konfiguraci aplikace - prehodit na test context
 *
 * TODO: hierarchie testu - testy manageru prenest do jednotlivych balicku
 *
 * TODO: Prenest unit test defaultnich manageru z jenero
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebAppConfiguration
@IntegrationTest("server.port:0")
//@ActiveProfiles("build_server")
@TestExecutionListeners(inheritListeners = false, listeners = {
	DependencyInjectionTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class})
public class CoreUserManagerTest extends AbstractTestNGSpringContextTests {

	private static final Logger LOG = LoggerFactory.getLogger(CoreUserManagerTest.class);
	@Autowired
	private CoreUserWithPersonManager coreUserManager;
	@Autowired
	protected SecurityService securityService;
	@Autowired
	private CorePersonManager corePersonManager;
	//
	private CoreUserWithPersonDto firstUser;
	private CoreUserWithPersonDto secondUser;
	private CoreUserWithPersonDto thirdUser;

	private void maxPers() {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setRange(new RecordRange(0, Integer.MAX_VALUE));
		LOG.info("max Person cnt: " + corePersonManager.count(searchParameters));
	}

	@BeforeClass
	public void setUp() {
		LOG.info("Running coreUserManager test");
		//
		maxPers();
		//
		firstUser = new CoreUserWithPersonDto("first" + System.currentTimeMillis(), "heslo".toCharArray());
		secondUser = new CoreUserWithPersonDto("second" + System.currentTimeMillis(), "heslo".toCharArray());
		thirdUser = new CoreUserWithPersonDto("third" + System.currentTimeMillis(), "heslo".toCharArray());

		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new RoleOrganizationGrantedAuthority("admin", null));
		
		SecurityContextHolder.getContext().setAuthentication(new UserAuthentication("admin", "admin", authorities, null));

		//zaindexujeme aktualni stav
		coreUserManager.reindexAll();
		corePersonManager.reindexAll();

		firstUser = prepareUser(firstUser);
		secondUser = prepareUser(secondUser);
		thirdUser = prepareUser(thirdUser);
	}

	private CoreUserWithPersonDto prepareUser(CoreUserWithPersonDto user) {
		maxPers();
		//
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setRange(new RecordRange(0, 1));
		List<CorePersonDto> persons = corePersonManager.search(searchParameters);
		Assert.assertEquals(1, persons.size());
		//
		CoreUserWithPersonDto u = coreUserManager.findByUsername(user.getUsername());
		if (u != null) {
			coreUserManager.remove(u.getId());
		}
		if (user.getPerson() == null) {
			user.setPerson(persons.get(0));
		}
		return coreUserManager.create(user);
	}

	@AfterClass
	public void cleanUp() {
		LOG.info("Finishing coreUserManager test");
		coreUserManager.remove(firstUser.getId());
		coreUserManager.remove(secondUser.getId());
		coreUserManager.remove(thirdUser.getId());
	}

	@Test
	public void coreUserManagerImplTest() {
		LOG.info("Test [" + coreUserManager + "]");
		SearchParameters sp = new SearchParameters(Locale.getDefault());
		sp.clearFilters();
		sp.addFilter(new FilterValue("username", FilterOperator.EQUALS, Lists.newArrayList(firstUser.getUsername(), secondUser.getUsername(), thirdUser.getUsername())));
		Assert.assertEquals(3, coreUserManager.search(sp).size());
	}
}
