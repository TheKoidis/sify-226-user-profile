# CHANGELOG
## [0.2.0] - 2015-06-23
### Fixed
- SIFY-64 Accept TINYINT for boolean columns
- SIFY-64 Do not check boolean constraints validity for MySQL connections in test suite
