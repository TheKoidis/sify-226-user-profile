--------------------------------------------------------
--  DDL for View ALL_FOREIGN_CONSTRAINTS_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "ALL_FOREIGN_CONSTRAINTS_VIEW" ("CONSTRAINT_NAME", "TABLE_NAME", "COLUMN_NAME", "R_TABLE_NAME", "R_COLUMN_NAME") AS 
  SELECT
  c.CONSTRAINT_NAME,
  c.table_name,
  col.column_name,
  p.table_name,
  a.column_name
FROM
  all_constraints c
JOIN ALL_CONS_COLUMNS col
ON
  col.CONSTRAINT_NAME = c.CONSTRAINT_NAME
JOIN all_constraints p
ON
  p.constraint_name=c.r_constraint_name
JOIN ALL_CONS_COLUMNS A
ON
  A.CONSTRAINT_NAME = p.CONSTRAINT_NAME and col.position=a.position
WHERE
    c.constraint_type='R'
AND c.owner        ='SET_DB_USER_HERE'
AND p.owner        ='SET_DB_USER_HERE'
AND c.status       ='ENABLED';

--------------------------------------------------------
--  DDL for View ALL_CHECK_CONSTRAINTS_VIEW
--------------------------------------------------------


  CREATE OR REPLACE FORCE VIEW "ALL_CHECK_CONSTRAINTS_VIEW" ("TABLE_NAME", "SEARCH_CONDITION") AS 
  SELECT
  table_name,
  search_condition
FROM
  all_constraints
WHERE
  constraint_type='C'
AND owner        ='SET_DB_USER_HERE'
AND status       ='ENABLED';  

--------------------------------------------------------
--  DDL for View ALL_INDEXES_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "ALL_INDEXES_VIEW" ("TABLE_NAME", "COLUMN_NAME", "INDEX_NAME") AS 
  SELECT
  i.table_name,
  a.column_name,
  i.index_name
FROM
  all_indexes i
JOIN all_ind_columns a
ON
  a.INDEX_NAME = i.INDEX_NAME
WHERE
  i.owner        ='SET_DB_USER_HERE';
--------------------------------------------------------
--  DDL for View ALL_OBJECTS_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "ALL_OBJECTS_VIEW" ("TABLE_NAME", "OBJECT_TYPE") AS 
  SELECT
  o.OBJECT_NAME,
  o.object_type
FROM
  ALL_OBJECTS o
WHERE
  o.owner ='SET_DB_USER_HERE' and o.object_type IN ('VIEW','TABLE');

--------------------------------------------------------
--  DDL for View ALL_SEQUENCES_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "ALL_SEQUENCES_VIEW" ("SEQUENCE_NAME", "LAST_NUMBER") AS 
  select s.sequence_name,s.last_number from all_sequences s
WHERE
s.sequence_owner            = 'SET_DB_USER_HERE';

--------------------------------------------------------
--  DDL for View ALL_TAB_COLUMNS_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "ALL_TAB_COLUMNS_VIEW" ("TABLE_NAME", "COLUMN_NAME", "DATA_TYPE", "CHAR_LENGTH", "DATA_PRECISION", "DATA_SCALE", "NULLABLE", "CHAR_USED", "OBJECT_TYPE") AS 
  SELECT
  c.table_name,
  c.column_name,
  c.data_type,
  c.char_length,
  c.data_precision,
  c.data_scale,
  c.nullable,
  c.char_used,
  o.object_type
FROM
  ALL_TAB_COLUMNS c
JOIN ALL_OBJECTS o
ON
  o.OBJECT_NAME    =c.TABLE_NAME
AND o.owner        ='SET_DB_USER_HERE'
AND o.object_type IN ('VIEW','TABLE')
WHERE
  c.owner ='SET_DB_USER_HERE';

--------------------------------------------------------
--  DDL for View ALL_TRIGGERS_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "ALL_TRIGGERS_VIEW" ("TRIGGER_NAME", "TABLE_NAME") AS 
  SELECT
  t.trigger_name,
  t.table_name
FROM
  all_triggers t
WHERE
t.trigger_type         = 'BEFORE EACH ROW'
AND t.triggering_event = 'INSERT OR UPDATE'
AND t.owner            = 'SET_DB_USER_HERE'
AND t.status           = 'ENABLED';




