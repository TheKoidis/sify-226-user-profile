package org.simplifyworks.test.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class ClassToReindex {

	private Class clazz;
	private String path;

	public ClassToReindex(Class clazz, String path) {
		this.clazz = clazz;
		this.path = path;
	}

	public Class getClazz() {
		return clazz;
	}

	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
