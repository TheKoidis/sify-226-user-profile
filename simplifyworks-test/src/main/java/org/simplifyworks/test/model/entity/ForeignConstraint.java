package org.simplifyworks.test.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
public class ForeignConstraint implements Serializable {

	@Id
	@Column(name = "CONSTRAINT_NAME")
	private String name;
	@Column(name = "TABLE_NAME")
	private String tableName;
	@Column(name = "COLUMN_NAME")
	private String column;
	@Column(name = "R_TABLE_NAME")
	private String referencedTable;
	@Column(name = "R_COLUMN_NAME")
	private String referencedColumn;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getReferencedTable() {
		return referencedTable;
	}

	public void setReferencedTable(String referencedTable) {
		this.referencedTable = referencedTable;
	}

	public String getReferencedColumn() {
		return referencedColumn;
	}

	public void setReferencedColumn(String referencedColumn) {
		this.referencedColumn = referencedColumn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
