package org.simplifyworks.test.model.entity;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class TableColumn {

	String dataType;
	BigInteger charLength;
	BigInteger dataPrecision;
	BigInteger dataScale;
	String nullable;
	String charUsed;
	Column column;
	ManyToOne manyToOne;
	OneToOne oneToOne;
	Field field;
	Size size;
	Max max;
	JoinColumn joinColumn;
	JoinColumns joinColumns;
	//pro join columns testujeme existenci cizího klíče a indexů najednou, proto další sloupce při této kontrole přeskakujeme
	boolean skipJoinColumns = false;

	public boolean isSkipJoinColumns() {
		return skipJoinColumns;
	}

	public void setSkipJoinColumns(boolean skipJoinColumns) {
		this.skipJoinColumns = skipJoinColumns;
	}

	public JoinColumns getJoinColumns() {
		return joinColumns;
	}

	public void setJoinColumns(JoinColumns joinColumns) {
		this.joinColumns = joinColumns;
	}

	public JoinColumn getJoinColumn() {
		return joinColumn;
	}

	public void setJoinColumn(JoinColumn joinColumn) {
		this.joinColumn = joinColumn;
	}

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	public Max getMax() {
		return max;
	}

	public void setMax(Max max) {
		this.max = max;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public Column getColumn() {
		return column;
	}

	public void setColumn(Column column) {
		this.column = column;
	}

	public ManyToOne getManyToOne() {
		return manyToOne;
	}

	public void setManyToOne(ManyToOne manyToOne) {
		this.manyToOne = manyToOne;
	}

	public OneToOne getOneToOne() {
		return oneToOne;
	}

	public void setOneToOne(OneToOne oneToOne) {
		this.oneToOne = oneToOne;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public BigInteger getCharLength() {
		return charLength;
	}

	public void setCharLength(BigInteger charLength) {
		this.charLength = charLength;
	}

	public BigInteger getDataPrecision() {
		return dataPrecision;
	}

	public void setDataPrecision(BigInteger dataPrecision) {
		this.dataPrecision = dataPrecision;
	}

	public BigInteger getDataScale() {
		return dataScale;
	}

	public void setDataScale(BigInteger dataScale) {
		this.dataScale = dataScale;
	}

	public String getNullable() {
		return nullable;
	}

	public void setNullable(String nullable) {
		this.nullable = nullable;
	}

	public String getCharUsed() {
		return charUsed;
	}

	public void setCharUsed(String charUsed) {
		this.charUsed = charUsed;
	}
}
