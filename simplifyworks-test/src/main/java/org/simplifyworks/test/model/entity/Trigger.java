package org.simplifyworks.test.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
public class Trigger implements Serializable {

	@Id
	@Column(name = "TRIGGER_NAME")
	private String triggerName;
	@Column(name = "TABLE_NAME")
	private String tableName;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTriggerName() {
		return triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}
}
