package org.simplifyworks.test.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum ObjectType {

	TABLE, VIEW;
}
