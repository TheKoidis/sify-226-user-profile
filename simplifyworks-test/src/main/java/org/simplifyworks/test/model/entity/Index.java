package org.simplifyworks.test.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
public class Index {

	@Id
	@Column(name = "INDEX_NAME")
	private String name;
	@Column(name = "TABLE_NAME")
	private String tableName;
	@Column(name = "COLUMN_NAME")
	private String column;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
