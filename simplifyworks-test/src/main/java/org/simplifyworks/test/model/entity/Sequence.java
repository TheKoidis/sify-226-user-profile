package org.simplifyworks.test.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
public class Sequence implements Serializable {

	@Id
	@Column(name = "SEQUENCE_NAME")
	private String sequenceName;

	public String getSequenceName() {
		return sequenceName;
	}

	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}
}
