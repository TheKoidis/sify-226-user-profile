require('./globals');

var Request = require('request');

var fs = require('fs');
var path = require('path');
var morgan = require('morgan');

var accessLogPath   = __dirname + path.sep + 'logs' + path.sep + 'access_log';
var proxyLogPath    = __dirname + path.sep + 'logs' + path.sep + 'proxy_log';

var package = require('./package.json');

var defaultAppName = process.env.APP ? process.env.APP : 'app';
var defaultBackendServerUrl = process.env.BACKEND ? process.env.BACKEND : 'http://localhost:8084/simplifyworks';
var routes = require('./src/jsx/'+defaultAppName+'/routes.jsx');

var webpack_host = process.env.WHOST ? process.env.WHOST : 'localhost';
var webpack_dev_server_port = process.env.WPORT ? process.env.WPORT : 8079;

var url = require('url');
var httpProxy = require('http-proxy');
var express = require('express');
var compression = require('compression');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var FileStore = require('session-file-store')(session);
var expressBeautify = require('express-beautify')();
var Auth = require('simplifyworks-core/auth/auth.jsx');

var app = express();

var proxyLogStream;
if (process.env.HAVE_ACCESS_LOG || process.env.HAVE_PROXY_LOG) {
  fs.mkdir(__dirname + path.sep + 'logs', 750, function() {
    if (process.env.HAVE_ACCESS_LOG) {
      console.log('Access logging: ' + accessLogPath + '.');

      app.use(morgan('combined', {
        stream: fs.createWriteStream(accessLogPath, {flags: 'a', mode: 640})
      }));
    }
    if (process.env.HAVE_PROXY_LOG) {
      console.log('Proxy  logging: ' + proxyLogPath + '.');
      proxyLogStream = fs.createWriteStream(proxyLogPath,  {flags: 'a', mode: 640});
    }
  });
}

app.use(compression());
app.use(/^(?!(\/?api))/, bodyParser.json());
app.use(/^(?!(\/?api))/, bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(session({
    store: new FileStore({ttl: (process.env.SESSION_EXPIRATION_TIMEOUT ? parseInt(process.env.SESSION_EXPIRATION_TIMEOUT) : 3600)}),
    secret: 'simplifyworks',
    resave: false,
    saveUninitialized: false
}));
app.use(expressBeautify);

Auth.init(app, defaultBackendServerUrl);

app.use(function(req, res, next) {
    if(req.user) {
        if(req.user.expiration && req.user.expiration < Date.now()) {
            var expirationUrl = req.user.expirationUrl;

            req.logout();
            req.session.destroy(function(err) {
                // cannot access session here
                console.log(err);
            });

            res.redirect(expirationUrl);

            return;
        }
    }

    next();
});

var html = fs.readFileSync(path.join(process.cwd(), 'src', 'jsx', defaultAppName, 'index.html'), {
  encoding: 'utf8'
});

var createStyleTag = function(file, media) {
  media = media || 'screen';
  return "    <link media='"+media+"' rel='stylesheet' type='text/css' href='"+file+"'>\n";
};

var stylesheets = '';
if(process.env.NODE_ENV === 'development') {
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/main.css', 'screen,print');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/theme.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/colors.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/font-faces.css');
  html = html.replace(new RegExp('{appscript}', 'g'), 'http://'+webpack_host+':'+webpack_dev_server_port+'/scripts/bundle.js');
} else {
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/main-blessed1.css', 'screen,print');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/main.css', 'screen,print');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/theme.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/colors-blessed1.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/colors.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/font-faces.css');
  html = html.replace(new RegExp('{appscript}', 'g'), '/js/'+defaultAppName+'/'+defaultAppName+'.js');
}

html = html.replace(new RegExp('{app}', 'g'), defaultAppName);
html = html.replace(new RegExp('{stylesheets}', 'g'), stylesheets);
html = html.replace(new RegExp('{version}', 'g'), package.version);
html = html.replace(new RegExp('{ssoUrl}', 'g'), process.env.AUTH_SAML_ENABLED ? '"/login/Shibboleth"' : 'null');

var ltr = html.replace(new RegExp('{dir}', 'g'), 'ltr');
var rtl = html.replace(new RegExp('{dir}', 'g'), 'rtl');

var Fluxxor = require('fluxxor');
//
var SimplifyworksCore = require('simplifyworks-core');

var renderApp = function(req, res, cb) {
  var stores = {
    SecurityStore: new SimplifyworksCore.CoreStores.SecurityStore(),
    FlashStore: new SimplifyworksCore.CoreStores.FlashStore()
  };

  var actions = {
    security: SimplifyworksCore.CoreActions.SecurityActions,
    flash: SimplifyworksCore.CoreActions.FlashActions
  };
  var flux = new Fluxxor.Flux(stores, actions);

  flux.on('dispatch', function(type, payload) {
    if (console && console.log) {
      console.log('[Dispatch server]', type, payload);
    }
  });


  var router = ReactRouter.create({
    routes: routes,
    location: req.url,
    onAbort: function(redirect) {
      cb({redirect: redirect});
    },
    onError: function(err) {
      console.log(err);
    }
  });

  router.run(function(Handler, state) {
    if(state.routes[0].name === 'not-found') {
      cb({notFound: true}, React.renderToStaticMarkup(<Handler flux={flux} />));
      return;
    }

    cb(null, React.renderToStaticMarkup(<Handler flux={flux} />));
  });
};

// session expiration info
app.get('/session', function(req, res) {
    if(req.user) {
        var options = {
            url: defaultBackendServerUrl + '/api/core/user-profile/current',
            headers: {
                'Authorization': 'Basic c2lmeTpwYXNz',
                'Current-Username': req.user.currentUsername,
                'Original-Username': req.user.originalUsername
            }
        };

        Request.get(options, function(err, response, body) {
            if(err || !body || response.statusCode != 200) {
                res.type('application/json');
                res.status(200);

                res.send(JSON.stringify({                    
                }));
            } else {
                var response = JSON.parse(body);

                res.type('application/json');
                res.status(200);

                var session = {
                    currentUser: response.resource.currentUser,
                    originalUser: response.resource.originalUser,
                    allRoles: response.resource.allRoles,
                    expirationUrl: req.user.expirationUrl,
                    expiration: req.user.expiration,
                    expirationWarning: req.user.expirationWarning
                }

                res.send(JSON.stringify(session));
            }

            res.end();
        });
    } else {
        res.type('application/json');
        res.status(200);

        res.send(JSON.stringify({}));
    }
});

// Create a proxy to our API here
//
// Origin must be changed appropriately if different from current. Otherwise some HTTP
// error 400/500 may appear with a nasty confusing message like:
// Hostname foo.org provided via SNI and hostname bar.org provided via HTTP are different
//

var apiProxy = httpProxy.createProxyServer({ target: defaultBackendServerUrl, changeOrigin: true });
app.all("/api/**", function(req, res) {
    req.headers['authorization'] = "Basic c2lmeTpwYXNz";
    req.headers['current-username'] = req.user.currentUsername;
    req.headers['original-username'] = req.user.originalUsername;

    apiProxy.web(req, res);
});

// e.g. timeouts
apiProxy.on('error', function(err, preq, pres) {
  pres.writeHead(500, { 'Content-Type': 'text/plain' });
  pres.write("An error happened at server. Please contact your administrator.");
  pres.end();
});

// General proxy logging is added where HAVE_PROXY_LOG is set
if (process.env.HAVE_PROXY_LOG) {
  apiProxy.on('proxyRes', function(proxyRes, req, res) {
    var remoteAddr = req.connection.remoteAddress;
    var actualTS = new Date().toISOString();
    proxyLogStream.write(actualTS + ' - ' + remoteAddr + ' - ' + req.url + ' - ' + JSON.stringify(proxyRes.headers, null, 0) + '\n');
  });
}

/** BEGIN X-EDITABLE ROUTES */
app.get('/xeditable/groups', function(req, res) {
  res.send([
    {value: 0, text: 'Guest'},
    {value: 1, text: 'Service'},
    {value: 2, text: 'Customer'},
    {value: 3, text: 'Operator'},
    {value: 4, text: 'Support'},
    {value: 5, text: 'Admin'}
  ]);
});

app.get('/xeditable/status', function(req, res) {
  res.status(500).end();
});

app.post('/xeditable/address', function(req, res) {
  res.status(200).end();
});

app.post('/dropzone/file-upload', function(req, res) {
  res.status(200).end();
});

/** END X-EDITABLE ROUTES */

app.get('/ltr', function(req, res, next) {
  res.redirect('/');
});

app.get('/rtl', function(req, res, next) {
  res.redirect('/');
});

/** CATCH-ALL ROUTE **/
app.get('*', function(req, res, next) {
  if(req.url === '/favicon.ico') return next();
  if(req.url === '/api') return next();
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  var isRTL = req.cookies.rubix_dir === 'rtl' ? true : false;
  try {
    renderApp(req, res, function(err, h, token) {
      if(isRTL)
        h = rtl.replace(new RegExp('{container}', 'g'), h || '');
      else
        h = ltr.replace(new RegExp('{container}', 'g'), h || '');

      if (!err) {
        res.sendHTML(h);
      } else if (error.redirect) {
        res.redirect(error.redirect.to);
      } else if (error.notFound) {
        res.status(404).sendHTML(h);
      }
    });
  } catch(e) {
    if(isRTL)
      res.sendHTML(rtl);
    else
      res.sendHTML(ltr);
  }
});

var server = app.listen(process.env.PORT, function() {
  try {
    process.send('CONNECTED');
  } catch(e) {}
});

process.on('uncaughtException', function(err) {
  console.log(err.stack);
  console.log(arguments);
  process.exit(-1);
});
