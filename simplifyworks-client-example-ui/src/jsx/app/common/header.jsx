var docCookies = require('./cookies.js');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var Breadcrumbs = require('simplifyworks-core').Breadcrumbs;

var Brand = React.createClass({


render: function () {
        return (
            <div className='breadcrumbs'>
                <Breadcrumbs separator=" | " excludes={[ 'root', 'template', 'system']}/>
            </div>
        );
    }
});

var LocaleMenuItem = React.createClass({
    render: function () {
        return (
            <MenuItem flag={this.props.flag} locale={this.props.locale} parent={this.props.parent} href='#'
                      active={this.props.active}>
                <Grid>
                    <Row>
                        <Col xs={2}>
                            <img src={'/imgs/flags/flags/flat/32/'+this.props.flag+'.png'} width='32' height='32'/>
                        </Col>
                        <Col xs={10}>
                            <Entity className='lang-menu-text' entity='languageMenu' data={{lang: this.props.lang}}/>
                        </Col>
                    </Row>
                </Grid>
            </MenuItem>
        );
    }
});

var DirectNavItem = React.createClass({
    mixins: [ReactRouter.State, ReactRouter.Navigation],
    render: function () {
        var classes = React.addons.classSet({
            'pressed': (this.getPathname() === this.props.path)
        });
        return (
            <NavItem className={classes.trim()} {...this.props}>
                <Link to={this.props.path}>
                    <Icon bundle={this.props.bundle || 'fontello'} glyph={this.props.glyph}/>
                </Link>
            </NavItem>
        );
    }
});

var Skins = React.createClass({
    statics: {
        skins: ['default', 'green', 'blue', 'purple', 'brown', 'cyan']
    },
    switchSkin: function (skin, e) {
        e.preventDefault();
        e.stopPropagation();
        for (var i = 0; i < Skins.skins.length; i++) {
            $('html').removeClass(Skins.skins[i]);
        }
        $('html').addClass(skin);
        vex.close(this.props.id);
    },
    render: function () {
        return (
            <Grid style={{margin: '-2em'}}>
                <Row>
                    <Col xs={12} className='text-center bg-darkgrayishblue75' style={{marginBottom: 25}}>
                        <div className='fg-white' style={{fontSize: 24, lineHeight: 1, padding: '25px 10px'}}>
                            Choose a theme:
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={4} className='text-center'>
                        <a href='#' style={{border: 'none'}} onClick={this.switchSkin.bind(this, 'default')}>
                            <Icon glyph='icon-fontello-stop icon-4x' style={{color: '#E76049'}}/>
                        </a>
                    </Col>
                    <Col xs={4} className='text-center'>
                        <a href='#' style={{border: 'none'}} onClick={this.switchSkin.bind(this, 'green')}>
                            <Icon glyph='icon-fontello-stop icon-4x' className='fg-darkgreen45'/>
                        </a>
                    </Col>
                    <Col xs={4} className='text-center'>
                        <a href='#' style={{border: 'none'}} onClick={this.switchSkin.bind(this, 'blue')}>
                            <Icon glyph='icon-fontello-stop icon-4x' className='fg-blue'/>
                        </a>
                    </Col>
                </Row>
                <Row>
                    <Col xs={4} className='text-center'>
                        <a href='#' style={{border: 'none'}} onClick={this.switchSkin.bind(this, 'purple')}>
                            <Icon glyph='icon-fontello-stop icon-4x' className='fg-purple'/>
                        </a>
                    </Col>
                    <Col xs={4} className='text-center'>
                        <a href='#' style={{border: 'none'}} onClick={this.switchSkin.bind(this, 'brown')}>
                            <Icon glyph='icon-fontello-stop icon-4x' className='fg-brown'/>
                        </a>
                    </Col>
                    <Col xs={4} className='text-center'>
                        <a href='#' style={{border: 'none'}} onClick={this.switchSkin.bind(this, 'cyan')}>
                            <Icon glyph='icon-fontello-stop icon-4x' className='fg-darkcyan'/>
                        </a>
                    </Col>
                </Row>
            </Grid>
        );
    }
});

var Navigation = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin, Fluxxor.StoreWatchMixin("SecurityStore")],

    getInitialState: function () {
        return {
            selectedFlag: 'United-Kingdom'
        };
    },

    getStateFromFlux: function () {
        return this.getFlux().store("SecurityStore").getState();
    },

    handleSkinSwitch: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var vexContent;
        vex.dialog.open({
            afterOpen: function ($vexContent) {
                vexContent = $vexContent;
                return React.render(<Skins id={$vexContent.data().vex.id}/>, $vexContent.get(0));
            },
            afterClose: function () {
                React.unmountComponentAtNode(vexContent);
            }
        });
    },

    handleLayoutRadioChange: function (e) {
        var dir = e.target.value;
        docCookies.setItem('rubix_dir', dir);
        window.location.reload();
    },

    bodyLayoutRadioChange: function (value) {
        if (!value) return;
        if (value === 'fixed-body') {
            $('html').removeClass('static');
            localStorage.setItem('bodyLayout', 'fixed-body');
            ReactBootstrap.Dispatcher.emit('sidebar:reinitialize');
        } else if (value === 'static-body') {
            $('html').addClass('static');
            localStorage.setItem('bodyLayout', 'static-body');
            ReactBootstrap.Dispatcher.emit('sidebar:destroy');
        }
        this.refs[value].setChecked(true);
    },

    handleBodyLayoutRadioChange: function (e) {
        this.bodyLayoutRadioChange(e.target.value);
    },

    changeFlag: function (props) {
        docCookies.setItem('selectedFlag', props.flag);
        this.setState({
            selectedFlag: props.flag
        }, function () {
            Preloader.show();
            l20n.changeLocale(props.locale);
        }.bind(this));
    },

    l20nContextReady: function () {
        selectedFlag = l20n.ctx.getSync('selectedFlag');
        this.state.selectedFlag = selectedFlag;
        this.refs['flag-menu'].selectItem('flag', selectedFlag);
        this.setState(this.state, function () {
            Preloader.hide();
        });
    },

    changeSettingsMenuItemState: function (item) {
        if (item === 'fluid' || item === null || item === undefined) {
            this.refs['settings-menu'].selectItem('data-val', 'fluid');
            $('html').removeClass('boxed');
        } else if (item === 'boxed') {
            this.refs['settings-menu'].selectItem('data-val', 'boxed');
            $('html').addClass('boxed');
        }
        setTimeout(function () {
            $(window).trigger('resize');
        }, 300);
    },
    changeViewport: function (props) {
        switch (props['data-type']) {
            case 'dimension':
                if (props['data-val'] === 'boxed') {
                    localStorage.setItem('settingsMenu', 'boxed');
                    this.changeSettingsMenuItemState('boxed');
                } else {
                    localStorage.setItem('settingsMenu', 'fluid');
                    this.changeSettingsMenuItemState('fluid');
                }
                break;
            default:
                break;
        }
    },
    handleLogout: function (e) {
        e.preventDefault();
        //var that = this;
        $('body').addClass('fade-out');
        setTimeout(function () {
            window.location = '/logout';
        }.bind(this), 250);
    },
    componentWillMount: function () {
        ReactBootstrap.Dispatcher.on('ctx:ready', this.l20nContextReady);
    },
    componentWillUnmount: function () {
        ReactBootstrap.Dispatcher.off('ctx:ready', this.l20nContextReady);
    },
    render: function () {
        return (
            <NavContent className='pull-right' {...this.props}>
                <Nav className='hidden-xs'>
                    <NavItem divider/>
                    <NavItem dropdown>
                        <DropdownButton id='flag-menu-btn' nav toggleOnHover container={this} menu='flag-menu'>
                            <img src={'/imgs/flags/flags/flat/32/' + this.state.selectedFlag + '.png'} width='32'
                                 height='32'/>
                        </DropdownButton>
                        <Menu alignRight noTimer bsStyle='theme' ref='flag-menu' id='flag-menu' className='double-width'
                              onItemSelect={this.changeFlag} style={{paddingBottom: 0}}>
                            <MenuItem header>
                                <Entity entity='languageMenuHeading'/>
                            </MenuItem>
                            <LocaleMenuItem lang='en' locale='en' flag='United-Kingdom'/>
                            <LocaleMenuItem lang='cs' locale='cs' flag='Czech-Republic'/>
                        </Menu>
                    </NavItem>
                </Nav>
                <Nav>
                    <NavItem className='logout'>
                        <a href='#' onClick={this.handleLogout}>
                            <Icon bundle='fontello' glyph='off-1' style={{lineHeight: 1, fontSize: 24 }}/>
                        </a>
                    </NavItem>
                </Nav>
            </NavContent>
        );
    }
});

var Header = React.createClass({

    mixins: [FluxMixin],

    render: function () {
        return (
            <Grid id='navbar' {...this.props}>
                <Row>
                    <Col xs={12}>
                        <NavBar fixedTop id='rubix-nav-header'>
                            <Container fluid>
                                <Row>
                                    <Col xs={3} visible='xs'>
                                        <SidebarBtn />
                                    </Col>
                                    <Col xs={6} sm={4}>
                                        <Brand />
                                    </Col>
                                    <Col xs={3} sm={8}>
                                        <Navigation pressed={this.props.pressed}/>
                                    </Col>
                                </Row>
                            </Container>
                        </NavBar>
                    </Col>
                </Row>
            </Grid>
        );
    }
});

module.exports = Header;
