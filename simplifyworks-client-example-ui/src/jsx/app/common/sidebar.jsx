var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var SimplifyworksCore = require('simplifyworks-core');

var ApplicationSidebar = React.createClass({

    mixins: [ReactRouter.State, FluxMixin],

    render: function () {
        return (
            <div>
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <div className='sidebar-header hidden'>PAGES</div>
                            <div className='sidebar-nav-container'>
                                <SidebarNav style={{marginBottom: 0}}>
                                    <SidebarNavItem glyph='icon-fontello-gauge'
                                                    name={<Entity entity='core_navigation' data={{item: 'dashboard'}}/>}
                                                    href='/'/>

                                        <SimplifyworksCore.systemSidebar/>
                                </SidebarNav>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
});

var DummySidebar = React.createClass({
    render: function () {
        return (
            <Grid>
                <Row>
                    <Col xs={12}>
                        <div className='sidebar-header'>DUMMY SIDEBAR</div>
                        <LoremIpsum query='1p'/>
                    </Col>
                </Row>
            </Grid>
        );
    }
});

var SidebarSection = React.createClass({

    mixins: [SidebarMixin, ReactRouter.State, FluxMixin, Fluxxor.StoreWatchMixin("SecurityStore")],

    getStateFromFlux: function () {
        return this.getFlux().store("SecurityStore").getState();
    },

    alert: function () {
        vex.dialog.alert('Not implemented');
        return false;
    },

    render: function () {
        // TODO: person surname FristnameWithTitles or username if empty
        var userLabel = "Guest";
        if (this.state.currentUser) {
            userLabel = this.state.currentUser.username;
        }

        return (
            <div id='sidebar' {...this.props}>
                <div id='avatar'>
                    <Grid>
                        <Row className='fg-white'>
                            <Col xs={12} id='avatar-col'>
                                <div style={{top: 10, fontSize: 16, lineHeight: 1, position: 'relative'}}>
                                    {userLabel}
                                </div>
                                <div>
                                    <Progress id='demo-progress' value={30} min={0} max={100} color='#ffffff'/>
                                    <a href='#' onClick={this.alert}><Icon id='demo-icon' bundle='fontello'
                                                                           glyph='lock-5'/></a>
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                </div>
                <SidebarControls>
                    <SidebarControlBtn bundle='fontello' glyph='docs' sidebar={0}/>
                    <SidebarControlBtn bundle='fontello' glyph='chat-1' sidebar={1}/>
                    <SidebarControlBtn bundle='fontello' glyph='chart-pie-2' sidebar={2}/>
                    <SidebarControlBtn bundle='fontello' glyph='th-list-2' sidebar={3}/>
                    <SidebarControlBtn bundle='fontello' glyph='bell-5' sidebar={4}/>
                </SidebarControls>

                <div id='sidebar-container'>
                    <Sidebar sidebar={0} active>
                        <ApplicationSidebar />
                    </Sidebar>
                    <Sidebar sidebar={1}>
                        <DummySidebar />
                    </Sidebar>
                    <Sidebar sidebar={2}>
                        <DummySidebar />
                    </Sidebar>
                    <Sidebar sidebar={3}>
                        <DummySidebar />
                    </Sidebar>
                    <Sidebar sidebar={4}>
                        <DummySidebar />
                    </Sidebar>
                </div>
            </div>
        );
    }
});

module.exports = SidebarSection;
