var SimplifyworksCore = require('simplifyworks-core');

/**
 * sify components
 *
 * TODO: package and move to vendor
 */
module.exports = window.SifyComponents = {

    BasicForm: SimplifyworksCore.BasicForm,
    FilterForm: SimplifyworksCore.FilterForm,
    InputText: SimplifyworksCore.InputText,
    TextArea: SimplifyworksCore.TextArea,
    CodeList: SimplifyworksCore.CodeList,
    FormTable: SimplifyworksCore.FormTable,
    MultiSelect: SimplifyworksCore.MultiSelect,
    Checkbox: SimplifyworksCore.Checkbox,
    Datepicker: SimplifyworksCore.Datepicker,
    Enumbox: SimplifyworksCore.Enumbox,
    FlashMessages: SimplifyworksCore.FlashMessages,
    BasicTable: SimplifyworksCore.BasicTable,
    ModalDialog: SimplifyworksCore.ModalDialog,
    ModalDialogManager: SimplifyworksCore.ModalDialog.ModalDialogManager,
    ModalDialogFooter: SimplifyworksCore.ModalDialog.ModalDialogFooter,
    ModalDialogHeader: SimplifyworksCore.ModalDialog.ModalDialogHeader,
    ModalDialogBody: SimplifyworksCore.ModalDialog.ModalDialogBody,
    Attachment: SimplifyworksCore.Attachment,
    Dropzone: SimplifyworksCore.Dropzone,
    DragAndDrop: SimplifyworksCore.DragAndDrop,
    WorkflowForm: SimplifyworksCore.WorkflowForm,
    AdvancedTable: SimplifyworksCore.AdvancedTable,
    AdvancedForm: SimplifyworksCore.AdvancedForm,
    AdvancedDetail: SimplifyworksCore.AdvancedDetail,
    Section: SimplifyworksCore.Section
};
