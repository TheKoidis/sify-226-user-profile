var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var AfterLogin = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    componentWillMount: function() {
        this.getFlux().actions.security.reloadContext(function() {
            this.transitionTo('/');
        }.bind(this));        
    },

    render: function() {
        return <div/>
    }
});

module.exports = AfterLogin;
