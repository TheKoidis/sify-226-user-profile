var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var LoginForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    render() {
        var fail = this.getQuery().fail;

        return (
            <Form method='post' action='/login/form' allowAutoComplete={true}>
                <PanelContainer noControls bordered>
                    <Panel>
                        <PanelHeader className='bg-primary fg-white'>
                            <Grid>
                                <Row>
                                    <Col xs={12}>
                                        <h3>
                                            <Entity entity='page_login' data={{message: 'panel_title'}}/>
                                        </h3>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelHeader>

                        <PanelBody>
                            <Grid>
                                <Row>
                                    <Col xs={12}>
                                        {fail
                                            ?   <div>{l20n.ctx.getSync(fail)}</div>
                                            :   null
                                        }
                                        <FormGroup>
                                            <InputGroup lg>
                                                <InputGroupAddon>
                                                    <Icon glyph='icon-fontello-user'/>
                                                </InputGroupAddon>
                                                <Input autoFocus
                                                       type='text'
                                                       id='username'
                                                       name='username'
                                                       placeholder={l20n.ctx.getSync('user_username')}/>
                                            </InputGroup>
                                        </FormGroup>
                                        <FormGroup>
                                            <InputGroup lg>
                                                <InputGroupAddon>
                                                    <Icon glyph='icon-fontello-key'/>
                                                </InputGroupAddon>
                                                <Input type='password'
                                                       id='password'
                                                       name='password'
                                                       placeholder={l20n.ctx.getSync('user_password')}/>
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelBody>

                        <PanelFooter>
                            <Grid>
                                <Row>
                                    <Col xs={12} className='text-right'>                                        
                                        {window.ssoUrl
                                            ?   <Button lg type='button' bsStyle='success' onClick={function() {
                                                    window.location = window.ssoUrl;
                                                }}>
                                                    {l20n.ctx.getSync('button_sso')}
                                                </Button>
                                            : null
                                        }

                                        <Button lg type='submit' bsStyle='primary'>
                                            <Entity entity='page_login' data={{message: 'button_login'}}/>
                                        </Button>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelFooter>
                    </Panel>
                </PanelContainer>
            </Form>
        );
    }
});

var Body = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    componentDidMount() {
        $('html').addClass('authentication');
    },

    componentWillUnmount() {
        $('html').removeClass('authentication');
    },

    render() {
        return (
            <Container id='auth-container' className='login'>
                <Container id='auth-row'>
                    <Container id='auth-cell'>
                        <Grid>
                            <Row>
                                <Col sm={12}>
                                    <LoginForm/>
                                </Col>
                            </Row>
                        </Grid>
                    </Container>
                </Container>
            </Container>
        );
    }
});

var classSet = React.addons.classSet;
var LoginPage = React.createClass({

    mixins: [SidebarMixin, FluxMixin],

    render() {
        var classes = classSet({
            'container-open': this.state.open
        });
        return (
            <Container id='container' className={classes}>
                <Body />
            </Container>
        );
    }
});

module.exports = LoginPage;
