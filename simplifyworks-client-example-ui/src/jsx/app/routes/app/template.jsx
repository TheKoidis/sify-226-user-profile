var Header = require('../../common/header.jsx');
var Sidebar = require('../../common/sidebar.jsx');
var Footer = require('../../common/footer.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var Template = React.createClass({
  mixins: [SidebarMixin, FluxMixin],
  render: function() {
    var classes = React.addons.classSet({
      'container-open': this.state.open
    });
    return (
        <Container id='container' className={classes}>
          <Sidebar />
          <Header />

          <RouteHandler/>

          <Footer />
        </Container>
    );
  }
});

module.exports = Template;
