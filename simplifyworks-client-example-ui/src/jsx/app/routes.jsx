var SimplifyworksCore = require('simplifyworks-core');

/* ERROR PAGES */
var notfound = require('./routes/notfound.jsx');

/* ROOT */
var root = require('./routes/root.jsx');

/* APP PAGES */
var template = require('./routes/app/template.jsx');
var blank = require('./routes/app/blank.jsx');
var dashboard = require('./routes/app/dashboard.jsx');
var login = require('./routes/app/login.jsx');
var afterLogin = require('./routes/app/after-login.jsx');

/* ROUTES */
module.exports = (
    <Route name='root'path='/root' handler={root}>
        <Route name='template' path='/template' handler={template}>
            <Route name='dashboard' path='/' handler={dashboard}/>

            {SimplifyworksCore.systemRoutes}

        </Route>

        <Route name='login' path='/login' handler={login}/>
        <Route name='after-login' path='/after-login' handler={afterLogin}/>
        <DefaultRoute name='default' handler={blank}/>
        <NotFoundRoute name='not-found' handler={notfound}/>
    </Route>
);
